**
2017-05-28
Latest release [0.5.5-U](https://bitbucket.org/dacucar/smart-fpg-editor/downloads/sfpge-inst-win32-0.5.5-U.exe).
For complete change-log refer to CHANGES.txt and the git log.
**

# Smart Fpg Editor #

Smart Fpg Editor is an easy-to-use, user friendly and powerful FPG editor tool for Windows. It is entirely written in VB6.

FPGs are graphic collections used by certain compilers/languages such as [Bennu](http://bennugd.org), [PixTudio](http://forum.bennugd.org/index.php?board=97.0), [Fenix](http://wiki.bennugd.org/index.php?title#Fenix) or Div Games Studio.

Smart Fpg Editor was conceived as a _painless FPG editor_, to fullfill the lack of several characteristics I missed in existing FPG editors, and with the ease of use in mind.

The project was previously hosted in Google code and was moved to Bitbucket in 2015.

[![Screenshot](media/sshot6_s.png)](media/sshot6.png)

If you want to see some other pictures jump to the [screenshots](#markdown-header-screenshots) section.

The following [video](https://www.youtube.com/watch?v=qS1XsM6njtE) may help you also to decide whether it is worth a try. I am sure it is :).

## Wait, VB6 in 2016?  ##
Smart Fpg Editor it is not daily maintained but it does get updated from time to time. For a while, it remained untouched, but there is still interest of keeping this project alive:

 * Many users are still happy with it and have asked to make sure it was migrated from Google Code to another platform.
 * The concept of Smart Fpg Editor is to my mind a nice concept and and worth to be kept.
 * Someone might find the code useful to make a port in a different language.

## Features ##
Don't be fooled by the fact that Smart Fpg Editor is in beta phase; Smart Fpg Editor features most of the basic characteristics of an FPG editor plus many others. And it is **very stable for daily use**. The only reason for not having reached a version 1 is that I was ambitious when I set the goals of what that version should have.

Some of the features at this moment are:

### General ###
  * Simple and clean interface.
  * Extremely easy to use and intuitive.
  * Multilanguage (development language is English but Smart Fpg Editor is currently in Spanish and English).

### FPG Creation and Loading ###
  * Support for 8, 16 and 32 bpp FPGs (Bennu/Fenix/DIV graphic collections).
  * Loading of DIV font files as FPGs.

### Working with graphics ###
  * Support for lots of graphic formats, including most popular such as MAP, PNG, GIF, JPG, BMP...
  * Adding multiple files at the same time.
  * Conversion of the added graphic to the depth of the FPG.
    * Color quantization to FPG palette when working with 8bpp FPGs.
    * Allows removing transparency when working with 16 and 32 bpp FPGs.
  * Graphic viewer with zoom and transparency capabilities.
  * Edition of graphic properties.
  * Export graphics to MAP and PNG formats.
  * Clipboard operations support.
  * Drag&Drop support.

### Control point edition ###
  * Visual control point editor (you can edit control points in the graphic viewer), much more user-friendly than the typical list-based control point editors included with other FPG edition tools.
  * Advanced (list-based) control point editor:
    * Several insertion modes that eases considerably the process of inserting, deleting, filling gaps, etc. of control points without having to redefine the whole list.
    * Edition of control points of more than one graphic at a once.
    * Supports basic arithmetic operators and WIDTH and HEIGHT parameters to define control point coordinates.

### 8bpp-specific features ###
  * Exporting the palette of the FPG.
  * Full-featured palette editor.

### Missing features / Limitations ###
There are some features that have not yet been implemented but are desired for future releases. For example:

  * 1bpp FPGs support.
  * Associate known file extensions.
  * Configuration options.

## Installing Smart Fpg Editor ##
Smart Fpg Editor runs at least in Windows Xp, Windows Vista, Windows 7, Window 8 and Windows 10.
The easiest way to install Smart Fpg Editor is to use the *.exe installer available in the download page. These are the packages named sfpge-inst-win32-X.X.X-X.exe.

If for any strange reason you decide that the installer is not an option for you, you might use alternative packages (binary or source), however that is going to require certain degree of understanding on how to register Active X components in your Windows version.

## Compiling Smart Fpg Editor ##
Smart Fpg Editor has been entirely developed in Visual Basic 6 (SP6) and it is only available for Windows.

Smart Fpg Editor is divided into several components:

 * Custom Active X components:
     * Bennulib (bennulib.dll): An Active X dll that can be used to give programs the capability to work with Fpgs, Maps, Pal, etc formats.
     * parselite (parselite.dll): An Active X dll which exposes a class to assist parsing simple mathematical expressions such as *WIDTH * 2 - 6*.
     * smartcpx (smartcpx.dll): An Active X dll that holds the functionality of the *Advanced control point editor*.
     * SmartBennuCtls (SmartBennuCtls.ocx): Custom OCX controls for Fpgs, Maps and Palletes.
 * Main application:
     * SmartFpgEditor (Gui) (SmartFpgEditor.exe): The Smart Fpg Editor GUI. End user application. Make uses of all above components.

Each component corresponds to one VB6 project.

Sources can be found in *source zip packages* or in *Git*. Zip packages are released with each new version. Git sources may be updated periodically. Notice though that **zip source packages only come with the sources of SmartFpgEditor gui**. If you wish to alter any of the other components you will have to download the sources from Git.

### General requeriments ###
This requeriments apply no matter which source distribution you chose to use.

 * A working copy of Microsoft Visual Basic 6.

 * Service Pack 6 for Visual Basic 6.0. Download of the [SP6 for VB 6.0](https://www.microsoft.com/en-us/download/details.aspx?id=5721) is available.

 * Cmct32.dll Version 5 or later. If you have Internet Explorer 5 or later in your system, you do not need to do anything. Alternatively, you can update your dll version by downloading [Microsoft Visual Basic 6.0 Common Controls package](http://www.microsoft.com/downloads/details.aspx?displaylang#en&FamilyID#25437d98-51d0-41c1-bb14-64662f5f62fe).

Note: GDI+ library, which was required for versions prior to 0.2.0 is no longer required.

#### Third-party Active X components ####
In order to compile SmartFpgEditor you need the following Active X components to be registered in your system.

* SSubTmr6.dll from vbAccelerator - Can be found in the “sfpge-third-party-comp-X” packages or in the “3-party” folder of the installed binary distribution.
* cNewMenu6.dll from vbAccelerator - Can be found in the “sfpge-third-party-comp-X” packages or in the “3-party” folder of the installed binary distribution.
* vbalTbar6.ocx from vbAccelerator - Can be found in the “sfpge-third-party-comp-X” packages or in the “3-party” folder of the installed binary distribution.
* vbalIml6.ocx from vbAccelerator - Can be found in the “sfpge-third-party-comp-X” packages or in the “3-party” folder of the installed binary distribution.
* CKXMLParser.dll - Can be found in the sfpge-third-party-comp-X" packages or in the "3-party" folder of the installed binary distribution.

Notice that the process of registering ActiveX components may be different depending on your Windows distribution. Traditionally they are registered with the [regsvr32](https://technet.microsoft.com/en-us/library/bb490985.aspx) command.

**If you have installed Smart Fpg Editor, these components are already registered in your system and you do not need to do anything**.

#### Third party libraries (dlls) ####
These dlls must be copied into the system folder. These are not COM components and do not need (and cannot be) registered.

* freeimage.dll - Can be found in the “sfpg-third-party-lib-X” packages or in the binary distribution installation folder.
* zlibvb.dll - Can be found in the “sfpg-third-party-lib-X” packages or in the binary distribution installation folder.

###Compiling from source package###

As already mentioned, source packages only include the code of the Smart Fpg Editor Gui. Apart from the mentioned third-part components you need to register the custom Active X components (bennulib.dll, SmartBennuCtls.ocx, parselite.dll and smartcpx.dll).

They can be found in the binary distribution "modules" folder. Copy this folder somewhere in your system and register them using [regsvr32](https://technet.microsoft.com/en-us/library/bb490985.aspx) command. Notice that the process of registering ActiveX components may be different depending on your Windows distribution.

**If you have installed Smart Fpg Editor, these components are already registered in your system and you do not need to do anything**.

### Compiling from Git sources ###
When downloading from Git you get the sources not only of Smart Fpg Editor GUI but also of the custom components, each in a different folder inside the *src/* subfolder.

You will still need the custom Active X components binaries (bennulib.dll, SmartBennuCtls.ocx, parselite.dll and smartcpx.dll), but because these components make use of "Project compatibility"  setting, and this expects to find the binaries in a specific location, you need to place the dlls and ocx inside the */bin/modules/* folder. If you have already a working Smart Fpg Editor in your system, you do not need to do anything more, otherwise, you will need to register this components, for example using [regsvr32](https://technet.microsoft.com/en-us/library/bb490985.aspx) tool.

Custom components binaries can be found in the binary package or in the installation folder of Smart Fpg Editor, within the *modules* folder

Make sure also to read the README that comes along each VB6 project file inside each project folder.

A convenience VB6 project group file *vbBennuTools.vbg* comes along with the Git sources. 

If you get a *Compile error. User defined-type not defined* when you attempt to run, perform the following steps:

- Select in the project viewer "Smart Fpg Editor"
- Go to references and uncheck all references to bennulib, parselite and smartcpx
- Accept changes
- Go to references again and check those references again.
- Accept changes
- Now you can run the SmartFpgEditor.

The reason why this needs to be done is out of my understanding (I need to do it every time I open the project group) but I believe is a bug in VB6.

### NOTICE ###

If you are building your own flavors of SmartFpgEditor and plan to make it available for the public, ***make sure to rename any ActiveX components that you plan to modify***. This is to minimize compatibility troubles with official Smart Fpg Editor release.

## Who do I talk to? ##
Use the Issue section.

## Screenshots ##

[![Screenshot](media/sshot1_s.png)](media/sshot1.png)
[![Screenshot](media/sshot2_s.png)](media/sshot2.png)
[![Screenshot](media/sshot3_s.png)](media/sshot3.png)
[![Screenshot](media/sshot4_s.png)](media/sshot4.png)
[![Screenshot](media/sshot5_s.png)](media/sshot5.png)
[![Screenshot](media/sshot6_s.png)](media/sshot6.png)
[![Screenshot](media/sshot7_s.png)](media/sshot7.png)
