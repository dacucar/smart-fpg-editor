"""Placeholder for SmartFpgEditor binary components."""

Here you should place custom binary rlease of Active X components that are part of SmarFpgEditor:

 * bennulib.dll
 * SmartBennuCtls.ocx
 * parselite.dll
 * smartcpx.dll

This is because the corresponding VB6 project file of each component has 
version compatibility option set to *Project compatibility* and the component
is expected to be found in this folder.

This components can be binary distribution packages *sfpg-bin-X.X.X-U.zip* or
in the installation folder of SmartFpgEditor if you used the installer..

""""ADVISE""""
It is very important that you understand Version Compatibility for Active X components 
if you are going to build your own versions of your components.
You can read more on this on [Microsoft MSDN website][(https://msdn.microsoft.com/en-us/library/aa733715%28v=vs.60%29.aspx)

