VERSION 5.00
Object = "{E142732F-A852-11D4-B06C-00500427A693}#1.14#0"; "vbalTbar6.ocx"
Object = "{396F7AC0-A0DD-11D3-93EC-00C0DFE7442A}#1.0#0"; "vbalIml6.ocx"
Object = "{FCE52DAF-EF3E-48E5-A71E-E7A56678C488}#41.0#0"; "SmartBennuCtls.ocx"
Begin VB.Form frmFpgEditor 
   Caption         =   "Smart FPG editor"
   ClientHeight    =   3630
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8655
   Icon            =   "frmFpgEditor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3630
   ScaleWidth      =   8655
   StartUpPosition =   3  'Windows Default
   Begin SmartBennuCtls.SmartFpgPane fp 
      Height          =   2055
      Left            =   840
      TabIndex        =   1
      Top             =   720
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   3625
      ForeColor       =   16711680
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GridColor       =   8421631
      CellWidth       =   70
      CellHeight      =   70
      NameWidth       =   60
      NameOffsetY     =   -16
   End
   Begin VB.Timer tmrAnim 
      Interval        =   1
      Left            =   6480
      Top             =   1560
   End
   Begin vbalIml6.vbalImageList iml 
      Left            =   4200
      Top             =   1080
      _ExtentX        =   953
      _ExtentY        =   953
      IconSizeX       =   32
      IconSizeY       =   32
      ColourDepth     =   24
      Size            =   70592
      Images          =   "frmFpgEditor.frx":AAD7
      Version         =   131072
      KeyCount        =   16
      Keys            =   "���������������"
   End
   Begin vbalTBar6.cReBar rebar 
      Left            =   2040
      Top             =   0
      _ExtentX        =   2778
      _ExtentY        =   661
   End
   Begin vbalTBar6.cToolbar tbrMain 
      Height          =   375
      Left            =   0
      Top             =   0
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
   End
   Begin VB.PictureBox picStBar 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      ScaleHeight     =   255
      ScaleWidth      =   8655
      TabIndex        =   0
      Top             =   3375
      Width           =   8655
   End
End
Attribute VB_Name = "frmFpgEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Implements IFpgEditorContext

Private Const MODULE_NAME As String = "SmartFpgEditor.frmFpgEditor"

Private Const RID_NEWFPG As Integer = 110
Private Const RID_OPENFPG As Integer = 111
Private Const RID_SAVEFPG As Integer = 112
Private Const RID_DUPLICATEFPG As Integer = 113
Private Const RID_ADDGRAPHIC As Integer = 114
Private Const RID_VIEWEDITGRAPHIC As Integer = 115
Private Const RID_EXPORTGRAPHIC As Integer = 116
Private Const RID_REMOVEGRAPHIC As Integer = 117
Private Const RID_NEWWINDOW As Integer = 118
Private Const RID_SAVE As Integer = 119
Private Const RID_SAVEAS As Integer = 120
Private Const RID_INDEXED As Integer = 121
Private Const RID_TOMAPFILE As Integer = 122
Private Const RID_TOPNGFILE As Integer = 123
Private Const RID_EDITCONTROLPOINTS As Integer = 124
Private Const RID_MODIFIED As Integer = 125
Private Const RID_SAVECHANGES_MSG As Integer = 126
Private Const RID_GRAPHICSSELECTED As Integer = 127
Private Const RID_GRAPHICS As Integer = 128
Private Const RID_READY As Integer = 129
Private Const RID_BUILD As Integer = 130 ' TODO: Obsolete
Private Const RID_MARQUEE1 As Integer = 131
Private Const RID_MARQUEE2 As Integer = 132
Private Const RID_MARQUEE3 As Integer = 133
Private Const RID_MARQUEE4 As Integer = 134
Private Const RID_NOFPGMSG As Integer = 171
Private Const RID_CUT As Integer = 175
Private Const RID_COPY As Integer = 176
Private Const RID_PASTE As Integer = 177
Private Const RID_PALETTEACTIONS As Integer = 181
Private Const RID_VIEWEDITPALETTE As Integer = 182
Private Const RID_EXPORTPALETTE As Integer = 183
Private Const RID_CTXVIEWEDIT As Integer = 172
Private Const RID_CTXRENAME As Integer = 173
Private Const RID_CTXREMOVE As Integer = 174
Private Const RID_CTXZOOMIN As Integer = 185
Private Const RID_CTXZOOMOUT As Integer = 186
Private Const RID_CTXVIEWERSEPARATOR As Integer = 187
Private Const RID_FPGTOSTRIP As Integer = 202
Private Const RID_FPGNOCOMPRESS As Integer = 207
Private Const RID_FPGCOMPRESS As Integer = 208
Private Const RID_FPGCOMPRESSIONOPTIONS = 209
Private Const RID_TRANSPARENTBG = 210

Private Const GID_SAVE As Integer = 3
Private Const GID_SAVE_COMPRESSED = 15

' For performance and asthetics reasons, the cell size is limited
Private Const MAX_CELL_HEIGHT As Integer = 128
Private Const MAX_CELL_WIDTH As Integer = 128
Private Const MIN_CELL_HEIGHT As Integer = 32
Private Const MIN_CELL_WIDTH As Integer = 32

'===============================================================================

'Private Type POINTAPI ' This holds the logical cursor information
'    iX As Long
'    iY As Long
'End Type
'Private Declare Sub GetCursorPos Lib "USER32" (lpPoint As POINTAPI)

'===============================================================================

Public WithEvents m_fCpEditor As cCpEditor ' TODO: Change
Attribute m_fCpEditor.VB_VarHelpID = -1
Private m_fPaletteEditor As frmPaletteEditor
Attribute m_fPaletteEditor.VB_VarHelpID = -1
' Holds a collection of all the map editors that exist
Private m_MapEditors As Collection

Private m_UseFpgCompression As Boolean

Private m_StatusBar As cNoStatusBar
Private WithEvents m_DDMenus As cPopupMenu
Attribute m_DDMenus.VB_VarHelpID = -1
Private WithEvents m_CtxMenus As cPopupMenu ' Contextual menus
Attribute m_CtxMenus.VB_VarHelpID = -1
Private WithEvents m_CV As cClipboardViewer
Attribute m_CV.VB_VarHelpID = -1

Private m_bDirty As Boolean
Private m_sFileName As String
Private m_bTempFilesCreated As Boolean
Private m_bUseEnc2 As Boolean
Private m_bOldUseEnc2 As Boolean
Private m_oDDDataObject As DataObject

'===============================================================================

Public Property Get IsDirty() As Boolean
    Const PROC_NAME As String = "IsDirty"

    On Error GoTo EH

    IsDirty = m_bDirty

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Let IsDirty(ByVal newVal As Boolean)
    Const PROC_NAME As String = "IsDirty"

    On Error GoTo EH

    m_bDirty = newVal

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property


Public Property Get UseFpgCompression() As Boolean

    Const PROC_NAME As String = "UseFpgCompression"

    On Error GoTo EH
    
    UseFpgCompression = m_UseFpgCompression

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Let UseFpgCompression(ByVal newVal As Boolean)

    Const PROC_NAME As String = "UseFpgCompression"

    On Error GoTo EH
    
    m_UseFpgCompression = newVal
    m_DDMenus.Checked(m_DDMenus.IndexForKey("UseFpgCompression")) = _
        m_UseFpgCompression
    m_DDMenus.Checked(m_DDMenus.IndexForKey("DoNotUseFpgCompression")) = _
        Not m_UseFpgCompression
    tbrMain.ButtonImage("SaveFpg") = IIf(m_UseFpgCompression, _
        GID_SAVE_COMPRESSED, GID_SAVE)
    tbrMain.ResizeToolbar
    
    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get Filename() As String
    Const PROC_NAME As String = "Filename"

    On Error GoTo EH

    Filename = m_sFileName

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Let Filename(ByVal newVal As String)
    Const PROC_NAME As String = "Filename"

    On Error GoTo EH

    m_sFileName = newVal
    UpdateTitleBar

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get Title() As String
    Const PROC_NAME As String = "Title"

    On Error GoTo EH

    If fp.FpgAvailable Then
        Title = IIf(Filename = "", FormatResString(RID_NEWFPG), GetFileTitle(Filename))
        If IsDirty Then Title = Title & " (" & FormatResString(RID_MODIFIED) & ")"
    End If

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get NoSelectionStBarMsg() As String
    Const PROC_NAME As String = "NoSelectionStBarMsg"

    On Error GoTo EH

    NoSelectionStBarMsg = IIf(Filename = "", FormatResString(RID_NEWFPG), Filename)
    If IsDirty Then NoSelectionStBarMsg = NoSelectionStBarMsg _
        & " (" & FormatResString(RID_MODIFIED) & ")"

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get RecommendedMaxCellWidth() As Long
    Const PROC_NAME As String = "RecommendedMaxCellWidth"

    On Error GoTo EH

    RecommendedMaxCellWidth = MAX_CELL_WIDTH

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get RecommendedMinCellWidth() As Long
    Const PROC_NAME As String = "RecommendedMinCellWidth"

    On Error GoTo EH

    RecommendedMinCellWidth = MIN_CELL_WIDTH

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get RecommendedMaxCellHeight() As Long
    Const PROC_NAME As String = "RecommendedMaxCellHeight"

    On Error GoTo EH

    RecommendedMaxCellHeight = MAX_CELL_HEIGHT

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get RecommendedMinCellHeight() As Long
    Const PROC_NAME As String = "RecommendedMinCellHeight"

    On Error GoTo EH
    
    RecommendedMinCellHeight = MIN_CELL_HEIGHT

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Set PaletteEditor(value As frmPaletteEditor)
    Const PROC_NAME As String = "PaletteEditor"

    On Error GoTo EH
    
    Set m_fPaletteEditor = value

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get PaletteEditor() As frmPaletteEditor

    Const PROC_NAME As String = "PaletteEditor"

    On Error GoTo EH
    
    Set PaletteEditor = m_fPaletteEditor

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property


'===============================================================================
Private Property Get CanPaste() As Boolean
    Const PROC_NAME As String = "CanPaste"
    
    Dim bCanPaste As Boolean
    Dim oCC As New cCustomClipboard
    
    On Error GoTo EH
    
    If CFMMapsL <> 0 Then
        bCanPaste = oCC.IsDataAvailableForFormat(CFMMapsL)
    End If
    bCanPaste = bCanPaste Or oCC.IsDataAvailableForFormat(CF_DIB)
    
    CanPaste = bCanPaste
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

' Purpose: If the FPG is "dirty", shows the "Do you want to save?". Returns true
' if the user save or discards the changes and false if the user selects cancel
Public Function AskToSaveIfDirty() As Boolean
    Dim eMsgRes As VbMsgBoxResult
    
    Const PROC_NAME As String = "AskToSaveIfDirty"

    On Error GoTo EH

    If fp.FpgAvailable = False Or IsDirty = False Then
        AskToSaveIfDirty = True
        Exit Function
    End If
    
    eMsgRes = MsgBox(FormatResString(RID_SAVECHANGES_MSG), vbYesNoCancel Or _
        vbQuestion)
    Select Case eMsgRes
    Case vbYes
        SaveFpgAction Me
        AskToSaveIfDirty = True
    Case vbNo
        AskToSaveIfDirty = True
    Case vbCancel
        AskToSaveIfDirty = False
    End Select

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Purpose: Enable or disable buttons according to the current state
Private Sub UpdateEnabledControls()
    Const PROC_NAME As String = "UpdateEnabledControls"
    
    Dim b As Boolean

    On Error GoTo EH

    ' Controls controlled by the existence of an FPG
    b = fp.FpgAvailable
    tbrMain.ButtonEnabled("SaveFpg") = b
    tbrMain.ButtonEnabled("AddGraphic") = b
    m_DDMenus.Enabled(m_DDMenus.IndexForKey("DuplicateFpg")) = b
    tbrMain.ButtonEnabled("Paste") = b And CanPaste
    m_CtxMenus.Enabled(m_CtxMenus.IndexForKey("Paste1")) = b And CanPaste
    m_CtxMenus.Enabled(m_CtxMenus.IndexForKey("Paste2")) = b And CanPaste
    m_CtxMenus.Enabled(m_CtxMenus.IndexForKey("Paste3")) = b And CanPaste
        
    tbrMain.ButtonEnabled("PaletteActions") = False
    If b Then
        If fp.GetFpg.Depth = 8 Then
            tbrMain.ButtonEnabled("PaletteActions") = True
        End If
    End If
    
    ' Controls controlled by the existence of a selected MAP
    b = (fp.SelectionCount > 0)
    tbrMain.ButtonEnabled("EditGraphic") = b
    tbrMain.ButtonEnabled("ExportGraphic") = b
    tbrMain.ButtonEnabled("RemoveGraphic") = b
    tbrMain.ButtonEnabled("Cut") = b
    tbrMain.ButtonEnabled("Copy") = b
    

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateMenus()
    Const PROC_NAME As String = "CreateMenus"

    On Error GoTo EH

    CreateToolBarDropDownMenus
    CreateContextualMenus
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateToolBarDropDownMenus()
    Const PROC_NAME As String = "CreateToolBarDropDownMenu"
    
    Dim lParent As Long
    Dim lParentLevel2 As Long

    On Error GoTo EH

    Set m_DDMenus = New cPopupMenu
    With m_DDMenus
    .hWndOwner = Me.hWnd
    .OfficeXpStyle = True
    
    ' IMPORTANT NOTE: The Key of the parent menus will be that of the toolbar
    ' button to which is related, plus the string "Menu"
    ' This way, no changes are required in the tbrMain_DropDownPress event to
    ' show the Menu
    
    lParent = .AddItem("New FPG", , , 0, , , , "NewFpgMenu")
    .AddItem "8bpp (" & FormatResString(RID_INDEXED) & ")", , , lParent, , , , _
        "NewFpg8"
    .AddItem "16bpp", , , lParent, , , , "NewFpg16"
    .AddItem "32bpp", , , lParent, , , , "NewFpg32"
    .AddItem "-", , , lParent
    .AddItem FormatResString(RID_DUPLICATEFPG), , , lParent, , , , "DuplicateFpg"
    
    lParent = .AddItem("Save FPG", , , 0, , , , "SaveFpgMenu")
    .AddItem FormatResString(RID_SAVE), , , lParent, , , , "SaveFpg"
    .AddItem FormatResString(RID_SAVEAS), , , lParent, , , , "SaveFpgAs"
    .AddItem "-", , , lParent
    .AddItem FormatResString(RID_FPGTOSTRIP), , , lParent, , , , "ExportFpgTo"
    lParentLevel2 = .AddItem(FormatResString(RID_FPGCOMPRESSIONOPTIONS), , , _
        lParent, , , , "FpgCompressionOptions")
    .AddItem FormatResString(RID_FPGNOCOMPRESS), , , lParentLevel2, , False, , "UseFpgCompression"
    .AddItem FormatResString(RID_FPGCOMPRESS), , , lParentLevel2, , True, , "DoNotUseFpgCompression"
    
    lParent = .AddItem("Palette Actions", , , 0, , , , "PaletteActionsMenu")
    .AddItem FormatResString(RID_VIEWEDITPALETTE), , , lParent, , , , "ViewEditPalette"
    .AddItem FormatResString(RID_EXPORTPALETTE), , , lParent, , , , "ExportPalette"
    
    lParent = .AddItem("Add Graphic", , , 0, , , , "AddGraphicMenu")
    
    lParent = .AddItem("Export Graphic", , , 0, , , , "ExportGraphicMenu")
    .AddItem FormatResString(RID_TOMAPFILE), , , lParent, , , , _
        "ExportToMapFile"
    .AddItem FormatResString(RID_TOPNGFILE), , , lParent, , , , _
        "ExportToPngFile"
    
    lParent = .AddItem("Edit Graphic", , , 0, , , , "EditGraphicMenu")
    .AddItem FormatResString(RID_VIEWEDITGRAPHIC), , , lParent, , , , _
        "ViewEditGraphic"
    .AddItem FormatResString(RID_EDITCONTROLPOINTS), , , lParent, , , , _
        "EditControlPoints"
    
    End With
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateContextualMenus()
    Const PROC_NAME As String = "CreateContextualMenus"

    Dim lParent As Long, lParent2 As Long
    
    On Error GoTo EH

    Set m_CtxMenus = New cPopupMenu
    With m_CtxMenus
    .hWndOwner = Me.hWnd
    .HeaderStyle = ecnmHeaderSeparator
    .OfficeXpStyle = True
    
    ' Single selection menu
    lParent = .AddItem("Single selection menu", , , 0, , , , "SingleSelCtxMenu")
    .AddItem FormatResString(RID_CTXVIEWEDIT), , , lParent, , , , _
        "ViewEdit"
    .AddItem FormatResString(RID_CTXRENAME), , , lParent, , , , "Rename"
    .AddItem "-", , , lParent
    .AddItem FormatResString(RID_CUT), , , lParent, , , , "Cut"
    .AddItem FormatResString(RID_COPY), , , lParent, , , , "Copy"
    .AddItem FormatResString(RID_PASTE), , , lParent, , , , "Paste1"
    .AddItem "-", , , lParent
     .AddItem FormatResString(RID_TOPNGFILE), , , lParent, , , , "ToPng"
      .AddItem FormatResString(RID_TOMAPFILE), , , lParent, , , , "ToMap"
    .AddItem "-", , , lParent
    .AddItem FormatResString(RID_CTXREMOVE), , , lParent, , , , "Remove"
    .AddItem "-" & FormatResString(RID_CTXVIEWERSEPARATOR), , , lParent
    .AddItem FormatResString(RID_CTXZOOMIN), , , lParent, , , , "ZoomIn"
    .AddItem FormatResString(RID_CTXZOOMOUT), , , lParent, , , , "ZoomOut"
    .AddItem FormatResString(RID_TRANSPARENTBG), , , lParent, , , , "ToggleTransparency"
    
    ' Multiple selection menu
    lParent = .AddItem("Multiple selection menu", , , 0, , , , "MultiSelCtxMenu")
    .AddItem FormatResString(RID_CUT), , , lParent, , , , "Cut"
    .AddItem FormatResString(RID_COPY), , , lParent, , , , "Copy"
    .AddItem FormatResString(RID_PASTE), , , lParent, , , , "Paste2"
    .AddItem "-", , , lParent
     .AddItem FormatResString(RID_TOPNGFILE), , , lParent, , , , "ToPng"
      .AddItem FormatResString(RID_TOMAPFILE), , , lParent, , , , "ToMap"
    .AddItem "-", , , lParent
    .AddItem FormatResString(RID_CTXREMOVE), , , lParent, , , , "Remove"
    .AddItem "-" & FormatResString(RID_CTXVIEWERSEPARATOR), , , lParent
    .AddItem FormatResString(RID_CTXZOOMIN), , , lParent, , , , "ZoomIn"
    .AddItem FormatResString(RID_CTXZOOMOUT), , , lParent, , , , "ZoomOut"
    .AddItem FormatResString(RID_TRANSPARENTBG), , , lParent, , , , "ToggleTransparency"
    
    ' No selection menu
    lParent = .AddItem("No selection menu", , , 0, , , , "NoSelCtxMenu")
    .AddItem FormatResString(RID_PASTE), , , lParent, , , , "Paste3"
    .AddItem "-", , , lParent
    .AddItem FormatResString(RID_DUPLICATEFPG), , , lParent, , , , "DuplicateFpg"
    .AddItem FormatResString(RID_NEWWINDOW), , , lParent, , , , "NewWindow"
    .AddItem "-", , , lParent
    .AddItem FormatResString(RID_CTXZOOMIN), , , lParent, , , , "ZoomIn"
    .AddItem FormatResString(RID_CTXZOOMOUT), , , lParent, , , , "ZoomOut"
    .AddItem FormatResString(RID_TRANSPARENTBG), , , lParent, , , , "ToggleTransparency"
    
    ' No active FPg menu
    lParent = .AddItem("No active fpg menu", , , 0, , , , "NoFpgCtxMenu")
    .AddItem FormatResString(RID_NEWFPG), , , lParent, , , , "NewFpg"
    .AddItem FormatResString(RID_OPENFPG), , , lParent, , , , "OpenFpg"
    End With
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub UpdateTitleBar()
    Const PROC_NAME As String = "UpdateTitleBar"

    Dim sTitle As String
    
    On Error GoTo EH
    
    sTitle = Title
    If Len(sTitle) > 0 Then sTitle = sTitle & " - "
    
    If IsExe Then
        Me.Caption = sTitle & App.Title & " " & Version
    Else
        Me.Caption = sTitle & App.Title & " " & " (development version)"
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub UpdateStatusBar()
    Dim oMap As cMap
    Dim s As String
    
    Const PROC_NAME As String = "UpdateStatusBar"

    On Error GoTo EH

    With m_StatusBar
    If fp.FpgAvailable Then
        If fp.SelectionCount = 1 Then
            Set oMap = fp.GetFpg.Maps(fp.SelectedIndex)
            s = CStr(oMap.Code) & ": " & oMap.Description
            s = s & " (" & CStr(oMap.Width) & "x" & CStr(oMap.Height) & ")"
            
        ElseIf fp.SelectionCount > 1 Then
            s = CStr(fp.SelectionCount) & " " & _
                FormatResString(RID_GRAPHICSSELECTED)
        Else
            s = NoSelectionStBarMsg
        End If
        .PanelText("First") = s
        .PanelText("Second") = CStr(fp.GetFpg.MapCount) & " " & _
            FormatResString(RID_GRAPHICS)
        .PanelText("Third") = fp.GetFpg.Depth & " bpp"
    Else
        .PanelText("First") = FormatResString(RID_READY)
        .PanelText("Second") = ""
        .PanelText("Third") = ""
    End If
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateStatusBar()
    Const PROC_NAME As String = "CreateStatusBar"

    On Error GoTo EH

    Set m_StatusBar = New cNoStatusBar
    
    With m_StatusBar
    .Create picStBar
    .AllowXPStyles = False
    .SizeGrip = True
    .AddPanel estbrStandard, , , , True, , , "First"
    .AddPanel estbrStandard, , , 80, , , , "Second"
    .AddPanel estbrStandard, , , , , , , "Third"
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateToolBars()
    Const PROC_NAME As String = "CreateToolBars"

    On Error GoTo EH

    With tbrMain
    .DrawStyle = TOOLBAR_STYLE
    .ImageSource = CTBExternalImageList
    .SetImageList iml.hIml, CTBImageListNormal
    .CreateToolbar 32, , True, True

    .AddButton FormatResString(RID_NEWFPG), 1, , , , CTBDropDown Or CTBAutoSize, _
        "NewFpg"
    .AddButton FormatResString(RID_OPENFPG), 2, , , , CTBNormal Or CTBAutoSize, _
        "OpenFpg"
    .AddButton FormatResString(RID_SAVEFPG), 3, , , , CTBDropDown Or _
        CTBAutoSize, "SaveFpg"
        
    .AddButton , , , , , CTBSeparator
    
    .AddButton FormatResString(RID_PALETTEACTIONS), 14, , , , CTBDropDownArrow Or _
        CTBAutoSize, "PaletteActions"
    
    .AddButton , , , , , CTBSeparator

    .AddButton FormatResString(RID_CUT), 11, , , , CTBNormal Or CTBAutoSize, _
        "Cut"
    .AddButton FormatResString(RID_COPY), 12, , , , CTBNormal Or CTBAutoSize, _
        "Copy"
    .AddButton FormatResString(RID_PASTE), 13, , , , CTBNormal Or CTBAutoSize, _
        "Paste"

    .AddButton , , , , , CTBSeparator

    .AddButton FormatResString(RID_ADDGRAPHIC), 5, , , , CTBNormal Or _
        CTBAutoSize, "AddGraphic"
    .AddButton FormatResString(RID_VIEWEDITGRAPHIC), 6, , , , CTBDropDown Or _
        CTBAutoSize, "EditGraphic"
    .AddButton FormatResString(RID_EXPORTGRAPHIC), 7, , , , CTBDropDownArrow _
        Or CTBAutoSize, "ExportGraphic"
    .AddButton FormatResString(RID_REMOVEGRAPHIC), 8, , , , CTBNormal Or _
        CTBAutoSize, "RemoveGraphic"
        
    .AddButton , , , , , CTBSeparator

    .AddButton FormatResString(RID_NEWWINDOW), 10, , , , CTBNormal Or _
        CTBAutoSize, "NewWindow"
    End With
    
    ' Configure the ReBar
    With rebar
    .CreateRebar Me.hWnd
    .AddBandByHwnd tbrMain.hWnd, , , , "tbrMain"
    .BandChildMinWidth(.BandCount - 1) = 24
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Initialize()
    Set m_MapEditors = New Collection
End Sub

'===============================================================================

Private Sub Form_Load()
    Const PROC_NAME As String = "Form_Load"
    
    On Error GoTo EH

    ' Create elements of the interface
    CreateToolBars
    CreateStatusBar
    CreateMenus
    UpdateEnabledControls
    UpdateStatusBar
    
    ' Configuration dependent GUI-elements
    UseFpgCompression = (LCase(DefaultFpgEncoderOptions("use-gzip").sValue) = "true")
    
    ' Init clipboard hook object
    Set m_CV = New cClipboardViewer
    m_CV.InitClipboardChangeNotification Me.hWnd
    
    UpdateTitleBar
    fp.FpgNotAvailableMsg = FormatResString(RID_NOFPGMSG)
    
    If Not IsExe Then
        ' This is used for DEBUGGING purposes
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Const PROC_NAME As String = "Form_QueryUnload"

    On Error GoTo EH

    Cancel = Not AskToSaveIfDirty

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Resize()
    Dim sgTop As Single
    Dim sgHeight As Single
    Dim sgWidth As Single
    
    Const PROC_NAME As String = "Form_Resize"

    On Error GoTo EH

    If Me.WindowState = vbMinimized Then Exit Sub
    
    rebar.RebarSize
    
    ' Resize the FPG viewer to cover everything
    sgTop = ScaleY(rebar.RebarHeight, vbPixels, vbTwips)
    sgWidth = Me.ScaleWidth
    sgWidth = IIf(sgWidth < 0, 0, sgWidth)
    sgHeight = Me.ScaleHeight - sgTop - picStBar.Height
    sgHeight = IIf(sgHeight < 0, 0, sgHeight)
    fp.Move 0, sgTop, sgWidth, sgHeight

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Const PROC_NAME As String = "Form_Unload"

    On Error GoTo EH

    ' Clean up several GUI objects, for safety
    rebar.RemoveAllRebarBands
    Set m_StatusBar = Nothing

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_DDCustomFormatDropped(avData() As Byte, iButton As Integer, _
        Shift As Integer, lX As Long, lY As Long, lIndex As Long)

    Const PROC_NAME As String = "fp_DDCustomFormatDropped"
    
    On Error GoTo EH
    
    If fp.FpgAvailable Then
        AddGraphicFromMMapsStream Me, avData
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_DDDIBDropped(oPic As IPicture, iButton As Integer, _
        Shift As Integer, lX As Long, lY As Long, lIndex As Long)

    Const PROC_NAME As String = "fp_DDDIBDropped"
    
    Dim oMap As cMap
    
    On Error GoTo EH
    
    If fp.FpgAvailable Then
        AddGraphic Me, CreatemapFromOlePicture(oPic)
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_DDFilesDropped(asFiles() As String, iButton As Integer, _
        Shift As Integer, lX As Long, lY As Long, lIndex As Long)
            
    Const PROC_NAME As String = "fp_DDFilesDropped"
    
    Dim f As frmFpgEditor
    Dim i As Integer
    
    On Error GoTo EH
    
    If fp.FpgAvailable Then
        AddGraphicFromFiles Me, asFiles
    Else ' Understand we want to open an fpg
        Set f = Me
        For i = LBound(asFiles) To UBound(asFiles)
            OpenFpg asFiles(i), f
            Set f = Nothing ' Only the first fpg is loaded into the first window
        Next
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_FpgBecameAvailable()
    Const PROC_NAME As String = "fp_FpgBecameAvailable"

    On Error GoTo EH

    UpdateEnabledControls

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_GraphicDblClick(ByVal lMapIndex As Long)
    Const PROC_NAME As String = "fp_GraphicDblClick"

    On Error GoTo EH

    ViewEditGraphicAction Me, lMapIndex

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_MouseUp(iButton As Integer, x As Single, y As Single)
    Const PROC_NAME As String = "fp_MouseUp"
    
    Dim lMnuIndex As Long
    Dim sMnuName As String
    
    On Error GoTo EH
    
    ' Will show a context menu based on the selection
    If iButton = vbRightButton Then
        If fp.FpgAvailable Then
            If fp.SelectionCount > 0 Then
                If fp.SelectionCount = 1 Then
                    sMnuName = "SingleSelCtxMenu"
                Else
                    sMnuName = "MultiSelCtxMenu"
                End If
            Else
                sMnuName = "NoSelCtxMenu"
            End If
        Else
            sMnuName = "NoFpgCtxMenu"
        End If
        
        ' Check or uncheck the transparency entry
        lMnuIndex = m_CtxMenus.IndexForKey("ToggleTransparency")
        m_CtxMenus.Checked(lMnuIndex) = fp.ShowTransparency
        
        
        lMnuIndex = m_CtxMenus.IndexForKey(sMnuName) + 1
        m_CtxMenus.ShowPopupMenuAtIndex _
            Me.ScaleX(CLng(x), fp.ScaleMode, Me.ScaleMode) + fp.Left, _
            Me.ScaleY(CLng(y), fp.ScaleMode, Me.ScaleMode) + fp.Top, _
            , , , , , lMnuIndex
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_MouseWheel(eScrollDirection As SmartBennuCtls.E_BC_ScrollDirection, lAmount As Long, _
        ByVal iShift As Integer)
        
    Const PROC_NAME As String = "mp_MouseWheel"

    Dim lDelta As Long
    Dim bResizingAllowed
    
    On Error GoTo EH
    
    If iShift And vbCtrlMask Then
        bResizingAllowed = (fp.CellWidth > MIN_CELL_WIDTH _
                And fp.CellHeight > MIN_CELL_HEIGHT And Sgn(lAmount) = -1) _
                Or _
                (fp.CellWidth < MAX_CELL_WIDTH _
                And fp.CellHeight < MAX_CELL_HEIGHT And Sgn(lAmount) = 1)
                
        If bResizingAllowed Then
            lDelta = 8 * Sgn(lAmount)
            fp.LockRePaint
            fp.ThumbHeight = fp.ThumbHeight + lDelta
            fp.ThumbWidth = fp.ThumbWidth + lDelta
            fp.CellHeight = fp.CellHeight + lDelta
            fp.CellWidth = fp.CellWidth + lDelta
            fp.NameWidth = fp.NameWidth + lDelta
            fp.UnlockRePaint
            fp.RePaint True
        End If
        lAmount = 0
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME

End Sub

Private Sub fp_OLECompleteDrag(Effect As Long)
    Const PROC_NAME As String = "fp_OLECompleteDrag"
    
    Dim sTempDir As String
    
    On Error GoTo EH
    
    sTempDir = TemporalFolder

    ' TODO: Handle exceptions such as:
    ' Dir can't access to temporal folder or drive
    ' Kill fails
    ' RmDir cant delete folder because there might be other folders or is read-only
    ' ...
    If DirExists(sTempDir) Then
        If Len(Dir(sTempDir)) > 0 Then
            Kill BuildPath(sTempDir, "*.*")
            Debug.Print "Killed sTempDir: " & sTempDir
        End If
        ' TODO: I believe the CompleteDrag event concludes before windows releases resources and the
        ' temp dir is still being used by it, so I cannot remove the temp folder for the moment
        ' Maybe we can do the clean-up in a global terminate event of the application??
        'RmDir sTempDir
    End If
    
    ' TODO: Decide what to do depending on the effect
    Select Case Effect
    Case vbDropEffectCopy
        ' Nothing
    Case vbDropEffectMove
        ' Delete graphics from fpg
    Case vbDropEffectScroll
        ' ???
    End Select
    
    ' Erase drag & drop data object
    Set m_oDDDataObject = Nothing
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub fp_OLEGiveFeedback(Effect As Long, DefaultCursors As Boolean, ByVal Shift As Integer)
    Const PROC_NAME As String = "fp_OLEGiveFeedback"
    On Error GoTo EH

    ' For DD integration with explorer, determine which encoder
    ' the user wants to use based on the Ctrl Key
    If (Shift And vbCtrlMask) = 0 Then
        m_bUseEnc2 = False
    Else
        m_bUseEnc2 = True
    End If
    
    ' If the encoder to be used has changed, clean the DDData object
    ' so as the OLESetData is called again an thus, the temporary files
    ' are re-created in the new format
    If m_bOldUseEnc2 <> m_bUseEnc2 Then
        Debug.Assert Not (m_oDDDataObject Is Nothing)
        If Not m_oDDDataObject Is Nothing Then m_oDDDataObject.Files.Clear
    End If
    
    m_bOldUseEnc2 = m_bUseEnc2

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub fp_OLESetData(data As DataObject, FormatID As Integer, ByVal Shift As Integer)
    Const PROC_NAME As String = "fp_OLESetData"

    Dim oPic As StdPicture
    Dim oMap As cMap
    Dim oEncoder As IMapEncoder
    Dim oStdEncoder As IStandardEncoder
    Dim alSelIndexes() As Long
    Dim sFile As String
    Dim sTempFolder As String
    Dim i As Long
    Dim bUseEnc2 As Boolean ' Secondary encoder to be used?
    Static bOldUseEnc2 As Boolean ' Status of the bUseEnc2 in the previous call
    
    On Error GoTo EH
    
    If Not fp.FpgAvailable Or fp.SelectionCount < 0 Then Exit Sub

    If FormatID = CFMMaps Then
        data.SetData GetMMapsStreamForSelectedGraphics(Me), CFMMaps
    ElseIf FormatID = vbCFDIB Then
        Set oPic = CreateOlePictureFromMap(fp.GetFpg.Maps(fp.SelectedIndex))
        data.SetData oPic, vbCFDIB
    ElseIf FormatID = vbCFFiles Then
        sTempFolder = TemporalFolder
        
        If Not DirExists(sTempFolder) Then
            MkDir sTempFolder
            ' TODO: Handle if MKDir fails
            Debug.Print "MKDir has failed"
        Else
            ' TODO: Handle if DIR fails
            If Len(Dir(sTempFolder)) > 0 Then  ' There are files in the folder
                Kill BuildPath(sTempFolder, "*.*")
                ' TODO: Handle if Kill fails
                Debug.Print "Kill BuildPath: " & sTempFolder
            End If
        End If
        
        ' Decide if use encoder 1 or encoder 2
        Set oEncoder = IIf(Not m_bUseEnc2, New cDefaultMapEncoder, New cPngEncoder)
        Set oStdEncoder = oEncoder
        alSelIndexes = fp.GetSelectedIndexes()
        
        For i = 0 To fp.SelectionCount - 1
            Set oMap = fp.GetFpg.Maps(alSelIndexes(i))
            sFile = "#" & CStr(oMap.Code) & "#" & _
                    IIf(Len(oMap.Description) > 0, " ", "") & oMap.Description
            
            sFile = BuildPath(sTempFolder, sFile & "." & oStdEncoder.DefaultFileExtension)
            
            ' TODO: Handle if bennulib.SaveMap fails
            bennulib.SaveMap oMap, sFile, oEncoder ' Save file to temp folder
            data.Files.Add sFile
            Debug.Print "Encoded to " & sFile
        Next
       
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_OLEStartDrag(data As DataObject, AllowedEffects As Long)
    Const PROC_NAME As String = "fp_SelectionChanged"

    On Error GoTo EH
    
    If fp.FpgAvailable = False Or fp.SelectionCount <= 0 Then Exit Sub
    
    ' Set the formats but not the data (it will be copied when requested)
    data.SetData , vbCFDIB
    If CFMMapsL <> 0 Then
        data.SetData , CFMMaps
    End If
    
    ' For OLEDrop as file, make sure that the tempfolder will be available
    If IsTempFolderWriteable Then ' TODO: Besides writable should be accessible
        data.SetData , vbCFFiles
    End If
    m_bUseEnc2 = False
    m_bOldUseEnc2 = False
    Set m_oDDDataObject = data ' So as the GiveFeedback has access to this object
    
    AllowedEffects = vbDropEffectCopy
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub fp_SelectionChanged(ByVal lSelectedIndex As Long)
    Const PROC_NAME As String = "fp_SelectionChanged"

    On Error GoTo EH

    UpdateEnabledControls
    UpdateStatusBar

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Property Get IFpgEditorContext_FpgTitle() As String
    Const PROC_NAME As String = "IFpgEditorContext_FpgTitle"

    On Error GoTo EH
    
    IFpgEditorContext_FpgTitle = GetFileTitle(Filename, True)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Private Sub IFpgEditorContext_MapEditorColorIndexChangeCallback()
    
    Const PROC_NAME As String = "IFpgEditorContext_MapEditorColorIndexChangeCallback"

    On Error GoTo EH
    
    If m_fPaletteEditor Is Nothing Then Exit Sub

    ' TODO: Inform PaletteEditor about it

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Public Function AddMapEditor(fEditor As frmMapEditor) As Long
    Const PROC_NAME As String = "AddMapEditor"

    On Error GoTo EH
    
    m_MapEditors.Add fEditor
    Set fEditor.Context = Me

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

Private Function IndexOfMapEditor(fEditor As frmMapEditor) As Long
    Dim i As Integer
    
    Const PROC_NAME As String = "IndexOfMapEditor"

    On Error GoTo EH
    
    For i = 1 To m_MapEditors.Count
        If m_MapEditors(i) Is fEditor Then
            IndexOfMapEditor = i
            Exit Function
        End If
    Next
    
    IndexOfMapEditor = 0

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
    
End Function

Private Sub IFpgEditorContext_MapEditorUnloadCallback(fEditor As frmMapEditor)
    Dim lIndex As Long
    Const PROC_NAME As String = "IFpgEditorContext_MapEditorUnloadCallback"

    On Error GoTo EH
    
    lIndex = IndexOfMapEditor(fEditor)
    
    Debug.Assert lIndex > 0
    
    m_MapEditors.Remove lIndex
    
    Me.IsDirty = Me.IsDirty Or fEditor.IsDirty

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFpgEditorContext_PaletteEditorPaletteChangeCallback(oCurrentPalette As bennulib.cPalette)
    Dim oFpg As cFpg
    Dim fMapEditor As frmMapEditor
    
    Const PROC_NAME As String = "m_fPaletteEditor_OnColorsChanged"

    On Error GoTo EH
    
    Set oFpg = fp.GetFpg()
    oFpg.SetPaletteColors oCurrentPalette
    fp.RePaint
    
    For Each fMapEditor In m_MapEditors
        fMapEditor.mp.RePaint
    Next

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub IFpgEditorContext_PaletteEditorUnloadCallback(fEditor As frmPaletteEditor)
    
    Const PROC_NAME As String = "IFpgEditorContext_PaletteEditorUnloadCallback"

    On Error GoTo EH
    
    Me.IsDirty = fEditor.IsDirty Or Me.IsDirty

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub m_CV_ClipboardChanged()
    Const PROC_NAME As String = "m_CV_ClipboardChanged"
    
    On Error GoTo EH
    
    UpdateEnabledControls
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub picStBar_Paint()
    Const PROC_NAME As String = "picStBar_Paint"

    On Error GoTo EH

    ' Required to display status bar
    m_StatusBar.Draw

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub m_fCpEditor_Action(ByVal iCount As Integer, asX() As String, _
        asY() As String, alIds() As Long)
        
    Dim i As Integer, j As Integer
    Dim lCodes() As Long
    Dim lMapIndex As Long
    Dim Fpg As cFpg
    Dim lX As Long, lY As Long
    Dim oParser As New cParseLite
    
    Const PROC_NAME As String = "m_fCpEditor_Action"

    On Error GoTo EH

    ' Exit if no MAP is selected
    If fp.SelectionCount < 0 Then Exit Sub
    
    lCodes() = fp.GetSelectedCodes()
    Set Fpg = fp.GetFpg
    
    ' Set the control points of each selected MAP
    For i = 0 To fp.SelectionCount - 1
        lMapIndex = Fpg.IndexByCode(lCodes(i))
        Fpg.Maps(lMapIndex).RemoveAllControlPoints
        ' Establish the constants of the parser
        oParser.SetConstant "WIDTH", Fpg.Maps(lMapIndex).Width
        oParser.SetConstant "HEIGHT", Fpg.Maps(lMapIndex).Height
        ' oParser.SetConstant "CENTER_X", fpg.Maps(lMapIndex).Height
        ' oParser.SetConstant "CENTER_Y", fpg.Maps(lMapIndex).Height
        ' Parse the coordinates and add the control point
        For j = 0 To iCount - 1
            oParser.Execute asX(j), lX
            oParser.Execute asY(j), lY
            Fpg.Maps(lMapIndex).SetControlPoint alIds(j), lX, lY
        Next
    Next

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub m_DDMenus_Click(ItemNumber As Long)
    Dim sKey As String
        
    Const PROC_NAME As String = "m_DDMenus_Click"

    On Error GoTo EH

    sKey = m_DDMenus.ItemKey(ItemNumber)
        
    Select Case sKey
    Case "NewFpg8"
        NewFpg8Action Me
    Case "NewFpg16"
        NewFpg16Action Me
    Case "NewFpg32"
        NewFpg32Action Me
    Case "DuplicateFpg"
        DuplicateFpgAction Me
    Case "SaveFpg"
        SaveFpgAction Me
    Case "SaveFpgAs"
        SaveFpgAsAction Me
    Case "ExportFpgTo"
        ExportFpgToStrip Me
    Case "ViewEditGraphic"
        ViewEditGraphicAction Me, fp.SelectedIndex
    Case "EditControlPoints"
        EditControlPointsAction Me
    Case "ExportToMapFile"
        ExportGraphicToType Me, fp.SelectedIndex, New cDefaultMapEncoder
    Case "ExportToPngFile"
        ExportGraphicToType Me, fp.SelectedIndex, New cPngEncoder
    Case "ExportPalette"
        ExportPaletteToType Me, New cDefaultPaletteEncoder
    Case "ViewEditPalette"
        ViewEditPaletteAction Me
    Case "UseFpgCompression"
        UseFpgCompression = True
    Case "DoNotUseFpgCompression"
        UseFpgCompression = False
    End Select
    
    UpdateStatusBar

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub m_CtxMenus_Click(ItemNumber As Long)
    Const PROC_NAME As String = "m_CtxMenus_Click"
    
    Dim sKey As String

    On Error GoTo EH

    sKey = m_CtxMenus.ItemKey(ItemNumber)
    
    Select Case sKey
    Case "Copy"
        CopyAction Me
    Case "Cut"
        CutAction Me
    Case "Paste1", "Paste2", "Paste3"
        PasteAction Me
    Case "Remove"
        RemoveGraphicAction Me
    Case "ToMap"
        ExportGraphicToType Me, fp.SelectedIndex, New cDefaultMapEncoder
    Case "ToPng"
        ExportGraphicToType Me, fp.SelectedIndex, New cPngEncoder
    Case "ViewEdit"
        ViewEditGraphicAction Me, fp.SelectedIndex
    Case "ZoomIn"
        ZoomInAction Me
    Case "ZoomOut"
        ZoomOutAction Me
    Case "ToggleTransparency"
        ToggleTransparencyAction Me
    Case "DuplicateFpg"
        DuplicateFpgAction Me
    Case "NewWindow"
        NewWindowAction
    Case "NewFpg"
        NewFpgAction Me
    Case "OpenFpg"
        OpenFpgAction Me
    Case "Rename"
        RenameAction Me
    End Select
    
    UpdateStatusBar
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub rebar_ChevronPushed(ByVal wId As Long, ByVal lLeft As Long, _
            ByVal lTop As Long, ByVal lRight As Long, ByVal lBottom As Long)

    Const PROC_NAME As String = "rebar_ChevronPushed"

    On Error GoTo EH

    ' For the moment we only have one toolbar with chevron button so we
    ' don't need to check which toolbar fired the event
    tbrMain.ChevronPress lRight \ Screen.TwipsPerPixelX + 1, _
                    lTop \ Screen.TwipsPerPixelY

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub tbrMain_ButtonClick(ByVal lButton As Long)
    Dim sKey As String
    
    Const PROC_NAME As String = "tbrMain_ButtonClick"

    On Error GoTo EH

    sKey = tbrMain.ButtonKey(lButton)
    
    Select Case sKey
    Case "NewFpg"
        NewFpgAction Me
    Case "OpenFpg"
        OpenFpgAction Me
    Case "SaveFpg"
        SaveFpgAction Me
    Case "DuplicateFpg"
        DuplicateFpgAction Me
    Case "AddGraphic"
        AddGraphicAction Me
    Case "EditGraphic"
        ViewEditGraphicAction Me, fp.SelectedIndex
    Case "RemoveGraphic"
        RemoveGraphicAction Me
    Case "Cut"
        CutAction Me
    Case "Copy"
        CopyAction Me
    Case "Paste"
        PasteAction Me
    Case "NewWindow"
        NewWindowAction
    End Select
    
    UpdateStatusBar

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub tbrMain_DropDownPress(ByVal lButton As Long)
    Dim lX As Long, lY As Long
    Dim lMnuIndex As Long
    Dim sKey As String
    
    Const PROC_NAME As String = "tbrMain_DropDownPress"

    On Error GoTo EH

    sKey = tbrMain.ButtonKey(lButton)
    tbrMain.GetDropDownPosition lButton, lX, lY
    lMnuIndex = m_DDMenus.IndexForKey(sKey & "Menu") + 1
    
    m_DDMenus.ShowPopupMenuAtIndex lX, lY, lIndex:=lMnuIndex

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub tmrAnim_Timer()
    Static st As Integer
    Dim s As String
    
    Const PROC_NAME As String = "tmrAnim_Timer"

    On Error GoTo EH

    If fp.FpgAvailable Then
        tmrAnim.Enabled = False
        Exit Sub
    End If
    
    If st = 0 Then
        s = FormatResString(RID_MARQUEE1, PA2S("%APP%", App.Title))
        st = 1
        tmrAnim.Interval = 2500
    ElseIf st = 1 Then
        s = "Copyright � 2009 - 2016 Dar�o Cutillas Carrillo"
        st = 2
        tmrAnim.Interval = 3000
    ElseIf st = 2 Then
        s = FormatResString(RID_MARQUEE2) & " smartfpgeditor@dacucar.com"
        st = 3
        tmrAnim.Interval = 4000
    ElseIf st = 3 Then
        s = FormatResString(RID_MARQUEE3, PA2S("%LICENSE%", "GNU GPL 3"))
        st = 4
        tmrAnim.Interval = 2500
    ElseIf st = 4 Then
        s = FormatResString(RID_MARQUEE4) & _
            " http://bitbucket.org/dacucar/smartfpgeditor/"
        st = 5
        tmrAnim.Interval = 5000
    ElseIf st = 5 Then
        tmrAnim.Enabled = False
        UpdateStatusBar
        Exit Sub
    End If
        
    m_StatusBar.PanelText("First") = s

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub
