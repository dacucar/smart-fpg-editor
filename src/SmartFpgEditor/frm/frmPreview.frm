VERSION 5.00
Object = "{E142732F-A852-11D4-B06C-00500427A693}#1.14#0"; "vbalTbar6.ocx"
Object = "{396F7AC0-A0DD-11D3-93EC-00C0DFE7442A}#1.0#0"; "vbalIml6.ocx"
Object = "{FCE52DAF-EF3E-48E5-A71E-E7A56678C488}#38.0#0"; "SmartBennuCtls.ocx"
Begin VB.Form frmPreview 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Preview"
   ClientHeight    =   4800
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4800
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtCode 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   2760
      MaxLength       =   3
      TabIndex        =   1
      Top             =   120
      Width           =   615
   End
   Begin vbalIml6.vbalImageList iml 
      Left            =   3840
      Top             =   120
      _ExtentX        =   953
      _ExtentY        =   953
      ColourDepth     =   24
      Size            =   9184
      Images          =   "frmPreview.frx":0000
      Version         =   131072
      KeyCount        =   8
      Keys            =   "�������"
   End
   Begin SmartBennuCtls.SmartMapPane mp 
      Height          =   2295
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   4048
      BorderStyle     =   2
   End
   Begin vbalTBar6.cReBar rebar 
      Left            =   0
      Top             =   0
      _ExtentX        =   2355
      _ExtentY        =   661
   End
   Begin vbalTBar6.cToolbar tbrMain 
      Height          =   375
      Left            =   1440
      Top             =   0
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
   End
End
Attribute VB_Name = "frmPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private Const RID_PREVIOUSGRAPHIC As Long = 199
Private Const RID_NEXTGRAPHIC As Long = 200
Private Const RID_STRETCH As Long = 201
Private Const RID_TRANSPARENCY As Long = 160

Private Const MODULE_NAME As String = "SmartFpgEditor.frmPreview"

Private m_oFpg As cFpg
Private m_lIndex As Long

Public Property Set Fpg(oFpg As cFpg)

    Const PROC_NAME As String = "Fpg"
    On Error GoTo EH

    Set m_oFpg = oFpg

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get Fpg() As cFpg

    Const PROC_NAME As String = "Fpg"
    On Error GoTo EH

    Set Fpg = m_oFpg

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get MapIndex() As Long

    Const PROC_NAME As String = "MapIndex"
    On Error GoTo EH

    MapIndex = m_lIndex
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property


Public Property Let MapIndex(ByVal newVal As Long)

    Const PROC_NAME As String = "MapIndex"
    On Error GoTo EH

    If m_lIndex >= 0 And m_lIndex < m_oFpg.MapCount Then
        m_lIndex = newVal
        mp.setmap m_oFpg.Maps(m_lIndex)
        txtCode = CStr(m_oFpg.Maps(m_lIndex).Code)
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Private Sub Form_Load()

    Const PROC_NAME As String = "Form_Load"
    On Error GoTo EH

    CreateToolBars

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub CreateToolBars()
    Const PROC_NAME As String = "CreateToolBars"
    On Error GoTo EH

    ' Configure main toolbar
    With tbrMain
    .DrawStyle = TOOLBAR_STYLE
    .ImageSource = CTBExternalImageList
    .SetImageList iml.hIml, CTBImageListNormal
    .CreateToolbar 16, , True, False
    
    .AddButton FormatResString(RID_PREVIOUSGRAPHIC), 0, , , , CTBNormal Or _
        CTBAutoSize, "PreviousGraphic"
        
    .AddControl txtCode.hWnd
        
    .AddButton FormatResString(RID_NEXTGRAPHIC), 1, , , , CTBNormal Or _
        CTBAutoSize, "NextGraphic"
    .AddButton , , , , , CTBSeparator
    .AddButton FormatResString(RID_STRETCH), 2, , , , CTBNormal Or _
        CTBAutoSize, "PickColor"
    .AddButton , , , , , CTBSeparator
    .AddButton FormatResString(RID_TRANSPARENCY), 3, , , , CTBNormal Or _
        CTBAutoSize, "ToogleTransparency"
    End With
    
    With rebar
    .CreateRebar Me.hWnd
    .AddBandByHwnd tbrMain.hWnd, , , , "tbrMain"
    .BandGripper(0) = False
    .RebarSize
    End With

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub Form_Resize()
    Dim sgTop As Single
    Dim sgHeight As Single
    Dim sgWidth As Single
    
    Const PROC_NAME As String = "Form_Resize"

    On Error GoTo EH

    If Me.WindowState = vbMinimized Then Exit Sub
    
    rebar.RebarSize

    ' Resize the MAP viewer to cover everything
    sgTop = ScaleY(rebar.RebarHeight, vbPixels, vbTwips)
    sgWidth = Me.ScaleWidth
    sgWidth = IIf(sgWidth < 0, 0, sgWidth)
    sgHeight = Me.ScaleHeight - sgTop '- picStBar.Height
    sgHeight = IIf(sgHeight < 0, 0, sgHeight)
    mp.Move 0, sgTop, sgWidth, sgHeight

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub tbrMain_ButtonClick(ByVal lButton As Long)
    
    Const PROC_NAME As String = "tbrMain_ButtonClick"
    On Error GoTo EH
    
    Select Case tbrMain.ButtonKey(lButton)
    Case "PreviousGraphic"
        MapIndex = m_lIndex - 1
        txtCode = CStr(m_oFpg.Maps(m_lIndex).Code)
    Case "NextGraphic"
        MapIndex = m_lIndex + 1
    End Select
    Exit Sub

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub txtCode_KeyPress(KeyAscii As Integer)
    Const PROC_NAME As String = "txtInsertAt_KeyPress"
    
    On Error GoTo EH
    
    If KeyAscii = 13 Then
        KeyAscii = 0
    ElseIf KeyAscii <> 8 Then
        If Not IsNumeric(Chr(KeyAscii)) Then
            Beep
            KeyAscii = 0
        End If
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtCode_Validate(Cancel As Boolean)
    Dim lCode As Long
    Dim bMapExists As Boolean
    
    ' Check if the textbox contains a valid code for the FPG given
    Const PROC_NAME As String = "txtCode_Validate"
    On Error GoTo EH

    If IsNumeric(txtCode.Text) Then
        lCode = CLng(txtCode.Text)
        If m_oFpg.IsCodeValid(lCode) Then
            If m_oFpg.ExistsCode(lCode) Then bMapExists = True
        End If
    End If
    
    If bMapExists Then
        MapIndex = m_oFpg.IndexByCode(lCode)
    Else
        Cancel = True
        Beep
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
