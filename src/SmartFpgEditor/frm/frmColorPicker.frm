VERSION 5.00
Begin VB.Form frmColorPicker 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select color"
   ClientHeight    =   4845
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7350
   Icon            =   "frmColorPicker.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MouseIcon       =   "frmColorPicker.frx":064A
   MousePointer    =   1  'Arrow
   ScaleHeight     =   323
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   490
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkVgaColorsOnly 
      Caption         =   "VGA Colors only"
      Height          =   255
      Left            =   3120
      TabIndex        =   29
      Top             =   4560
      Width           =   1815
   End
   Begin VB.CheckBox chbPreview 
      Caption         =   "Preview"
      Enabled         =   0   'False
      Height          =   225
      Left            =   315
      TabIndex        =   22
      Top             =   4590
      Width           =   1005
   End
   Begin VB.PictureBox picBigBox 
      Height          =   3870
      Left            =   225
      MouseIcon       =   "frmColorPicker.frx":079C
      MousePointer    =   99  'Custom
      ScaleHeight     =   254
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   255
      TabIndex        =   21
      Top             =   465
      Width           =   3885
   End
   Begin VB.PictureBox picThinBox 
      AutoRedraw      =   -1  'True
      FillColor       =   &H00C0C0FF&
      ForeColor       =   &H00C0FFC0&
      Height          =   3840
      Left            =   4260
      Picture         =   "frmColorPicker.frx":08EE
      ScaleHeight     =   252
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   20
      Top             =   465
      Width           =   315
   End
   Begin VB.TextBox txtHexColor 
      Height          =   285
      Left            =   4995
      MaxLength       =   6
      TabIndex        =   14
      Text            =   "HexColor"
      Top             =   4230
      Width           =   1005
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   5
      Left            =   5280
      TabIndex        =   13
      Text            =   "B"
      Top             =   3720
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   4
      Left            =   5280
      TabIndex        =   12
      Text            =   "G"
      Top             =   3345
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   3
      Left            =   5280
      TabIndex        =   11
      Text            =   "R"
      Top             =   2940
      Width           =   450
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   2
      Left            =   5280
      TabIndex        =   10
      Text            =   "Brightness"
      Top             =   2520
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   1
      Left            =   5280
      TabIndex        =   9
      Text            =   "Saturation"
      Top             =   2145
      Width           =   420
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   0
      Left            =   5265
      TabIndex        =   8
      Text            =   "Hue"
      Top             =   1755
      Width           =   435
   End
   Begin VB.OptionButton objOption 
      Caption         =   "B:"
      Height          =   255
      Index           =   5
      Left            =   4785
      TabIndex        =   7
      Top             =   3735
      Width           =   495
   End
   Begin VB.OptionButton objOption 
      Caption         =   "G:"
      Height          =   255
      Index           =   4
      Left            =   4740
      TabIndex        =   6
      Top             =   3345
      Width           =   510
   End
   Begin VB.OptionButton objOption 
      Caption         =   "R:"
      Height          =   255
      Index           =   3
      Left            =   4740
      TabIndex        =   5
      Top             =   3000
      Width           =   495
   End
   Begin VB.OptionButton objOption 
      Caption         =   "B:"
      Height          =   255
      Index           =   2
      Left            =   4815
      TabIndex        =   4
      Top             =   2520
      Width           =   480
   End
   Begin VB.OptionButton objOption 
      Caption         =   "S:"
      Height          =   375
      Index           =   1
      Left            =   4800
      TabIndex        =   3
      Top             =   2070
      Width           =   480
   End
   Begin VB.OptionButton objOption 
      Caption         =   "H:"
      Height          =   255
      Index           =   0
      Left            =   4800
      TabIndex        =   2
      Top             =   1785
      Width           =   465
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   6225
      TabIndex        =   1
      Top             =   600
      Width           =   885
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H0000FF00&
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   345
      Left            =   6240
      TabIndex        =   0
      Top             =   120
      Width           =   885
   End
   Begin VB.Line linTriang1Rising 
      X1              =   277
      X2              =   282
      Y1              =   261
      Y2              =   256
   End
   Begin VB.Line linTriang2Falling 
      X1              =   318
      X2              =   327
      Y1              =   184
      Y2              =   189
   End
   Begin VB.Line linTriang2Rising 
      X1              =   325
      X2              =   334
      Y1              =   195
      Y2              =   189
   End
   Begin VB.Line linTriang2Vert 
      X1              =   318
      X2              =   315
      Y1              =   185
      Y2              =   200
   End
   Begin VB.Label lblThinContainer 
      BackColor       =   &H00FFFF80&
      ForeColor       =   &H80000007&
      Height          =   3870
      Left            =   4200
      TabIndex        =   28
      Top             =   465
      Width           =   525
   End
   Begin VB.Line linTriang1Falling 
      X1              =   277
      X2              =   282
      Y1              =   251
      Y2              =   256
   End
   Begin VB.Line linTriang1Vert 
      X1              =   277
      X2              =   277
      Y1              =   251
      Y2              =   261
   End
   Begin VB.Label lblComplementaryColor 
      BackColor       =   &H80000017&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "C.C."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Index           =   3
      Left            =   5745
      TabIndex        =   27
      ToolTipText     =   "Complementary Color (adds 180� to Hue Angle)."
      Top             =   645
      Width           =   435
   End
   Begin VB.Label lblSuffix 
      Caption         =   "%"
      Height          =   270
      Index           =   2
      Left            =   5790
      TabIndex        =   25
      Top             =   2550
      Width           =   210
   End
   Begin VB.Label lblSuffix 
      Caption         =   "%"
      Height          =   270
      Index           =   1
      Left            =   5775
      TabIndex        =   24
      Top             =   2160
      Width           =   195
   End
   Begin VB.Label lblSuffix 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Index           =   0
      Left            =   5775
      TabIndex        =   23
      Top             =   1755
      Width           =   210
   End
   Begin VB.Image imgMarker 
      Height          =   165
      Left            =   2535
      Picture         =   "frmColorPicker.frx":4532
      Top             =   4560
      Width           =   165
   End
   Begin VB.Label lblOldColor 
      BackColor       =   &H00FFFF80&
      Height          =   495
      Left            =   4800
      TabIndex        =   19
      Top             =   1005
      Width           =   900
   End
   Begin VB.Label lblNewColor 
      Appearance      =   0  'Flat
      BackColor       =   &H0099CCDD&
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   4800
      TabIndex        =   18
      Top             =   495
      Width           =   900
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   15
      Left            =   5475
      TabIndex        =   17
      Top             =   1845
      Width           =   15
   End
   Begin VB.Label Label2 
      Caption         =   "Select color:"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   120
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   4785
      TabIndex        =   15
      Top             =   4245
      Width           =   195
   End
   Begin VB.Label lblContainer 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Height          =   1050
      Left            =   4755
      TabIndex        =   26
      Top             =   465
      Width           =   690
   End
End
Attribute VB_Name = "frmColorPicker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Welcome to use, improve and share this utility. It gives you more control
' than the standard vb-colorpicker.
' Anna-Carin who created this program gives it away for free.

Dim bDrag As Boolean, iSystemColorAngleMax1530 As Integer, vSaturationMax255 As _
    Byte, vBrightnessMax255 As Byte
Dim m_SngRValue As Single, m_SngGValue As Single, m_SngBValue As Single
Dim bNotFirstTimeMarker As Boolean, m_BteMarkerOldX As Integer, m_BteMarkerOldY _
    As Integer
Dim alMarkerColorStore(11, 11) As Long
Dim m_bBigBoxReady As Boolean
Dim m_bRecentThinBoxPress As Boolean

Private Declare Function SetPixelWin32 Lib "gdi32" Alias "SetPixelV" ( _
        ByVal hDC As Long, ByVal x As Long, ByVal y As Long, _
        ByVal Color As Long) As Byte

Private Type HSL
    Hue As Integer '0 To 360.
    Saturation As Byte
    Luminance As Byte
End Type

Private m_bVGAColorsOnly As Boolean
Private m_bDialogResult As VbMsgBoxResult

Public Event OnSelectionChanged(ByVal r As Byte, ByVal g As Byte, ByVal b As Byte)

Public Property Get DialogResult() As VbMsgBoxResult
    DialogResult = m_bDialogResult
End Property

Public Property Get RedComponent() As Byte
    RedComponent = CByte(Text1(3))
End Property

Public Property Get BlueComponent() As Byte
    BlueComponent = CByte(Text1(5))
End Property

Public Property Get GreenComponent() As Byte
    GreenComponent = CByte(Text1(4))
End Property

' Programatically select a color
Public Function SetRgb(ByVal r As Byte, ByVal g As Byte, ByVal b As Byte)
    Text1(3).Text = CStr(r)
    Text1(4).Text = CStr(g)
    Text1(5).Text = CStr(b)
    
    ' Force update
    Text1_LostFocus (3)
End Function

' Converts the color into the (2^6, 2^6, 2^6) color space
Private Function RgbToVgaRgb(ByVal lColor As Long) As Long
    Dim r As Long
    Dim g As Long
    Dim b As Long
    Dim lNewColor As Long

    r = ((lColor And &HFF0000) \ 16 \ 4) * 4
    g = ((lColor And &HFF00&) \ 8 \ 4) * 4
    b = ((lColor And &HFF&) \ 4) * 4

    lNewColor = (r * 16) Or (g * 8) Or b

    RgbToVgaRgb = lNewColor
End Function

Private Function SetPixelV(ByVal hDC As Long, ByVal lX As Long, ByVal lY As Long, _
        ByVal lColor As Long) As Byte
        
    Dim lNewColor As Long

    If m_bVGAColorsOnly Then
        lNewColor = RgbToVgaRgb(lColor)
    Else
        lNewColor = lColor
    End If

    SetPixelV = SetPixelWin32(hDC, lX, lY, lNewColor)
End Function


Private Sub chkVgaColorsOnly_Click()
    m_bVGAColorsOnly = CBool(chkVgaColorsOnly.value = 1)
    picBigBox_Colorize
End Sub

Private Sub Command1_Click()
    m_bDialogResult = vbOK
    Unload Me
End Sub

Public Sub Form_Load()
    m_bDialogResult = vbCancel

    Dim udtAngelSaturationBrightness As HSL, bteValdRadioKnapp As Byte

    Dim Ctr As Byte, bteExtraWidth As Byte, bteExtraHeight As Byte

    m_bRecentThinBoxPress = True 'TO GET RID OF GREY SQUARES IN THE PICTURE.

    Me.Width = 7380
    Me.Height = 5280

    chbPreview.Move 13, 299, 103, 15 ' Left,Top,Width,Height.
    chbPreview.Visible = False ' Does not have a purpose for the moment
    imgMarker.Visible = False ' Hiding improves the look

    For Ctr = 0 To 2
        Text1(Ctr).Move 351, 117 + Ctr * 25, 30, 21
        objOption(Ctr).Move 320, 120 + Ctr * 25, 33, 17
    Next Ctr

    For Ctr = 3 To 5
        Text1(Ctr).Move 350, 196 + (Ctr - 3) * 26, 30, 21
        objOption(Ctr).Move 320, 198 + (Ctr - 3) * 26, 33, 17
    Next Ctr

    txtHexColor.Move 336, 281, 56, 19

    Label1.Move 319, 283, 13, 14 'tecknet #

    lblNewColor.Move 322, 33, 58, 33
    lblOldColor.Move 322, 66, 58, 33
    lblContainer.Move 321, 32, 60, 68
    ' Starts at the same color
    lblOldColor.BackColor = lblNewColor.BackColor

    ' LOADS WITH NOTHING TO GET GET THE JPG-IMAGE OUT OF SIGHT.
    picThinBox.Picture = Nothing
    picThinBox.ScaleMode = vbPixels

    ' Calculate the frame before styling
    ' Outer measure minus actual inner measure = framewidth
    bteExtraWidth = picThinBox.Width - picThinBox.ScaleWidth
    ' outer measure minus actual inner measure = framewidth
    bteExtraHeight = picThinBox.Height - picThinBox.ScaleHeight
    'frames are 4 units broad. curiosity fact is that teh frames of all
    ' vbcontrols except for forms are measured from the frame center,
    ' so you actually get half the width, but it works since vb use the
    ' same logic all the way
    picThinBox.Move 284, 31, 19 + bteExtraWidth, 256 + bteExtraHeight

    lblThinContainer.BackStyle = 0 'Transparent
    lblThinContainer.Left = 284 - 10: lblThinContainer.Top = picThinBox.Top: _
        lblThinContainer.Width = picThinBox.Width + 20: lblThinContainer.Height = _
        picThinBox.Height

    linTriang1Vert.X1 = 277: linTriang1Vert.X2 = 277: linTriang1Vert.Y1 = 251: _
        linTriang1Vert.Y2 = 261
    linTriang1Rising.X1 = 277: linTriang1Rising.X2 = 283: linTriang1Rising.Y1 = _
        261: linTriang1Rising.Y2 = 256
    linTriang1Falling.X1 = 277: linTriang1Falling.X2 = 283: _
        linTriang1Falling.Y1 = 251: linTriang1Falling.Y2 = 256

    linTriang2Vert.X1 = 314: linTriang2Vert.X2 = 314: linTriang2Vert.Y1 = 251: _
        linTriang2Vert.Y2 = 261
    linTriang2Rising.X1 = 309: linTriang2Rising.X2 = 314: linTriang2Rising.Y2 = _
        261: linTriang2Rising.Y1 = 256
    linTriang2Falling.X1 = 309: linTriang2Falling.X2 = 314: _
        linTriang2Falling.Y2 = 251: linTriang2Falling.Y1 = 256

    '256 increasing by 4 since vb probably cheats the same way as it did
    ' in picthinbox
    picBigBox.Width = 256 + 4
    picBigBox.Height = 256 + 4
    picBigBox.ScaleWidth = 256
    picBigBox.ScaleHeight = 256
    picBigBox.Left = 13
    picBigBox.Top = 31


    'objOption(0) = True 'STATES Hue AS DEFAULT. ***ATT!!!!!  THIS BOOTS THE CLICK ROUTINE TO DECORATE ThinBox AND BigBox.
    SplitlblNewColorToRGBboxes 'ALSO THE SYSTEM CONSTANTS OF RGB GETS UPDATED.
    udtAngelSaturationBrightness = RGBToHSL201(lblNewColor.BackColor, True) 'TRUE MEANS THAT HSL IS UPDATING BOTH THE textboxes AND THE systemConstants.

    objOption(bteValdRadioKnapp) = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = vbFormControlMenu Or UnloadMode = vbFormCode Then
        Cancel = True
        Me.Hide
    End If
End Sub

Private Sub objOption_Click(Index As Integer) 'Choosing modus.
    Dim Ctr As Integer

    For Ctr = 286 To 336
        txtHexColor.Move Ctr, 281, 56, 20
    Next Ctr
    DoEvents 'Problems with visual jam.

    If Index = 0 Then 'MsgBox "Hue"
        'picThinBox.Visible = True
        Call PaintThinBox(0)
        m_BteMarkerOldX = vSaturationMax255: m_BteMarkerOldY = 255 - _
            vBrightnessMax255
        If m_bBigBoxReady = True Then Call picBigBox_Colorize 'NO, MAKE THIS EASIER - REDRAW ONLY IF setup HAS FINISHED.
        Call picBigBox_Colorize
    End If

    If Index = 1 Then
        'picThinBox.Visible = True
        Call PaintThinBox(1)
        m_BteMarkerOldX = iSystemColorAngleMax1530 / 6: m_BteMarkerOldY = 255 - _
            vBrightnessMax255
        Call picBigBox_Colorize 'Speciell design.
        End If

    If Index = 2 Then ' "Brightness"
        'MsgBox "Saturation" 'cOLOR ANGEL IS NOW HORIZONTAL FROM LEFT TO RIGHT. TEXTBOXES ARE NOW IMPORTANT.
        'picThinBox.Visible = True
        Call PaintThinBox(2)
        m_BteMarkerOldX = iSystemColorAngleMax1530 / 6: m_BteMarkerOldY = 255 - _
            vSaturationMax255
        Call picBigBox_Colorize 'Speciell design.
        'Set objAnyPictureBox = Nothing 'Kanske sparar minne.
    End If

    If Index = 3 Then ' "R"
        'picThinBox.Visible = True
        'Call ColorCirkel("Red") 'Speciell design.
        'Call SampleMarkerBackground   'Sparar bakgrunden bakom Marker om d�r finns n�gon.
        'Call PaintMarker(m_BteMarkerOldX, m_BteMarkerOldY) 'Fyll i Markeren igen (om d�r finns n�gon).
        Call opt3RedPaintPicThinBox(ByVal Text1(4), Text1(5))
        m_BteMarkerOldX = Text1(5): m_BteMarkerOldY = 255 - Text1(4)
        Call picBigBox_Colorize 'Speciell design.

    End If
    If Index = 4 Then ' "G"
        Call opt4GreenPaintPicThinBox(ByVal Text1(3), Text1(5))
        m_BteMarkerOldX = Text1(5): m_BteMarkerOldY = 255 - Text1(3)
        Call picBigBox_Colorize 'Speciell design.

    End If
    If Index = 5 Then ' "B"
        Call opt5BluePaintPicThinBox(ByVal Text1(3), Text1(4))
        m_BteMarkerOldX = Text1(3): m_BteMarkerOldY = 255 - Text1(4)
        Call picBigBox_Colorize 'Speciell design.

    End If

    If Index = 9 Then ' "PictureBrowse"
        bNotFirstTimeMarker = False 'LOSES AN ALIEN SUGAR CUBE IN BigBox.
        picThinBox.Visible = False 'BackColor = HSLToRGB(ByVal iSystemColorAngleMax1530, ByVal vSaturationMax255, ByVal 255, False) 'Gets a lighter shade of the active color. 'Sets the whole square for easy fading.

        Call MoveHexBox
    End If

    Call imgArrowsModeDepending 'MOVING imgArrows

    picThinBox.Refresh
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub
Public Sub picBigBox_Colorize()
    Dim blnFadeToGrey As Boolean, r As Single, g As Single, b As Single

    picBigBox.Visible = False 'HIDES THE SLOW DRAWIING PROCEDURE.
    picBigBox.AutoRedraw = True 'ELSE YOU WONT SEE ANYTHING.
    'If bNotFirstTimeMarker = True Then Call EraseMarker 'raderar Marker eftersom en ny bakgrundsf�rg (11x11) beh�ver samplas n�r denna procedur �r klar.

    'Set objAnyPictureBox = picBigBox 'Ritar om canvas.
    '*****     ********     **********     **********
    If objOption(0) Then 'IN CASE Option(0) WE SHALL FETCH a fully saturated version of color AND MAKE A 3-D FADE.'
        Call Bigbox3D 'NEW VERSION
        'Call FadeToGrey(objAnyPictureBox, 255, False) 'Alla systemkonstanter borde vara uppdaterade redan.
        'Call FadeToBlack(objAnyPictureBox, 255, 255)
    End If

    If objOption(1) Then
        'Call RainBowSurface(objAnyPictureBox, 255, blnFadeToGrey, True)
        Call RainBowBigbox(vbFalse, vbTrue) 'FadeToGrey=False & FadeToBlack=True
    End If
    If objOption(2) Then
        'Call RainBowSurface(objAnyPictureBox, 255, True, False)
        Call RainBowBigbox(vbTrue, vbFalse) 'FadeToGrey= true & FadeToBlack=false
    End If

    If objOption(3) Then
        Call opt3RedPaintPicBigBox
    End If
    If objOption(4) Then
        Call opt4GreenPaintPicBigBox
    End If
    If objOption(5) Then
        Call opt5BluePaintPicBigBox
    End If

    picBigBox.Visible = True 'SHOWS THE PICBOX AFTER THE SLOW DECORATION.
    picThinBox.Visible = True 'IS NEEDED TO SHOW IN CASE THE FORMER MODE WAS POSTCARDVIEW WHICH THUS HIDES ThinBox.

    If bNotFirstTimeMarker = True Then 'IN CASE THERE IS A marker-coordinate...
        Call SampleMarkerBackground   'SAVES THE BACKGROUND OF MARKER IF THERE IS ANY.
        Call PaintMarker(m_BteMarkerOldX, m_BteMarkerOldY) 'REPAINT THE MARKER (if there is any).
        lblNewColor.BackColor = picBigBox.Point(m_BteMarkerOldX, _
            m_BteMarkerOldY)
    End If
    If m_bBigBoxReady = False Then 'PLACES A MARKER AT CORRECT LOCATION AT THE SETUP STAGE.
        bNotFirstTimeMarker = True 'PASSWORD.
        'MODE DEPENDING NEW MARKER POSITION.
        If objOption(0) Then m_BteMarkerOldX = vSaturationMax255: _
            m_BteMarkerOldY = 255 - vBrightnessMax255 'Transmitting logical values.
        If objOption(1) Then m_BteMarkerOldX = iSystemColorAngleMax1530 / 6: _
            m_BteMarkerOldY = 255 - vBrightnessMax255 'Transmitting logical values.
        If objOption(2) Then m_BteMarkerOldX = iSystemColorAngleMax1530 / 6: _
            m_BteMarkerOldY = 255 - vSaturationMax255 'Transmitting logical values.

        Call SampleMarkerBackground   'SAVES THE BACKGROUND OF MARKER IF THERE IS ANY.
        Call PaintMarker(m_BteMarkerOldX, m_BteMarkerOldY) 'REPAINT THE MARKER (if there is any).
        m_bBigBoxReady = True 'NOW AT LEAST THE FIRST SPONTANEOUS REDRAW HAS FINISHED.
    End If
End Sub
Private Sub picBigBox_MouseMove(Knapp As Integer, Shift As Integer, x As Single, _
    y As Single)
    
    Dim lngColor As Long, udtAngelSaturationBrightness As HSL

    If bDrag = False Then Exit Sub
    
    If x > 255 Then x = 255 'LIMITER.
    If x < 0 Then x = 0
    If y > 255 Then y = 255
    If y < 0 Then y = 0

    If objOption(0) Then
        lngColor = HSLToRGB(iSystemColorAngleMax1530, x, 255 - y, True)
    ElseIf objOption(1) Then
        lngColor = HSLToRGB(x * 6, vSaturationMax255, 255 - y, True)
        PaintThinBox (1)
    ElseIf objOption(2) Then
        lngColor = HSLToRGB(x * 6, 255 - y, vBrightnessMax255, True)
        PaintThinBox 2
    ElseIf objOption(3) Then
        BigBoxOpt3Reaction x, y
    ElseIf objOption(4) Then
        BigBoxOpt4Reaction x, y
    ElseIf objOption(5) Then
        BigBoxOpt5Reaction x, y
    End If

    m_bRecentThinBoxPress = False

End Sub
Private Sub picBigBox_MouseDown(Button As Integer, Shift As Integer, x As _
        Single, y As Single)

    Dim lColor As Long
    
    ' TODO: There should be no need to have this control block
    If m_bBigBoxReady = False Then
        MsgBox "m_bBigBoxReady = False i BigBox MouseDown! There are no colors to show in bigbox", _
        vbCritical
        Exit Sub 'Bail if no color in bigbox.
    End If

    bDrag = True

    'HIDING THE MARKER NOT TO RISK OF GETTING JAM IN MY PROBE.
    If bNotFirstTimeMarker = True Then
        Call EraseMarker
    End If

    If objOption(0) Then
        lColor = HSLToRGB(iSystemColorAngleMax1530, ByVal x, _
            ByVal 255 - y, True) 'CONVERT AND UPDATE TEXTBOXES.

    ElseIf objOption(1) Then
        lColor = HSLToRGB(ByVal x * 6, ByVal vSaturationMax255, ByVal 255 - y, _
            True) 'CONVERT AND UPDATE TEXTBOXES.
        Call FadeThinBoxToGrey 'REPAINT ThinBox - FADE SATURATED COLORS; THE SYSTEM CONSTANTS ARE ALREADY UPDATED.
        picThinBox.Refresh

    ElseIf objOption(2) Then
        'SETTING THE BRIGHT COLOR THAT IS TO BE FADED. CONVERTING AND UPDATING TEXTBOXES.
        picThinBox.BackColor = HSLToRGB(ByVal x * 6, ByVal 255 - y, 255, False)

        lColor = HSLToRGB(ByVal x * 6, ByVal 255 - y, ByVal vBrightnessMax255, _
            True)  'UPDATING THE REAL, NONSATURATED SYSTEM CONSTANTS AND lblNewColor.

        'REPAINTING ThinBox - FADE SATURATED COLORS ; THE SYSTEM CONSTANTS ARE ALREADY UPDATED.
        Call FadeThinBoxToBlack
        picThinBox.Refresh

    ElseIf objOption(3) Then
        Call BigBoxOpt3Reaction(ByVal x, y)

    ElseIf objOption(4) Then
        Call BigBoxOpt4Reaction(ByVal x, y)

    ElseIf objOption(5) Then
        Call BigBoxOpt5Reaction(ByVal x, y)
    End If

    bNotFirstTimeMarker = True
End Sub
Private Sub picBigBox_MouseUp(Knapp As Integer, Shift As Integer, x As Single, _
    y As Single)
    
    If bDrag = False Then Exit Sub
    
    If m_bBigBoxReady = False Then MsgBox "m_bBigBoxReady = False!", _
        vbInformation: Exit Sub 'Baile if no color in bigbox.

    bDrag = False
    If x > 255 Then x = 255 'LIMITER
    If x < 0 Then x = 0
    If y > 255 Then y = 255
    If y < 0 Then y = 0

    m_BteMarkerOldX = x
    m_BteMarkerOldY = y
    Call SampleMarkerBackground
    Call PaintMarker(x, y) 'PAINT MARKER ON ITS NEW LOCATION.
End Sub
Public Sub EraseMarker()
    'If blnSetup
    Dim CtrY As Byte, CtrX As Byte
        For CtrY = 0 To 10
        For CtrX = 0 To 10
        picBigBox.PSet (m_BteMarkerOldX - 5 + CtrX, m_BteMarkerOldY - 5 + CtrY), _
            alMarkerColorStore(CtrX, CtrY)
        Next CtrX
        Next CtrY
End Sub
Private Sub picThinBox_MouseDown(Button As Integer, Shift As Integer, x As _
    Single, y As Single)
    ' set flag to start drawing
    m_bRecentThinBoxPress = True
    bDrag = True: Call picThinBox_MouseMove(Button, Shift, x, y) 'REUSING THE UPDATE ROUTINES.

End Sub
Private Sub lblThinContainer_MouseDown(Button As Integer, Shift As Integer, x _
    As Single, y As Single)
    Dim sngScaleConst As Single
    sngScaleConst = Screen.TwipsPerPixelY 'GIVING ME THE ACTUAL SIZE OF THE PIXELS OF THE SCREEN, HERE = 15.

    m_bRecentThinBoxPress = True
    y = y / sngScaleConst 'CONVERTING FROM THE UNIT TWIP TO PIXELS. ATT! PROBLEM! SHOULD BE /20 BUT IS 15.
    bDrag = True: Call picThinBox_MouseMove(Button, Shift, x, y) 'REUSING THE UPDATE ROUTINES.
End Sub
Private Sub lblThinContainer_MouseMove(Knapp As Integer, Shift As Integer, x As _
    Single, y As Single)
    y = y / 15 'CONVERTING FROM THE UNIT TWIP TO PIXELS. ATT! PROBLEM! SHOULD BE /20 BUT IS 15.
    Call picThinBox_MouseMove(Knapp, Shift, x, y)
End Sub

Private Sub picThinBox_MouseMove(Knapp As Integer, Shift As Integer, x As _
    Single, y As Single)
    Dim lngColor As Long, udtAngelSaturationBrightness As HSL

    If bDrag = False Then Exit Sub

    If y < 0 Then y = 0 'LIMITER
    If y > 255 Then y = 255

    TriangelMove y 'ANIMATION

    If objOption(0) Then lngColor = HSLToRGB((255 - y) * 6, ByVal _
        vSaturationMax255, ByVal vBrightnessMax255, True): Exit Sub 'Convert and _
        update textboxes.
    If objOption(1) Then lngColor = HSLToRGB(ByVal iSystemColorAngleMax1530, _
        255 - y, ByVal vBrightnessMax255, True): Exit Sub 'Convert and update _
        textboxes.
    If objOption(2) Then lngColor = HSLToRGB(ByVal iSystemColorAngleMax1530, _
        ByVal vSaturationMax255, 255 - y, True) 'Convert and update textboxes.
    If objOption(3) Then
        Text1(3) = 255 - y
        udtAngelSaturationBrightness = _
            RGBToHSL201(RGB(Text1(3), Text1(4), Text1(5)), True) 'Convert and _
            update textboxes.
        lblNewColor.BackColor = RGB(Text1(3), Text1(4), Text1(5))
    End If
    If objOption(4) Then
        Text1(4) = 255 - y
        udtAngelSaturationBrightness = _
            RGBToHSL201(RGB(Text1(3), Text1(4), Text1(5)), True) 'Convert and _
            update textboxes.
        lblNewColor.BackColor = RGB(Text1(3), Text1(4), Text1(5))
    End If
    If objOption(5) Then
        Text1(5) = 255 - y
        udtAngelSaturationBrightness = _
            RGBToHSL201(RGB(Text1(3), Text1(4), Text1(5)), True) 'Convert and _
            update textboxes.
        lblNewColor.BackColor = RGB(Text1(3), Text1(4), Text1(5))
    End If
End Sub
Private Sub picThinBox_MouseUp(Button As Integer, Shift As Integer, x As Single, _
    y As Single)
    ' set flag to start drawing
    bDrag = False
    Call picBigBox_Colorize '(m_SngRValue, m_SngGValue, m_SngBValue)
End Sub
Private Sub lblThinContainer_MouseUp(Button As Integer, Shift As Integer, x As _
    Single, y As Single)
    ' set flag to start drawing
    y = y / 20 'CONVERTING FROM THE UNIT TWIP TO PIXELS.
    bDrag = False
    Call picBigBox_Colorize '(m_SngRValue, m_SngGValue, m_SngBValue)
End Sub

Public Sub FadeThinBoxToGrey()
    Dim sng255saturation As Single, sngLokalBrightness As Single, x As Byte, y _
        As Integer ', YCtr As Integer

    sng255saturation = 255: sngLokalBrightness = vBrightnessMax255

    For x = 0 To 19
        y = 0 'Sets YCtr for making a new countdown.
        Do 'Interesting if there would raise an error, thus a leap directly to EndSub.
        SetPixelV picThinBox.hDC, x, y, HSLToRGB(ByVal iSystemColorAngleMax1530, _
            ByVal Round(sng255saturation - sng255saturation * y / 255), ByVal _
            sngLokalBrightness, False)
        y = y + 1
        Loop While y < 256 'Because Y gets to big when the loop has finished.
    Next x

End Sub

Public Sub Bigbox3D()
    Dim sngLokalSaturation As Single, sngLokalBrightness As Single, YRADNOLL As _
        Integer
    Dim sngR256delToBlack As Single, sngG256delToBlack As Single, _
        sngB256delToBlack As Single
    Dim r As Single, g As Single, b As Single, lColor As Long, y As Integer, x _
        As Integer

    sngLokalSaturation = 255: sngLokalBrightness = 255 'There is a need for intense start color.
    'If R > G Then lSuperior = R Else lSuperior = G 'Det skulle g� att halvera denna rutin medelst en superior, men koden blir d� sv�rare att fatta.
    'If B > lSuperior Then lSuperior = B
    '********* Firstly a single fade from saturated to grey on the uppermost row.
        For x = 0 To 255
        SetPixelV picBigBox.hDC, x, YRADNOLL, HSLToRGB(ByVal _
            iSystemColorAngleMax1530, ByVal Round(sngLokalSaturation * x / 255), _
            ByVal sngLokalBrightness, False)
        Next x 'Resets Y for a new row.

    '********* Here will be an FADE TO BLACK for all columns ********

    For x = 255 To 0 Step -1
    'If blnVertical = True Then R = Ro: G = Go: B = Bo ' If line is vertical the reset for a new round.
    lColor = picBigBox.Point(x, 0) 'Reading the uppermost pixel which is to be faded.
    r = lColor And &HFF
    g = (lColor And &HFF00&) \ &H100&
    b = (lColor And &HFF0000) \ &H10000
    sngR256delToBlack = r / 255  'The fraction blocks which lead down to black.
    sngG256delToBlack = g / 255
    sngB256delToBlack = b / 255
    For y = 0 To 255 'Interesting if there would raise an error, thus a leap back to EndSub.
        'objAnyPictureBox.PSet (X, Y), RGB(R, G, B)
        SetPixelV picBigBox.hDC, x, y, RGB(r, g, b) 'Painting with API.
        r = r - sngR256delToBlack 'Darkening the shade one of a 256:th.
        g = g - sngG256delToBlack
        b = b - sngB256delToBlack
    Next y
    y = y - 1 'Because that Y gets too big when the loop is completed.
    Next x

End Sub

Public Sub FadeThinBoxToBlack()
    Dim sngR256delToBlack As Single, sngG256delToBlack As Single, _
        sngB256delToBlack As Single
    Dim r As Single, g As Single, b As Single, lColor As Long, x As Byte, y As _
        Integer

    For x = 0 To 19
    lColor = picThinBox.Point(x, 0) 'Reads the uppermost pixel MAX LIGHT which is to be faded.
    r = lColor And &HFF
    g = (lColor And &HFF00&) \ &H100&
    b = (lColor And &HFF0000) \ &H10000
    sngR256delToBlack = r / 255  'Fractions which leads down to black.
    sngG256delToBlack = g / 255
    sngB256delToBlack = b / 255
    'If blnVertical = True Then R = Ro: G = Go: B = Bo 'Om Vertical linje s� �terst�ller sig originalf�rgen f�r en ny runda.
    For y = 0 To 255 'Interesting if the is an error, thus a jump directly to EndSub.
        'objAnyPictureBox.PSet (X, Y), RGB(R, G, B)
        SetPixelV picThinBox.hDC, x, y, RGB(r, g, b) 'Painting with API.
        r = r - sngR256delToBlack 'Darkening the shade of one 256th.
        g = g - sngG256delToBlack
        b = b - sngB256delToBlack
    Next y
    y = y - 1 'Because Y gets too big when loop is complete.
    Next x

End Sub
Public Sub RainBowBigbox(blnFadeToGrey, blnFadeToBlack) 'Is used by both radiobutton 1 & 2.
    Dim Ctr As Byte, blnUpdateTextBoxes As Boolean, bteK4243 As Byte
    Dim Saturation As Single, Luminance As Single
    Static intNODE As Integer, YCtr As Integer, XCtr As Integer, _
        intRainbowAngle As Integer
    'There is no risk for getting dull shades since I use the native principal by adding/subtracting values against at constant FF-component.
    'The algoritm gives med decimal values which increases the importance for mathematical models for choosing color, not pic.point.
    'XCtr = X
    intRainbowAngle = 0 'Protects the systemcolorangel

    If blnFadeToGrey = vbTrue And blnFadeToBlack = vbFalse Then
        Saturation = 255
        Luminance = vBrightnessMax255 'Starting value fully saturated. Brightness is to be the same for the whole of bigbox.
    Else
        Saturation = vSaturationMax255
        Luminance = 255 'Fading from fully bright.
    End If

    'For intRainbowAngle = 0 To 1529
    bteK4243 = 42 'Has to alternate between 42 and 43 pixels per colorfield to make even at 256 pixels.
    'For intNODE = 0 To 1275 Step 255

    XCtr = 0 'To255
    For YCtr = 0 To 255
      Do 'X loopen 0 To 255.
        '1 Red in in direction towards yellow. Green is counting up.
        For Ctr = 1 To bteK4243  'Has to alternate between 42 and 43 pixels per colorfield to make even at 256 pixels.
            If blnFadeToBlack Then Luminance = 255 - YCtr 'Round(vBrightnessMax255 - (vBrightnessMax255 / 255 * YCtr))
            If blnFadeToGrey Then Saturation = 255 - YCtr 'Round(vSaturationMax255 - (vSaturationMax255 / 255 * YCtr))
            intRainbowAngle = intNODE + ((254 * (Ctr - 1)) / (bteK4243 - 1)) 'Wonderful solution: this logic about going from zero to the full value (here 254) I have been seeking for a long time.
            SetPixelV picBigBox.hDC, XCtr, YCtr, HSLToRGB(ByVal intRainbowAngle, _
                ByVal Saturation, ByVal Luminance, False)
            XCtr = XCtr + 1
        Next Ctr '
        If bteK4243 = 43 Then bteK4243 = 42 Else bteK4243 = 43
        intNODE = intNODE + 255 'Bistabile switch.
      Loop While XCtr < 255
        intRainbowAngle = 0 'Painting the last fully red which lies outside the logic.
        picBigBox.PSet (XCtr, YCtr), HSLToRGB(ByVal intRainbowAngle, ByVal _
            Saturation, ByVal Luminance, blnUpdateTextBoxes)
        intNODE = 0: XCtr = 0: Next YCtr
End Sub

Public Sub RainBowThinBox() 'By swapping the XY-vvalues at the call you can paint either horisontal or vertical.
    Dim Ctr As Byte, blnUpdateTextBoxes As Boolean, bteK4243 As Byte
    Dim blnHorizontal As Boolean, Saturation As Single, Luminance As Single
    Static intNODE As Integer, YCtr As Integer, XCtr As Integer, _
        intRainbowAngle As Integer
    'There is no risk for getting dull shades since I use the native principal by adding/subtracting values against at constant FF-component.
    'The algoritm gives med decimal values which increases the importance for mathematical models for choosing color, not pic.point.
    'picThinBox.ScaleMode = vbPixels
    intRainbowAngle = 0 'Protecting systemcolorangel
    Saturation = 255: Luminance = 255 'Fully shining colors.
    'If blnFadeToGrey = True And blnFadeToBlack = False Then Saturation = 255: Luminance = vBrightnessMax255 'Starting value is full saturation. Brightness is to be the same for the whole bigbox.
    'Horizontal or vertical kan be chosen by intKoordSuperior/intKoordInferior.
    'YCtr = 255: If XCtr = YCtr Then blnHorizontal = True

    'For intRainbowAngle = 0 To 1529
    bteK4243 = 42 'Has to alternate between 42 and 43 pixels per colorfield to make even at 256 pixels.
    'For intNODE = 0 To 1275 Step 255

    'Vertical
    For XCtr = 0 To 19
        Do 'Y loopen 255 To 0.
        '1 Red in in direction towards yellow. Green is counting up.
        For Ctr = 1 To bteK4243  'Has to alternate between 42 and 43 pixels per colorfield to make even at 256 pixels.
            intRainbowAngle = intNODE + ((254 * (Ctr - 1)) / (bteK4243 - 1)) 'Wonderful solution: this logic about going from zero to the full value (here 254) I have been seeking for a long time.
            'objAnyPictureBox.PSet (XCtr, YCtr), HSLToRGB(ByVal intRainbowAngle, ByVal Saturation, ByVal Luminance, blnUpdatetextBoxes)
            SetPixelV picThinBox.hDC, XCtr, YCtr, HSLToRGB(ByVal _
                intRainbowAngle, ByVal Saturation, ByVal Luminance, _
                blnUpdateTextBoxes)
            YCtr = YCtr - 1
        Next Ctr '
        If bteK4243 = 43 Then bteK4243 = 42 Else bteK4243 = 43
        intNODE = intNODE + 255 'Bistabile switch.
        Loop While YCtr > 0
        intRainbowAngle = 0 'Painting the last fully red which is outside the logic of the routine.
        SetPixelV picThinBox.hDC, XCtr, YCtr, HSLToRGB(ByVal intRainbowAngle, _
            ByVal Saturation, ByVal Luminance, blnUpdateTextBoxes)
        intNODE = 0
        YCtr = 255
        Next XCtr
    'End If

End Sub

Private Sub picThinBox_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim y As Integer, intDirektion As Integer
    'MsgBox "�r i rutinen picThinBox_KeyDown!"
    'Y = imgArrows.Top - 28

    If objOption(0) Then
        If KeyCode = vbKeyUp Then
            intDirektion = 1
            Call NudgeHueValue(ByVal intDirektion)
            Call picBigBox_Colorize
        End If
        If KeyCode = vbKeyDown Then
            intDirektion = -1
            Call NudgeHueValue(ByVal intDirektion)
            Call picBigBox_Colorize
        End If
    End If
    If objOption(1) Then '******
        MsgBox _
            "Add code for radio1! Probably just writing in textbox Saturation!", _
            vbInformation
    End If
    If objOption(2) Then '*****
        MsgBox "Add code for radio2!", vbInformation
    End If

End Sub

Public Sub NudgeHueValue(ByVal intDirektion)
'1530 levels. The triangels are moving every sixth step and are lying on the byte level of 1530/6.
'RGBtxtboxes tells the nudge level:
Dim lngColor  As Long
'NudgeValue goes from ZERO to 1536.
iSystemColorAngleMax1530 = iSystemColorAngleMax1530 + intDirektion 'Calculating the new value of iSystemColorAngleMax1530, thus +1 or -1.

If iSystemColorAngleMax1530 > 1530 Then iSystemColorAngleMax1530 = 1530 'Limiter.
If iSystemColorAngleMax1530 < 0 Then iSystemColorAngleMax1530 = 0

lngColor = HSLToRGB(ByVal iSystemColorAngleMax1530, vSaturationMax255, _
    vBrightnessMax255, True) 'lngColor as a function of HSLToRGB. System constants _
    are being updated at the same time.
Call TriangelMove(255 - (iSystemColorAngleMax1530 / 1530 * 255))  'Moving the triangel.

End Sub

Public Sub SampleMarkerBackground()
Dim CtrX As Byte, CtrY As Byte
'Saving the background behind Marker.
If bNotFirstTimeMarker = True Then
    For CtrY = 0 To 10
    For CtrX = 0 To 10
    alMarkerColorStore(CtrX, CtrY) = picBigBox.Point(m_BteMarkerOldX - 5 + CtrX, _
        m_BteMarkerOldY - 5 + CtrY)
    Next CtrX
    Next CtrY
End If
End Sub

Public Sub PaintMarker(x, y)

If vBrightnessMax255 < 200 Then 'White marker if the surroundings are grey.
    picBigBox.Circle (x, y), 5, vbWhite
    Exit Sub
End If

If Text1(0) < 26 Or Text1(0) > 200 Then 'Shades of blue.
    If vSaturationMax255 > 70 Then ' And vSaturationMax255 < 150 Then 'White marker if the surroundings are grey..
        picBigBox.Circle (x, y), 5, vbWhite
        Exit Sub
    End If
End If
    picBigBox.PaintPicture imgMarker, x - 5, y - 5, 11, 11, 0, 0, 11, 11, _
        vbSrcInvert 'Complementary colors
    picBigBox.PaintPicture imgMarker, x - 5, y - 5, 11, 11, 0, 0, 11, 11, _
        vbDstInvert

End Sub

Public Sub SplitlblNewColorToRGBboxes() 'Updating the system constants and textboxes regarding to RGB.
    m_SngRValue = lblNewColor.BackColor And &HFF: Text1(3) = m_SngRValue
    m_SngGValue = (lblNewColor.BackColor And &HFF00&) \ &H100&: Text1(4) = _
        m_SngGValue
    m_SngBValue = (lblNewColor.BackColor And &HFF0000) \ &H10000: Text1(5) = _
        m_SngBValue
End Sub

Private Function RGBToHSL201(ByVal RGBValue As Long, ByVal blnUpdateTextBoxes _
        As Boolean) As HSL
    Dim r As Long, g As Long, b As Long
    Dim lMax As Long, lMin As Long, lDiff As Long, lSum As Long

    r = RGBValue And &HFF&
    g = (RGBValue And &HFF00&) \ &H100&
    b = (RGBValue And &HFF0000) \ &H10000

    If r > g Then lMax = r: lMin = g Else lMax = g: lMin = r 'Finds the Superior and inferior components.
    If b > lMax Then lMax = b Else If b < lMin Then lMin = b

    lDiff = lMax - lMin
    lSum = lMax + lMin
    'Luminance, thus brightness' Adobe photoshop uses the logic that the site VBspeed regards (regarded) as too primitive = superior decides the level of brightness.
    RGBToHSL201.Luminance = lMax / 255 * 100
    'Saturation******
    If lMax <> 0 Then 'Protecting from the impossible operation of division by zero.
        RGBToHSL201.Saturation = 100 * lDiff / lMax 'The logic of Adobe Photoshops is this simple.
    Else
        RGBToHSL201.Saturation = 0
    End If
    'Hue ************** R is situated at the angel of 360 eller noll degrees; G vid 120 degrees; B vid 240 degrees. iSystemColorAngleMax1530
    Dim q As Single
    If lDiff = 0 Then q = 0 Else q = 60 / lDiff 'Protecting from the impossible operation of division by zero.
    Select Case lMax
        Case r
            If g < b Then
                RGBToHSL201.Hue = 360& + q * (g - b)
            iSystemColorAngleMax1530 = (360& + q * (g - b)) * 4.25 'Converting from degrees to my resolution of detail.
            Else
                RGBToHSL201.Hue = q * (g - b)
            iSystemColorAngleMax1530 = (q * (g - b)) * 4.25
            End If
        Case g
            RGBToHSL201.Hue = 120& + q * (b - r) ' (R - G)
        iSystemColorAngleMax1530 = (120& + q * (b - r)) * 4.25
        Case b
            RGBToHSL201.Hue = 240& + q * (r - g)
        iSystemColorAngleMax1530 = (240& + q * (r - g)) * 4.25
    End Select 'The case of B was missing.

    If blnUpdateTextBoxes = True Then
        'txtHexColor = Hex$(R * 65536 + G * 256 + B): txtHexColor.Refresh 'Applying to internetstandard<>VBstandard
        If r < &H10 Then
            txtHexColor = Right$("00000" & Hex$(r * 65536 + g * 256 + b), 6) 'Adds letters of zero to the left which is a necessary so called padding.
        Else
            txtHexColor = Hex$(r * 65536 + g * 256 + b)
        End If

        txtHexColor.Refresh 'End of hexabox routine.
        Text1(0) = Round(iSystemColorAngleMax1530 / 1530 * 360)

        If lMax = 0 Then
            vSaturationMax255 = 0
        Else
            vSaturationMax255 = 255 * lDiff / lMax
            ' Saturation both 0 To 255 and 0 To 100%.
            Text1(1) = RGBToHSL201.Saturation
        End If

        ' Brighness both 0 To 255 and 0 To 100%.
        vBrightnessMax255 = lMax
        Text1(2) = RGBToHSL201.Luminance
    End If
End Function
Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If (KeyAscii < 48 Or KeyAscii > 57) Then 'Limiting the numerical textboxes (Text1[x]) to just register numerical enters.
        KeyAscii = 0
    End If
End Sub

Private Sub Text1_LostFocus(Index As Integer)
    Dim udtAngelSaturationBrightness As HSL, lngColor As Long 'Has to take care of iSystemColorAngleMax1530 0 To 1529.
    'If m_bBigBoxReady = False Then Exit Sub 'Even the computers own enters are giving undesired calls to this routine.
        m_bBigBoxReady = False 'Gives me fresh coordinates, but only in the RBG-model at this stage.
        bNotFirstTimeMarker = False '-"-

    'HAVE TO ADD THE FUNCTIONALITY: img.Pilars position is totally dependent of the actual mode.
    If Index = 0 Then 'The user adjusted Hue so RGB will be aproximately calculated.
        If Text1(0) > 360 Then MsgBox _
            "An integer between 0 and 360 i required. Closest value inserted!", _
            vbCritical, "Color Picker": Text1(0) = 360 'Checking both the precense of _
            decimals and numbers greater than 360.
    '    If Text1(0) < 0 Then MsgBox "An integer between 0 and 360 i required. Closest value inserted!", vbCritical, "Color Picker": Text1(0) = 0 'Checking both the precense of decimals and numbers greater than 360.
        If Text1(0) <> Round(Text1(0)) Then MsgBox _
            "An integer between 0 and 360 i required. Closest value inserted!", _
            vbCritical, "Color Picker": Text1(0) = Round(Text1(0))  'Checking both the _
            precense of decimals and numbers greater than 360.

        lngColor = HSLToRGB(Text1(0) / 360 * 255 * 6, vSaturationMax255, _
            vBrightnessMax255, True)
        'imgArrows.Top = 255 - (iSystemColorAngleMax1530 / 1530 * 255) + 28 'Animating imgArrows
    End If
    If Index = 1 Then 'The user adjusted Saturation so RGB will be aproximately calculated.
        If Text1(1) > 100 Then MsgBox _
            "An number between 0 and 100 i required. Closest value inserted!", _
            vbCritical, "Color Picker": Text1(1) = 100 'Checking both the precense of _
            decimals and numbers greater than 360.
        If Text1(1) < 0 Then MsgBox _
            "An number between 0 and 100 i required. Closest value inserted!", _
            vbCritical, "Color Picker": Text1(1) = 0 'Checking both the precense of _
            decimals and numbers greater than 360.

        lngColor = HSLToRGB(iSystemColorAngleMax1530, Text1(1) / 100 * 255, _
            vBrightnessMax255, True)
    End If
    If Index = 2 Then 'The user adjusted Luminance so RGB will be aproximately calculated.
        If Text1(2) > 100 Then MsgBox _
            "An number between 0 and 100 i required. Closest value inserted!", _
            vbCritical, "Color Picker": Text1(2) = 100 'Checking both the precense of _
            decimals and numbers greater than 360.
        If Text1(2) < 0 Then MsgBox _
            "An number between 0 and 100 i required. Closest value inserted!", _
            vbCritical, "Color Picker": Text1(2) = 0 'Checking both the precense of _
            decimals and numbers greater than 360.

        lngColor = HSLToRGB(iSystemColorAngleMax1530, vSaturationMax255, Text1(2) / _
            100 * 255, True)
    End If


    'ByVal RGBValue As Long, ByVal blnUpdateTextBoxes As Boolean
    If Index > 2 Then 'The user adjusted RGB so HSL is to calculated aproximately.
        'lngColor = RGB(Text1(3), Text1(4), Text1(5))
        udtAngelSaturationBrightness = RGBToHSL201(RGB(Text1(3), Text1(4), _
            Text1(5)), True)
    End If

    'Justera imgArrows beroende p� modus.
    Call imgArrowsModeDepending
    Call picBigBox_Colorize   '(m_SngRValue, m_SngGValue, m_SngBValue)'Redrawing BigBox

End Sub

Private Sub txtHexColor_Change()
    If Me.Visible Then
        RaiseEvent OnSelectionChanged(CByte(Text1(3)), CByte(Text1(4)), CByte(Text1(5)))
    End If
End Sub

Private Sub txtHexColor_KeyPress(KeyAscii As Integer) 'Limits the textbox to numerics and A-F and capitals and to six pieces of letters.
    'The limitation to six letters probably has to be done in the vb editor poreperties window (see Greg Perry VB in 6 days).
    'txtHexColor.Refresh 'Otherwise the limitation of length wont work because i doesnt undestand its current length.'
    'If Len(txtHexColor) > 6 Then Exit Sub 'Max sex letters in the box.
    If (KeyAscii > 64 And KeyAscii < 71) Then Exit Sub 'A-F are OK.
    If (KeyAscii > 96 And KeyAscii < 103) Then KeyAscii = KeyAscii - 32: Exit Sub 'a-f becomes A-F. OK.
    If (KeyAscii > 47 And KeyAscii < 58) Then Exit Sub 'Numerics are OK.

    KeyAscii = 0 'All other letters are unwanted.
End Sub

Private Sub txtHexColor_LostFocus()
    'Dim udtAngelSaturationBrightness As HSL, lngColor As Long 'Must take care of iSystemColorAngleMax1530 0 To 1529.
    'On Error GoTo Bajs
    Dim sShift As String 'OBS! Must shift RGB into BGR to fit vb-standard.
    sShift = txtHexColor: sShift = Mid(sShift, 5) & Mid(sShift, 3, 2) & Mid(sShift, _
        1, 2) 'Shifting RGB to BGR.
    lblNewColor.BackColor = ("&H" + sShift) 'OBS! Must shift RGB into BGR to fit vb-standard.
    Call SplitlblNewColorToRGBboxes 'Automatic update of the RGB textboxes.
    Call Text1_LostFocus(3) 'Simulating that the user adjusted the RGBtxtboxes->Total update. 3 means that the RedTextbox has been adjusted.

    Exit Sub

Bajs:
    MsgBox "An error occured while translating hexnumber to decimal number!", _
        vbInformation
End Sub
Public Function HSLToRGB(ByVal intLocalColorAngle As Integer, ByVal Saturation _
        As Long, ByVal Luminance As Long, ByVal blnUpdateTextBoxes As Boolean) As Long

    Dim r As Long, g As Long, b As Long, lMax As Byte, lMid As Byte, lMin As Long, _
        q As Single
    lMax = Luminance
    lMin = (255 - Saturation) * lMax / 255 '255 - (Saturation * lMax / 255)
    q = (lMax - lMin) / 255

    Select Case intLocalColorAngle
        Case 0 To 255
            lMid = (intLocalColorAngle - 0) * q + lMin
            r = lMax: g = lMid: b = lMin
        Case 256 To 510 'This period surpasses the node border with one unit - over to gren color. CHECK by F8.
            lMid = -(intLocalColorAngle - 255) * q + lMax '-(intLocalColorAngle - 256) * q + lMin
            r = lMid: g = lMax: b = lMin
        Case 511 To 765
            lMid = (intLocalColorAngle - 510) * q + lMin
            r = lMin: g = lMax: b = lMid
        Case 766 To 1020
            lMid = -(intLocalColorAngle - 765) * q + lMax
            r = lMin: g = lMid: b = lMax
        Case 1021 To 1275
            lMid = (intLocalColorAngle - 1020) * q + lMin
            r = lMid: g = lMin: b = lMax
        Case 1276 To 1530
            lMid = -(intLocalColorAngle - 1275) * q + lMax
            r = lMax: g = lMin: b = lMid
        Case Else
            MsgBox "Error occured in HSLToRGB. intSystemColorAngleMax1530= " & _
                Str(intLocalColorAngle), vbInformation
    End Select

    m_SngRValue = r: m_SngGValue = g: m_SngBValue = b 'Updating the sustem constants automatically. Perhaps must exclude this to give them protection.
    HSLToRGB = b * &H10000 + g * &H100& + r 'Delivers lngColor in VB-format.

    If blnUpdateTextBoxes = True Then 'Then the calling routine is not any of the complex automatic routines for fading etc.
    'Since this is a single time called session I can safely update my system constants and convert my hifgh resolution system constants to textbox dito.
        Text1(0) = Round(intLocalColorAngle / 255 / 6 * 360)
        Text1(1) = Round(Saturation / 255 * 100)
        Text1(2) = Round(Luminance / 255 * 100)
        Text1(3) = m_SngRValue
        Text1(4) = m_SngGValue
        Text1(5) = m_SngBValue
        Text1(0).Refresh
        Text1(1).Refresh
        Text1(2).Refresh
        Text1(3).Refresh
        Text1(4).Refresh
        Text1(5).Refresh
        'txtHexColor = Hex$(m_SngRValue * 65536 + m_SngGValue * 256 + m_SngBValue): txtHexColor.Refresh 'Applies to internetstandard<>VBstandard
        If m_SngRValue < &H10 Then
            txtHexColor = Right$("00000" & Hex$(m_SngRValue * 65536 + m_SngGValue * _
                256 + m_SngBValue), 6) 'Padding with zeroletters to the left.
        Else
            txtHexColor = Hex$(m_SngRValue * 65536 + m_SngGValue * 256 + _
                m_SngBValue)
        End If
        txtHexColor.Refresh 'End of the Hexabox routine.
        lblNewColor.BackColor = HSLToRGB
        lblNewColor.Refresh
        iSystemColorAngleMax1530 = intLocalColorAngle 'Sometims there is only a mouse Y coordinate tha is delivered from the calling routinen.
        vSaturationMax255 = Saturation
        vBrightnessMax255 = Luminance
    End If
End Function
Private Sub lblNewColor_Click()
    Dim udtAngelSaturationBrightness As HSL ', lngColor As Long

    lblOldColor.BackColor = lblNewColor.BackColor
    m_bBigBoxReady = False 'Delivers fresh coordinates, but only in the HSL-model at this stage.
    bNotFirstTimeMarker = False '-"-

    udtAngelSaturationBrightness = RGBToHSL201(lblNewColor.BackColor, True) 'True means that HSL is updating both the textboxes and the system constants.

    Call TriangelMove(255 - (iSystemColorAngleMax1530 / 1530 * 255) + 28) 'Animates the triangeln.
    Call picBigBox_Colorize   'Redraw BigBox

End Sub
Private Sub lblOldColor_Click()
    Dim udtAngelSaturationBrightness As HSL ', lngColor As Long

    lblNewColor.BackColor = lblOldColor.BackColor
    m_bBigBoxReady = False 'Delivers fresh coordinates, but only in the HSL-model at this stage.
    bNotFirstTimeMarker = False '-"-

    udtAngelSaturationBrightness = RGBToHSL201(lblNewColor.BackColor, True) 'True means that HSL is updating both the textboxes and the system constants.

    Call TriangelMove(255 - (iSystemColorAngleMax1530 / 1530 * 255) + 28) 'Animates the triangel.
    Call picBigBox_Colorize   'Rita om BigBox

End Sub
Private Sub imgArrowsModeDepending()
    'AdjustingJusterar imgArrows depending on current mode.
    If objOption(0) Then Call TriangelMove(255 - (iSystemColorAngleMax1530 / 1530 * _
        255)) 'Animating the triangel.
    If objOption(1) Then Call TriangelMove(255 - (Text1(1) * 2.55))  'Animating the triangel.
    If objOption(2) Then Call TriangelMove(255 - (Text1(2) * 2.55))  'Animating the triangel.
    If objOption(3) Then Call TriangelMove(255 - Text1(3))   'Animating the triangel.

End Sub

Private Sub lblComplementaryColor_Click(Index As Integer)
    If Text1(0) < 180 Then Text1(0) = Text1(0) + 180 Else Text1(0) = Text1(0) - 180
    Call Text1_LostFocus(0) 'Noll stands for Hue.
End Sub
Private Sub PaintThinBox(Index As Integer)
    Dim blnFadeToGrey As Boolean, blnFadeToBlack As Boolean

    If Index = 0 Then
        Call RainBowThinBox
    End If

    If Index = 1 Then
        Call FadeThinBoxToGrey
        picThinBox.Refresh
        'Set objAnyPictureBox = Nothing 'Kanske sparar minne.
        End If

    If Index = 2 Then ' "Brightness"
        'Crucial to give the thin box maximunm lightness as a starting point for the lightness fade.
        'Delivers a lighter shade of the active color. 'Setting the whole square for easy fading.
        picThinBox.BackColor = HSLToRGB(ByVal iSystemColorAngleMax1530, ByVal _
            vSaturationMax255, ByVal 255, False)
        Call FadeThinBoxToBlack
        picThinBox.Refresh
    End If
    picThinBox.Visible = True

End Sub
Private Sub TriangelMove(y)
    linTriang1Vert.Y1 = y + 28: linTriang1Vert.Y2 = y + 28 + 10
    linTriang1Rising.Y1 = y + 28 + 10: linTriang1Rising.Y2 = y + 28 + 4
    linTriang1Falling.Y1 = y + 28: linTriang1Falling.Y2 = y + 28 + 6

    linTriang2Vert.Y1 = y + 28: linTriang2Vert.Y2 = y + 28 + 10
    linTriang2Rising.Y2 = y + 28 + 10: linTriang2Rising.Y1 = y + 28 + 5
    linTriang2Falling.Y2 = y + 28: linTriang2Falling.Y1 = y + 28 + 5
End Sub
Public Sub opt3RedPaintPicThinBox(ByVal g, b)
    Dim bteX As Byte, intCtr As Integer
    'Painting picThinBox (19, 255)
    For bteX = 0 To 19
        For intCtr = 0 To 255
            SetPixelV picThinBox.hDC, bteX, intCtr, RGB(255 - intCtr, g, b) 'Painting with API.
        Next intCtr
    Next bteX
End Sub
Public Sub opt4GreenPaintPicThinBox(ByVal r, b)
    Dim bteX As Byte, intCtr As Integer
    'Painting picThinBox (19, 255)
    For bteX = 0 To 19
        For intCtr = 0 To 255
            SetPixelV picThinBox.hDC, bteX, intCtr, RGB(r, 255 - intCtr, b) 'Painting by API.
        Next intCtr
    Next bteX
End Sub
Public Sub opt5BluePaintPicThinBox(ByVal r, g)
    Dim bteX As Byte, intCtr As Integer
    'Painting picThinBox (19, 255)
    For bteX = 0 To 19
        For intCtr = 0 To 255
            SetPixelV picThinBox.hDC, bteX, intCtr, RGB(r, g, 255 - intCtr) 'Painting by API.
        Next intCtr
    Next bteX
End Sub

Public Sub BigBoxOpt3Reaction(ByVal x, y)
    Dim udtAngelSaturationBrightness As HSL

    lblNewColor.BackColor = picBigBox.Point(x, y): lblNewColor.Refresh
        Call SplitlblNewColorToRGBboxes 'Updating the module global m_SngRValue etc.
        Call opt3RedPaintPicThinBox(ByVal m_SngGValue, m_SngBValue)
        picThinBox.Refresh
        udtAngelSaturationBrightness = RGBToHSL201(RGB(Text1(3), Text1(4), _
            Text1(5)), True) 'Causes an update of all the constants.
End Sub
Public Sub BigBoxOpt4Reaction(ByVal x, y)
    Dim udtAngelSaturationBrightness As HSL

    lblNewColor.BackColor = picBigBox.Point(x, y): lblNewColor.Refresh
        Call SplitlblNewColorToRGBboxes 'Updating the module global m_SngRValue etc.
        Call opt4GreenPaintPicThinBox(ByVal m_SngRValue, m_SngBValue)
        picThinBox.Refresh
        udtAngelSaturationBrightness = RGBToHSL201(RGB(Text1(3), Text1(4), _
            Text1(5)), True) 'Causes an update of all the constants. The letter of _
            three stands for the RED-box.
End Sub
Public Sub BigBoxOpt5Reaction(ByVal x, y)
    Dim udtAngelSaturationBrightness As HSL

    lblNewColor.BackColor = picBigBox.Point(x, y): lblNewColor.Refresh
        Call SplitlblNewColorToRGBboxes 'Updating the module global m_SngRValue etc.
        Call opt5BluePaintPicThinBox(ByVal m_SngRValue, m_SngGValue)
        picThinBox.Refresh
        udtAngelSaturationBrightness = RGBToHSL201(RGB(Text1(3), Text1(4), _
            Text1(5)), True) 'Causes an update of all the constants. The letter of _
            three stands for the RED-box..
End Sub
Private Sub opt3RedPaintPicBigBox()
    Dim r As Single, g As Single, b As Single
    'Paint the picBigBox
    r = Text1(3) 'Red
    For b = 255 To 0 Step -1
    For g = 255 To 0 Step -1 'Interesting if there is an error, thus a jump directly to EndSub.
        SetPixelV picBigBox.hDC, b, 255 - g, RGB(r, g, b) 'Painting by API.
    Next g
    'G = G - 1 'Because that G becomes too big when the loop has finishes.
    Next b

End Sub
Private Sub opt4GreenPaintPicBigBox()
    Dim r As Single, g As Single, b As Single
    'Paint picBigBox
    g = Text1(4) 'Green
    For b = 255 To 0 Step -1
    For r = 255 To 0 Step -1 'Interesting if there is an error, thus a jump directly to EndSub.
        SetPixelV picBigBox.hDC, b, 255 - r, RGB(r, g, b) 'Painting by API.
    Next r
    r = r - 1 'Because that R becomes too big when the loop has finishes.
    Next b

End Sub
Private Sub opt5BluePaintPicBigBox()
    Dim r As Single, g As Single, b As Single
    'Paint picBigBox
    b = Text1(5) 'Blue
    For r = 255 To 0 Step -1
    For g = 255 To 0 Step -1 'Interesting if there is an error, thus a jump directly to EndSub.
        SetPixelV picBigBox.hDC, r, 255 - g, RGB(r, g, b) 'Ritar medelst API.
    Next g
    g = g - 1 'Because that G becomes too big when the loop has finishes..
    Next r

End Sub

Private Sub MoveHexBox()
    Dim Ctr As Integer
    'MsgBox "Move HexBox"
    For Ctr = 336 To 286 Step -1
    txtHexColor.Move Ctr, 281, 56, 20

    Next Ctr
End Sub

