VERSION 5.00
Object = "{E142732F-A852-11D4-B06C-00500427A693}#1.14#0"; "vbalTbar6.ocx"
Object = "{396F7AC0-A0DD-11D3-93EC-00C0DFE7442A}#1.0#0"; "vbalIml6.ocx"
Object = "{FCE52DAF-EF3E-48E5-A71E-E7A56678C488}#38.0#0"; "SmartBennuCtls.ocx"
Begin VB.Form frmPaletteEditor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Palette Editor"
   ClientHeight    =   5970
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5250
   Icon            =   "frmPaletteEditor.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   398
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   350
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin vbalIml6.vbalImageList iml 
      Left            =   3360
      Top             =   240
      _ExtentX        =   953
      _ExtentY        =   953
      ColourDepth     =   24
      Size            =   5740
      Images          =   "frmPaletteEditor.frx":058A
      Version         =   131072
      KeyCount        =   5
      Keys            =   "����"
   End
   Begin VB.PictureBox picStBar 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      ScaleHeight     =   255
      ScaleWidth      =   5250
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   5715
      Width           =   5250
   End
   Begin SmartBennuCtls.SmartPalettePane pp 
      Height          =   5295
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   5250
      _ExtentX        =   9260
      _ExtentY        =   9340
      BorderStyle     =   2
      CellWidth       =   20
      CellHeight      =   20
      ColSpace        =   2
      RowSpace        =   2
      CanEdit         =   -1  'True
   End
   Begin vbalTBar6.cReBar rebar 
      Left            =   0
      Top             =   0
      _ExtentX        =   2355
      _ExtentY        =   661
   End
   Begin vbalTBar6.cToolbar tbrMain 
      Height          =   375
      Left            =   1440
      Top             =   0
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
   End
End
Attribute VB_Name = "frmPaletteEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private Const MODULE_NAME As String = "SmartFpgEditor.frmPaletteEditor"

Private Const RID_RESTORE As Long = 203
Private Const RID_TITLE  As Long = 192
Private Const RID_EFFECTS As Long = 191
Private Const RID_IMPORT As Long = 190
Private Const RID_EXPORT As Long = 183
Private Const RID_PICKCOLOR As Long = 193
Private Const RID_MODIFIED As Long = 194
Private Const RID_EFF_GRAYSCALE As Long = 195
Private Const RID_EFF_INVERT As Long = 196
Private Const RID_EFF_GRADIENT As Long = 197
Private Const RID_EFF_FILL As Long = 198
Private Const RID_EXPORT_DEFAULT_FILENAME As Integer = 204
Private Const RID_NOPALETTEFILE_MSG As Integer = 155
Private Const RID_STATUSBAR_SINGLE_SELECTION As Integer = 205
Private Const RID_STATUSBAR_MULTIPLE_SELECTION As Integer = 206

Private WithEvents m_DDMenus As cPopupMenu
Attribute m_DDMenus.VB_VarHelpID = -1
Private m_StatusBar As cNoStatusBar
Private WithEvents m_fColorPicker As frmColorPicker
Attribute m_fColorPicker.VB_VarHelpID = -1

Private m_bIsDirty As Boolean
Private m_oOriginalPalette As cPalette
Private m_oContext As IFpgEditorContext

' Context is an abstraction of the "environment" in which
' the edition takes operation and allows sharing of information
' between the different editors.
Public Property Set Context(value As IFpgEditorContext)

    Const PROC_NAME As String = "Context"

    On Error GoTo EH
    
    Set m_oContext = value

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get IsDirty() As Boolean
    Const PROC_NAME As String = "IsDirty"

    On Error GoTo EH
    
    IsDirty = m_bIsDirty

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Private Property Let IsDirty(ByVal value As Boolean)
    
    Const PROC_NAME As String = "IsDirty"
    Dim sTitle As String
    
    On Error GoTo EH
    
    sTitle = FormatResString(RID_TITLE)
    
    If value = True Then
        sTitle = sTitle & " " & FormatResString(RID_MODIFIED)
    End If
    
    Me.Caption = sTitle
    m_bIsDirty = value

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME

End Property


Public Property Get currentPalette() As cPalette

    Const PROC_NAME As String = "CurrentPalette"

    On Error GoTo EH
    
    Set currentPalette = pp.GetPalette()

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Public Property Get OriginalPalette() As cPalette

    Const PROC_NAME As String = "OriginalPalette"

    On Error GoTo EH
    
    Set OriginalPalette = m_oOriginalPalette

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property


Public Sub SetPalette(ByVal oPalette As cPalette)

    Const PROC_NAME As String = "SetPalette"

    On Error GoTo EH
    
    Set m_oOriginalPalette = oPalette
    pp.SetPalette m_oOriginalPalette.GetCopy
    UpdateStatusBar

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Load()

    Const PROC_NAME As String = "Form_Load"
    On Error GoTo EH

    ' Internationalization
    Me.Caption = FormatResString(RID_TITLE)
    
    CreateMenus
    CreateToolBars
    CreateStatusBar

    Exit Sub
    
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateMenus()
    Dim lParent As Long
    
    Const PROC_NAME As String = "CreateMenus"
    On Error GoTo EH

    Set m_DDMenus = New cPopupMenu
    With m_DDMenus
    .hWndOwner = Me.hWnd
    .OfficeXpStyle = True
    
    ' IMPORTANT NOTE: The Key of the parent menus will be that of the toolbar
    ' button to which is related, plus the string "Menu"
    ' This way, no changes are required in the tbrMain_DropDownPress event to
    ' show the Menu
    
    lParent = .AddItem(FormatResString(RID_EFFECTS), , , 0, , , , "EffectsMenu")
    .AddItem FormatResString(RID_EFF_GRAYSCALE), , , lParent, , , , _
        "GreyScale"
    .AddItem FormatResString(RID_EFF_INVERT), , , lParent, , , , "Invert"
    .AddItem FormatResString(RID_EFF_GRADIENT), , , lParent, , , , "Gradient"
    .AddItem FormatResString(RID_EFF_FILL), , , lParent, , , , "Fill"
    '.AddItem FormatResString(RID_EFF_ROLL_LEFT), , , lParent, , , , "RollLeft"
    '.AddItem FormatResString(RID_EFF_ROLL_RIGHT), , , lParent, , , , "RollRight"
    End With
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateToolBars()
    Const PROC_NAME As String = "CreateToolBars"
    On Error GoTo EH

    ' Configure main toolbar
    With tbrMain
    .DrawStyle = TOOLBAR_STYLE
    .ImageSource = CTBExternalImageList
    .SetImageList iml.hIml, CTBImageListNormal
    .CreateToolbar 16, , True, False
    

    .AddButton FormatResString(RID_IMPORT), 0, , , , CTBNormal Or _
        CTBAutoSize, "ImportPalette"
    .AddButton FormatResString(RID_EXPORT), 1, , , , CTBNormal Or _
        CTBAutoSize, "ExportPalette"
    .AddButton , , , , , CTBSeparator
    .AddButton FormatResString(RID_PICKCOLOR), 2, , , , CTBNormal Or _
        CTBAutoSize, "PickColor"
    .AddButton FormatResString(RID_EFFECTS), 3, , , _
        FormatResString(RID_EFFECTS), CTBDropDownArrow Or _
         CTBAutoSize, "Effects"
    .AddButton , , , , , CTBSeparator
    .AddButton FormatResString(RID_RESTORE), 4, , , , CTBNormal Or CTBAutoSize, _
        "Restore"
    End With
    
    With rebar
    .CreateRebar Me.hWnd
    .AddBandByHwnd tbrMain.hWnd, , , , "tbrMain"
    .BandGripper(0) = False
    .RebarSize
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateStatusBar()
    Const PROC_NAME As String = "CreateStatusBar"

    On Error GoTo EH

    Set m_StatusBar = New cNoStatusBar
    
    With m_StatusBar
    .Create picStBar
    .AllowXPStyles = False
    .SizeGrip = True
    .AddPanel estbrStandard, , , , True, , , "First"
    '.AddPanel estbrStandard, , , , , , , "Second"
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub UpdateStatusBar()
    Dim s As String
    Dim c As T_BL_RGBColor
    
    Const PROC_NAME As String = "UpdateStatusBar"

    On Error GoTo EH

    With m_StatusBar
    If pp.PaletteAvailable Then
        If pp.SelectionEnd = pp.SelectionStart Then
            c = currentPalette.Color(pp.SelectionStart)
            s = FormatString(FormatResString(RID_STATUSBAR_SINGLE_SELECTION), _
                    PA2S("|index", pp.SelectionStart, "|r", c.r, "|g", c.g, "|b", c.b))
        Else
            s = FormatString(FormatResString(RID_STATUSBAR_MULTIPLE_SELECTION), _
                    PA2S("|start", pp.SelectionStart, "|end", pp.SelectionEnd))
        End If
        
        .PanelText("First") = s
    Else
'        .PanelText("First") = FormatResString(RID_READY)
    End If
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Const PROC_NAME As String = "Form_Unload"

    On Error GoTo EH
    
    m_oContext.PaletteEditorUnloadCallback Me

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub m_DDMenus_Click(ItemNumber As Long)
    
    Dim sKey As String
    
    Const PROC_NAME As String = "m_DDMenus_Click"
    On Error GoTo EH

    sKey = m_DDMenus.ItemKey(ItemNumber)
        
    Select Case sKey
    Case "GreyScale"
        ToGrayScaleAction
    Case "Invert"
        InvertColorsAction
    Case "Gradient"
        FillGradientAction
    Case "Fill"
        FillSolidAction
    End Select

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub ChangePaletteColor(ByVal lIndex As Long, uColor As T_BL_RGBColor)
    Dim oPalette As cPalette
    Dim uOriginalColor As T_BL_RGBColor
    
    Const PROC_NAME As String = "ChangePaletteColor"

    On Error GoTo EH
    
    Set oPalette = pp.GetPalette()
    
    uOriginalColor = oPalette.Color(lIndex)
    
    If Not (uColor.r = uOriginalColor.r _
        And uColor.g = uOriginalColor.g _
        And uColor.b = uOriginalColor.b) Then
        
        oPalette.Color(lIndex) = uColor
        pp.RePaint
        
        m_oContext.PaletteEditorPaletteChangeCallback currentPalette
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
        
End Sub


Private Sub m_fColorPicker_OnSelectionChanged(ByVal r As Byte, ByVal g As Byte, ByVal b As Byte)
    
    Const PROC_NAME As String = "m_fColorPicker_OnSelectionChanged"

    On Error GoTo EH
    
    Dim i As Integer
    Dim uNewColor As T_BL_RGBColor
    Dim auColors() As T_BL_RGBColor
    Dim lIndex0 As Long, lIndex1 As Long
    
    uNewColor.r = r
    uNewColor.g = g
    uNewColor.b = b
    
    GetSelectedIndexesSorted lIndex0, lIndex1
    ReDim auColors(lIndex1 - lIndex0) As T_BL_RGBColor
    
    For i = 0 To lIndex1 - lIndex0
        auColors(i) = uNewColor
    Next
    
    SetColorArray auColors, lIndex0

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Function GetColorArray(ByVal lStart As Long, lEnd As Long) As T_BL_RGBColor()
    Dim auColors() As T_BL_RGBColor
    Dim i As Integer
    
    Const PROC_NAME As String = "GetColorArray"

    On Error GoTo EH
    
    ReDim auColors(lEnd - lStart) As T_BL_RGBColor
    
    For i = 0 To lEnd - lStart
        auColors(i) = currentPalette.Color(i + lStart)
    Next
    
    GetColorArray = auColors

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

Private Sub SetColorArray(auColors() As T_BL_RGBColor, lStart As Long)
    Dim i As Integer
    
    Const PROC_NAME As String = "SetColorArray"

    On Error GoTo EH
    
    For i = 0 To UBound(auColors)
        currentPalette.Color(i + lStart) = auColors(i)
    Next
    
    pp.RePaint
    m_oContext.PaletteEditorPaletteChangeCallback currentPalette '
    UpdateStatusBar

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub GetSelectedIndexesSorted(lIndex0 As Long, lIndex1 As Long)
    Const PROC_NAME As String = "GetSelectionIndexesSorted"

    On Error GoTo EH
    
    If pp.SelectionStart > pp.SelectionEnd Then
        lIndex0 = pp.SelectionEnd
        lIndex1 = pp.SelectionStart
    Else
        lIndex0 = pp.SelectionStart
        lIndex1 = pp.SelectionEnd
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub PickColorAction()
    Const PROC_NAME As String = "PickColorAction"

    On Error GoTo EH
    
    Dim uStartColor As T_BL_RGBColor
    Dim auOriginalColors() As T_BL_RGBColor
    Dim lIndex0 As Long, lIndex1 As Long
    
    ' Store original colors to be able to restore if cancel
    GetSelectedIndexesSorted lIndex0, lIndex1
    auOriginalColors = GetColorArray(lIndex0, lIndex1)
    
    Set m_fColorPicker = New frmColorPicker
    uStartColor = currentPalette.Color(pp.SelectionStart)
    m_fColorPicker.SetRgb uStartColor.r, uStartColor.g, uStartColor.b
    m_fColorPicker.Show vbModal, Me
    
    ' Restore the original palette color when cancel
    If m_fColorPicker.DialogResult = vbCancel Then
        SetColorArray auOriginalColors, lIndex0
    Else
        IsDirty = True
    End If
    
    Unload m_fColorPicker
    Set m_fColorPicker = Nothing

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub InvertColorsAction()
    Const PROC_NAME As String = "InvertColorsAction"

    On Error GoTo EH
    
    Dim auColors() As T_BL_RGBColor
    Dim lIndex0 As Long, lIndex1 As Long
    
    GetSelectedIndexesSorted lIndex0, lIndex1
    auColors = GetColorArray(lIndex0, lIndex1)
    
    ColorArrayInvert auColors
    
    SetColorArray auColors, lIndex0

    IsDirty = True
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub ToGrayScaleAction()
    Const PROC_NAME As String = "ToGrayScaleAction"

    On Error GoTo EH
    
    Dim auColors() As T_BL_RGBColor
    Dim lIndex0 As Long, lIndex1 As Long
    
    GetSelectedIndexesSorted lIndex0, lIndex1
    auColors = GetColorArray(lIndex0, lIndex1)
    
    ColorArrayGrayScale auColors
    
    SetColorArray auColors, lIndex0

    IsDirty = True
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub FillSolidAction()
    Const PROC_NAME As String = "FillSolidAction"

    On Error GoTo EH
    
    Dim auColors() As T_BL_RGBColor
    Dim lIndex0 As Long, lIndex1 As Long
    
    GetSelectedIndexesSorted lIndex0, lIndex1
    auColors = GetColorArray(lIndex0, lIndex1)
    
    ColorArrayFillSolid auColors, currentPalette.Color(pp.SelectionStart)
    
    SetColorArray auColors, lIndex0
    
    IsDirty = True

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub FillGradientAction()
    Const PROC_NAME As String = "FillSolidAction"

    On Error GoTo EH
    
    Dim auColors() As T_BL_RGBColor
    Dim lIndex0 As Long, lIndex1 As Long
    
    GetSelectedIndexesSorted lIndex0, lIndex1
    auColors = GetColorArray(lIndex0, lIndex1)
    
    ColorArrayFillGradient auColors, auColors(0), auColors(UBound(auColors))
    
    SetColorArray auColors, lIndex0
    
    IsDirty = True

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' TODO this code is almost the same as the one in frmNewFpg
Private Sub ImportColorsAction()
    Dim sDir As String
    Dim asFileTitles() As String
    Dim oDecoder As IPaletteDecoder
    
    Const PROC_NAME As String = "ImportColorsAction"

    On Error GoTo EH

    asFileTitles = ShowOpenDialog(sDir, False, G_sOpenPaletteDlgFilter, , _
        "Select Palette", Me)
    
    If sDir <> "" Then
        Set oDecoder = GetDecoder(BuildPath(sDir, asFileTitles(0)), _
            eblDecoderTypePalette)
        
        If Not oDecoder Is Nothing Then
            pp.LoadPalette BuildPath(sDir, asFileTitles(0)), oDecoder
            
            pp.RePaint
            m_oContext.PaletteEditorPaletteChangeCallback currentPalette
            IsDirty = True
            
        Else
            MsgBox RID_NOPALETTEFILE_MSG, vbExclamation, PA2S("%FILE%", _
                asFileTitles(0))
        End If
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Note: The oEncoder parameter is because it was copied from mnuActions
' TODO: Remove code duplication as it is very similar to mnuActions
Private Sub ExportColorsAction(Optional oEncoder As IPaletteEncoder)
    ' Keeps track of the number of times the palette is exported to generate
    ' different palette names
    Static lVersionCount As Long

    Dim oStdEncoder As IStandardEncoder
    Dim sFileName As String, sDefaultFileName As String
    Dim oPal As cPalette
    
    Const PROC_NAME As String = "ExportPaletteToType"
    
    On Error GoTo EH
    
    Set oPal = currentPalette
    
    If oEncoder Is Nothing Then Set oEncoder = New cDefaultPaletteEncoder
    
    Set oStdEncoder = oEncoder
    
    sDefaultFileName = m_oContext.FpgTitle & "_" _
        & FormatResString(RID_EXPORT_DEFAULT_FILENAME) & "_" _
        & CStr(lVersionCount)
    sFileName = sDefaultFileName
    
    If ShowSaveDialog(sFileName, _
            BuildDialogFilter(oStdEncoder.DefaultFileExtension, "ALL"), _
            oStdEncoder.DefaultFileExtension, "Extract Palette", Me) Then
        
        bennulib.SavePalette oPal, sFileName, oEncoder
        If sFileName <> sDefaultFileName Then
            lVersionCount = lVersionCount + 1
        End If
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub RestoreAction()
    Const PROC_NAME As String = "RestoreAction"

    On Error GoTo EH
    
    pp.SetPalette m_oOriginalPalette.GetCopy
    UpdateStatusBar
    pp.RePaint
    IsDirty = False
    
    m_oContext.PaletteEditorPaletteChangeCallback currentPalette
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub
Private Sub pp_ColorDblClick(ByVal lColorIndex As Long)
    
    Const PROC_NAME As String = "pp_ColorDblClick"
    On Error GoTo EH

    PickColorAction
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub pp_MouseMove(ByVal lColorIndex As Long)

    Const PROC_NAME As String = "pp_MouseMove"

    On Error GoTo EH
    
    UpdateStatusBar

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub pp_SelectionChanged()
    Const PROC_NAME As String = "pp_SelectionChanged"

    On Error GoTo EH
    
    UpdateStatusBar

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub tbrMain_ButtonClick(ByVal lButton As Long)

    Const PROC_NAME As String = "tbrMain_ButtonClick"
    On Error GoTo EH

    Select Case tbrMain.ButtonKey(lButton)
    Case "ImportPalette"
        ImportColorsAction
    Case "ExportPalette"
        ExportColorsAction
    Case "PickColor"
        PickColorAction
    Case "Restore"
        RestoreAction
    End Select
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub tbrMain_DropDownPress(ByVal lButton As Long)
    Dim lX As Long, lY As Long
    Dim lMnuIndex As Long
    Dim sKey As String
    
    Const PROC_NAME As String = "tbrMain_DropDownPress"
    On Error GoTo EH

    sKey = tbrMain.ButtonKey(lButton)
    tbrMain.GetDropDownPosition lButton, lX, lY
    lMnuIndex = m_DDMenus.IndexForKey(sKey & "Menu") + 1
    
    m_DDMenus.ShowPopupMenuAtIndex lX, lY, lIndex:=lMnuIndex

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub
