VERSION 5.00
Object = "{FCE52DAF-EF3E-48E5-A71E-E7A56678C488}#38.0#0"; "SmartBennuCtls.ocx"
Begin VB.Form frmNewFpg 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "New FPG"
   ClientHeight    =   4215
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4215
   Icon            =   "frmNewFpg.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   281
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   281
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin SmartBennuCtls.SmartPalettePane pp 
      Height          =   1935
      Left            =   180
      TabIndex        =   9
      Top             =   1680
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   3413
      BorderStyle     =   2
      CellWidth       =   15
      CellHeight      =   7
   End
   Begin VB.CommandButton btCancel 
      Cancel          =   -1  'True
      Height          =   375
      Left            =   3000
      TabIndex        =   6
      Top             =   3720
      Width           =   1095
   End
   Begin VB.CommandButton btOk 
      Default         =   -1  'True
      Height          =   375
      Left            =   1800
      TabIndex        =   8
      Top             =   3720
      Width           =   1095
   End
   Begin VB.CommandButton btOpen 
      DisabledPicture =   "frmNewFpg.frx":038A
      Enabled         =   0   'False
      Height          =   345
      Left            =   840
      Picture         =   "frmNewFpg.frx":0623
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   1245
      Width           =   375
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   240
      ScaleHeight     =   255
      ScaleWidth      =   3735
      TabIndex        =   1
      Top             =   960
      Width           =   3735
      Begin VB.OptionButton op32 
         Caption         =   "32 bpp"
         Height          =   255
         Left            =   2760
         TabIndex        =   4
         Top             =   0
         Width           =   855
      End
      Begin VB.OptionButton op16 
         Caption         =   "16 bpp"
         Height          =   255
         Left            =   1500
         TabIndex        =   3
         Top             =   0
         Value           =   -1  'True
         Width           =   855
      End
      Begin VB.OptionButton op8 
         Caption         =   "8 bpp"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   4215
   End
   Begin VB.Image Image1 
      Height          =   600
      Left            =   0
      Picture         =   "frmNewFpg.frx":0A4A
      Stretch         =   -1  'True
      Top             =   0
      Width           =   4335
   End
   Begin VB.Label lblPalette 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   645
   End
   Begin VB.Label lblDepth 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   2445
   End
End
Attribute VB_Name = "frmNewFpg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================

Private Const MODULE_NAME As String = "SmartFpgEditor.frmNewFpg"

Private Const RID_NEWFPG As Integer = 110
Private Const RID_DEPTH As Integer = 153
Private Const RID_PALETTE As Integer = 154
Private Const RID_OK As Integer = 106
Private Const RID_CANCEL As Integer = 107
Private Const RID_NOPALETTEFILE_MSG As Integer = 155
Private Const RID_PALETTEREQUIRED_MSG As Integer = 156

'===============================================================================

Private m_eDialogResult As VbMsgBoxResult

'===============================================================================

Private Sub ShowOpenPaletteDialog()
    Dim sDir As String
    Dim asFileTitles() As String
    Dim oDecoder As IPaletteDecoder
    
    Const PROC_NAME As String = "ShowOpenPaletteDialog"

    On Error GoTo EH

    asFileTitles = ShowOpenDialog(sDir, False, G_sOpenPaletteDlgFilter, , _
        "Select Palette", Me)
    
    If sDir <> "" Then
        Set oDecoder = GetDecoder(BuildPath(sDir, asFileTitles(0)), _
            eblDecoderTypePalette)
        
        If Not oDecoder Is Nothing Then
            pp.LoadPalette BuildPath(sDir, asFileTitles(0)), oDecoder
        Else
            MsgBox RID_NOPALETTEFILE_MSG, vbExclamation, PA2S("%FILE%", _
                asFileTitles(0))
        End If
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Property Get DialogResult() As VbMsgBoxResult
    Const PROC_NAME As String = "DialogResult"

    On Error GoTo EH

    DialogResult = m_eDialogResult

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Private Sub btCancel_Click()
    Const PROC_NAME As String = "btCancel_Click"

    On Error GoTo EH

    Me.Hide

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub btOk_Click()
    Const PROC_NAME As String = "btOk_Click"

    On Error GoTo EH

    If op8.value And Not pp.PaletteAvailable Then
        MsgBox RID_PALETTEREQUIRED_MSG, vbExclamation
        Exit Sub
    End If
    m_eDialogResult = vbOK
    Me.Hide

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub btOpen_Click()
    Const PROC_NAME As String = "btOpen_Click"

    On Error GoTo EH

    ShowOpenPaletteDialog

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Load()
    Const PROC_NAME As String = "Form_Load"

    On Error GoTo EH
    
    ' Internationalization
    Me.Caption = FormatResString(RID_NEWFPG)
    lblTitle.Caption = FormatResString(RID_NEWFPG)
    lblDepth.Caption = FormatResString(RID_DEPTH) & ":"
    lblPalette.Caption = FormatResString(RID_PALETTE) & ":"
    btOpen.Move lblPalette.Left + 12 + lblPalette.Width
    btOk.Caption = FormatResString(RID_OK)
    btCancel.Caption = FormatResString(RID_CANCEL)
    
    m_eDialogResult = vbCancel ' By default, cancel action

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub op16_Click()
    Const PROC_NAME As String = "op16_Click"

    On Error GoTo EH

    btOpen.Enabled = op8.value
    pp.Enabled = op8.value

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub op32_Click()
    Const PROC_NAME As String = "op32_Click"

    On Error GoTo EH

    btOpen.Enabled = op8.value
    pp.Enabled = op8.value

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub op8_Click()
    Const PROC_NAME As String = "op8_Click"

    On Error GoTo EH

    btOpen.Enabled = op8.value
    pp.Enabled = op8.value
    
    ' If there is no loaded palette, show the open dialog
    If pp.PaletteAvailable = False Then ShowOpenPaletteDialog

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub
