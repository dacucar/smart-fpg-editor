VERSION 5.00
Begin VB.Form frmError 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Critical Error"
   ClientHeight    =   2250
   ClientLeft      =   4260
   ClientTop       =   2745
   ClientWidth     =   6705
   Icon            =   "frmError.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   150
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   447
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtMsg 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   1200
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   840
      Width           =   5415
   End
   Begin VB.CheckBox chkView 
      Height          =   375
      Left            =   1200
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1800
      Width           =   975
   End
   Begin VB.CommandButton cmdOk 
      Height          =   375
      Left            =   5640
      TabIndex        =   2
      Top             =   1800
      Width           =   975
   End
   Begin VB.CommandButton cmdCopy 
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   1800
      Width           =   975
   End
   Begin VB.TextBox txtReport 
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Text            =   "frmError.frx":000C
      Top             =   2280
      Width           =   6495
   End
   Begin VB.Label lblWow 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   6735
   End
   Begin VB.Image imgCritical 
      Height          =   855
      Left            =   120
      Picture         =   "frmError.frx":0049
      Top             =   840
      Width           =   855
   End
   Begin VB.Image imgTitle 
      Height          =   600
      Left            =   0
      Picture         =   "frmError.frx":0B55
      Stretch         =   -1  'True
      Top             =   0
      Width           =   6765
   End
End
Attribute VB_Name = "frmError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' FORM OVERVIEW
'===============================================================================
' A form to display a report of unhandled errors. Used by the cError class.
'===============================================================================

' String resource ids used in this form
Private Const RID_WOW As Integer = 101
Private Const RID_ERROR_MESSAGE As Integer = 102
Private Const RID_TITLE As Integer = 103
Private Const RID_VIEW As Integer = 104
Private Const RID_COPY As Integer = 105
Private Const RID_OK As Integer = 106

Private Const CONTACT_EMAIL As String = "smartfpgeditor@dacucar.com"
Private Const EXPANDED_HEIGHT As Integer = 5745
Private Const COLLAPSED_HEIGHT As Integer = 2625

'===============================================================================

Private Sub cmdCopy_Click()
    On Error Resume Next
    
    Clipboard.SetText txtReport.Text
    
    On Error GoTo 0
End Sub

Private Sub cmdOk_Click()
    On Error Resume Next
    
    Unload Me
    
    On Error GoTo 0
End Sub

Private Sub Form_Load()
    On Error Resume Next
    
    Me.Caption = FormatResString(RID_TITLE)
    lblWow = FormatResString(RID_WOW)
    cmdOk.Caption = FormatResString(RID_OK)
    cmdCopy.Caption = FormatResString(RID_COPY)
    chkView.Caption = FormatResString(RID_VIEW)
    txtMsg.Text = FormatResString(RID_ERROR_MESSAGE, _
        PA2S("%APP_NAME%", App.Title, "%CONTACT_EMAIL%", CONTACT_EMAIL))
        
    On Error GoTo 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End ' Abort application
End Sub

Private Sub chkView_Click()
    On Error Resume Next
    
    Me.Height = Switch(chkView.value = 1, EXPANDED_HEIGHT, _
            chkView.value = 0, COLLAPSED_HEIGHT)
            
    On Error GoTo 0
End Sub
