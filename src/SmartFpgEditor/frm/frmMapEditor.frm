VERSION 5.00
Object = "{E142732F-A852-11D4-B06C-00500427A693}#1.14#0"; "vbalTbar6.ocx"
Object = "{396F7AC0-A0DD-11D3-93EC-00C0DFE7442A}#1.0#0"; "vbalIml6.ocx"
Object = "{FCE52DAF-EF3E-48E5-A71E-E7A56678C488}#38.0#0"; "SmartBennuCtls.ocx"
Begin VB.Form frmMapEditor 
   ClientHeight    =   4860
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7035
   Icon            =   "frmMapEditor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4860
   ScaleWidth      =   7035
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cmbZoom 
      Height          =   315
      ItemData        =   "frmMapEditor.frx":2B8A
      Left            =   4680
      List            =   "frmMapEditor.frx":2BC7
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   2520
      Width           =   855
   End
   Begin VB.TextBox txtCp 
      Alignment       =   2  'Center
      Height          =   315
      Left            =   4200
      Locked          =   -1  'True
      TabIndex        =   2
      Text            =   "0"
      Top             =   3960
      Width           =   495
   End
   Begin VB.PictureBox picStBar 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      ScaleHeight     =   255
      ScaleWidth      =   7035
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   4605
      Width           =   7035
   End
   Begin VB.PictureBox picDescription 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   120
      ScaleHeight     =   375
      ScaleWidth      =   3975
      TabIndex        =   3
      Top             =   4080
      Width           =   3975
      Begin VB.TextBox txtDescription 
         Height          =   315
         Left            =   960
         TabIndex        =   5
         Top             =   0
         Width           =   3015
      End
      Begin VB.Label lblDescription 
         AutoSize        =   -1  'True
         Caption         =   "Description:"
         Height          =   195
         Left            =   0
         TabIndex        =   4
         Top             =   60
         Width           =   840
      End
   End
   Begin vbalTBar6.cReBar rebar 
      Left            =   1080
      Top             =   0
      _ExtentX        =   2355
      _ExtentY        =   661
   End
   Begin vbalTBar6.cToolbar tbrMain 
      Height          =   375
      Left            =   0
      Top             =   0
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
   End
   Begin SmartBennuCtls.SmartMapPane mp 
      Height          =   3375
      Left            =   0
      TabIndex        =   0
      Top             =   480
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   5953
      BorderStyle     =   2
      Stretch         =   0   'False
      Zoom            =   200
   End
   Begin vbalTBar6.cToolbar tbrEdit 
      Height          =   375
      Left            =   2520
      Top             =   0
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
   End
   Begin vbalIml6.vbalImageList iml 
      Left            =   3720
      Top             =   480
      _ExtentX        =   953
      _ExtentY        =   953
      ColourDepth     =   24
      Size            =   14924
      Images          =   "frmMapEditor.frx":2C0F
      Version         =   131072
      KeyCount        =   13
      Keys            =   "������������"
   End
End
Attribute VB_Name = "frmMapEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================

Private Const MODULE_NAME As String = "SmartFpgEditor.frmMapEditor"

Private Const RID_RESTOREZOOM As Integer = 157
Private Const RID_ZOOMIN As Integer = 158
Private Const RID_ZOOMOUT As Integer = 159
Private Const RID_TOOGLETRANSPARENCY As Integer = 160
Private Const RID_PREVIOUSCP As Integer = 161
Private Const RID_NEXTCP As Integer = 162
Private Const RID_REMOVECP As Integer = 163
Private Const RID_TOOGLECPEDITION As Integer = 164
Private Const RID_RESTOREDESCRIPTION As Integer = 165
Private Const RID_COPY As Integer = 105
Private Const RID_DESCRIPTION As Integer = 143
Private Const RID_READY As Integer = 129
Private Const RID_CENTER As Integer = 166
Private Const RID_TITLE As Integer = 169
Private Const RID_PREVIOUSFREECPID As Integer = 178
Private Const RID_NEXTFREECPID As Integer = 179
Private Const RID_TOOGLEVIEWCPS As Integer = 180
Private Const RID_MODIFIED As Integer = 194

Private m_StatusBar As cNoStatusBar
Attribute m_StatusBar.VB_VarHelpID = -1
Private m_oContext As IFpgEditorContext
Private m_bIsDirty As Boolean

' Context is an abstraction of the "environment" in which
' the edition takes operation and allows sharing of information
' between the different editors.
Public Property Set Context(value As IFpgEditorContext)

    Const PROC_NAME As String = "Context"

    On Error GoTo EH
    
    Set m_oContext = value

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get IsDirty() As Boolean
    Const PROC_NAME As String = "IsDirty"

    On Error GoTo EH
    
    IsDirty = m_bIsDirty

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Private Property Let IsDirty(ByVal value As Boolean)
    
    Const PROC_NAME As String = "IsDirty"
    Dim sTitle As String
    
    On Error GoTo EH
    
    sTitle = FormatResString(RID_TITLE)
    
    If value = True Then
        sTitle = sTitle & " " & FormatResString(RID_MODIFIED)
    End If
    
    Me.Caption = sTitle
    m_bIsDirty = value

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME

End Property

Private Sub NextCp()
    Dim i As Integer
    
    Const PROC_NAME As String = "NextCp"

    On Error GoTo EH

    i = CInt(txtCp.Text)
    If i < MAX_CP_ID Then i = i + 1
    txtCp.Text = CStr(i)

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub PreviousCp()
    Dim i As Integer
    
    Const PROC_NAME As String = "PreviousCp"

    On Error GoTo EH

    i = CInt(txtCp.Text)
    If i > 0 Then i = i - 1
    txtCp.Text = CStr(i)

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub PreviousFreeCp()
    Const PROC_NAME As String = "PreviousFreeCp"

    Dim i As Integer
    Dim lID As Long
    
    On Error GoTo EH
    
    i = CInt(txtCp.Text)

    lID = mp.GetMap.SearchNonDefControlPointIDrev(i)
    If lID > -1 Then
        txtCp.Text = CStr(lID)
    Else
        txtCp.SetFocus
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub NextFreeCp()
    Const PROC_NAME As String = "NextFreeCp"

    Dim i As Integer
    Dim lID As Long
    
    On Error GoTo EH
    
    i = CInt(txtCp.Text)

    lID = mp.GetMap.SearchNonDefControlPointID(i)
    If lID > -1 Then
        txtCp.Text = CStr(lID)
    Else
        txtCp.SetFocus
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub


Private Sub RemoveCp()

    Const PROC_NAME As String = "RemoveCp"

    On Error GoTo EH
    
    mp.GetMap.RemoveControlPoint CLng(txtCp.Text)
    mp.RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Property Let EditCpMode(ByVal newVal As Boolean)
    Const PROC_NAME As String = "EditCpMode"

    On Error GoTo EH

    txtCp.Locked = Not newVal
    tbrMain.ButtonEnabled("RemoveCp") = newVal
    tbrMain.ButtonEnabled("NextCp") = newVal
    tbrMain.ButtonEnabled("PreviousCp") = newVal
    tbrMain.ButtonEnabled("PreviousFreeCp") = newVal
    tbrMain.ButtonEnabled("NextFreeCp") = newVal
    
    mp.ShowActiveControlPointIndicator = newVal
    mp.ShowControlPointSelector = newVal

    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Private Property Get EditCpMode() As Boolean
    
    Const PROC_NAME As String = "EditCpMode"

    On Error GoTo EH

    EditCpMode = tbrMain.ButtonChecked("ToogleCpEdit")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Private Property Let ViewCpsMode(ByVal newVal As Boolean)
    Const PROC_NAME As String = "ViewCpMode"
    
    On Error GoTo EH
    
    mp.ShowControlPoints = newVal
    
    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

Private Property Get ViewCpsMode() As Boolean
    
    Const PROC_NAME As String = "ViewCpMode"

    On Error GoTo EH

    ViewCpsMode = tbrMain.ButtonChecked("ToogleViewCps")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Private Sub CreateToolBars()
    Dim i As Integer
    
    Const PROC_NAME As String = "CreateToolBars"

    On Error GoTo EH

    ' Configure Main Toolbar
    With tbrMain
    .DrawStyle = TOOLBAR_STYLE
    .ImageSource = CTBExternalImageList
    .SetImageList iml.hIml, CTBImageListNormal
    .CreateToolbar 16, , True, False
    
    .AddButton FormatResString(RID_RESTOREZOOM), 1, , , , CTBNormal Or _
        CTBAutoSize, "ZoomRestore"
    .AddControl cmbZoom.hWnd
    .AddButton FormatResString(RID_ZOOMIN), 0, , , , CTBNormal Or CTBAutoSize, _
        "ZoomIn"
    .AddButton FormatResString(RID_ZOOMOUT), 2, , , , CTBNormal Or CTBAutoSize, _
        "ZoomOut"
    
    .AddButton , , , , , CTBSeparator
    .AddButton FormatResString(RID_COPY), 3, , , , CTBNormal Or CTBAutoSize, _
        "Copy"
    .ButtonEnabled("Copy") = False
    .AddButton , , , , , CTBSeparator
    .AddButton FormatResString(RID_TOOGLETRANSPARENCY), 4, , , , CTBCheck Or _
        CTBAutoSize, "ToogleTransparency"
    .ButtonChecked("ToogleTransparency") = mp.ShowTransparency
    .AddButton , , , , , CTBSeparator
    .AddButton FormatResString(RID_PREVIOUSFREECPID), 10, , , , CTBNormal + CTBAutoSize, _
        "PreviousFreeCp"
    .AddButton FormatResString(RID_PREVIOUSCP), 8, , , , CTBNormal + _
        CTBAutoSize, "PreviousCp"
    .AddControl txtCp.hWnd
    .AddButton FormatResString(RID_NEXTCP), 9, , , , CTBNormal + CTBAutoSize, _
        "NextCp"
    .AddButton FormatResString(RID_NEXTFREECPID), 11, , , , CTBNormal + CTBAutoSize, _
        "NextFreeCp"
    .AddButton FormatResString(RID_REMOVECP), 6, , , , CTBNormal Or CTBAutoSize, _
        "RemoveCp"
    .AddButton FormatResString(RID_TOOGLECPEDITION), 5, , , , CTBCheck Or _
        CTBAutoSize, "ToogleCpEdit"
    .AddButton FormatResString(RID_TOOGLEVIEWCPS), 12, , , , CTBCheck Or _
        CTBAutoSize, "ToogleViewCps"
    .ButtonChecked("ToogleCpEdit") = False
    .ButtonChecked("ToogleViewCps") = False
    End With
    
    With tbrEdit
    .DrawStyle = TOOLBAR_STYLE
    .ImageSource = CTBExternalImageList
    .SetImageList iml.hIml, CTBImageListNormal
    .CreateToolbar 16, , True, False
    .AddControl picDescription.hWnd
    .AddButton FormatResString(RID_RESTOREDESCRIPTION), 7, , , , CTBNormal + _
        CTBAutoSize, "RestoreDescription"
    End With

    
    ' Configure the ReBar
    With rebar
    .CreateRebar Me.hWnd
    .AddBandByHwnd tbrEdit.hWnd, , , , "tbrEdit"
    .AddBandByHwnd tbrMain.hWnd, , , , "tbrMain"
    .BandGripper(0) = False
    .BandGripper(1) = False
    .RebarSize
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub CreateStatusBar()
    Const PROC_NAME As String = "CreateStatusBar"

    On Error GoTo EH

    Set m_StatusBar = New cNoStatusBar
    
    With m_StatusBar
    .Create picStBar
    .AllowXPStyles = False
    .SizeGrip = True
    .AddPanel estbrStandard, , , , True, , , "Name"
    .AddPanel estbrStandard, , , 120, , , , "Center"
    .AddPanel estbrStandard, , , , , , , "Depth"
    .AddPanel estbrStandard, , , 90, , , , "Coords"
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub UpdateStatusBar()
    Dim oMap As cMap
    Dim s As String
    
    Const PROC_NAME As String = "UpdateStatusBar"

    On Error GoTo EH

    With m_StatusBar
    If mp.MapAvailable Then
        Set oMap = mp.GetMap
        s = oMap.Description & " (" & CStr(oMap.Width) _
            & "x" & CStr(oMap.Height) & ")"
        .PanelText("Name") = s
        .PanelText("Center") = " " & FormatResString(RID_CENTER) _
            & ": (" & CStr(oMap.GetCenterX) _
            & ", " & CStr(oMap.GetCenterY) & ")"
        .PanelText("Depth") = oMap.Depth & " bpp"
    Else
        .PanelText("Name") = FormatResString(RID_READY)
        .PanelText("Center") = ""
        .PanelText("Depth") = ""
    End If
    End With

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Purpose: Selects the text of a text box, and set the focus to it
Private Sub SelectTextBox(oTB As TextBox)
    Const PROC_NAME As String = "SelectTextBox"

    On Error GoTo EH

    If oTB Is Nothing Then Exit Sub
    
    oTB.SetFocus
    oTB.SelStart = 0
    oTB.SelLength = Len(oTB.Text)

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub cmbZoom_Click()
    Const PROC_NAME As String = "cmbZoom_Click"

    On Error GoTo EH

    mp.Zoom = cmbZoom.ItemData(cmbZoom.ListIndex)

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Load()
    Const PROC_NAME As String = "Form_Load"

    On Error GoTo EH

    ' Internationalization
    Me.Caption = FormatResString(RID_TITLE)
    lblDescription.Caption = FormatResString(RID_DESCRIPTION) & ":   "
    txtDescription.Move lblDescription.Left + lblDescription.Width
    picDescription.Width = lblDescription.Left + lblDescription.Width + _
        txtDescription.Width
        
    txtDescription.MaxLength = modGlobals.MAX_GRAPHIC_DESC_LENGTH
    
    CreateToolBars
    CreateStatusBar
    UpdateStatusBar
    
    EditCpMode = False
    cmbZoom.Text = "100%"

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Const PROC_NAME As String = "Form_QueryUnload"

    On Error GoTo EH

    mp.GetMap.Description = txtDescription

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Resize()
    Dim sgTop As Single
    Dim sgHeight As Single
    Dim sgWidth As Single
    
    Const PROC_NAME As String = "Form_Resize"

    On Error GoTo EH

    If Me.WindowState = vbMinimized Then Exit Sub
    
    rebar.RebarSize
    
    ' Resize the MAP viewer to cover everything
    sgTop = ScaleY(rebar.RebarHeight, vbPixels, vbTwips)
    sgWidth = Me.ScaleWidth
    sgWidth = IIf(sgWidth < 0, 0, sgWidth)
    sgHeight = Me.ScaleHeight - sgTop - picStBar.Height
    sgHeight = IIf(sgHeight < 0, 0, sgHeight)
    mp.Move 0, sgTop, sgWidth, sgHeight

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Const PROC_NAME As String = "Form_Unload"

    On Error GoTo EH
    
    m_oContext.MapEditorUnloadCallback Me

    ' Clean up several GUI objects, for safety
    rebar.RemoveAllRebarBands
    Set m_StatusBar = Nothing

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub mp_MapBecameAvailable()
    Const PROC_NAME As String = "mp_MapBecameAvailable"

    On Error GoTo EH

    txtDescription = mp.GetMap.Description
    UpdateStatusBar

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub mp_MouseMove(ByVal lButton As Long, ByVal lX As Long, ByVal lY As _
        Long, ByVal lRelX As Long, ByVal lRelY As Long)
        
    Dim s As String
    Const PROC_NAME As String = "mp_MouseMove"

    On Error GoTo EH

    s = "(x,y): " & lRelX & ", " & lRelY
    m_StatusBar.PanelText("Coords") = s

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub mp_MouseDown(ByVal lButton As Long, ByVal lX As Long, ByVal lY As _
        Long, ByVal lRelX As Long, ByVal lRelY As Long)

    Const PROC_NAME As String = "mp_MouseDown"

    On Error GoTo EH
    
    If EditCpMode Then
        mp.GetMap.SetControlPoint CLng(txtCp.Text), lRelX, lRelY
        mp.ActiveControlPointId = CLng(txtCp.Text)
        If CLng(txtCp.Text) = 0 Then UpdateStatusBar
        IsDirty = True
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub mp_MouseWheel(eScrollDirection As SmartBennuCtls.E_BC_ScrollDirection, _
    lAmount As Long, ByVal iShift As Integer)
    
    Const PROC_NAME As String = "mp_MouseWheel"

    On Error GoTo EH
    
    Dim i As Integer
    Dim c As Integer
    
    i = cmbZoom.ListIndex
    c = cmbZoom.ListCount
    
    If iShift And vbCtrlMask Then
        If lAmount < 0 Then
            cmbZoom.ListIndex = IIf(i < c - 1, i + 1, i)
        Else
            cmbZoom.ListIndex = IIf(i > 0, i - 1, i)
        End If
        lAmount = 0
    ElseIf iShift And vbShiftMask Then
        eScrollDirection = ebcScrollHorizontally
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub picStBar_Paint()
    Const PROC_NAME As String = "picStBar_Paint"

    On Error GoTo EH

    m_StatusBar.Draw

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub tbrEdit_ButtonClick(ByVal lButton As Long)
    Const PROC_NAME As String = "tbrEdit_ButtonClick"

    On Error GoTo EH
    
    Select Case tbrEdit.ButtonKey(lButton)
    Case "RestoreDescription"
        txtDescription.Text = mp.GetMap.Description
        SelectTextBox txtDescription
    End Select
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub tbrMain_ButtonClick(ByVal lButton As Long)
    Const PROC_NAME As String = "tbrMain_ButtonClick"

    Dim b As Boolean
    
    On Error GoTo EH

    Select Case tbrMain.ButtonKey(lButton)
    Case "ToogleTransparency"
        mp.ShowTransparency = tbrMain.ButtonChecked(lButton)
    Case "NextCp"
        NextCp
    Case "NextFreeCp"
        NextFreeCp
    Case "PreviousCp"
        PreviousCp
    Case "PreviousFreeCp"
        PreviousFreeCp
    Case "RemoveCp"
        RemoveCp
        IsDirty = True
    Case "ZoomIn"
        If cmbZoom.ListIndex < cmbZoom.ListCount - 1 Then
            cmbZoom.ListIndex = cmbZoom.ListIndex + 1
        End If
    Case "ZoomOut"
        If cmbZoom.ListIndex > 0 Then
            cmbZoom.ListIndex = cmbZoom.ListIndex - 1
        End If
    Case "ZoomRestore"
        cmbZoom.ListIndex = 2
    Case "ToogleCpEdit"
        b = tbrMain.ButtonChecked(lButton)
        If b = True Then
            tbrMain.ButtonChecked("ToogleViewCps") = True
            ViewCpsMode = True
        End If
        EditCpMode = tbrMain.ButtonChecked(lButton)
        
    Case "ToogleViewCps"
        ViewCpsMode = tbrMain.ButtonChecked(lButton)
    End Select

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtCp_Change()
    Const PROC_NAME As String = "txtCp_Change"

    On Error GoTo EH
    
    If IsNumeric(txtCp.Text) Then
        mp.ActiveControlPointId = CLng(txtCp.Text)
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub txtCp_GotFocus()
    Const PROC_NAME As String = "txtCp_GotFocus"

    On Error GoTo EH

    SelectTextBox txtCp

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtCp_KeyPress(KeyAscii As Integer)
    Const PROC_NAME As String = "txtCp_KeyPress"

    On Error GoTo EH

    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    ElseIf KeyAscii <> 8 Then
        If Not IsNumeric(Chr(KeyAscii)) Then
            Beep
            KeyAscii = 0
        End If
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtCp_Validate(Cancel As Boolean)
    Dim l As Long
    
    Const PROC_NAME As String = "txtCp_Validate"

    On Error GoTo EH

    If Not IsNumeric(txtCp) Then
        Cancel = True
    Else
        l = CLng(txtCp.Text)
        If l < MIN_CP_ID Or l > MAX_CP_ID Then
            Cancel = True
        End If
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtDescription_Change()

    Const PROC_NAME As String = "txtDescription_Change"

    On Error GoTo EH
    
    If mp.GetMap().Description <> txtDescription Then
        IsDirty = True
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtDescription_GotFocus()
    Const PROC_NAME As String = "txtDescription_GotFocus"

    On Error GoTo EH

    SelectTextBox txtDescription

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub
