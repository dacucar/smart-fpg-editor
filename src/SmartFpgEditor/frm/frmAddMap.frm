VERSION 5.00
Object = "{FCE52DAF-EF3E-48E5-A71E-E7A56678C488}#38.0#0"; "SmartBennuCtls.ocx"
Begin VB.Form frmAddMap 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Add Graphic"
   ClientHeight    =   6270
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10440
   Icon            =   "frmAddMap.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   418
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   696
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbOptions 
      Height          =   315
      Left            =   6120
      Style           =   2  'Dropdown List
      TabIndex        =   20
      Top             =   5520
      Width           =   4215
   End
   Begin SmartBennuCtls.SmartMapPane mpPreview 
      Height          =   4215
      Left            =   6120
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   960
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   7435
      BorderStyle     =   1
      MarginTop       =   10
      MarginBottom    =   10
      MarginLeft      =   10
      MarginRight     =   10
      ShowTransparency=   -1  'True
   End
   Begin SmartBennuCtls.SmartMapPane mpSrc 
      Height          =   2055
      Left            =   120
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   960
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   3625
      BorderStyle     =   2
      ShowTransparency=   -1  'True
   End
   Begin VB.CommandButton btGetDesc 
      Height          =   315
      Left            =   4800
      Picture         =   "frmAddMap.frx":038A
      Style           =   1  'Graphical
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   3690
      Width           =   375
   End
   Begin VB.CommandButton btPreviousCode 
      Height          =   315
      Left            =   2100
      Picture         =   "frmAddMap.frx":0463
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   3240
      Width           =   375
   End
   Begin VB.CommandButton btNextCode 
      Height          =   315
      Left            =   3300
      Picture         =   "frmAddMap.frx":04DE
      Style           =   1  'Graphical
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   3240
      Width           =   375
   End
   Begin VB.CommandButton btNextFree 
      Height          =   315
      Left            =   3795
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmAddMap.frx":0558
      Style           =   1  'Graphical
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   3240
      Width           =   375
   End
   Begin VB.CommandButton btPreviousFree 
      Height          =   315
      Left            =   1605
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmAddMap.frx":06A2
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   3240
      Width           =   375
   End
   Begin VB.TextBox txtDescription 
      Height          =   315
      Left            =   1440
      TabIndex        =   12
      Top             =   3690
      Width           =   3255
   End
   Begin VB.TextBox txtInsertAt 
      Alignment       =   2  'Center
      Height          =   315
      Left            =   2580
      MaxLength       =   3
      TabIndex        =   8
      Top             =   3240
      Width           =   600
   End
   Begin VB.CommandButton btCancel 
      Cancel          =   -1  'True
      Height          =   375
      Left            =   4560
      TabIndex        =   22
      Top             =   5760
      Width           =   1095
   End
   Begin VB.CommandButton btOk 
      Default         =   -1  'True
      Height          =   375
      Left            =   3360
      TabIndex        =   21
      Top             =   5760
      Width           =   1095
   End
   Begin VB.Frame grbCPs 
      Height          =   1575
      Left            =   120
      TabIndex        =   14
      Top             =   4080
      Width           =   5535
      Begin VB.CommandButton btOpenCP 
         Enabled         =   0   'False
         Height          =   375
         Left            =   5040
         TabIndex        =   17
         Top             =   1080
         Width           =   375
      End
      Begin VB.OptionButton opOwn 
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   360
         Value           =   -1  'True
         Width           =   5055
      End
      Begin VB.OptionButton opLoad 
         Enabled         =   0   'False
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   1200
         Width           =   4695
      End
      Begin VB.OptionButton opUseSame 
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   780
         Width           =   5055
      End
   End
   Begin SmartBennuCtls.SmartMapPane mpDest 
      Height          =   1935
      Left            =   3720
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1020
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   3413
      BorderStyle     =   2
      MarginTop       =   4
      MarginBottom    =   4
      MarginLeft      =   4
      MarginRight     =   4
      ShowTransparency=   -1  'True
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   0
      TabIndex        =   24
      Top             =   0
      Width           =   10455
   End
   Begin VB.Label lblOptions 
      Height          =   195
      Left            =   6120
      TabIndex        =   19
      Top             =   5280
      Width           =   4185
   End
   Begin VB.Line Line 
      Index           =   0
      X1              =   392
      X2              =   392
      Y1              =   40
      Y2              =   408
   End
   Begin VB.Label lblPreview 
      Height          =   195
      Left            =   6120
      TabIndex        =   2
      Top             =   720
      Width           =   4170
   End
   Begin VB.Label lblReplaced 
      Height          =   195
      Left            =   3720
      TabIndex        =   1
      Top             =   720
      Width           =   1965
   End
   Begin VB.Label lblInsertAt 
      Alignment       =   2  'Center
      Height          =   195
      Left            =   2220
      TabIndex        =   5
      Top             =   3000
      Width           =   1335
   End
   Begin VB.Label lblDescription 
      Alignment       =   1  'Right Justify
      Height          =   195
      Left            =   75
      TabIndex        =   11
      Top             =   3720
      Width           =   1320
   End
   Begin VB.Image imgTitle 
      Height          =   600
      Left            =   0
      Picture         =   "frmAddMap.frx":07EB
      Stretch         =   -1  'True
      Top             =   0
      Width           =   10485
   End
   Begin VB.Image Image1 
      Height          =   915
      Left            =   2460
      Picture         =   "frmAddMap.frx":0909
      Top             =   1560
      Width           =   960
   End
   Begin VB.Label lblOriginal 
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   2010
   End
End
Attribute VB_Name = "frmAddMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================

Private Const MODULE_NAME As String = "SmartFpgEditor.frmAddMap"

Private Const RID_ADDGRAPHIC As Integer = 114
Private Const RID_ORIGINAL As Integer = 135
Private Const RID_REPLACED As Integer = 136
Private Const RID_PREVIEW As Integer = 137
Private Const RID_CODE As Integer = 138
Private Const RID_PREVIOUSFREE As Integer = 139
Private Const RID_NEXTFREE As Integer = 140
Private Const RID_PREVIOUSCODE As Integer = 141
Private Const RID_NEXTCODE As Integer = 142
Private Const RID_DESCRIPTION As Integer = 143
Private Const RID_GETDESC As Integer = 144
Private Const RID_OK As Integer = 106
Private Const RID_CANCEL As Integer = 107
Private Const RID_CPSHORTCUTS As Integer = 145
Private Const RID_OWNCPS As Integer = 146
Private Const RID_REPLACEDCPS As Integer = 147
Private Const RID_LOADCPS As Integer = 148
Private Const RID_OPTIONS As Integer = 149
Private Const RID_ADJUSTTOFPGPALETTE As Integer = 150
Private Const RID_REMOVETRANSPARENCY As Integer = 151
Private Const RID_NONE As Integer = 152

'===============================================================================

Private m_oFpg As cFpg
Private m_eDialogResult As VbMsgBoxResult

'===============================================================================

' Sets the FPG where the MAP will be added. Note: set this property before the
' mapSrc property
Public Property Set Fpg(newVal As cFpg)
    Const PROC_NAME As String = "Fpg"
    
    On Error GoTo EH
    
    Set m_oFpg = newVal
    
    Exit Property
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Property

' Sets the source MAP, which will be shown in the 'Original' map pane. Note: Set
' the Fpg before setting this map
Public Property Set MapSrc(newVal As cMap)
    Const PROC_NAME As String = "MapSrc"
    
    On Error GoTo EH

    mpSrc.setmap newVal
    
    txtDescription.Text = newVal.Description
    
    Debug.Assert Not (m_oFpg Is Nothing)
    
    ' Fill option list depending on the depth of the fpg and the map
    ' NOTE: we use the resource id also for the item data so as we can determine
    ' which option was selected
    cmbOptions.Clear
    cmbOptions.AddItem FormatResString(RID_NONE)
    cmbOptions.ItemData(cmbOptions.ListCount - 1) = RID_NONE
    lblOriginal = FormatResString(RID_ORIGINAL) & " " _
            & Switch(newVal.Depth = 8, "(8bpp)", newVal.Depth = 16, "(16bpp)", _
                newVal.Depth = 32, "(32bpp)") & ":"
            
    Select Case m_oFpg.Depth
    Case 8
        If newVal.Depth = 8 Then
            cmbOptions.AddItem FormatResString(RID_ADJUSTTOFPGPALETTE)
            cmbOptions.ItemData(cmbOptions.ListCount - 1) = RID_ADJUSTTOFPGPALETTE
        End If
    Case 16
        cmbOptions.AddItem FormatResString(RID_REMOVETRANSPARENCY)
        cmbOptions.ItemData(cmbOptions.ListCount - 1) = RID_REMOVETRANSPARENCY
    Case 32
        cmbOptions.AddItem FormatResString(RID_REMOVETRANSPARENCY)
        cmbOptions.ItemData(cmbOptions.ListCount - 1) = RID_REMOVETRANSPARENCY
    End Select
    cmbOptions.ListIndex = 0
    
    Exit Property
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Property

' Retrieves the result of the dialog
Public Property Get DialogResult() As VbMsgBoxResult
    Const PROC_NAME As String = "DialogResult"
    
    On Error GoTo EH
    
    DialogResult = m_eDialogResult
    Exit Property
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Property

'===============================================================================

' Purpose: Based on the depth of the FPG and the selected option in the Options
' combo box, this method applies the required conversion options to the MAP
' passed as argument.
Private Sub ApplyConversionOptions(oMap As cMap)
    Dim iOp As Integer
    
    Const PROC_NAME As String = "ApplyConversionOptions"
    
    On Error GoTo EH
    
    iOp = cmbOptions.ItemData(cmbOptions.ListIndex)
    
    Select Case m_oFpg.Depth
    Case 8
        If iOp = RID_ADJUSTTOFPGPALETTE Or oMap.Depth <> 8 Then
            oMap.ConvertTo8bpp m_oFpg.GetPaletteCopy
        Else
            oMap.SetPalette m_oFpg.GetPaletteCopy
        End If
    Case 16
        oMap.ConvertTo16bpp
        If iOp = RID_REMOVETRANSPARENCY Then oMap.RemoveTransparency
    Case 32
        oMap.ConvertTo32bpp
        If iOp = RID_REMOVETRANSPARENCY Then oMap.RemoveTransparency
    End Select
    
    Exit Sub
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Sub

' Purpose: Applies the desired control point options configuration
Private Sub ApplyControlPointOptions(oMap As cMap)

    Const PROC_NAME As String = "ApplyControlPointOptions"
    
    On Error GoTo EH

    If opOwn.value = True Then
        ' Do nothing
    ElseIf opUseSame.value = True Then
        Debug.Assert Not (mpDest.GetMap Is Nothing)
        oMap.CopyControlPointsFromMap mpDest.GetMap
    ElseIf opLoad.value = True Then
        ' TODO Load control points from file
    End If
    
    Exit Sub
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Sub

' Purpose: Selects the text of a text box, and set the focus to it
Private Sub SelectTextBox(oTB As TextBox)
    Const PROC_NAME As String = "SelectTextBox"
    
    On Error GoTo EH
    
    If oTB Is Nothing Then Exit Sub
    
    oTB.SetFocus
    oTB.SelStart = 0
    oTB.SelLength = Len(oTB.Text)
    
    Exit Sub
EH:
  Error.Show MODULE_NAME, PROC_NAME
End Sub

'===============================================================================

Private Sub btCancel_Click()
    Const PROC_NAME As String = "btCancel_Click"

    On Error GoTo EH

    Me.Hide

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub btGetDesc_Click()
    On Error GoTo EH
    
    Const PROC_NAME As String = "btGetDesc_Click"
    
    If Not mpDest.GetMap Is Nothing Then
        txtDescription.Text = mpDest.GetMap.Description
        SelectTextBox txtDescription
    Else
        Beep
    End If
    
    Exit Sub
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub btNextCode_Click()
    Dim lCode As Long
    
    Const PROC_NAME As String = "btNextCode_Click"
    
    On Error GoTo EH
    
    lCode = CLng(txtInsertAt.Text)
    If m_oFpg.IsCodeValid(lCode + 1) Then
        lCode = lCode + 1
    Else
        Beep
    End If
    txtInsertAt.Text = CStr(lCode)
    SelectTextBox txtDescription
    
    Exit Sub
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub btOk_Click()
    Const PROC_NAME As String = "btOk_Click"
    
    On Error GoTo EH
    
    m_eDialogResult = vbOK
    ApplyConversionOptions mpSrc.GetMap
    ApplyControlPointOptions mpSrc.GetMap
    Me.Hide
    
    Exit Sub
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub btPreviousCode_Click()
    Dim lCode As Long
    
    Const PROC_NAME As String = "btPreviousCode_Click"
    
    On Error GoTo EH
    
    lCode = CLng(txtInsertAt.Text)
    If m_oFpg.IsCodeValid(lCode - 1) Then
        lCode = lCode - 1
    Else
        Beep
    End If
    txtInsertAt.Text = CStr(lCode)
    SelectTextBox txtDescription
    
    Exit Sub
EH:
    Error.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub btNextFree_Click()
    Dim lCode As Long
    Dim lFreeCode As Long
    
    Const PROC_NAME As String = "btNextFree_Click"
    
    On Error GoTo EH
    
    lCode = CLng(txtInsertAt.Text)
    If m_oFpg.IsCodeValid(lCode + 1) Then
        lFreeCode = m_oFpg.FreeCode(lCode + 1)
        If m_oFpg.IsCodeValid(lFreeCode) Then lCode = lFreeCode
        txtInsertAt.Text = CStr(lCode)
        SelectTextBox txtDescription
    End If
    
    If lCode <> lFreeCode Then Beep
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub btPreviousFree_Click()
    Dim lCode As Long
    Dim lFreeCode As Long
    
    Const PROC_NAME As String = "btPreviousFree_Click"
    
    On Error GoTo EH
    
    lCode = CLng(txtInsertAt.Text)
    If m_oFpg.IsCodeValid(lCode - 1) Then
        lFreeCode = m_oFpg.FreeCodeRev(lCode - 1)
        If m_oFpg.IsCodeValid(lFreeCode) Then lCode = lFreeCode
        txtInsertAt.Text = CStr(lCode)
        SelectTextBox txtDescription
    End If
    
    If lCode <> lFreeCode Then Beep
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub cmbOptions_Click()
    Dim oMap As cMap
    
    Const PROC_NAME As String = "cmbOptions_Click"
    
    On Error GoTo EH
    
    Set oMap = mpSrc.GetMap.GetCopy
    ApplyConversionOptions oMap
    
    mpPreview.setmap oMap
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub Form_Load()
    Const PROC_NAME As String = "Form_Load"
    
    On Error GoTo EH
    
    ' Internationalization
    Me.Caption = FormatResString(RID_ADDGRAPHIC)
    lblTitle.Caption = FormatResString(RID_ADDGRAPHIC)
    'lblOriginal.Caption = FormatResString(RID_ORIGINAL) & ":"
    lblReplaced.Caption = FormatResString(RID_REPLACED) & ":"
    lblPreview.Caption = FormatResString(RID_PREVIEW) & ":"
    lblInsertAt.Caption = FormatResString(RID_CODE)
    btPreviousFree.ToolTipText = FormatResString(RID_PREVIOUSFREE)
    btNextFree.ToolTipText = FormatResString(RID_NEXTFREE)
    btPreviousCode.ToolTipText = FormatResString(RID_PREVIOUSCODE)
    btNextCode.ToolTipText = FormatResString(RID_NEXTCODE)
    lblDescription.Caption = FormatResString(RID_DESCRIPTION)
    btGetDesc.ToolTipText = FormatResString(RID_GETDESC)
    grbCPs.Caption = FormatResString(RID_CPSHORTCUTS)
    opOwn.Caption = FormatResString(RID_OWNCPS)
    opUseSame.Caption = FormatResString(RID_REPLACEDCPS)
    opLoad.Caption = FormatResString(RID_LOADCPS)
    btOk.Caption = FormatResString(RID_OK)
    btCancel.Caption = FormatResString(RID_CANCEL)
    lblOptions.Caption = FormatResString(RID_OPTIONS) & ":"
    
    txtDescription.MaxLength = modGlobals.MAX_GRAPHIC_DESC_LENGTH
    
    m_eDialogResult = vbCancel ' By default, cancel action
    
    Exit Sub
  
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtDescription_GotFocus()
    Const PROC_NAME As String = "txtDescription_GotFocus"
    
    On Error GoTo EH
    
    SelectTextBox txtDescription
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtInsertAt_Change()
    Dim lCode As Long
    Dim bMapExists As Boolean
    
    Const PROC_NAME As String = "txtInsertAt_Change"
    
    On Error GoTo EH
    
    If m_oFpg Is Nothing Then Exit Sub
    
    ' Check if the textbox contains a valid code for the FPG given
    If IsNumeric(txtInsertAt.Text) Then
        lCode = CLng(txtInsertAt.Text)
        If m_oFpg.IsCodeValid(lCode) Then
            If m_oFpg.ExistsCode(lCode) Then bMapExists = True
        End If
    End If
    
    If bMapExists Then
        mpDest.setmap m_oFpg.Maps(m_oFpg.IndexByCode(lCode))
        opUseSame.Enabled = True
        opUseSame.value = True
    Else
        mpDest.setmap Nothing
        opOwn.value = True
        opUseSame.Enabled = False
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtInsertAt_GotFocus()
    Const PROC_NAME As String = "txtInsertAt_GotFocus"
    
    On Error GoTo EH
    
    SelectTextBox txtInsertAt
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtInsertAt_KeyPress(KeyAscii As Integer)
    Const PROC_NAME As String = "txtInsertAt_KeyPress"
    
    On Error GoTo EH
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    ElseIf KeyAscii <> 8 Then
        If Not IsNumeric(Chr(KeyAscii)) Then
            Beep
            KeyAscii = 0
        End If
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub txtInsertAt_Validate(Cancel As Boolean)
    Dim i As Integer
    
    Const PROC_NAME As String = "txtInsertAt_Validate"
    
    On Error GoTo EH
    
    If Not IsNumeric(txtInsertAt) Then
        Cancel = True
    Else
        i = CInt(txtInsertAt.Text)
        If i < bennulib.MIN_CODE Or i > bennulib.MAX_CODE Then
            Cancel = True
        End If
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub
