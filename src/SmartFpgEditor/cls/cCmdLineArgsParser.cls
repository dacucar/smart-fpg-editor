VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCmdLineArgsParser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit
Option Base 0

Private Const MODULE_NAME As String = "SmartFpgEditor.cCmdLineArgsParser"

Private m_Args() As String
Private m_ArgsCount As Integer

' Retrieves the total number of arguments parsed
Public Property Get ArgC() As Integer
    Const PROC_NAME As String = "ArgC"
    
    On Error GoTo EH

    ArgC = m_ArgsCount
    
    Exit Sub

EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property

' Returns argument iIndex
' @param iIndex the index of the argument to be retrieved
Public Property Get ArgV(ByVal iIndex As Long) As String
    Const PROC_NAME As String = "ArgV"
   
    On Error GoTo EH

    If iIndex < 0 Or iIndex > UBound(m_Args) Then
        ' TODO: ... ??
        Debug.Assert False
    Else
        ArgV = m_Args(iIndex)
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Parses the command-line string s. The space is considered the separator
' between different arguments. If an argument must contain spaces, use the
' double quotes ("argument with spaces") so as the parser considers it a unique
' string. The number of quotes must be even, otherwise the parser will produce
' no arguments.
' @param s the string to be parsed
Public Sub Parse(ByVal s As String)
    Const PROC_NAME As String = "Parse"
    
    Dim nQuotes As Integer
    Dim sArgs() As String
    Dim i As Integer
    
    Const SPC_REPLACEMENT As String = "???"

    On Error GoTo EH

    m_ArgsCount = 0
    Erase m_Args
    
    s = Trim(s)
    nQuotes = CountQuotes(s)
    If (nQuotes Mod 2) = 0 Then ' Odd number
        If nQuotes > 0 Then ReplaceSpacesBetweenQuotes s, SPC_REPLACEMENT
        
        s = Replace(s, Chr(34), "")  ' Remove quotes

        sArgs = Split(s, " ")
        If UBound(sArgs) >= LBound(sArgs) Then
            For i = LBound(sArgs) To UBound(sArgs)
                If Len(sArgs(i)) > 0 Then ' Skip empty strings
                    ReDim Preserve m_Args(m_ArgsCount) As String
                    m_Args(m_ArgsCount) = Replace(sArgs(i), SPC_REPLACEMENT, " ")
                    m_ArgsCount = m_ArgsCount + 1
                End If
            Next
        End If
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Counts the number of quotes (chr(34)) in the string
Private Function CountQuotes(ByVal s As String) As Integer
    Const PROC_NAME As String = "CountQuotes"
    On Error GoTo EH

    CountQuotes = CountCharCodeInString(s, 34)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function CountCharCodeInString(ByVal s As String, ByVal lCharCode As Long) As Integer
    Dim p As Long
    Dim c As Integer
    
    Const PROC_NAME As String = "CountCharCodeInString"
    On Error GoTo EH

    p = InStr(1, s, Chr(lCharCode))
    While p > 0
        c = c + 1
        p = InStr(p + 1, s, Chr(lCharCode))
    Wend
    
    CountCharCodeInString = c

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Replaces spaces found between 2 quotes. Requires an even number
' of quotes in the string (otherwise results are not guarranteed).
Private Sub ReplaceSpacesBetweenQuotes(s As String, _
        ByVal sReplacement As String)
    
    Const PROC_NAME As String = "ReplaceSpacesBetweenQuotes"
    
    Dim p1 As Long
    Dim p2 As Long
    Dim iDeltaLength As Integer ' Replacement length - replaced length
    Dim sBetweenQuotes As String
    Dim iNReplacements As Integer
    
    On Error GoTo EH

    iDeltaLength = Len(sReplacement) - Len(" ")
    
    p1 = InStr(1, s, Chr(34))
    p2 = InStr(p1 + 1, s, Chr(34))
    While p1 > 0 And p2 > p1
        sBetweenQuotes = Mid(s, p1 + 1, p2 - p1 - 1)
        iNReplacements = CountCharCodeInString(sBetweenQuotes, Asc(" "))
        sBetweenQuotes = Replace(sBetweenQuotes, " ", sReplacement)
        s = Left(s, p1) & sBetweenQuotes & Right(s, Len(s) - p2 + 1)
        
        p1 = InStr(p2 + 1 + iDeltaLength * iNReplacements, s, Chr(34))
        p2 = InStr(p1 + 1, s, Chr(34))
    Wend

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub


