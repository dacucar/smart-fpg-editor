VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cStripFpgEncoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' FPG encoder for DIV / Fenix/ Bennu Fpg file format. This is also the default
' FPG encoder.
'===============================================================================

Implements IFpgEncoder
Implements IStandardEncoder

'===============================================================================

Private Const Version As Byte = &H0 ' Current writeable FPG version
Private Const MODULE_NAME As String = "bennulib.cStripFpgEncoder"

'===============================================================================

Private Sub IFpgEncoder_Encode(oFpg As cFpg, ByVal sFileName As String)

    Dim oMap As cMap
    Dim lMaxW As Long, lMaxH As Long
    Dim oBmp As cBitmap, oBmpCpy As cBitmap
    Dim auPalette() As RGBQUAD
    Dim lRMask As Long, lGMask As Long, lBMask As Long
    Dim l As Long
    
    Const PROC_NAME As String = "IFpgEncoder_Encode"
    On Error GoTo EH

    If oFpg.MapCount = 0 Then Exit Sub
    
    ' Get the maximum height and width of all the maps in the fpg
    For Each oMap In oFpg
        If oMap.Width > lMaxW Then lMaxW = oMap.Width
        If oMap.Height > lMaxH Then lMaxH = oMap.Height
    Next
    
    Debug.Assert lMaxW > 0
    Debug.Assert lMaxH > 0
    
    ' Create a bitmap of the size of the strip and color depth of the fpg
    
    Set oBmp = New cBitmap

    If oFpg.Depth = 8 Then
        auPalette = GetRGBQuadPaletteFromPalette(oFpg.GetPaletteCopy)
    ElseIf oFpg.Depth = 16 Then
        ' Nothing to do... (the bitmap image is treated as 32bpp)
    ElseIf oFpg.Depth = 32 Then
        ' Nothing to do...
    End If
    
    oBmp.Allocate lMaxW * oFpg.MapCount, lMaxH, IIf(oFpg.Depth = 8, 8, 32), _
        lRMask, lGMask, lBMask
    
    Select Case oFpg.Depth
    Case 8
        oBmp.SetPalette auPalette
        oBmp.SetTransparentIndex 0
    Case 16, 32
        oBmp.SetTransparent True
    End Select
    
    l = 0
    For Each oMap In oFpg
        Set oBmpCpy = oMap.GetBitmapCopy ' Necessary to retrieve a copy to call the Dib property
        oBmp.Paste oBmpCpy.Dib, _
                l * lMaxW + lMaxW / 2 - oMap.GetCenterX, _
                lMaxH / 2 - oMap.GetCenterY
        l = l + 1
    Next
    
    oBmp.Save sFileName, FIF_PNG

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Property Get IStandardEncoder_DefaultFileExtension() As String

    Const PROC_NAME As String = "IStandardEncoder_DefaultFileExtension"
    On Error GoTo EH

    IStandardEncoder_DefaultFileExtension = "png"

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Private Property Let IStandardEncoder_EncoderOption(ByVal sKey As String, ByVal sValue As String)

End Property

Private Property Get IStandardEncoder_KnownFileExtensions() As String()
    
    Dim asExt(0) As String
    
    Const PROC_NAME As String = "IStandardEncoder_KnownFileExtensions"
    On Error GoTo EH

    asExt(0) = "png"
    IStandardEncoder_KnownFileExtensions = asExt()

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Private Function IStandardEncoder_KnowsFileExtension(ByVal sExt As String) As Boolean
    
    Const PROC_NAME As String = "IStandardEncoder_KnowsFileExtension"
    On Error GoTo EH

    If sExt = "png" Then
        IStandardEncoder_KnowsFileExtension = True
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function
