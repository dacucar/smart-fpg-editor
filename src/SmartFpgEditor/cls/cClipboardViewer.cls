VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cClipboardViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' vbAccelerator Software License
' Version 1#
'
' Copyright (c) 2002 vbAccelerator.com
'
' Redistribution and use in source and binary forms, with or without
' modification, are permitted provided that the following conditions are met:
'
' Redistributions of source code must retain the above copyright notice, this
' list of conditions and the following disclaimer
'
' Redistributions in binary form must reproduce the above copyright notice,
' this list of conditions and the following disclaimer in the documentation
' and/or other materials provided with the distribution.
'
' The end-user documentation included with the redistribution, if any, must
' include the following acknowledgment:
' "This product includes software developed by vbAccelerator
' (http://vbaccelerator.com/)."
'
' Alternately, this acknowledgment may appear in the software itself, if and
' wherever such third-party acknowledgments normally appear.
'
' The names "vbAccelerator" and "vbAccelerator.com" must not be used to endorse
' or promote products derived from this software without prior written
' permission. For written permission, please contact vbAccelerator through
' steve@vbaccelerator.com.
' Products derived from this software may not be called "vbAccelerator",
' nor may "vbAccelerator" appear in their name, without prior written permission
' of vbAccelerator.
'
' THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES,
' INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
' AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
' VBACCELERATOR OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
' INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
' NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
' DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
' OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
' NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
' EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'
' This software consists of voluntary contributions made by many individuals on
' behalf of the vbAccelerator. For more information, please see
' http://vbaccelerator.com/.
'
' The vbAccelerator licence is based on the Apache Software Foundation Software
' Licence, Copyright (c) 2000 The Apache Software Foundation.
' All rights reserved.

Option Explicit

Implements ISubclass

Private Declare Function SetClipboardViewer Lib "USER32" (ByVal hWnd As Long) As Long
Private Declare Function ChangeClipboardChain Lib "USER32" (ByVal hWnd As Long, ByVal hWndNext As Long) As Long
Private Declare Function SendMessageByString Lib "USER32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As String) As Long
Private Declare Function SendMessageByLong Lib "USER32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Private Declare Function SendMessage Lib "USER32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long

Private Const WM_CHANGECBCHAIN = &H30D
Private Const WM_DRAWCLIPBOARD = &H308

Private m_hWnd As Long
Private m_hWndNextViewer As Long
Private m_bInClipboardChangeNotification As Boolean
Private m_emr As EMsgResponse

Public Event ClipboardChanged()

Public Sub InitClipboardChangeNotification(hWndA As Long)
    StopClipboardChangeNotification
    m_hWnd = hWndA
    If (m_hWnd <> 0) Then
        ' Attach Clipboard 'viewer' notification messages
        AttachMessage Me, m_hWnd, WM_CHANGECBCHAIN
        AttachMessage Me, m_hWnd, WM_DRAWCLIPBOARD
        ' Place me in the clipboard viewer notification chain:
        m_hWndNextViewer = SetClipboardViewer(m_hWnd)
        m_bInClipboardChangeNotification = True
    End If
End Sub
Public Sub StopClipboardChangeNotification()
    If (m_bInClipboardChangeNotification) Then
        If (m_hWnd <> 0) Then
            ' Take myself out of the clipboard chain:
            ChangeClipboardChain m_hWnd, m_hWndNextViewer
            ' Stop subclassing for clipboard messages:
            DetachMessage Me, m_hWnd, WM_CHANGECBCHAIN
            DetachMessage Me, m_hWnd, WM_DRAWCLIPBOARD
        End If
    End If
    m_bInClipboardChangeNotification = False
End Sub
Private Sub Class_Terminate()
   StopClipboardChangeNotification
End Sub

Private Function ISubclass_WindowProc(ByVal hWnd As Long, _
                                      ByVal iMsg As Long, _
                                      ByVal wParam As Long, _
                                      ByVal lParam As Long) As Long
    Select Case iMsg
    Case WM_CHANGECBCHAIN
        If (wParam = m_hWndNextViewer) Then
            ' If the next viewer window is closing, repair the chain:
            m_hWndNextViewer = lParam
        ElseIf (m_hWndNextViewer <> 0) Then
            ' Otherwise if there is a next window, pass the message on:
            SendMessageByLong m_hWndNextViewer, iMsg, wParam, lParam
        End If
        ISubclass_WindowProc = 0
        
    Case WM_DRAWCLIPBOARD
        ' the content of the clipboard has changed.
        ' We raise a ClipboardChanged message and pass the message on:
        RaiseEvent ClipboardChanged
        SendMessageByLong m_hWndNextViewer, iMsg, wParam, lParam
        ISubclass_WindowProc = 0
        
    End Select
End Function

Private Property Get ISubclass_MsgResponse() As EMsgResponse
    ISubclass_MsgResponse = m_emr
End Property
Private Property Let ISubclass_MsgResponse(ByVal emrA As EMsgResponse)
    m_emr = emrA
End Property

