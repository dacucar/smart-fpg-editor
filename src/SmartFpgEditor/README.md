# SMART FPG EDITOR README

## CONTENTS
I.            Introduction
II.           Authors
III.          Copyright, Credits & Aplicable Licenses
IV.           Requeriments
V.            Getting Smart Fpg Editor
VI.           Installing and working with Smart Fpg Editor
VII.          Getting the sources and compiling

## I.      INTRODUCTION
Smart Fpg Editor is an easy-to-use, user friendly and powerful FPG editor tool.

FPGs are graphic collections used by certain compilers/languages such as Bennu,
Fenix or DIV Games Studio. Refer to Bennu Wiki (http://wiki.bennugd.org) to know
more about these languages and the FPG graphic collections.

Smart Fpg Editor was concived as a "Painless Fpg editor" to fullfill the lack of
several characteristics I missed in the existing FPG editors.

## II.     AUTHORS
- Darío Cutillas Carrillo <dacucar> smartfpgeditor AT dacucar DOT com)

If you wish to colaborate with the project, have any comment, suggestion or 
doubt feel free to contact me.

## III.    COPYRIGHT, CREDITS & APLICABLE LICENSES
- Copyright (C) 2009 - 2011 Darío Cutillas Carrillo

- All Smart Fpg Editor code, unless otherwise noted, is licensed under GNU 
  General Public License, Version 3. You should have received a copy of the 
  license along with this software (see license-gpl3). You can also find this 
  license at http://www.opensource.org/licenses/gpl-3.0.html.   
  
- This software uses the FreeImage open source image library. 
  See http://freeimage.sourceforge.net for details. FreeImage is licensed under
  GNU General Public License, Version 2. You should have received a copy of the
  license along with this software (see license-gpl2). You can also find this
  license at http://www.opensource.org/licenses/gpl-2.0.php.
  
  You have the right to access to the FreeImage source code of the binary 
  version that comes along with this software. You can do that by accessing to
  http://sourceforge.net/projects/freeimage/files/Source%20Distribution/3.15.0/
  
- This product includes software developed by vbAccelerator 
  (http://vbaccelerator.com/).
  Software and source code developed by vbAccelerator is distributed under the
  vbAccelerator Software License Version 1.0. You should have received a copy of
  the license along with this software (see license-vbAccelerator). You can also
  find this license at http://quantumriftsoftware.com/help/homemovielibrary/vbac
  celeratorlicense.html.
    
- Some of the graphics used in this software are directly taken, or 
  derived from the "Mint", "Silk" and "Mini" icon sets from famfamfam. These
  icons sets can be found at http://www.famfamfam.com/lab/icons/silk/. 
  The "Mint" and "Silk" icons sets are distributed under Creative Commons
  Attribution 2.5 License (see license-cc-attribution or 
  http://creativecommons.org/licenses/by/2.5/).
  The "Mini" icon set is not released under any particular license but
  the Author has make it available for free use for any purpose, as 
  stated in http://www.famfamfam.com/lab/icons/mini/. 
    
- This project uses CKXML Parser & Builder, an Object Oriented XML Parser and 
  builder, by Cem Kalyoncu. CKXML Parser & Builder is distributed under
  PHP-License (see license-php). http://sourceforge.net/projects/ckxml/
  
- The orange right arrow image used in the 'Add Graphic' window was created by
  Jean Victor Balin and is of Public Domain. Can be found at 
  http://openclipart.org/Xiph/files/jean_victor_balin/8648
  
- The error sign used in the 'Error Report' window was created by Jakub 
  Jankiewicz and is of Public Domain. Can be found at 
  http://openclipart.org/media/files/kuba/2051

- The open, save, palette options, cut, copy and paste icons are from the tango 
  iconset and are of the public domain. These icons can be found in 
  http://openclipart.org (search for tango).
  
- Some of the icons used in the main toolbar of the FPG Editor window are
  from the 'Pixelicious' iconset by Michael Flarup. These icons are free for 
  personal use or non profit organisations. http://www.pixelresort.com.
  
## IV.     REQUIREMENTS
Smart Fpg Editor should work in any Win32 windows system with the following 
components installed:

- Microsoft Visual Basic 6 Runtimes (recommended Service Pack 6). Find it at
  http://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=7b9ba2
  61-7a9c-43e7-9117-f673077ffb3c.

- A comctl32.dll version 5 or greater. If you have Internet Explorer 5 or above
  you do not need to install anything, if not, you can find an updated version 
  at http://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=254
  37d98-51d0-41c1-bb14-64662f5f62fe.

Note that previous Smart Fpg Editor (prior to 0.2.0) releases required GDI+ library, 
but this is no longer a requirement.

Smart Fpg Editor has been reported to work well with Windows XP/Vista/7/8/10. If you
encounter any problem please report it in the issues section of the Bitbuck project
page (https://bitbucket.org/dacucar/smart-fpg-editor/issues?status=new&status=open)

V.      GETTING SMART FPG EDITOR
Since version 0.4.8, only installer packages are distributed and is the only officially
supported way to install the program. The installer packages have the following name:

``` 
sfpge-inst-win32-X.Y.Z-U.exe
```

Where X.Y.Z is the version number.

If you do not wish to use the installer, you can still open it with a compression utility
such as 7-zip (http://www.7-zip.org/). Notice that you will have to handle manually the 
registration of Active X components in your system (dlls and ocx in ```./modules/``` and
```required``` folders). Different Windows versions have different ways to handle 
this so refer to the documentation of your operative system.

Source packages are not distributed anymore because all the source code is available in
the git repository (https://bitbucket.org/dacucar/smart-fpg-editor.git). Specific versions
are always tagged.

## VI.     INSTALLING AND WORKING WITH SMALL FPG EDITOR
Installing Smart Fpg Editor is pretty straightforward. Just double click in the
downloaded exe file and follow the steps. However, is is highly encouraged to
uninstall any installed version of Smart Fpg Editor before installing a new one.

The uninstall process is same as with any other Windows program.

For the moment, there are no specific instructions on how to use the program. 
You should, however, find it easy to use and intuitive. If you have any doubt,
the best place to ask is on the Bennu forum (Smart Fpg Editor Topic, find it at
http://forum.bennugd.org/index.php?topic=399.0).

## VII.    GETTING AND COMPILING THE SOURCES
Follow the instructions in the project page 
(https://bitbucket.org/dacucar/smart-fpg-editor/overview)
