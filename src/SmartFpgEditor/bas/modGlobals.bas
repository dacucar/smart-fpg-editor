Attribute VB_Name = "modGlobals"
Option Explicit

' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

'===============================================================================
' MODULE OVERVIEW
'===============================================================================
' Utility functions and constants used in several parts of the application or
' that define its behaviour
'===============================================================================

' Control point ID limits
Public Const MAX_CP_ID As Integer = 999
Public Const MIN_CP_ID As Integer = 0

' Description of graphics max length
Public Const MAX_GRAPHIC_DESC_LENGTH = 32

' Error handler replacement
Public Err As New cError

'===============================================================================

Private Const MODULE_NAME As String = "SmartFpgEditor.modGlobals"

' Graphic export formats TODO: Reconsider when Encoders can be registered...
#If 1 = 0 Then ' Trick to prevent VB to rename our enum constants
    Private Const egefMap As Integer = 1
    Private Const egefPng As Integer = 2
#End If
Public Enum E_GraphicExportFormats
    egefMap
    egefPng
End Enum

' Used in string formatting functions
Private Const MACRO_VALUE_SEPARATOR = ";"

'===============================================================================

' Formats a certain set of macros of an string with a certain set of values.
' For example: FormatString("My name is %NAME%", "%NAME%;Dar�o") returns
' Dar�o.
' @param sString the string to be formatted
' @param sReplacements an string containing the list of macros-values in
' the format "macro1;value1;macro2;value2"
' @return the formatted string
Public Function FormatString(ByVal sString As String, _
        Optional ByVal sReplacements As String) As String
    
    Dim iMacro As Integer
    Dim asReplacements() As String
    
    Const PROC_NAME As String = "FormatString"

    On Error GoTo EH

    asReplacements = Split(sReplacements, MACRO_VALUE_SEPARATOR)
              
    ' For each macro/value pair passed in ...
    For iMacro = LBound(asReplacements) To UBound(asReplacements) Step 2
        Dim sMacro As String
        Dim sValue As String
        sMacro = CStr(asReplacements(iMacro))
        sValue = vbNullString
         
        If iMacro < UBound(asReplacements) Then
            sValue = asReplacements(iMacro + 1)
        End If
         
        ' Replace all occurrences of sMacro with sValue.
        Do
            Dim iPos As Integer
            iPos = InStr(sString, sMacro)
            If 0 <> iPos Then
                sString = Left(sString, iPos - 1) & sValue & _
                    Mid(sString, iPos + Len(sMacro))
            End If
        Loop Until iPos = 0
    Next iMacro
     
    FormatString = sString

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Formats the resource string whose id is lResId. See FormatString.
' @param lResId the resource id
' @param sReplacements an string containing the list of macros-values in
' the format "macro1;value1;macro2;value2"
' @return the formatted string
Public Function FormatResString(ByVal lResId As Long, _
        Optional ByVal sReplacements As String) As String
    
    Dim sString As String
    On Error Resume Next
    sString = LoadResString(lResId)
    On Error GoTo 0
    FormatResString = FormatString(sString, sReplacements)
End Function

' Builds an string of the format "Var1;Var2;Var3;Var4..." This is to
' use with the FormatString and related functions. What we want is to
' avoid using "Macro" & ";" & Value & ";Macro2" & Value2 & ... all the
' time when calling FormatString.
' The strange name is due to the wish of being short and mnemotechnic
' (Param Array 2 String)
Public Function PA2S(ParamArray wVars() As Variant) As String
    Const PROC_NAME As String = "PA2S"

    On Error GoTo EH

    PA2S = Join(wVars, MACRO_VALUE_SEPARATOR)

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' MsgBox Replacement. Works in a similar way than MsgBox but if wMsg is
' an interger value, it will be used to load the resource string of the same
' ID as message. The wReplacements provides a list of macro-values to format
' the message (See FormatString)
' @param wMsg an string, interger or long value
' @param uStyle style of the MsgBox
' @param sReplacements an string containing the list of macros-values in
' the format "macro1;value1;macro2;value2"
' @return the MsgBox result
Public Function MsgBox(ByVal wMsg As Variant, _
        ByVal uStyle As VbMsgBoxStyle, _
        Optional ByVal sReplacements As String) As VbMsgBoxResult
    
    Dim uResult As VbMsgBoxResult

    Const PROC_NAME As String = "MsgBox"

    On Error GoTo EH

    ' Only format when we have a resource id or an string.
    If VarType(wMsg) = vbLong Or VarType(wMsg) = vbInteger Then
        wMsg = FormatResString(CLng(wMsg), sReplacements)
    ElseIf VarType(wMsg) = vbString Then
        wMsg = FormatString(CStr(wMsg), sReplacements)
    End If
    
    uResult = VBA.Interaction.MsgBox(wMsg, uStyle, App.Title)

    MsgBox = uResult

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Checks wether we are running from the IDE or from a compiled file
' @return true if we are running from the IDE or false if we are running
' from an executable file
Public Function IsExe() As Boolean
    Const PROC_NAME As String = "IsExe"

    On Error GoTo EH

    IsExe = True
    Debug.Assert Not DummyIsExe(IsExe)

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Converts an unsigned interger (32 bits) to a signed integer (16 bits). This is
' to overcome VB6 limitation of not having unsigned interger types.
Public Function UInt32ToSInt16(ByVal UInt32 As Long) As Integer
    Const PROC_NAME As String = "UInt32ToSInt16"
    Dim x%

    On Error GoTo EH

    If UInt32 > 65535 Then
       MsgBox "You passed a value larger than 65535", vbCritical
       Exit Function
    End If
    
    x% = UInt32 And &H7FFF
    UInt32ToSInt16 = x% Or -(UInt32 And &H8000)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function
'===============================================================================

' Purpose: Used in conjuction with IsExe
Private Function DummyIsExe(ByRef b As Boolean)
    Const PROC_NAME As String = "DummyIsExe"

    On Error GoTo EH

    b = False

    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function
