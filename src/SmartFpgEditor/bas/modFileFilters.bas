Attribute VB_Name = "modFileFilters"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' MODULE OVERVIEW
'===============================================================================
' File filter functions and Open/Save dialog routines.
' A file filter is a pair {description, file extensions} which is used to set
' which files will be filtered in an Open/Save dialog. All file filters have
' an identifier string (key) associated with them.
'
' File filters are defined in the "conf.xml" file. You can edit this file with
' any plain-text editor, (sintax is self-exaplanatory).
'===============================================================================

' Dialog filters
Public G_sOpenFpgDlgFilter As String
Public G_sOpenGraphicDlgFilter As String
Public G_sSaveGraphicDlgFilter As String
Public G_sOpenPaletteDlgFilter As String
Public G_sSaveFpgDlgFilter As String
Public G_sSavePaletteDlgFilter As String

'===============================================================================

Private Const MODULE_NAME As String = "SmartFpgEditor.modFileFilters"

' A collection to store all file filters. Each element in the collection is an
' array of two elements. Index 0 contains the description of the file filter
' while index 1 contains the list of extensions. Elements of the collection
' can be accessed by its KEY, which is an unique string that identifies the
' filter. This key corresponds to the "id" parameter of the filter definitions
' on the "conf.xml" file.
Private m_oFileFilters As New Collection

'===============================================================================

' Adds a file filter to the collection of file filters. The sExt parameter
' allows specifying which extensions will be filtered. Use "|" to separate
' several extensions, for example "jpg|gif|bmp".
' @param sKey a unique string to identify the file filter
' @param sDesc a string containing a description of the filte filter
' @param sExt a string with a list of the extensions to filter by the file.
Public Sub AddFileFilter(ByVal sKey As String, _
            ByVal sDesc As String, ByVal sExt As String)
    
    Dim s(1) As String
    
    Const PROC_NAME As String = "AddFileFilter"
    
    On Error GoTo EH
    
    s(0) = sDesc
    s(1) = sExt
    m_oFileFilters.Add s, sKey
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Composes the extensions of several file filters. This function was used before
' the conf.xml file was introduced and it is no lorger used. However, it may be
' useful if a file filter is to be built from the code.
' @deprecated
' @param awKeys the file filters list specified by their keys
Public Function ComposeFiltersExts(ParamArray awKeys()) As String
    Dim wKey As Variant
    Dim sRes As String
    
    Const PROC_NAME As String = "ComposeFiltersExts"
    
    On Error GoTo EH
    
    ' Create an string containing all filters
    For Each wKey In awKeys
        sRes = sRes & "|" & m_oFileFilters(wKey)(1)
    Next
    sRes = Right(sRes, Len(sRes) - 1) 'remove the first "|"
    
    ComposeFiltersExts = sRes
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Creates an Open/Save dialog suitable filter from one or several file filters.
' Use this functions to retrieve an string you can pass to the ShowOpenDialog
' and ShowSaveDialog as the sFilter param. Each argument must represent a valid
' key of a file filter (a key that must be in the m_oFileFilter collection).
' The returned string will have the following format:
'       "desc1 (*.ext11, ..., *.ext1N)|*.ext11;...;*.ext1N|" _
'     & "desc2 (*.ext21, ..., *.ext2N)|*.ext21;...;*.ext2N|" _
'     & "..." _
'     & "descN (*.extN1, ..., *.extNN)|*.extN1;...;*.extNN"
' @param awKeys the file filters list specified by their keys
Public Function BuildDialogFilter(ParamArray awKeys()) As String
    Dim sRes As String
    Dim asFilters() As String
    Dim wKey As Variant
    Dim i As Integer
    
    Const PROC_NAME As String = "BuildDialogFilter"
    
    On Error GoTo EH
    
    'Create the filter for each wKey
    For Each wKey In awKeys
        asFilters() = Split(m_oFileFilters(CStr(wKey))(1), "|")
        For i = LBound(asFilters) To UBound(asFilters)
            asFilters(i) = "*." + asFilters(i)
        Next
        sRes = sRes & m_oFileFilters(CStr(wKey))(0) _
            & " (" & Join(asFilters, ", ") & ")" & "|" _
            & Join(asFilters, ";") & "|"
    Next
    sRes = Left(sRes, Len(sRes) - 1) 'Remove the last | symbol
    
    BuildDialogFilter = sRes
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Shows a Save common dialog. You can specify the filter of the files to be
' shown with the sFilterParam.
' You can specify the filter of the files to be shown with the sFilterParam.
' It is important to notice that this param is a CommonDialog-like filter.
' This is NOT the same than a file filter. See BuildDialogFilter to know how
' to get an appropiate filter from a file filter.
' @param sFileName used to retrieve the path to the file to be saved
' @param sFilter a filter to determine which files will be shown
' @param sDefaultExt the default extension with which the file will be saved
' @param sDialogTitle the caption of the dialog title bar
' @param fOwner the Form that owns the dialog. If true, the dialog will be shown
' in vbModal mode
' @return true if the user selected OK, false if cancelled
Public Function ShowSaveDialog(sFileName As String, _
                Optional ByVal sFilter As String, _
                Optional ByVal sDefaultExt As String, _
                Optional ByVal sDialogTitle As String, _
                Optional fOwner As Form) As Boolean
    
    Dim bDlgResult As Boolean
    Dim c As New cCommonDialog
    Dim lOwner As Long
    
    Const PROC_NAME As String = "ShowSaveDialog"
    
    On Error GoTo EH
        
    If Not fOwner Is Nothing Then lOwner = fOwner.hWnd
    
    bDlgResult = c.VBGetSaveFileName(sFileName, , , sFilter, , , sDialogTitle, _
        sDefaultExt, lOwner)
    
    ShowSaveDialog = bDlgResult
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Shows an Open File common dialog. The function returns an array containing the
' title (i.e not directoy path) of the selected files. The path of the selected
' files can be got in the sDir parameter, which also specifies the initial
' directory.
' You can specify the filter of the files to be shown with the sFilterParam.
' It is important to notice that this param is a CommonDialog-like filter.
' This is NOT the same than a file filter. See BuildDialogFilter to know how
' to get an appropiate filter from a file filter. Additionaly, the iFilterIndex
' may be used to retrieve the index of the selected filter (starting by 0).
' @param sDir used to retrieve the folder where the files are located.
' Folder path does not contain the final backslash "\"
' @param bMultiSelect whether multiple files selection is allowed
' @param sFilter used to determine which files will be shown
' @param iFilterIndex used to determine which is the initially selected filter
' and to retrieve the index of the selected filter
' @param sDialogTitle the caption of the dialog title bar
' @param fOwner the Form that owns the dialog. If true, the dialog will be shown
' in vbModal mode
' @return an array containing the title of the files
Public Function ShowOpenDialog(sDir As String, _
                Optional ByVal bMultiSelect As Boolean, _
                Optional ByVal sFilter As String, _
                Optional iFilterIndex As Integer = 0, _
                Optional ByVal sDialogTitle As String, _
                Optional fOwner As Form) As String()
                    
    Dim i As Integer
    Dim bDlgResult As Boolean
    Dim c As New cCommonDialog
    Dim sFiles As String
    Dim asFiles() As String
    Dim asFileTitles() As String
    Dim lOwner As Long
    Dim lFilterIndex As Long
    
    Const PROC_NAME As String = "ShowOpenDialog"
    
    On Error GoTo EH
    
    If Not fOwner Is Nothing Then lOwner = fOwner.hWnd
    
    ' NOTE: VBGetOpenFileName function returns the file names in the first
    ' parameter. The behaviour is different depending of AllowMultiSelect param
    '   - If false, sFiles will contain the absolute path of the file
    '   - If true, sFiles will contain the Path of the current list and all the
    '     selected files in the following format:
    '       "X:\...\CurrentDir" & vbNullChar & "file1.ext" & ... & "fileN.ext"
    lFilterIndex = iFilterIndex + 1
    bDlgResult = c.VBGetOpenFileName(sFiles, , , bMultiSelect, , , sFilter, _
            lFilterIndex, , sDialogTitle, , lOwner)
    
    If bDlgResult = False Then
        Exit Function
    End If
    
    ' Return the selected filter in base 0
    iFilterIndex = lFilterIndex - 1
    
    ' Separate the path and the files
    asFiles = Split(sFiles, vbNullChar)
    
    If UBound(asFiles) = 0 Then ' Only one element -> Only one file
        ReDim asFileTitles(0) As String
        sDir = Left(asFiles(0), InStrRev(asFiles(0), "\") - 1)
        asFileTitles(0) = Right(asFiles(0), Len(asFiles(0)) - Len(sDir) - 1)
    Else ' Multiple files selected
        ReDim asFileTitles(UBound(asFiles) - 1) As String
        sDir = asFiles(0)
        For i = 1 To UBound(asFiles)
            asFileTitles(i - 1) = asFiles(i)
        Next
    End If
    
    ShowOpenDialog = asFileTitles()
    Exit Function
    
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function
