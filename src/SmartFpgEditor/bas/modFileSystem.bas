Attribute VB_Name = "modFileSystem"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private Const MODULE_NAME As String = "SmartFpgEditor.modFileSystem"

Private Declare Function GetLongPathName Lib "kernel32.dll" _
    Alias "GetLongPathNameA" (ByVal lpszShortPath As String, _
    ByVal lpszLongPath As String, ByVal cchBuffer As Long) As Long
    
Private Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" ( _
    ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
    
Private m_sCachedTemporalFolder As String


' Creates an absolute path to the sFileTitle file by joining it to sDir. sDir
' can end with or without a backslash "\". If the sFileTitle param is omitted
' the function returns sDir with the ending backslash.
' @param sDir the directoy of the file
' @param sFileTitle the file title (file name without the path).
' @return the path to the file resulting from joining sDir and sFileTitle
Public Function BuildPath(ByVal sDir As String, _
        Optional ByVal sFileTitle As String) As String
    
    Const PROC_NAME As String = "BuildPath"
    
    On Error GoTo EH
    
    sDir = IIf(Right(sDir, 1) = "\", sDir, sDir & "\")
    
    If Len(sFileTitle) > 0 Then
        sFileTitle = IIf(Left(sFileTitle, 1) = "\", _
                Right(sFileTitle, Len(sFileTitle) - 1), sFileTitle)
    End If
            
    BuildPath = sDir & sFileTitle
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Gets the extension of the specified file
' @param sFile the file name or path
' @return the extension of the file, i.e. the characters after the last dot of
' the filename
Public Function GetFileExtension(ByVal sFile As String) As String
    Dim i As Integer
    
    Const PROC_NAME As String = "GetFileExtension"
    
    On Error GoTo EH
    
    i = InStrRev(sFile, ".")
    If i > 0 Then
        GetFileExtension = Right(sFile, Len(sFile) - i)
    End If
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Retrieves the file title of a file path
' @param sPath the path of the file
' @param bWithoutExt if true, the returned file title will not contain the extension
' @return the file title, with or without the extension
Public Function GetFileTitle(ByVal sPath As String, _
        Optional ByVal bWithoutExt As Boolean) As String
    
    Const PROC_NAME As String = "GetFileTitle"
    
    Dim p As Integer
    Dim s As String
    
    p = InStrRev(sPath, "\")
    If p > 0 And Len(sPath) > p Then
        s = Right(sPath, Len(sPath) - p)
    End If
    
    If bWithoutExt Then
        p = InStrRev(s, ".")
        If p > 0 And Len(s) > p Then
            s = Left(s, p - 1)
        End If
    End If
    
    GetFileTitle = s
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Retrieves windows temporal folder.
' @return a path pointing to windows temporal folder
Public Property Get WinTempFolderPath() As String
    Dim sPath As String
    
    Const PROC_NAME As String = "GetWinTempFolderPath"
    On Error GoTo EH
    
    ' Temporal folder is not goind to change throught the
    ' application lifetime. Store it.
    If m_sCachedTemporalFolder <> "" Then
        sPath = m_sCachedTemporalFolder
    Else
        If Environ("tmp") <> "" Then
            sPath = Environ("tmp")
        ElseIf Environ("temp") <> "" Then
            sPath = Environ("temp")
        Else
            sPath = CurDir()
            ' TODO: Perhaps the temporal folder should be
            ' configured in configuration file?
        End If
        
        sPath = GetFullPath(sPath)
        m_sCachedTemporalFolder = sPath
    End If
    
    WinTempFolderPath = sPath
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Gets the full path for a certain path
' Note: Currently limited to 260 characters because we are using
' ANSI versions of Win32API.
' TODO: Consider using a unicode API version. Visit
' to http://support.microsoft.com/en-us/kb/145727
Public Function GetFullPath(ByVal sPath As String)
    Dim sBuffer As String, lRet As Long
    
    Const PROC_NAME As String = "GetFullPath"
    On Error GoTo EH

    sBuffer = Space(259)
    lRet = GetLongPathName(sPath, sBuffer, 259)
    
    If Not lRet = 0 Then
        sBuffer = Left(sBuffer, lRet)
        GetFullPath = sBuffer
    Else
        ' TODO: Raise error
    End If
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Determines if we have permissions on the temp folder to write files
Public Function IsTempFolderWriteable() As Boolean
    Const PROC_NAME As String = "IsTempFolderWriteable"
    On Error GoTo EH

    If DirExists(WinTempFolderPath) Then
        IsTempFolderWriteable = (GetAttr(WinTempFolderPath) And vbReadOnly) = 0
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Determines if a file exists
Public Function FileExists(sFile As String) As Boolean
    Const PROC_NAME As String = "FileExists"
    On Error GoTo EH

    FileExists = (GetAttr(sFile) And vbDirectory) = 0
    Exit Function
EH:
    'Returns False if an error occurs
End Function

' Determines if a file exists
Public Function DirExists(sDir As String) As Boolean
    Const PROC_NAME As String = "DirExists"
    On Error GoTo EH
    
    DirExists = GetAttr(sDir) And vbDirectory
    Exit Function
EH:
    'Returns False if an error occurs
End Function

' Gets the temporal folder using Win32Api
' Note: Currently limited to 260 characters because we are using
' ANSI versions of Win32API.
' TODO: Consider using a unicode API version. Visit
' to http://support.microsoft.com/en-us/kb/145727
Private Function GetTempFolderPath() As String
    Dim sPath As String, lRet As Long
    'Create a buffer
    sPath = String(500, Chr$(0))
    'Get the temporary path
    lRet = GetTempPath(500, sPath)
    
    If lRet = 0 Then
        Err.Raise
    End If
    
    'strip the rest of the buffer
    sPath = Left$(sPath, lRet)
    
    GetTempFolderPath = sPath
End Function

' Fault tolerant verification of GZip header
Public Function IsFileGzip(sFileName As String) As Boolean
    Dim bResult As Boolean
    Dim n As Integer
    Dim b As Byte
    n = FreeFile()
    
    On Error GoTo EH
        
    Open sFileName For Binary Access Read Lock Read Write As #n
    If LOF(n) >= 2 Then
        Get #n, , b
        If b = &H1F Then
            Get #n, , b
            If b = &H8B Then
                bResult = True
            End If
        End If
    End If
    Close n
    
    IsFileGzip = bResult

    Exit Function

EH:
    Err.Clear
    Close n
End Function
