Attribute VB_Name = "modConfiguration"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private Const MODULE_NAME As String = "SmartFpgEditor.modConfiguration"

Private Const RID_CONFFILENOTFOUND As Integer = 109

Private oDefaultFpgEncoderOptions As New cOptionSet
Private bDefaultFpgEditorTransparentBg As Boolean

Public Sub LoadConf()
    Dim oXml As CKXML
    
    Dim oMainTag As XMLTag
    Dim oMainTags As XMLTags
    Dim oFpgEditorTag As XMLTag
    Dim oFiltersTag As XMLTag
    Dim oDialogTag As XMLTag
    Dim oEncodersTag As XMLTag
    Dim oOptionTag As XMLTag
    
    Dim oTags As XMLTags
    Dim oTag As XMLTag
    Dim s As String
    
    Dim sOptionKey As String
    Dim sOptionValue As String
    
    Const PROC_NAME As String = "LoadConf"
    
    On Error GoTo EH
    
    ' Load configuration file
    Set oXml = New CKXML
    
    oXml.OpenXML BuildPath(App.Path, "conf.xml")
    
    Set oMainTag = oXml.Tags.GetTag("SmartFpgEditor")
    Set oMainTags = oMainTag.SubTags
    
    ' FPG EDITOR CONFIGURATION

    Set oFpgEditorTag = oMainTags.GetTag("FpgEditor")
    If Not oFpgEditorTag Is Nothing Then
        For Each oOptionTag In oFpgEditorTag.SubTags.GetTags("Option")
            sOptionKey = oOptionTag.Elements("key")
            sOptionValue = oOptionTag.Elements("value")
            ProcessFpgEditorOption sOptionKey, sOptionValue
        Next
    End If
    
    ' FILE FILTERS DETINITIONS
    Set oFiltersTag = oMainTags.GetTag("Filters")
    For Each oTag In oFiltersTag.SubTags.GetTags("Filter")
        ' CleanSpaces is used to remove blank spaces when the value
        ' is splitted within several lines, but this is only done
        ' for extension.
        AddFileFilter oTag.Elements("id"), oTag.Elements("desc"), _
            CleanSpaces(oTag.Elements("ext"))
    Next
    
    ' COMMON DIALOGS FILE FILTERS
    Set oTags = oMainTags.GetTag("OpenSaveDialogs").SubTags
    
    ' Open Fpg Dialog
    Set oDialogTag = oTags.GetTag("OpenFpg")
    s = ""
    For Each oTag In oDialogTag.SubTags.GetTags("filter")
        s = s & BuildDialogFilter(oTag.value) & "|"
    Next
    modFileFilters.G_sOpenFpgDlgFilter = Left(s, Len(s) - 1)
    
    ' Save Fpg Dialog
    Set oDialogTag = oTags.GetTag("SaveFpg")
    s = ""
    For Each oTag In oDialogTag.SubTags.GetTags("filter")
        s = s & BuildDialogFilter(oTag.value) & "|"
    Next
    modFileFilters.G_sSaveFpgDlgFilter = Left(s, Len(s) - 1)
    
    ' Open Palette Dialog
    Set oDialogTag = oTags.GetTag("OpenPalette")
    s = ""
    For Each oTag In oDialogTag.SubTags.GetTags("filter")
        s = s & BuildDialogFilter(oTag.value) & "|"
    Next
    modFileFilters.G_sOpenPaletteDlgFilter = Left(s, Len(s) - 1)
    
    ' Open Graphic Dialog
    Set oDialogTag = oTags.GetTag("OpenGraphic")
    s = ""
    For Each oTag In oDialogTag.SubTags.GetTags("filter")
        s = s & BuildDialogFilter(oTag.value) & "|"
    Next
    modFileFilters.G_sOpenGraphicDlgFilter = Left(s, Len(s) - 1)
    
    ' Save Graphic Dialog
    Set oDialogTag = oTags.GetTag("SaveGraphic")
    s = ""
    For Each oTag In oDialogTag.SubTags.GetTags("filter")
        s = s & BuildDialogFilter(oTag.value) & "|"
    Next
    modFileFilters.G_sSaveGraphicDlgFilter = Left(s, Len(s) - 1)
    
    ' Save Palette Dialog
    Set oDialogTag = oTags.GetTag("SavePalette")
    s = ""
    For Each oTag In oDialogTag.SubTags.GetTags("filter")
        s = s & BuildDialogFilter(oTag.value) & "|"
    Next
    modFileFilters.G_sSavePaletteDlgFilter = Left(s, Len(s) - 1)
    
    ' Encoder Options
    Dim sClassName As String
    Set oEncodersTag = oMainTags.GetTag("Encoders")
    If Not oEncodersTag Is Nothing Then
        For Each oTag In oEncodersTag.SubTags.GetTags("Encoder")
            sClassName = oTag.Elements("class")
            For Each oOptionTag In oTag.SubTags.GetTags("Option")
                sOptionKey = oOptionTag.Elements("key")
                sOptionValue = oOptionTag.Elements("value")
                RegisterEncoderOption sClassName, sOptionKey, sOptionValue
            Next
        Next
    End If
    
    Exit Sub
EH:
    ' Attempt to use a non-initialized object means a problem loading or
    ' parsing the file
    If Err.Number = 91 Then
        MsgBox App.Title & " " & RID_CONFFILENOTFOUND, vbCritical
        End
    Else
        Err.Show MODULE_NAME, PROC_NAME
    End If
End Sub

' Removes spaces, tab, newline+linefeed and newline characters
Private Function CleanSpaces(ByVal sText As String)
    Dim sResult As String
    
    Const PROC_NAME As String = "CleanSpaces"
    On Error GoTo EH
    
    ' TODO: This method should better look for the newline+linefeed
    ' character, then remove the spaces until next non space character
    sResult = Replace(sText, " ", "")
    sResult = Replace(sResult, vbTab, "")
    sResult = Replace(sResult, vbCrLf, "")
    sResult = Replace(sResult, vbCr, "")

    CleanSpaces = sResult

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function RegisterEncoderOption(ByVal sClassName As String, _
    ByVal sOptionKey As String, ByVal sOptionValue As String)

    Const PROC_NAME As String = "RegisterEncoderOption"
    On Error GoTo EH

    If sClassName = "cDefaultFpgEncoder" Then
        oDefaultFpgEncoderOptions.SetOption sOptionKey, sOptionValue
        ' TODO: Might fail if key is repeated
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Sub ProcessFpgEditorOption(ByVal sOptionKey As String, _
    ByVal sOptionValue As String)
    
    Const PROC_NAME As String = "ProcessFpgEditorOption"

    On Error GoTo EH
    
    If sOptionKey = "transparent-bg" Then
        bDefaultFpgEditorTransparentBg = IIf(LCase(sOptionValue) = "true", True, False)
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Function DefaultFpgEditorTransparentBg() As Boolean
    DefaultFpgEditorTransparentBg = bDefaultFpgEditorTransparentBg
End Function

' TODO: Must be converted to Property, but does not work in modules
Public Function DefaultFpgEncoderOptions() As cOptionSet
    Set DefaultFpgEncoderOptions = oDefaultFpgEncoderOptions
End Function

