Attribute VB_Name = "mnuActions"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit
Option Base 0

'===============================================================================
' MODULE OVERVIEW
'===============================================================================
' Methods associated with menu and toolbar actions.
'===============================================================================

Private Declare Sub CopyMemoryL Lib "kernel32" Alias "RtlMoveMemory" ( _
        ByVal pDest As Long, ByVal pSrc As Long, ByVal ByteLen As Long)

Private Const MODULE_NAME As String = "SmartFpgEditor.mnuActions"

Private Const RID_INVALIDFPG_MSG As Integer = 108
Private Const RID_INVALIDMAP_MSG As Integer = 167
Private Const RID_CANNOTOPENFPG_MSG As Integer = 170
Private Const RID_REMOVEGRAPHICS_MSG As Integer = 168
Private Const RID_UNTITLED As Integer = 184
Private Const RID_RENAMEPROMPT As Integer = 188
Private Const RID_RENAMEPROMPTTITLE As Integer = 189

' Opens an FPG. If f is not used, the FPG will open in a new window
Public Sub OpenFpg(ByVal sFileName As String, Optional f As frmFpgEditor)

    Const PROC_NAME As String = "OpenFpgAction"
    
    Dim oDecoder As IFpgDecoder
    Dim oFpg As cFpg

    On Error GoTo EH
    
    Set oDecoder = GetDecoder(sFileName, eblDecoderTypeFpg)
    If Not oDecoder Is Nothing Then
        Set oFpg = CreateFpgFromFile(sFileName, oDecoder)
        If f Is Nothing Then
            Set f = NewFpgEditorWindow(oFpg)
        Else
            f.fp.SetFpg oFpg
        End If
        ' Make the compression option to be adjusted to
        ' the type of the file
        f.UseFpgCompression = IsFileGzip(sFileName)
        f.fp.SetFocus
        f.Filename = sFileName
        f.IsDirty = False
    Else
        MsgBox RID_INVALIDFPG_MSG, vbExclamation, _
            PA2S("%FILE%", sFileName)
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Shows the AddGraphic dialog
Public Sub AddGraphic(f As frmFpgEditor, oMap As cMap, _
        Optional ByVal lCode As Long = -1)
        
    Const PROC_NAME As String = "AddGraphic"
    
    On Error GoTo EH
    
    If Not f.fp.FpgAvailable Or oMap Is Nothing Then Exit Sub
    
    ' The code has not been specified
    If lCode = -1 Then
        If f.fp.SelectedIndex < 0 Then ' Look for a free code
            lCode = f.fp.GetFpg.FreeCode
        Else ' Use the selected graphic code
            lCode = f.fp.GetFpg.Maps(f.fp.SelectedIndex).Code
        End If
    End If
        
    Debug.Assert lCode <= bennulib.MAX_CODE And lCode >= bennulib.MIN_CODE

    f.fp.LockRePaint ' To avoid flickering
    
    ' Show the add graphic dialog for the created MAP
    If ShowAddGraphicDialog(f.fp.GetFpg, oMap, f, lCode) Then
        f.fp.AddMap oMap, lCode, True
        f.IsDirty = True
    End If
    
    f.fp.UnlockRePaint
    f.fp.RePaint
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Exports the palette of the fpg using the specified encoder
Public Sub ExportPaletteToType(f As frmFpgEditor, Optional oEncoder As IPaletteEncoder)
    
    Dim oStdEncoder As IStandardEncoder
    Dim sFileName As String
    Dim oPal As cPalette
    
    Const PROC_NAME As String = "ExportPaletteToType"
    
    On Error GoTo EH
    
    Debug.Assert f.fp.FpgAvailable
    Debug.Assert f.fp.GetFpg.Depth = 8
    
    If f.fp.FpgAvailable = False Then Exit Sub
    If f.fp.GetFpg.Depth <> 8 Then Exit Sub
    
    Set oPal = f.fp.GetFpg.GetPaletteCopy
    
    If oEncoder Is Nothing Then Set oEncoder = New cDefaultPaletteEncoder
    
    Set oStdEncoder = oEncoder
    
    sFileName = IIf(f.Filename = "", FormatResString(RID_UNTITLED), GetFileTitle(f.Filename, True))
    
    If ShowSaveDialog(sFileName, _
            BuildDialogFilter(oStdEncoder.DefaultFileExtension, "ALL"), _
            oStdEncoder.DefaultFileExtension, "Extract Palette", f) Then
        
        bennulib.SavePalette oPal, sFileName, oEncoder
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Exports a graphic to a file using the specified encoder
Public Sub ExportGraphicToType(f As frmFpgEditor, ByVal lMapIndex As Long, _
        oEncoder As IMapEncoder)
    
    Dim oMap As cMap
    Dim oStdEncoder As IStandardEncoder
    Dim sFileName As String
    
    Const PROC_NAME As String = "ExportGraphicToType"
    
    On Error GoTo EH

    ' If FPG not available, cancel
    If f.fp.FpgAvailable = False Then Exit Sub

    ' If no selection, cancel
    If f.fp.SelectionCount <= 0 Then Exit Sub
    
    If oEncoder Is Nothing Then Set oEncoder = New cDefaultMapEncoder
    
    Set oStdEncoder = oEncoder
    
    Set oMap = f.fp.GetFpg.Maps(lMapIndex)
    
    If Len(oMap.Description) < 1 Then
        sFileName = "Untitled" & "." & oStdEncoder.DefaultFileExtension
    Else
        sFileName = oMap.Description & "." & oStdEncoder.DefaultFileExtension
    End If
    
    If ShowSaveDialog(sFileName, _
            BuildDialogFilter(oStdEncoder.DefaultFileExtension, "ALL"), _
            oStdEncoder.DefaultFileExtension, "Extract Graphic", f) Then
        
        bennulib.SaveMap oMap, sFileName, oEncoder
    End If

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Invokes AddGraphic for each file of the asFiles array
Public Sub AddGraphicFromFiles(f As frmFpgEditor, asFiles() As String)
    Dim oMap As cMap
    Dim oDecoder As IMapDecoder
    Dim i As Integer
    
    Const PROC_NAME As String = "AddGraphicFromFiles"
    
    On Error GoTo EH
    
    If Not f.fp.FpgAvailable Then Exit Sub
    
    For i = 0 To UBound(asFiles)
        Set oDecoder = GetDecoder(asFiles(i), eblDecoderTypeMap)
        
        If Not oDecoder Is Nothing Then
            Set oMap = CreateMapFromFile(asFiles(i), oDecoder)
            AddGraphic f, oMap
        Else
            MsgBox RID_INVALIDMAP_MSG, vbExclamation, _
                PA2S("%FILE%", asFiles(i))
        End If
    Next
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Purpose: Adds a graphic from a Multiple Maps stream of bytes (used
' to exange data with the clipboard and drag&drop inside sfpge
Public Sub AddGraphicFromMMapsStream(f As frmFpgEditor, avMMaps() As Byte)
    Const PROC_NAME As String = "AddGraphicFromMMapsStream"
    
    Dim oMap As cMap
    Dim oMapCol As New Collection
    Dim lNumMaps As Long
    Dim lCodes() As Long
    Dim lStreamLength As Long
    Dim avStream() As Byte
    Dim lMMapsPtr As Long
    Dim l As Long
    
    On Error GoTo EH
    
    Debug.Assert f.fp.FpgAvailable
    Debug.Assert UBound(avMMaps) >= LBound(avMMaps)
    
    ' TODO: We should include a kind of "header" for the stream of maps
    ' to prevent errors when using this procedure
    
    ' IMPORTANT: VB DataObject object retrieves a 1-based array so it is
    ' important to use LBound because we don't know if avMMaps will be
    ' 0 or 1 based.
    lMMapsPtr = VarPtr(avMMaps(LBound(avMMaps)))
    CopyMemoryL VarPtr(lNumMaps), lMMapsPtr, 4
    lMMapsPtr = lMMapsPtr + 4
    
    ReDim lCodes(lNumMaps - 1) As Long
    
    Debug.Assert (lNumMaps > 0)
    
    ' Read the maps
    For l = 0 To lNumMaps - 1
        CopyMemoryL VarPtr(lCodes(l)), lMMapsPtr, 4
        lMMapsPtr = lMMapsPtr + 4
        CopyMemoryL VarPtr(lStreamLength), lMMapsPtr, 4
        lMMapsPtr = lMMapsPtr + 4
        ReDim avStream(lStreamLength - 1) As Byte
        CopyMemoryL VarPtr(avStream(0)), lMMapsPtr, lStreamLength
        lMMapsPtr = lMMapsPtr + lStreamLength
        Set oMap = CreateMapFromSerializedMap(avStream)
        oMapCol.Add oMap, CStr(lCodes(l))
    Next
    
    Set oMap = Nothing
    l = 0
    For Each oMap In oMapCol
        AddGraphic f, oMap, lCodes(l)
        l = l + 1
    Next
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Function GetMMapsStreamForSelectedGraphics(f As frmFpgEditor) As Byte()
    Const PROC_NAME As String = "GetMMapsStreamForSelectedGraphics"
    
    Dim alSelIndexes() As Long
    
    Dim avMMaps() As Byte ' Will hold the byte array copied to the clipboard
    Dim lMMapsPtr As Long
    Dim lMMapsStreammSize As Long
    
    Dim oMMapsCol As New Collection
    
    Dim av() As Byte
    Dim avStream As Variant ' To access a serialized byte array in oMMapsCol
    Dim lStreamSize As Long
    Dim lCode As Long
    
    Dim l As Long
    
    On Error GoTo EH
    
    Debug.Assert f.fp.SelectionCount > 0
    
    ' Create a collection with the serialized maps byte streams
    alSelIndexes = f.fp.GetSelectedIndexes
    For l = 0 To f.fp.SelectionCount - 1
        avStream = f.fp.GetFpg.Maps(alSelIndexes(l)).Serialize
        lMMapsStreammSize = lMMapsStreammSize + _
            (UBound(avStream) - LBound(avStream) + 1)
        oMMapsCol.Add avStream
    Next
    
    ' Create an array of bytes to hold all the data that will be copied
    ' 4 bytes for the number of elements
    ' Plus 4 bytes for each graphic indicating the code
    ' Plus 4 bytes for each graphic indicating the size of the serialized map
    ' Plus the size required to hold all the serialized objects
    ReDim avMMaps(4 + oMMapsCol.Count * (4 + 4) + lMMapsStreammSize)
    lMMapsPtr = VarPtr(avMMaps(0))
    CopyMemoryL lMMapsPtr, VarPtr(oMMapsCol.Count), 4
    lMMapsPtr = lMMapsPtr + 4
    
    l = 0
    For Each avStream In oMMapsCol
        lCode = f.fp.GetFpg.Maps(alSelIndexes(l)).Code
        CopyMemoryL lMMapsPtr, VarPtr(lCode), 4 ' Code
        lMMapsPtr = lMMapsPtr + 4
        
        lStreamSize = UBound(avStream) - LBound(avStream) + 1
        CopyMemoryL lMMapsPtr, VarPtr(lStreamSize), 4
        lMMapsPtr = lMMapsPtr + 4
        
        av = avStream
        CopyMemoryL lMMapsPtr, VarPtr(av(0)), lStreamSize
        lMMapsPtr = lMMapsPtr + lStreamSize
        
        l = l + 1
    Next
     
    GetMMapsStreamForSelectedGraphics = avMMaps
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

'===============================================================================

' New Fpg Action
' @param f an FPG Editor form
' @param iDepth initially selected depth
Public Sub NewFpgAction(f As frmFpgEditor, _
        Optional ByVal iDepth As Integer = 16)
    
    Const PROC_NAME As String = "NewFpgAction"
    
    Dim oPalette As cPalette
    Dim fEditor As frmFpgEditor
    
    On Error GoTo EH
    
    If ShowNewFpgDialog(iDepth, oPalette, f) Then
        If Not f.AskToSaveIfDirty Then Exit Sub
        
        If f.fp.FpgAvailable Then
            Set fEditor = NewFpgEditorWindow()
        Else
            Set fEditor = f
        End If
        
        If iDepth = 8 Then
            fEditor.fp.NewFpg ebcDepthMode8bpp, oPalette
        ElseIf iDepth = 16 Then
            fEditor.fp.NewFpg ebcDepthMode16bpp
        ElseIf iDepth = 32 Then
            fEditor.fp.NewFpg ebcDepthMode32bpp
        End If
        fEditor.Filename = ""
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' New 8bpp Fpg Action
' @param f an FPG Editor form
Public Sub NewFpg8Action(f As frmFpgEditor)
    Const PROC_NAME As String = "NewFpg8Action"
    
    On Error GoTo EH
    
    NewFpgAction f, 8

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' New 16bpp Fpg Action
' @param f an FPG Editor form
Public Sub NewFpg16Action(f As frmFpgEditor)
    Const PROC_NAME As String = "NewFpg16Action"
    
    Dim fEditor As frmFpgEditor
    
    On Error GoTo EH
    
    If Not f.AskToSaveIfDirty Then Exit Sub
    
    If f.fp.FpgAvailable Then
        Set fEditor = NewFpgEditorWindow()
    Else
        Set fEditor = f
    End If
    fEditor.fp.NewFpg 16
    fEditor.Filename = ""
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' New 32bpp Fpg Action
' @param f an FPG Editor form
Public Sub NewFpg32Action(f As frmFpgEditor)
    Const PROC_NAME As String = "NewFpg32Action"
    
    Dim fEditor As frmFpgEditor
    
    On Error GoTo EH
    
    If Not f.AskToSaveIfDirty Then Exit Sub
    
    If f.fp.FpgAvailable Then
        Set fEditor = NewFpgEditorWindow()
    Else
        Set fEditor = f
    End If
    fEditor.fp.NewFpg 32
    fEditor.Filename = ""
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub DuplicateFpgAction(f As frmFpgEditor)
    Const PROC_NAME As String = "DuplicateFpgAction"
    
    On Error GoTo EH
    
    NewFpgEditorWindow f.fp.GetFpg.GetCopy
    
    ' TODO: Control Points are not copied since GetCopy does not currently support CPs copying
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Open Fpg Action
' @param f an FPG Editor form
Public Sub OpenFpgAction(f As frmFpgEditor)
    Const PROC_NAME As String = "OpenFpgAction"
    
    Dim sDir As String
    Dim asFileTitles() As String
    Dim sFileName As String
    
    On Error GoTo EH
    
    If Not f.AskToSaveIfDirty Then Exit Sub
    
    asFileTitles() = ShowOpenDialog(sDir, False, G_sOpenFpgDlgFilter, , , f)
    If sDir <> "" Then
        sFileName = BuildPath(sDir, asFileTitles(0))
        If f.fp.FpgAvailable Then
            OpenFpg sFileName
        Else
            OpenFpg sFileName, f
        End If
    End If
    
    Exit Sub
EH:
'    Select Case (Err.Number Xor vbObjectError) ' Handle custom errors
'    Case bennulib.E_BL_ErrorConstants.eblCannotOpenFile, _
'            bennulib.E_BL_ErrorConstants.eblUnsupportedVersion, _
'            bennulib.E_BL_ErrorConstants.eblNotAnFpgFile, _
'            bennulib.E_BL_ErrorConstants.eblPaletteTruncated, _
'            bennulib.E_BL_ErrorConstants.eblGammaTruncated, _
'            bennulib.E_BL_ErrorConstants.eblCpInfoTruncated, _
'            bennulib.E_BL_ErrorConstants.eblAnimationFlagActive, _
'            bennulib.E_BL_ErrorConstants.eblBitmapDataTruncated
'
'        MsgBox RID_CANNOTOPENFPG_MSG, vbCritical, _
'            PA2S("%FILE%", asFileTitles(0), "%DESC%", Err.Description)
'    End Select
    
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Private Sub saveFpg(f As frmFpgEditor)

    Const PROC_NAME As String = "saveFpg"

    On Error GoTo EH
    
    Dim encoderOptions As New cOptionSet
    If f.UseFpgCompression Then
        encoderOptions.SetOption "use-gzip", "True"
    Else
        encoderOptions.SetOption "use-gzip", "False"
    End If
    encoderOptions.SetOption "compression-level", _
        DefaultFpgEncoderOptions("compression-level").sValue

    f.fp.saveFpg f.Filename, , encoderOptions
    f.IsDirty = False

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub SaveFpgAction(f As frmFpgEditor)
    Const PROC_NAME As String = "SaveFpgAction"

    On Error GoTo EH
    
    If f.Filename = "" Then
        SaveFpgAsAction f
        Exit Sub
    End If
    
    saveFpg f
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub SaveFpgAsAction(f As frmFpgEditor)
    Dim sFileName As String
    
    Const PROC_NAME As String = "SaveFpgAsAction"
    
    On Error GoTo EH
    
    sFileName = FormatResString(RID_UNTITLED)
    ShowSaveDialog sFileName, G_sSaveFpgDlgFilter, "fpg", , f
    
    If sFileName <> "" Then
        f.Filename = sFileName
        saveFpg f
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub ExportFpgToStrip(f As frmFpgEditor)
    Dim sFileName As String
    
    Const PROC_NAME As String = "ExportFpgToStrip"
    
    On Error GoTo EH
    
    sFileName = FormatResString(RID_UNTITLED)
    ShowSaveDialog sFileName, BuildDialogFilter("PNG"), "png", , f
    
    If sFileName <> "" Then
        f.fp.saveFpg sFileName, New cStripFpgEncoder
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Copies selected graphics to clipboard and removes them from the FPG
Public Sub CutAction(f As frmFpgEditor)
    Const PROC_NAME As String = "CutAction"
    
    On Error GoTo EH
    
    If CopyGraphics(f) Then
        RemoveGraphics f, f.fp.GetSelectedCodes
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Copies selected graphics to clipboard
Public Sub CopyAction(f As frmFpgEditor)
    Const PROC_NAME As String = "CopyAction"
    
    On Error GoTo EH
    
    CopyGraphics f
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Pastes a graphics from clipboard
Public Sub PasteAction(f As frmFpgEditor)
    Const PROC_NAME As String = "PasteAction"
    
    Dim oCC As New cCustomClipboard
    Dim avMMaps() As Byte
    
    On Error GoTo EH
    
    If Not oCC.ClipboardOpen(f.hWnd) Then Exit Sub
    
    If CFMMapsL <> 0 And _
                oCC.IsDataAvailableForFormat(CFMMapsL) Then ' Custom MMaps format
        
        If oCC.GetBinaryData(CFMMapsL, avMMaps) Then
            AddGraphicFromMMapsStream f, avMMaps
        End If
        
    ElseIf oCC.IsDataAvailableForFormat(CF_DIB) Then ' DIB Format
        oCC.ClipboardClose
        AddGraphic f, CreatemapFromOlePicture(Clipboard.GetData(vbCFDIB))
    End If
    
    oCC.ClipboardClose
    
    Exit Sub
EH:
    oCC.ClipboardClose
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Asks for files to show the addgraphic dialog
Public Sub AddGraphicAction(f As frmFpgEditor)
    Dim asFileTitles() As String
    Dim asFiles() As String
    Dim sDir As String
    Dim i As Integer

    Const PROC_NAME As String = "AddGraphicAction"
    
    On Error GoTo EH
        
    If Not f.fp.FpgAvailable Then Exit Sub
        
    ' Show the Open Dialog
    asFileTitles = ShowOpenDialog(sDir, True, G_sOpenGraphicDlgFilter, , , f)
    
    If sDir = "" Then Exit Sub ' User cancelled
    
    ReDim asFiles(UBound(asFileTitles)) As String
    
    ' Generate complete paths
    For i = 0 To UBound(asFileTitles)
        asFiles(i) = BuildPath(sDir, asFileTitles(i))
    Next
    
    AddGraphicFromFiles f, asFiles
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Opens the palette editor
Public Sub ViewEditPaletteAction(f As frmFpgEditor)
    
    Const PROC_NAME As String = "ViewEditPaletteAction"
    On Error GoTo EH
    
    Dim fEditor As frmPaletteEditor
    
    If f.PaletteEditor Is Nothing Then
        Set fEditor = New frmPaletteEditor
        Load fEditor
        ' The Fpg editor acts as the context to handle generated
        ' events of the Palette Editor
        Set f.PaletteEditor = fEditor
        Set fEditor.Context = f
    Else
        Set fEditor = f.PaletteEditor
    End If
    
    fEditor.SetPalette f.fp.GetFpg().GetPaletteCopy()
    fEditor.Show , f

    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub ViewEditGraphicAction(f As frmFpgEditor, ByVal lMapIndex As Long)
    Dim fEditor As New frmMapEditor
    
    Const PROC_NAME As String = "ViewEditGraphicAction"
    
    On Error GoTo EH
    
    Load fEditor
    f.AddMapEditor fEditor
    fEditor.mp.setmap f.fp.GetFpg.Maps(lMapIndex)
    fEditor.Show , f
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub EditControlPointsAction(f As frmFpgEditor)
    Dim iCount As Integer
    Dim alX() As Long
    Dim alY() As Long
    Dim alIds() As Long
    Dim lCode As Long
    Dim oFpg As cFpg
    Dim i As Integer
    
    Const PROC_NAME As String = "EditControlPointsAction"
    
    On Error GoTo EH
    
    ' Create the CP Editor if it does not exist yet
    If f.m_fCpEditor Is Nothing Then Set f.m_fCpEditor = New cCpEditor
    
    If f.fp.SelectionCount = 1 Then
        lCode = f.fp.GetSelectedCodes()(0)
        Set oFpg = f.fp.GetFpg
        
        With oFpg.Maps(oFpg.IndexByCode(lCode))
        
        iCount = .ControlPointsCount
        If iCount > 0 Then
            ReDim alX(iCount - 1) As Long
            ReDim alY(iCount - 1) As Long
            alIds() = .GetControlPointsIds()
            For i = 0 To iCount - 1
                .GetControlPoint alIds(i), alX(i), alY(i)
            Next
        End If
        f.m_fCpEditor.SetControlPoints iCount, alX, alY, alIds
        
        End With
    ElseIf f.fp.SelectionCount > 1 Then
        f.m_fCpEditor.MultipleEditingMode = True
    End If
    
    f.m_fCpEditor.ReplacementConfirmation = True
    f.m_fCpEditor.Show True, f.hWnd
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub RemoveGraphicAction(f As frmFpgEditor)
    Const PROC_NAME As String = "RemoveGraphicAction"
    
    Dim eMsgResult As VbMsgBoxResult
    
    On Error GoTo EH
    
    ' If no selection, cancel
    If f.fp.SelectionCount <= 0 Then Exit Sub
    
    eMsgResult = MsgBox(FormatResString(RID_REMOVEGRAPHICS_MSG), _
        vbYesNo Or vbQuestion)
            
    ' Remove if the user answered yes
    If eMsgResult = vbYes Then
        RemoveGraphics f, f.fp.GetSelectedCodes
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub NewWindowAction()
    Const PROC_NAME As String = "NewWindowAction"
    
    On Error GoTo EH
    
    NewFpgEditorWindow
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub ZoomInAction(f As frmFpgEditor)
    Const PROC_NAME As String = "ZoomInAction"
    
    On Error GoTo EH
    
    Zoom f, True
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub ZoomOutAction(f As frmFpgEditor)
    Const PROC_NAME As String = "ZoomOutAction"
    
    On Error GoTo EH
    
    Zoom f, False
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub RenameAction(f As frmFpgEditor)
    Const PROC_NAME As String = "RenameAction"
    
    Dim oMap As cMap
    Dim sDesc As String
    
    On Error GoTo EH
    
    Set oMap = f.fp.GetFpg.Maps(f.fp.SelectedIndex)
    sDesc = InputBox(FormatResString(RID_RENAMEPROMPT), _
            FormatResString(RID_RENAMEPROMPTTITLE, PA2S("%CODE%", CStr(oMap.Code))), _
            oMap.Description)
    sDesc = Left(sDesc, MAX_GRAPHIC_DESC_LENGTH) ' TODO: The maximum allowed chars whould be given by the bennulib library or the FPG
    
    ' TODO: With VB inputbox there is no way to distinguish between Cancel action and empty
    ' description... assume cancel...
    If oMap.Description <> sDesc And sDesc <> "" Then
        oMap.Description = sDesc
        f.fp.RePaint
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Purpose: Zooms In our out the fpg pane of a Fpg Editor
Private Sub Zoom(f As frmFpgEditor, ByVal bIn As Boolean)
    Const PROC_NAME As String = "Zoom"
    
    Dim bResizingAllowed As Boolean
    Dim lDelta As Long
    
    On Error GoTo EH
    
    With f.fp
    
    bResizingAllowed = (.CellWidth > f.RecommendedMinCellWidth _
            And .CellHeight > f.RecommendedMinCellHeight And bIn = False) _
            Or _
            (.CellWidth < f.RecommendedMaxCellWidth _
            And .CellHeight < f.RecommendedMaxCellHeight And bIn = True)
            
    If bResizingAllowed Then
        lDelta = 16 * IIf(bIn, 1, -1)
        .LockRePaint
        .ThumbHeight = .ThumbHeight + lDelta
        .ThumbWidth = .ThumbWidth + lDelta
        .CellHeight = .CellHeight + lDelta
        .CellWidth = .CellWidth + lDelta
        .NameWidth = .NameWidth + lDelta
        .UnlockRePaint
        .RePaint True
    End If
    
    End With
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

Public Sub ToggleTransparencyAction(f As frmFpgEditor)

    Const PROC_NAME As String = "ToggleTransparency"

    On Error GoTo EH
    
    f.fp.ShowTransparency = Not f.fp.ShowTransparency
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

' Purpose: Shows the New FPG dialog and captures its result. Returns true
' if the user selected Ok. Otherwise false. The depth selected is returned in
' the iDepth parameter and the Palette (if any), in the oPalette parameter.
' The iDepth parameter also allows to specify the initially selected depth
Private Function ShowNewFpgDialog(iDepth As Integer, oPalette As cPalette, _
            Optional fOwner As Form) As Boolean
    
    Dim f As New frmNewFpg
    
    Const PROC_NAME As String = "ShowNewFpgDialog"
    
    On Error GoTo EH
    
    Load f
    
    ' Initially selected depth
    Select Case iDepth
    Case 8
        f.op8.value = True
    Case 16
        f.op16.value = True
    Case 32
        f.op32.value = True
    Case Else
        Debug.Assert False
    End Select
    
    f.Show vbModal, fOwner
    
    If f.DialogResult = vbOK Then
        Set oPalette = f.pp.GetPalette
        iDepth = Switch(f.op8.value, 8, f.op16.value, 16, f.op32.value, 32)
        ShowNewFpgDialog = True
    End If
    
    Unload f
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Purpose: Shows the Add Map dialog and captures its result. Returns true
' if the user selected Ok. Otherwise false. The Code where the MAP is to be
' inserted is stored in lCode
Private Function ShowAddGraphicDialog(oFpg As cFpg, oMap As cMap, _
        fOwner As Form, lCode As Long) As Boolean
        
    Dim f As New frmAddMap
    
    Const PROC_NAME As String = "ShowAddGraphicDialog"
    
    On Error GoTo EH

    Load f

    Set f.Fpg = oFpg
    Set f.MapSrc = oMap ' Map to be added
    f.txtInsertAt.Text = CStr(lCode) ' Default insertion code
    
    f.Show vbModal, fOwner
    
    ' Capture the result
    If f.DialogResult = vbOK Then '
        lCode = CLng(f.txtInsertAt.Text)
        oMap.Description = f.txtDescription.Text
        ShowAddGraphicDialog = True
    End If
    
    Unload f
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Purpose: Creates a copy of the selected graphics in the clipboard
' @returns true if a copy was successfully created in sfpg custom format
Private Function CopyGraphics(f As frmFpgEditor) As Boolean
    Const PROC_NAME As String = "CopyGraphics"
    
    Dim oCC As New cCustomClipboard
    Dim avMMaps() As Byte
    
    On Error GoTo EH
    
    If f.fp.SelectionCount < 0 Then Exit Function
    
    Clipboard.Clear
    
    ' DIB
    Clipboard.SetData CreateOlePictureFromMap( _
        f.fp.GetFpg.Maps(f.fp.SelectedIndex)), vbCFDIB
    
    ' Custom format
    If CFMMapsL <> 0 And oCC.ClipboardOpen(f.hWnd) Then
        
        avMMaps = GetMMapsStreamForSelectedGraphics(f)
        If Not UBound(avMMaps) < LBound(avMMaps) Then
            CopyGraphics = oCC.SetBinaryData(CFMMapsL, avMMaps)
        Else
            ' TODO: error? No stream received
            Debug.Assert False
        End If
        
    Else ' Could not open clipboard or custom format not available
        ' TODO: Warning?
        Debug.Assert False
    End If
    
    oCC.ClipboardClose
    
    Exit Function
EH:
    oCC.ClipboardClose
    Err.Show MODULE_NAME, PROC_NAME
End Function

' Purpose: removes graphics from the fpg
Private Sub RemoveGraphics(f As frmFpgEditor, alCodes() As Long)
    Const PROC_NAME As String = "RemoveGraphics"
    
    Dim i As Integer
    
    On Error GoTo EH
    
    If UBound(alCodes) < LBound(alCodes) Then Exit Sub
    
    ' TODO: This will cause the selectionchanged event every time the map is removed...
    ' Should be called once only...
    f.fp.LockRePaint ' Avoids repaint every time one Map is removed
    For i = 0 To UBound(alCodes)
        f.fp.RemoveMapByCode alCodes(i)
    Next
    f.fp.UnlockRePaint
    f.fp.RePaint
    f.IsDirty = True
    f.fp.DeselectAll
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

