Attribute VB_Name = "modColorArrayEffects"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private Const MODULE_NAME As String = "SmartFpgEditor.modColorArrayEffects"

Private Sub ColorInvert(uColor As T_BL_RGBColor)
    Const PROC_NAME As String = "ColorInvert"

    On Error GoTo EH
    
    uColor.r = 255 - uColor.r
    uColor.g = 255 - uColor.g
    uColor.b = 255 - uColor.b

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub ColorGrayScale(uColor As T_BL_RGBColor)
    Dim vGrayTone As Byte
    
    Const PROC_NAME As String = "ColorGrayScale"

    On Error GoTo EH
    
    vGrayTone = CByte(CDbl(uColor.r) * 0.299 + CDbl(uColor.g) * 0.587 + CDbl(uColor.b) * 0.114)
    uColor.r = vGrayTone
    uColor.g = vGrayTone
    uColor.b = vGrayTone

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
Public Sub ColorArrayInvert(uColors() As T_BL_RGBColor)
    Dim i As Integer
    
    Const PROC_NAME As String = "ColorArrayInvert"

    On Error GoTo EH
    
    For i = LBound(uColors) To UBound(uColors)
        ColorInvert uColors(i)
    Next

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Public Sub ColorArrayGrayScale(uColors() As T_BL_RGBColor)
    Dim i As Integer
    
    Const PROC_NAME As String = "ColorArrayGrayScale"

    On Error GoTo EH
    
    For i = LBound(uColors) To UBound(uColors)
        ColorGrayScale uColors(i)
    Next

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Public Sub ColorArrayFillSolid(uColors() As T_BL_RGBColor, uColor As T_BL_RGBColor)
    
    Const PROC_NAME As String = "ColorArrayFillSolid"

    On Error GoTo EH
    
    Dim i As Integer
    
    For i = LBound(uColors) To UBound(uColors)
        uColors(i).r = uColor.r
        uColors(i).g = uColor.g
        uColors(i).b = uColor.b
    Next

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Public Sub ColorArrayFillGradient(uColors() As T_BL_RGBColor, _
    uStartColor As T_BL_RGBColor, uEndColor As T_BL_RGBColor)
    
    Dim i As Integer
    Dim lNColors As Long
    Dim dRedInc As Double, dGreenInc As Double, dBlueInc As Double
    
    Const PROC_NAME As String = "ColorArrayFillGradient"

    On Error GoTo EH
    
    lNColors = UBound(uColors) - LBound(uColors) + 1
    
    dRedInc = (CLng(uEndColor.r) - uStartColor.r) / CDbl(lNColors)
    dGreenInc = (CLng(uEndColor.g) - uStartColor.g) / CDbl(lNColors)
    dBlueInc = (CLng(uEndColor.b) - uStartColor.b) / CDbl(lNColors)
    
    uColors(0) = uStartColor
    uColors(UBound(uColors)) = uEndColor
    For i = LBound(uColors) + 1 To UBound(uColors) - 1
        uColors(i).r = CInt(uStartColor.r + dRedInc * (i - LBound(uColors)))
        uColors(i).g = CInt(uStartColor.g + dGreenInc * (i - LBound(uColors)))
        uColors(i).b = CInt(uStartColor.b + dBlueInc * (i - LBound(uColors)))
    Next

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
