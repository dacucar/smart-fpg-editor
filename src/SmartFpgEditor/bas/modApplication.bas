Attribute VB_Name = "modApplication"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Fpg Editor.
'
' Smart Fpg Editor is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Fpg Editor is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Fpg Editor. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================

Public Const TOOLBAR_STYLE = ECTBToolbarDrawStyle.CTBDrawNoVisualStyles

'===============================================================================

Private Const MODULE_NAME As String = "SmartFpgEditor.modMain"

Public Const CLIPBOARDFORMATNAME_MMAPS As String = "sfpge/mmaps" ' Multiple maps
Private Const SFPG_TEMP_SUBFOLDER As String = "~sfpge\"

Private m_lCFMMapsL  As Long

'===============================================================================

Public Property Get Version() As String
    On Error GoTo EH
    
    Const PROC_NAME As String = "Version"
    
    Version = App.Major & "." & App.Minor & "." & App.Revision
    
    Exit Property
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Property


' Retrieve application's temporal folder
Public Property Get TemporalFolder() As String
    
    Const PROC_NAME As String = "TemporalFolder"
    On Error GoTo EH

    TemporalFolder = BuildPath(modFileSystem.WinTempFolderPath) _
        & SFPG_TEMP_SUBFOLDER

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Note: Clipboard formats are unsigned ints. The cCustomClipboard
' API retrieves the format IDs as longs, but VB expects a signed
' interger then using clipboard or DD functios. Thus:
'     - When using VB Clipboard or DD functions, use CFMMaps
'     - When using cCustomClipboard class, use CFMMapsL
Public Property Get CFMMapsL() As Long
    Const PROC_NAME As String = "CFMMapsL"
    On Error GoTo EH
    
    CFMMapsL = m_lCFMMapsL
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Note: Clipboard formats are unsigned ints. The cCustomClipboard
' API retrieves the format IDs as longs, but VB expects a signed
' interger then using clipboard or DD functios. Thus:
'     - When using VB Clipboard or DD functions, use CFMMaps
'     - When using cCustomClipboard class, use CFMMapsL
Public Property Get CFMMaps() As Integer
    Const PROC_NAME As String = "CFMMaps"
    On Error GoTo EH
    
    CFMMaps = UInt32ToSInt16(m_lCFMMapsL)
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Function NewFpgEditorWindow(Optional oFpg As cFpg) As frmFpgEditor
    Dim f As New frmFpgEditor
    
    Const PROC_NAME As String = "NewFpgEditorWindow"
    
    On Error GoTo EH
    
    Load f
    f.fp.LockRePaint
    If Not oFpg Is Nothing Then
        f.fp.SetFpg oFpg
    End If
    f.fp.ShowTransparency = modConfiguration.DefaultFpgEditorTransparentBg()
    f.Show
    f.fp.UnlockRePaint
    
    Set NewFpgEditorWindow = f
    
    Exit Function
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Function

Public Sub Main()
    Const PROC_NAME As String = "Main"
    
    Dim oCC As cCustomClipboard
    Dim oCmdParser As New cCmdLineArgsParser
    Dim i As Integer
    
    On Error GoTo EH
    
    LoadConf

    ' Register clipboard formats
    Set oCC = New cCustomClipboard
    m_lCFMMapsL = oCC.AddFormat(CLIPBOARDFORMATNAME_MMAPS)
    
    SmartBennuCtls.CustomDDFormat = CFMMaps
    
    ' Parse command line argument string and decide what to do
    oCmdParser.Parse Command
    If oCmdParser.ArgC > 0 Then
        For i = 0 To oCmdParser.ArgC - 1 ' Each argument is a file
            If FileExists(oCmdParser.ArgV(i)) Then
                OpenFpg oCmdParser.ArgV(i), NewFpgEditorWindow
            Else
                ' TODO: What to do when the file does not exist?
            End If
        Next
    Else
        NewFpgEditorWindow ' Empty window
    End If
    
    Exit Sub
EH:
    Err.Show MODULE_NAME, PROC_NAME
End Sub

'===============================================================================
