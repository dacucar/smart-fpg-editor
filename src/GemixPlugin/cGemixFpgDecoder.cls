VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGemixFpgDecoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Sub CopyMemoryL Lib "kernel32" Alias "RtlMoveMemory" ( _
                        ByVal pDst As Long, _
                        ByVal pSrc As Long, _
                        ByVal ByteLen As Long)
                        
Implements IFpgDecoder
Implements IStandardDecoder

Private Function IFpgDecoder_Decode(ByVal sFileName As String) As bennulib.cFpg
    Dim lFile As Long
    Dim sMagicFileType As String * 3
    Dim avMagicDescriptor(4) As Byte
    Dim iDepth As Integer
    Dim avPalette() As Byte
    Dim avPaletteUnused() As Byte
    
    'MAP
    Dim lMapLen As Long
    Dim lWidth As Long, lHeight As Long
    Dim lCode As Long
    Dim sName As String * 256
    Dim sDescription As String * 256
    Dim lMapDataLength As Long
    Dim lFlags As Long
    Dim iNumCP As Integer ' Number of control points
    Dim aiControlPoints() As Integer
    Dim avData8() As Byte
    Dim aiData16() As Integer
    Dim alData32() As Long
    Dim lDataPtr As Long
    Dim iCPX As Integer, iCPY As Integer
    Dim lAux As Long
    
    Dim i As Integer
    Dim oMap As cMap
    Dim oPal As cPalette
    Dim oFpg As cFpg
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        'RaiseError blFpgFileCannotBeOpened, ClassName, sFileName & "|reading"
        MsgBox "Error"
        Stop
    End If
    
    ' Identify file as a valid FPG file
    gzread lFile, ByVal sMagicFileType, 3
    sMagicFileType = LCase(sMagicFileType)
    If Not sMagicFileType = "fpg" Then
        'RaiseError blNotFpgFile, ClassName
        MsgBox "Error"
        Stop
    End If
    
    ' FPG depth
    gzread lFile, avMagicDescriptor(0), 5
    iDepth = CInt(Hex(CStr(avMagicDescriptor(2))))
'    Stop
    ' Last byte of avMagicDescriptor represents the version
    ' TODO: if avMagicDescriptor(4) = ...
    
    ' Palette if 8bpp
    If iDepth = 8 Then
        ReDim avPalette(PALETTE_SIZE - 1) As Byte
        ReDim avPaletteUnused(PALETTE_UNUSEDBYTES - 1) As Byte
        If gzread(lFile, avPalette(0), PALETTE_SIZE) < PALETTE_SIZE Then
            ' RaiseError blPaletteIsCorruptedInMapFile, ClassName
            MsgBox "Error"
            Stop
        End If
        If gzread(lFile, avPaletteUnused(0), PALETTE_UNUSEDBYTES) _
                < PALETTE_UNUSEDBYTES Then
            'RaiseError blGammaDataIsCorruptedInFpgFile, ClassName
            MsgBox "Error"
            Stop
        End If
    End If
    
    ' Create the palette object
    If iDepth = 8 Then
        Set oPal = bennulib.CreatePaletteFromByteArray(avPalette, True)
    End If
    
    ' Create the FPG object
    Set oFpg = bennulib.CreateNewFpg(iDepth, oPal)
    
    Do
        gzread lFile, lCode, 4
        If (gzeof(lFile) <> 0) Then Exit Do ' EOF
        gzread lFile, lMapLen, 4
        gzread lFile, ByVal sDescription, 256
        gzread lFile, ByVal sName, 256
        gzread lFile, lAux, 4 ' ???? (still I haven't guess out what is it for)
        gzread lFile, lWidth, 4
        gzread lFile, lHeight, 4
        
        lMapDataLength = lWidth * lHeight * (iDepth / 8)
        
        ' Control Points
        ' First 12 bits of the iFlags short integer tells the higher id for the
        ' defined controlpoint
        ' Bit 13 set to 1 means that the map has animation activated (this is
        ' not supported by Fenix or Bennu)
        gzread lFile, lFlags, 4
        iNumCP = (lFlags And F_NCPOINTS)
        ReDim aiControlPoints(IIf(iNumCP > 0, iNumCP * 2 - 1, 1)) As Integer
        aiControlPoints(0) = lWidth \ 2 ' Default center X coord
        aiControlPoints(1) = lHeight \ 2 ' Default Center Y coord
        If iNumCP > 0 Then
            If gzread(lFile, aiControlPoints(0), iNumCP * 4) < (iNumCP * 4) Then
                ' RaiseError blControlPointsDataCorruptedInFpgFile, ClassName
                MsgBox "Error"
                Stop
            End If
        Else
            iNumCP = 1 ' At least we'll have the center
        End If
        
        ' Animation (not supported in Bennu/Fenix/Div so consider an error)
        If (lFlags And F_ANIMATION) = 1 Then
            'RaiseError blAnimationNotSupportedInMapFile, ClassName
            MsgBox "Error"
            Stop
        End If
        
        ' Bitmap data
        Select Case iDepth
        Case 8
            ReDim avData8(lWidth * lHeight - 1) As Byte
            lDataPtr = VarPtr(avData8(0))
        Case 16
            ReDim aiData16(lWidth * lHeight - 1) As Integer
            lDataPtr = VarPtr(aiData16(0))
        Case 32
            ReDim alData32(lWidth * lHeight - 1) As Long
            lDataPtr = VarPtr(alData32(0))
        End Select
        
        If gzread(lFile, ByVal lDataPtr, lMapDataLength) < lMapDataLength Then
            ' RaiseError blMapDataIsCorruptedInFpgFile, ClassName
            MsgBox "Error"
            
        End If
        
        ' 32 BPP Unpremultiply
'        Dim j As Integer
'        Dim r As Byte, g As Byte, b As Byte, Alpha As Byte, Ptr As Long
'        If iDepth = 32 Then
'            For i = 0 To lHeight - 1
'                For j = 0 To lWidth - 1
'                    Alpha = ShiftRightL(alData32(i * lWidth + j), 24)
'                    r = (ShiftRightL(alData32(i * lWidth + j), 16) And &HFF)
'                    g = (ShiftRightL(alData32(i * lWidth + j), 8) And &HFF)
'                    b = alData32(i * lWidth + j) And &HFF
'    '                Debug.Print Hex(alData32(i * lWidth + j))
'    '                Debug.Print Hex(Alpha), Hex(r), Hex(g), Hex(b)
'                    If Alpha > 0 Then
''                        r = r * (255 / Alpha)
''                        g = g * (255 / Alpha)
''                        b = b * (255 / Alpha)
'                        Ptr = VarPtr(alData32(i * lWidth + j))
'                        CopyMemoryL Ptr, VarPtr(b), 1
'                        CopyMemoryL Ptr + 8, VarPtr(g), 1
'                        CopyMemoryL Ptr + 16, VarPtr(r), 1
'                        CopyMemoryL Ptr + 24, VarPtr(Alpha), 1
'                        Debug.Print Hex(alData32(i * lWidth + j))
'    '                    Call CopyMemoryL(VarPtr(alData32(i * lWidth + j) + 8), VarPtr(g), 8)
'    '                    Call CopyMemoryL(VarPtr(alData32(i * lWidth + j) + 16), VarPtr(r), 8)
'    '                    Call CopyMemoryL(VarPtr(alData32(i * lWidth + j) + 24), VarPtr(Alpha), 8)
'    ''                    alData32(i * lWidth + j) = (Alpha * &H1000000) Or (r * &H10000) _
'    '                            Or (g * &H100&) Or b
'                    Else
'                        alData32(i * lWidth + j) = &H0
'                    End If
'                Next
'            Next
'        End If
        
        ' Create the MAP object
        Set oMap = bennulib.CreateMapFromBits(lWidth, lHeight, iDepth, lDataPtr, _
                    AsciiZToString(sDescription), oPal)
        
        ' Control points
        For i = 0 To iNumCP - 1
            iCPX = aiControlPoints(i * 2)
            iCPY = aiControlPoints(i * 2 + 1)
            If Not (iCPX = -1 And iCPY = -1) Then
                oMap.SetControlPoint i, iCPX, iCPY
            End If
        Next
        
        ' Add the MAP to the FPG
        If oFpg.Add(oMap, lCode, False) = False Then
            MsgBox "Hay dos MAPs con el mismo code!"
        End If
        
        ' Clean up
        Erase avData8
        Erase aiData16
        Erase alData32
        Erase aiControlPoints
        Set oMap = Nothing
        
    Loop
    
    gzclose lFile

    Erase avPalette
    Erase avPaletteUnused
    
    Set IFpgDecoder_Decode = oFpg
    
EH:
    If Err.Number <> 0 Then
        Set oFpg = Nothing
        Erase avData8
        Erase aiData16
        Erase alData32
        Erase aiControlPoints
        Erase avPalette
        Erase avPaletteUnused
        If lFile <> 0 Then gzclose lFile
        Err.Raise Err.Number, Err.Source, Err.Description
    End If
End Function

Private Function IStandardDecoder_CanRead(ByVal sFileName As String) As Boolean
    Const PROC_NAME As String = "IStandardDecoder_CanRead"
    Dim lFile As Long
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        'Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
        '    ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    IStandardDecoder_CanRead = ReadMagic(lFile)
    
    Call gzclose(lFile)
    
    Exit Function
EH:
    If lFile <> 0 Then gzclose (lFile)
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Reads the MAGIC of the file and optionally retrieves the Depth and
' the version. Returns false if the file is not a valid FPG file.
' NOTE: Use carefully. lFile must be a valid file id.
Private Function ReadMagic(ByVal lFile As Long, Optional iDepth As Integer, _
        Optional vVersion As Byte) As Boolean
    
    Const PROC_NAME As String = "ReadMagic"
    Dim sMagicFileType As String * 3
    Dim avMagicDescriptor(4) As Byte
    
    On Error GoTo EH
    
    Debug.Assert (lFile <> 0)
    
    ReadMagic = True
    
    ' 3 first bytes describe the depth of the FPG
    gzread lFile, ByVal sMagicFileType, 3
    sMagicFileType = LCase(sMagicFileType)
    
    ReadMagic = sMagicFileType = "fpg"
    
    If ReadMagic Then
        ' Next 4 bytes are MS-DOS termination, and last is the FPG version
        gzread lFile, avMagicDescriptor(0), 5
        
        iDepth = avMagicDescriptor(2)
        ' ...
        If Not (avMagicDescriptor(0) = &H1A And _
                avMagicDescriptor(1) = &HD And _
                avMagicDescriptor(2) = &HA And _
                avMagicDescriptor(3) = &H0) Then
            ReadMagic = False
        End If
        
        If ReadMagic Then
            vVersion = avMagicDescriptor(4)
            If vVersion > Version Then ReadMagic = False ' Different version
        End If
    End If
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Public Function ShiftRightL(ByVal lValue As Long, ByVal n As Long) As Long
    Dim i As Integer
    
    If lValue < 0 Then
        lValue = ShiftRightL(((lValue And &HFFFFFFFE) \ 2) And &H7FFFFFFF, n - 1)
    Else
        lValue = lValue \ 2 ^ n
    End If
    ShiftRightL = lValue
End Function
