VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SmartControlsGlobals"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Bennu Controls.
'
' Smart Bennu Controls is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Bennu Controls is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Bennu Controls. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' Public enumerations, types, constants and functions to use by the client
' application.
'===============================================================================

Public Enum E_BC_DepthModeConstants
    ebcDepthMode8bpp = 8
    ebcDepthMode16bpp = 16
    ebcDepthMode32bpp = 32
End Enum

Public Enum E_BC_BorderStyleConstants
    ebcNone
    ebcSunken
    ebcSunkenOuter
End Enum

Public Enum E_BC_ScrollDirection
    ebcScrollHorizontally = EFSScrollBarConstants.efsHorizontal
    ebcScrollVertically = EFSScrollBarConstants.efsVertical
End Enum

Private Const MODULE_NAME As String = "SmartBennuCtls.SmartControlsGlobals"

Public Property Get CustomDDFormat() As Integer
    Const PROC_NAME = "CustomDDFormat"
    
    On Error GoTo EH
    
    CustomDDFormat = G_iCustomDDFormat
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let CustomDDFormat(ByVal newVal As Integer)
    Const PROC_NAME = "CustomDDFormat"
    
    On Error GoTo EH
    
    G_iCustomDDFormat = newVal
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property
