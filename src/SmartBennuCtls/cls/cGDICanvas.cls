VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGDICanvas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Bennu Controls.
'
' Smart Bennu Controls is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Bennu Controls is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Bennu Controls. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' This class encapsulates a GDI canvas over which several GDI operations can be
' performed.
'
' NOTE: This class includes code from the 'GDI Blend' code sample by Mike D
' Sutton (Visual Basic MVP), available at http://edais.mvps.org.
'===============================================================================

Public Enum E_PenStyles
    PS_SOLID = 0
    PS_DASH = 1
    PS_DOT = 2
    PS_DASHDOT = 3
    PS_DASHDOTDOT = 4
End Enum

#If 0 Then
Private Const PS_SOLID As Long = 0
Private Const PS_DASH As Long = 1
Private Const PS_DASHDOT As Long = 3
Private Const PS_DASHDOTDOT As Long = 4
Private Const PS_DOT As Long = 2
#End If

Public Enum E_RopModes
    RM_BLACKNESS = 1
    RM_NOTMERGEPEN
    RM_MASKNOTPEN
    RM_NOTCOPYPEN
    RM_MASKPENNOT
    RM_INVERT
    RM_XORPEN
    RM_NOTMASKPEN
    RM_MASKPEN
    RM_NOTXORPEN
    RM_NOP
    RM_MERGENOTPEN
    RM_COPYPEN
    RM_MERGEPENNOT
    RM_MERGEPEN
    RM_WHITENESS
End Enum

Public Enum E_BkModes
    BM_OPAQUE = 2&
    BM_TRANSPARENT = 1&
End Enum
'===============================================================================

Private Declare Function CreateDIBSection Lib "gdi32.dll" (ByVal hdc As Long, _
            ByRef pBitmapInfo As BITMAPINFOHEADER, ByVal un As Long, _
            ByRef lplpVoid As Long, ByVal handle As Long, _
            ByVal dw As Long) As Long
Private Declare Function GetDeviceCaps Lib "gdi32.dll" (ByVal hdc As Long, _
            ByVal nIndex As Long) As Long
Private Declare Function GetDesktopWindow Lib "User32.dll" () As Long
Private Declare Function GetDC Lib "User32.dll" (ByVal hWnd As Long) As Long
Private Declare Function ReleaseDC Lib "User32.dll" (ByVal hWnd As Long, _
            ByVal hdc As Long) As Long
Private Declare Function CreateCompatibleDC Lib "gdi32.dll" ( _
            ByVal hdc As Long) As Long
Private Declare Function DeleteDC Lib "gdi32.dll" (ByVal hdc As Long) As Long
Private Declare Function DeleteObject Lib "gdi32.dll" ( _
            ByVal hObject As Long) As Long
Private Declare Function SelectObject Lib "gdi32.dll" (ByVal hdc As Long, _
            ByVal hObject As Long) As Long
Private Declare Function BitBlt Lib "gdi32.dll" (ByVal hDestDC As Long, _
            ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, _
            ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, _
            ByVal ySrc As Long, ByVal dwRop As Long) As Long
Private Declare Function AlphaBlend Lib "MSImg32.dll" (ByVal hdcDest As Long, _
            ByVal nXOriginDest As Long, ByVal nYOriginDest As Long, _
            ByVal nWidthDest As Long, ByVal nHeightDest As Long, _
            ByVal hdcSrc As Long, ByVal nXOriginSrc As Long, _
            ByVal nYOriginSrc As Long, ByVal nWidthSrc As Long, _
            ByVal nHeightSrc As Long, ByVal blendFunction As Long) As Long
Private Declare Function GetCurrentObject Lib "gdi32.dll" (ByVal hdc As Long, _
            ByVal uObjectType As Long) As Long
Private Declare Function GetObject Lib "gdi32.dll" Alias "GetObjectA" ( _
            ByVal hObject As Long, ByVal nCount As Long, ByRef lpObject _
            As Any) As Long
Private Declare Sub RtlMoveMemory Lib "Kernel32.dll" ( _
            ByRef Destination As Any, ByRef Source As Any, ByVal Length As Long)
Private Declare Function CreateSolidBrush Lib "gdi32.dll" ( _
            ByVal crColor As Long) As Long
Private Declare Function FillRect Lib "User32.dll" (ByVal hdc As Long, _
            ByRef lpRect As RECT, ByVal hBrush As Long) As Long
Private Declare Function FrameRect Lib "User32.dll" (ByVal hdc As Long, _
            lpRect As RECT, ByVal hBrush As Long) As Long
Private Declare Function SetRect Lib "User32.dll" (ByRef lpRect As RECT, _
            ByVal X1 As Long, ByVal Y1 As Long, _
            ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function SetTextColor Lib "gdi32.dll" (ByVal hdc As Long, _
            ByVal crColor As Long) As Long
Private Declare Function DrawTextEx Lib "User32.dll" Alias "DrawTextExA" ( _
            ByVal hdc As Long, ByVal lpsz As String, ByVal n As Long, _
            lpRect As RECT, ByVal un As Long, _
            lpDrawTextParams As Any) As Long
Private Declare Function LineTo Lib "gdi32.dll" ( _
            ByVal hdc As Long, _
            ByVal X As Long, _
            ByVal Y As Long) As Long
Private Declare Function CreatePen Lib "gdi32.dll" ( _
            ByVal nPenStyle As Long, _
            ByVal nWidth As Long, _
            ByVal crColor As Long) As Long
Private Declare Function MoveToEx Lib "gdi32.dll" ( _
            ByVal hdc As Long, _
            ByVal X As Long, _
            ByVal Y As Long, _
            ByVal lpPoint As Long) As Long
Private Declare Function Ellipse Lib "gdi32.dll" ( _
     ByVal hdc As Long, _
     ByVal X1 As Long, _
     ByVal Y1 As Long, _
     ByVal X2 As Long, _
     ByVal Y2 As Long) As Long
Private Declare Function GetROP2 Lib "gdi32.dll" ( _
     ByVal hdc As Long) As Long
Private Declare Function SetROP2 Lib "gdi32.dll" ( _
     ByVal hdc As Long, _
     ByVal nDrawMode As Long) As Long
Private Declare Function Rectangle Lib "gdi32.dll" ( _
     ByVal hdc As Long, _
     ByVal X1 As Long, _
     ByVal Y1 As Long, _
     ByVal X2 As Long, _
     ByVal Y2 As Long) As Long
Private Declare Function GetStockObject Lib "gdi32.dll" ( _
     ByVal nIndex As Long) As Long
Private Const NULL_BRUSH As Long = 5
Private Declare Function SetBkMode Lib "gdi32.dll" ( _
     ByVal hdc As Long, _
     ByVal nBkMode As Long) As Long
Private Declare Function SetBkColor Lib "gdi32.dll" ( _
     ByVal hdc As Long, _
     ByVal crColor As Long) As Long
Private Const TRANSPARENT As Long = 1
Private Const OPAQUE As Long = 2
Private Declare Function SaveDC Lib "gdi32.dll" ( _
     ByVal hdc As Long) As Long
Private Declare Function RestoreDC Lib "gdi32.dll" ( _
     ByVal hdc As Long, _
     ByVal nSavedDC As Long) As Long

            
Private Type BITMAPINFOHEADER ' 40 bytes
    biSize As Long
    biWidth As Long
    biHeight As Long
    biPlanes As Integer
    biBitCount As Integer
    biCompression As Long
    biSizeImage As Long
    biXPelsPerMeter As Long
    biYPelsPerMeter As Long
    biClrUsed As Long
    biClrImportant As Long
End Type

Private Type BITMAP ' 24 bytes
    bmType As Long
    bmWidth As Long
    bmHeight As Long
    bmWidthBytes As Long
    bmPlanes As Integer
    bmBitsPixel As Integer
    bmBits As Long
End Type

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type POINTAPI
    X As Long
    Y As Long
End Type

Private Const DT_END_ELLIPSIS As Long = &H8000
Private Const DT_VCENTER As Long = &H4
Private Const DT_SINGLELINE As Long = &H20
Private Const DT_MODIFYSTRING As Long = &H10000
Private Const DT_CENTER As Long = &H1

Private Const SHADEBLENDCAPS As Long = 120 ' Shading and blending caps
Private Const BITSPIXEL As Long = &HC      ' Number of bits per pixel
Private Const SB_CONST_ALPHA As Long = &H1
Private Const BI_RGB As Long = &H0
Private Const HALFTONE As Long = &H4
Private Const OBJ_BITMAP As Long = &H7

'===============================================================================

Private m_hDC As Long
Private m_hDIB As Long
Private m_hOldDIB As Long
Private m_lWidth As Long
Private m_lHeight As Long
Private m_lStride As Long ' Size of a Scan Line
Private m_lDataPtr As Long
Private m_lBitDepth As Long
Private m_lFont As StdFont
Private m_bUseInternalBlend As Boolean

'===============================================================================

Public Property Get hdc() As Long
    hdc = m_hDC
End Property

Public Property Get Width() As Long
    Width = m_lWidth
End Property

Public Property Get Height() As Long
    Height = m_lHeight
End Property

Public Property Get Font() As StdFont
    Set Font = m_lFont
End Property

Public Property Set Font(newVal As StdFont)
    Set m_lFont = newVal
End Property

Private Property Let UseInternalBlend(ByVal newVal As Boolean)
    Dim DeskDC As Long
    
    ' Check to see if this device supports source-constant alpha blending
    DeskDC = GetDC(0)
    m_bUseInternalBlend = newVal _
        Or (Not CBool(GetDeviceCaps(DeskDC, SHADEBLENDCAPS) And SB_CONST_ALPHA))
    Call ReleaseDC(0, DeskDC)
End Property

Public Function Create(ByVal lWidth As Long, ByVal lHeight As Long) As Boolean
    Dim lDeskWnd As Long
    Dim lDeskDC As Long
    Dim lDeskDepth As Long
    Dim uDIBInfo As BITMAPINFOHEADER
    
    Dispose
    
    If (lWidth < 1) Or (lHeight < 1) Then Exit Function
    
    ' Get desktop bit-display
    lDeskWnd = GetDesktopWindow()
    lDeskDC = GetDC(lDeskWnd)
    lDeskDepth = GetDeviceCaps(lDeskDC, BITSPIXEL)
    ReleaseDC lDeskWnd, lDeskDC
    
    ' DIB structure
    With uDIBInfo
    .biSize = Len(uDIBInfo)
    .biWidth = lWidth
    .biHeight = lHeight
    .biPlanes = 1
    .biBitCount = IIf(lDeskDepth = 32, 32, 24)
    .biCompression = BI_RGB
    
    m_lStride = (((.biWidth * (.biBitCount \ 8)) + 3) \ 4) * 4
    .biSizeImage = m_lStride * .biHeight
    End With
    
    ' Create the DC
     m_hDC = CreateCompatibleDC(0&)
     
    If m_hDC Then
        ' Create the DIB section
        m_hDIB = CreateDIBSection(m_hDC, uDIBInfo, 0, m_lDataPtr, 0, 0)
       
        If m_hDIB Then
            ' Select the DIB Bitmap into the DC
            m_hOldDIB = SelectObject(m_hDC, m_hDIB)
            Create = CBool(m_hOldDIB)
        End If
    End If
    
    If Not Create Then ' Something went wrong
        Dispose
    Else
        m_lWidth = lWidth
        m_lHeight = lHeight
        m_lBitDepth = uDIBInfo.biBitCount
    End If
End Function

Public Function DrawToDC(ByVal lDestDC As Long, _
        Optional ByVal lDestX As Long = 0, _
        Optional ByVal lDestY As Long = 0, _
        Optional ByVal vOpacity As Byte = &HFF) As Boolean
            
    If (m_hOldDIB = 0) Then Exit Function
        
    If (vOpacity = &HFF) Then ' Full opacity, just draw
        DrawToDC = BitBlt(lDestDC, lDestX, lDestY, m_lWidth, m_lHeight, _
                            m_hDC, 0, 0, vbSrcCopy) <> 0
    Else
        If Not m_bUseInternalBlend Then
            DrawToDC = AlphaBlend(lDestDC, lDestX, lDestY, m_lWidth, m_lHeight, _
                m_hDC, 0, 0, m_lWidth, m_lHeight, vOpacity * &H10000) <> 0
        Else
            Dim bb As cGDICanvas
            ' Create blended buffer
            Set bb = InternalBlend(lDestDC, lDestX, lDestY, vOpacity)
            ' Draw to target DC
            If Not (bb Is Nothing) Then
                DrawToDC = bb.DrawToDC(lDestDC, lDestX, lDestY, &HFF)
            End If
            ' Clean up back buffer
            Set bb = Nothing
        End If
    End If
End Function

Public Function Fill(ByVal lColor As Long) As Boolean
    Dim FillArea As RECT
    Dim hBrush As Long
    
    If (m_hOldDIB = 0) Then Exit Function
    
    ' Create a solid brush, set the area and fill
    hBrush = CreateSolidBrush(lColor)
    Call SetRect(FillArea, 0, 0, m_lWidth, m_lHeight)
    Fill = FillRect(m_hDC, FillArea, hBrush) <> 0
    Call DeleteObject(hBrush)
End Function

Public Function FillRectangle(ByVal lX As Long, ByVal lY As Long, _
        ByVal lWidth As Long, ByVal lHeight As Long, ByVal lColor As Long _
        ) As Boolean
        
    Dim FillArea As RECT
    Dim hBrush As Long
    
    If (m_hOldDIB = 0) Then Exit Function
    
    ' Create a solid brush, set the area and fill
    hBrush = CreateSolidBrush(lColor)
    
    Call SetRect(FillArea, lX, lY, lX + lWidth, lY + lHeight)
    FillRectangle = FillRect(m_hDC, FillArea, hBrush) <> 0
    
    Call DeleteObject(hBrush)
End Function

Public Function FillCircle(ByVal lX As Long, ByVal lY As Long, _
        ByVal lRadius As Long, lClrFg As Long, lClrBg As Long, _
        lBorderWidth As Long) As Boolean
            
    Dim hBrush As Long, hPen As Long
    Dim hOldBrush As Long, hOldPen As Long
    
    If (m_hOldDIB = 0) Then Exit Function
    
    hBrush = CreateSolidBrush(lClrBg)
    hPen = CreatePen(PS_SOLID, lBorderWidth, lClrFg)
    hOldBrush = SelectObject(m_hDC, hBrush)
    hOldPen = SelectObject(m_hDC, hPen)
    
    FillCircle = Ellipse(m_hDC, lX - lRadius, lY - lRadius, _
            lX + lRadius, lY + lRadius)
    
    SelectObject m_hDC, hOldBrush
    SelectObject m_hDC, hOldPen
    DeleteObject hOldBrush
    DeleteObject hOldPen
End Function

' Fills the canvas with the specified canvas pattern
Public Function DrawPattern(ByVal oSrcCanvas As cGDICanvas) As Boolean
    Dim repX As Integer, repY As Integer
    Dim i As Integer, j As Integer
    
    If (m_hOldDIB = 0) Or (oSrcCanvas Is Nothing) Then Exit Function
    
    repX = Width \ oSrcCanvas.Width
    repX = IIf(Width Mod oSrcCanvas.Width > 0, repX + 1, repX)
    repY = Height \ oSrcCanvas.Height
    repY = IIf(Height Mod oSrcCanvas.Height > 0, repY + 1, repY)
    
    For i = 0 To repY - 1
        For j = 0 To repX - 1
            oSrcCanvas.DrawToDC hdc, j * oSrcCanvas.Width, i * oSrcCanvas.Height
        Next
    Next
    
    DrawPattern = True
End Function

Public Function DrawLine(ByVal lX1 As Long, ByVal lY1 As Long, _
        ByVal lX2 As Long, ByVal lY2 As Long, Optional ByVal lColor As Long, _
        Optional ByVal eStyle As E_PenStyles = PS_SOLID, _
        Optional ByVal eRopMode As E_RopModes = RM_COPYPEN, _
        Optional ByVal lBkColor As Long, _
        Optional ByVal lBkMode As E_BkModes = BM_TRANSPARENT, _
        Optional ByVal lWidth As Long = 1)
            
    Dim hPen As Long
    Dim hOldDC As Long
    
    If (m_hOldDIB = 0) Then Exit Function
    
    hPen = CreatePen(eStyle, lWidth, lColor)
    hOldDC = SaveDC(m_hDC)
    Call SelectObject(m_hDC, hPen)
    Call SelectObject(m_hDC, GetStockObject(NULL_BRUSH))
    Call SetROP2(m_hDC, eRopMode)
    Call SetBkMode(m_hDC, lBkMode)
    Call SetBkColor(m_hDC, lBkColor)
    
    MoveToEx m_hDC, lX1, lY1, 0&
    DrawLine = LineTo(m_hDC, lX2, lY2)
    
    RestoreDC m_hDC, hOldDC
    DeleteObject hPen
End Function

Public Function DrawBorder(ByVal lX1 As Long, ByVal lY1 As Long, _
        ByVal lX2 As Long, ByVal lY2 As Long, Optional ByVal lColor As Long, _
        Optional ByVal eStyle As E_PenStyles = PS_SOLID, _
        Optional ByVal eRopMode As E_RopModes = RM_COPYPEN, _
        Optional ByVal lBkColor As Long, _
        Optional ByVal lBkMode As E_BkModes = BM_TRANSPARENT, _
        Optional ByVal lWidth As Long = 1)
        
    Dim hPen As Long
    Dim hOldDC As Long
    
    If (m_hOldDIB = 0) Then Exit Function
    
    hPen = CreatePen(eStyle, lWidth, lColor)
    hOldDC = SaveDC(m_hDC)
    Call SelectObject(m_hDC, hPen)
    Call SelectObject(m_hDC, GetStockObject(NULL_BRUSH))
    Call SetROP2(m_hDC, eRopMode)
    Call SetBkMode(m_hDC, lBkMode)
    Call SetBkColor(m_hDC, lBkColor)
    
    DrawBorder = Rectangle(m_hDC, lX1, lY1, lX2, lY2)
    
    RestoreDC m_hDC, hOldDC
    DeleteObject hPen
End Function

' Centers a text (horizontally and vertically) in the area defined by
' lX, lY, lWidth and lHeight. If the text is too large, the text will
' be replaced by ellipses, so that the result fits in the rectangle
Public Function CenterText(ByVal sText As String, _
            ByVal lX As Long, ByVal lY As Long, _
            ByVal lWidth As Long, ByVal lHeight As Long, _
            Optional ByVal lColor As Long = 0) As Long
    
    Dim uTextRect As RECT
    Dim hOldFont As Long
    
    If (m_hOldDIB = 0) Then Exit Function
    
    SetTextColor m_hDC, lColor
    SetRect uTextRect, lX, lY, lX + lWidth, lY + lHeight
    
    If Font Is Nothing Then Exit Function ' TODO: Set a default font
    
    hOldFont = SelectObject(m_hDC, IFontCast(Font).hFont)
    CenterText = DrawTextEx(m_hDC, sText, -1, uTextRect, _
            DT_SINGLELINE + DT_MODIFYSTRING + DT_END_ELLIPSIS _
            + DT_CENTER + DT_VCENTER, _
            ByVal 0&)
    SelectObject m_hDC, hOldFont
End Function

Public Function Clone() As cGDICanvas
    Set Clone = New cGDICanvas
    
    ' Create new back buffer the same size as this one
    If Clone.Create(m_lWidth, m_lHeight) Then ' Copy current image to clone
        If BitBlt(Clone.hdc, 0, 0, m_lWidth, m_lHeight, m_hDC, _
                    0, 0, vbSrcCopy) = 0 Then
            Set Clone = Nothing
        End If
    Else
        Set Clone = Nothing
    End If
End Function

Public Function CreateSnapshop(ByVal inDC As Long, ByVal inX As Long, ByVal inY _
    As Long, ByVal inWidth As Long, ByVal inHeight As Long) As Boolean
    ' Create a new back-buffer and fill it with a snapshot from the DC
    If (Me.Create(inWidth, inHeight)) Then CreateSnapshop = SnapShot(inDC, inX, _
        inY)
End Function

'===============================================================================

Private Function InternalBlend(ByVal inDC As Long, ByVal inX As Long, ByVal inY _
    As Long, ByVal inOpacity As Byte) As cGDICanvas
            
    Dim Background As cGDICanvas
    Dim hBackDIB As Long
    Dim BackInf As BITMAP
    Dim LocalData() As Byte, BackData() As Byte
    Dim LocalPos As Long, LocalStep As Long
    Dim BackPos As Long, BackStep As Long
    Dim LoopX As Long, LoopY As Long
    Dim BlendAmt As Single
    
    If (inOpacity = &HFF) Then ' Just clone
        Set InternalBlend = Me.Clone()
        Exit Function
    End If
    
    ' Create new back-buffer
    Set Background = New cGDICanvas
    
    ' Create a snapshot of the current target DC
    If (Background.CreateSnapshop(inDC, inX, inY, m_lWidth, m_lHeight)) Then
        If (inOpacity) Then ' Anything other than 0%...
            ' Get back-buffer's Bitmap
            hBackDIB = GetCurrentObject(Background.hdc, OBJ_BITMAP)
            ' Get information about Bitmap
            If (GetObject(hBackDIB, Len(BackInf), BackInf)) Then
                ' Allocate and read image data buffers locally
                ReDim BackData((BackInf.bmWidthBytes * BackInf.bmHeight) - 1) _
                    As Byte
                ReDim LocalData((m_lStride * m_lHeight) - 1) As Byte
                Call RtlMoveMemory(BackData(0), ByVal BackInf.bmBits, _
                    UBound(BackData()) + 1)
                Call RtlMoveMemory(LocalData(0), ByVal m_lDataPtr, _
                    UBound(LocalData()) + 1)
                
                ' Calculate byte per pixel counts for both buffers
                BackStep = BackInf.bmBitsPixel \ 8
                LocalStep = m_lBitDepth \ 8
                BlendAmt = inOpacity / &HFF ' Floating point opacity
                
                ' Blend loop
                For LoopY = 0 To m_lHeight - 1
                    LocalPos = LoopY * m_lStride
                    BackPos = LoopY * BackInf.bmWidthBytes
                    
                    ' Blend source and destination pixels using linear
                    ' interpolation
                    For LoopX = 0 To m_lWidth - 1
                        BackData(BackPos) = (BackData(BackPos) * (1 - _
                            BlendAmt)) + (LocalData(LocalPos) * BlendAmt)
                        BackData(BackPos + 1) = (BackData(BackPos + 1) * (1 - _
                            BlendAmt)) + (LocalData(LocalPos + 1) * BlendAmt)
                        BackData(BackPos + 2) = (BackData(BackPos + 2) * (1 - _
                            BlendAmt)) + (LocalData(LocalPos + 2) * BlendAmt)
                        
                        ' Increment data position
                        LocalPos = LocalPos + LocalStep
                        BackPos = BackPos + BackStep
                    Next LoopX
                Next LoopY
                
                ' Push blended data back into background DIB
                Call RtlMoveMemory(ByVal BackInf.bmBits, BackData(0), _
                    UBound(BackData()) + 1)
            Else ' Couldn't get Bitmap information
                Set Background = Nothing
            End If
        End If
    Else ' Failed to create snapshot buffer
        Set Background = Nothing
    End If
    
    ' Return blended buffer
    Set InternalBlend = Background
End Function

Private Function SnapShot(ByVal inDC As Long, ByVal inX As Long, _
        ByVal inY As Long) As Boolean
        
    If (m_hDC = 0) Then Exit Function ' Grab a snapshot from this DC
    SnapShot = BitBlt(m_hDC, 0, 0, m_lWidth, m_lHeight, inDC, inX, inY, _
        vbSrcCopy) <> 0
End Function

Private Function IFontCast(oFont As StdFont) As IFont
    Set IFontCast = oFont
End Function

Private Sub Dispose()
    If (m_hOldDIB) Then SelectObject m_hDC, m_hOldDIB
    If (m_hDC) Then DeleteDC (m_hDC)
    If (m_hDIB) Then DeleteObject (m_hDIB)
    
    Set m_lFont = Nothing
    
    m_hDC = 0
    m_hDIB = 0
    m_hOldDIB = 0
    m_lWidth = 0
    m_lHeight = 0
End Sub

Private Sub Class_Initialize()
    Set m_lFont = New StdFont
    m_lFont.Name = "Arial"
    UseInternalBlend = False ' By Default, try to use AlphaBlendCapabilities
End Sub

Private Sub Class_Terminate()
    Dispose
End Sub
