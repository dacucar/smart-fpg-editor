Attribute VB_Name = "modWindowStyles"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Bennu Controls.
'
' Smart Bennu Controls is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Bennu Controls is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Bennu Controls. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' MODULE OVERVIEW
'===============================================================================
' Methods to set window styles to controls
'===============================================================================

Public Enum E_BorderStyleConstants
    eNone = 0
    eSunken = &H200
    eSunkenOuter = &H20000
End Enum

'===============================================================================

Private Const MODULE_NAME As String = "SmartBennuCtls.modWindowStyles"

Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long) As Long
  
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long, _
    ByVal dwNewLong As Long) As Long
Private Declare Function SetWindowPos Lib "user32" ( _
    ByVal hWnd As Long, _
    ByVal hWndInsertAfter As Long, _
    ByVal X As Long, _
    ByVal Y As Long, _
    ByVal cx As Long, _
    ByVal cy As Long, _
    ByVal wFlags As Long) As Long

' Consts for SetWindowLong and GetWindowLong
Private Const GWL_EXSTYLE = (-20)
Private Const WS_EX_CLIENTEDGE = &H200
Private Const WS_EX_STATICEDGE = &H20000

' Consts for SetWindowPos
Private Const SWP_NOMOVE = &H2
Private Const SWP_NOSIZE = &H1
Private Const SWP_FRAMECHANGED = &H20
Private Const SWP_NOACTIVATE = &H10
Private Const SWP_NOZORDER = &H4
Private Const SWP_DRAWFRAME = SWP_FRAMECHANGED
Private Const SWP_FLAGS = SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOMOVE _
    Or SWP_DRAWFRAME
    
'===============================================================================

Public Sub SetWindowBorder(ByVal hWnd As Long, ByVal eStyle As _
        E_BorderStyleConstants)
    
    Dim lStyle As Long
    
    Const PROC_NAME As String = "SetWindowBorder"

    On Error GoTo EH

    lStyle = GetWindowLong(hWnd, GWL_EXSTYLE)
    lStyle = lStyle And Not WS_EX_CLIENTEDGE And Not WS_EX_STATICEDGE And Not _
        WS_EX_CLIENTEDGE
    
    lStyle = lStyle Or eStyle
    
    SetWindowLong hWnd, GWL_EXSTYLE, lStyle ' Apply the style
    
    ' Refresh
    SetWindowPos hWnd, 0, 0, 0, 0, 0, SWP_NOACTIVATE Or SWP_NOZORDER Or _
        SWP_FRAMECHANGED Or SWP_NOSIZE Or SWP_NOMOVE

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

