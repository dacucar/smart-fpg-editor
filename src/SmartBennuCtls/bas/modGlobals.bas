Attribute VB_Name = "modGlobals"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Bennu Controls.
'
' Smart Bennu Controls is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Bennu Controls is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Bennu Controls. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' MODULE OVERVIEW
'===============================================================================
' Public methods and definitions that are likely to be used in several parts of
' the application
'===============================================================================

Public Err As New cError

'===============================================================================

Private Const MODULE_NAME As String = "SmartBennuCtls.modGlobals"

Private Declare Function OleTranslateColor Lib "OLEPRO32.DLL" (ByVal OLE_COLOR _
        As Long, ByVal HPALETTE As Long, pccolorref As Long) As Long
        
Private Const CLR_INVALID = -1

Global G_iCustomDDFormat As Integer
'===============================================================================

' Purpose: Convert an OLE_COLOR to a Long RGB color
Public Function TranslateColor(ByVal oClr As OLE_COLOR, _
        Optional hPal As Long = 0) As Long
    
    Const PROC_NAME As String = "TranslateColor"

    On Error GoTo EH

    If OleTranslateColor(oClr, hPal, TranslateColor) Then
        TranslateColor = CLR_INVALID
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Maps a number to the next smaller interger
Public Function Floor(ByVal dNumber As Double) As Long
    Floor = Int(dNumber)
End Function

' Purpose: Maps a number to the next greater interger
Public Function Ceil(ByVal dNumber As Double) As Long
    If Int(dNumber) < dNumber Then
        Ceil = CLng(Int(dNumber) + 1)
    Else
        Ceil = CLng(dNumber)
    End If
End Function
