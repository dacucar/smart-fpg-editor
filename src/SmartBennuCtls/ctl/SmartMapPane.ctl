VERSION 5.00
Begin VB.UserControl SmartMapPane 
   ClientHeight    =   4365
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5055
   ControlContainer=   -1  'True
   ScaleHeight     =   291
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   337
   ToolboxBitmap   =   "SmartMapPane.ctx":0000
   Begin VB.Timer tmrCPs 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   2040
      Top             =   1080
   End
End
Attribute VB_Name = "SmartMapPane"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Bennu Controls.
'
' Smart Bennu Controls is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Bennu Controls is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Bennu Controls. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CONTROL OVERVIEW
'===============================================================================
' A MAP pane.
'===============================================================================

Public Event MapBecameAvailable()
Public Event MouseMove(ByVal lButton As Long, ByVal lX As Long, ByVal lY As _
    Long, ByVal lRelX As Long, ByVal lRelY As Long)
Public Event MouseUp(ByVal lButton As Long, ByVal lX As Long, ByVal lY As Long, _
    ByVal lRelX As Long, ByVal lRelY As Long)
Public Event MouseDown(ByVal lButton As Long, ByVal lX As Long, ByVal lY As _
    Long, ByVal lRelX As Long, ByVal lRelY As Long)
Public Event MouseWheel(eScrollDirection As E_BC_ScrollDirection, lAmount As Long, _
    ByVal iShift As Integer)
    
Private Const MODULE_NAME As String = "SmartBennuCtls.SmartMapPane"

'===============================================================================

' Default control configuration
Private Const DEF_SCROLLY_SMALL_CHANGE As Long = 4
Private Const DEF_SCROLLY_LARGE_CHANGE As Long = 90
Private Const DEF_SCROLLX_SMALL_CHANGE As Long = 4
Private Const DEF_SCROLLX_LARGE_CHANGE As Long = 90
Private Const DEF_STRETCH As Boolean = True
Private Const DEF_MARGIN_TOP As Long = 0
Private Const DEF_MARGIN_BOTTOM As Long = 0
Private Const DEF_MARGIN_LEFT As Long = 0
Private Const DEF_MARGIN_RIGHT As Long = 0
Private Const DEF_BORDERSTYLE As Long = 0
Private Const DEF_SHOW_TRANSPARENCY As Boolean = False
Private Const DEF_ZOOM As Long = 100
Private Const DEF_SHOWCPS As Boolean = False
Private Const DEF_SHOWACTIVECPINDICATOR As Boolean = False
Private Const DEF_SHOWCPSELECTOR As Boolean = False
Private Const DEF_ACTIVECPID As Long = 0

Private Const CHESS_BG_SIZE As Long = 16

'===============================================================================

Private Declare Function GetSystemMetrics Lib "User32.dll" (ByVal nIndex As _
    Long) As Long
Private Declare Function GetWindowRect Lib "User32.dll" (ByVal hWnd As Long, _
    lpRect As RECT) As Long
Private Declare Function GetClientRect Lib "User32.dll" (ByVal hWnd As Long, _
    lpRect As RECT) As Long
Private Declare Function SetRect Lib "User32.dll" (lpRect As RECT, ByVal X1 As _
    Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function GetCursorPos Lib "User32.dll" (lpPoint As POINTAPI) As _
    Long
Private Declare Function ScreenToClient Lib "User32.dll" (ByVal hWnd As Long, _
    lpPoint As POINTAPI) As Long
    
Private Declare Function SetCapture Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function ReleaseCapture Lib "user32" () As Long
Private Declare Function GetCapture Lib "user32" () As Long

Private Const SM_CXVSCROLL As Long = 2

Private Type POINTAPI
    X As Long
    Y As Long
End Type

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'===============================================================================

Private m_eBorderStyle As E_BC_BorderStyleConstants

Private Type tViewerParams
    lMarginTop As Long
    lMarginLeft As Long
    lMarginBottom As Long
    lMarginRight As Long
    bStretch As Boolean
    bShowTransparency As Boolean
    lZoom As Long
    bShowControlPoints As Boolean
    bShowActiveControlPointIndicator As Boolean
    bShowControlPointSelector As Boolean
    lActiveCPId As Long
    lScrollYLargeChange As Long
    lScrollYSmallChange As Long
    lScrollXLargeChange As Long
    lScrollXSmallChange As Long
End Type

Private m_uVP As tViewerParams
Private m_Map As cMap
Private WithEvents m_cScroll As cScrollBars
Attribute m_cScroll.VB_VarHelpID = -1

Private m_bRepaintLocked As Boolean

Private m_bMouseOver As Boolean

Private m_lScrollYGlobal As Long
Private m_lScrollXGlobal As Long
Private m_bInvertClr As Boolean

Private Type tDrawingDimensions
    ' Displacement
    lOffX As Long
    lOffY As Long
    ' Real available area to paint the MAP
    lEffViewerW As Long
    lEffViewerH As Long
    ' Size of the MAP once painted
    lDrawingW As Long '
    lDrawingH As Long
    ' Region of the original MAP that will be painted
    lSrcX As Long
    lSrcY As Long
    lSrcWidth As Long
    lSrcHeight As Long
    lMapZoomedW As Long
    lMapZoomedH As Long
End Type
Private m_uDD As tDrawingDimensions

Private Type tMousePosition
    lScreenX As Long
    lScreenY As Long
    lWindowX As Long
    lWindowY As Long
    lGraphicX As Long ' Relative to the MAP
    lGraphicY As Long
End Type
'===============================================================================

Public Property Get BorderStyle() As E_BC_BorderStyleConstants
    Const PROC_NAME As String = "BorderStyle"

    On Error GoTo EH

    BorderStyle = m_eBorderStyle

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let BorderStyle(ByVal newVal As E_BC_BorderStyleConstants)
    Dim eBS As E_BorderStyleConstants
    
    Const PROC_NAME As String = "BorderStyle"

    On Error GoTo EH

    Select Case newVal
    Case ebcNone
        eBS = eNone
    Case ebcSunken
        eBS = eSunken
    Case ebcSunkenOuter
        eBS = eSunkenOuter
    Case Else
        Exit Property
    End Select
    
    m_eBorderStyle = newVal
    SetWindowBorder UserControl.hWnd, eBS
    PropertyChanged ("BorderStyle")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get Stretch() As Boolean
    Const PROC_NAME As String = "Stretch"

    On Error GoTo EH

    Stretch = m_uVP.bStretch

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let Stretch(ByVal newVal As Boolean)
    Const PROC_NAME As String = "Stretch"

    On Error GoTo EH

    m_uVP.bStretch = newVal
    PropertyChanged ("Stretch")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get MarginTop() As Long
    Const PROC_NAME As String = "MarginTop"

    On Error GoTo EH

    MarginTop = m_uVP.lMarginTop

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let MarginTop(ByVal newVal As Long)
    Const PROC_NAME As String = "MarginTop"

    On Error GoTo EH

    m_uVP.lMarginTop = newVal
    PropertyChanged ("MarginTop")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get MarginBottom() As Long
    Const PROC_NAME As String = "MarginBottom"

    On Error GoTo EH

    MarginBottom = m_uVP.lMarginBottom

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let MarginBottom(ByVal newVal As Long)
    Const PROC_NAME As String = "MarginBottom"

    On Error GoTo EH

    m_uVP.lMarginBottom = newVal
    PropertyChanged ("MarginBottom")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get MarginLeft() As Long
    Const PROC_NAME As String = "MarginLeft"

    On Error GoTo EH

    MarginLeft = m_uVP.lMarginLeft

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let MarginLeft(ByVal newVal As Long)
    Const PROC_NAME As String = "MarginLeft"

    On Error GoTo EH

    m_uVP.lMarginLeft = newVal
    PropertyChanged ("MarginLeft")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get MarginRight() As Long
    Const PROC_NAME As String = "MarginRight"

    On Error GoTo EH

    MarginRight = m_uVP.lMarginRight

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let MarginRight(ByVal newVal As Long)
    Const PROC_NAME As String = "MarginRight"

    On Error GoTo EH

    m_uVP.lMarginRight = newVal
    PropertyChanged ("MarginRight")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get MapAvailable() As Boolean
    Const PROC_NAME As String = "MapAvailable"

    On Error GoTo EH

    If Not m_Map Is Nothing Then MapAvailable = True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ShowTransparency() As Boolean
    Const PROC_NAME As String = "ShowTransparency"

    On Error GoTo EH

    ShowTransparency = m_uVP.bShowTransparency
    PropertyChanged ("ShowTransparency")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ShowTransparency(ByVal newVal As Boolean)
    Const PROC_NAME As String = "ShowTransparency"

    On Error GoTo EH

    m_uVP.bShowTransparency = newVal
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get Zoom() As Long
    Const PROC_NAME As String = "Zoom"

    On Error GoTo EH

    Zoom = m_uVP.lZoom

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let Zoom(ByVal newVal As Long)
    Const PROC_NAME As String = "Zoom"

    On Error GoTo EH

    m_uVP.lZoom = newVal
    PropertyChanged ("Zoom")
    UserControl_Resize

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ScrollYSmallChange() As Long
    Const PROC_NAME As String = "ScrollYSmallChange"

    On Error GoTo EH

    ScrollYSmallChange = m_cScroll.SmallChange(efsVertical)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ScrollYSmallChange(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollYSmallChange"

    On Error GoTo EH

    m_cScroll.SmallChange(efsVertical) = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("ScrollYSmallChange")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ScrollYLargeChange() As Long
    Const PROC_NAME As String = "ScrollYLargeChange"

    On Error GoTo EH

    ScrollYLargeChange = m_cScroll.LargeChange(efsVertical)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ScrollYLargeChange(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollYLargeChange"

    On Error GoTo EH

    m_cScroll.LargeChange(efsVertical) = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("ScrollYLargeChange")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ScrollXSmallChange() As Long
    Const PROC_NAME As String = "ScrollXSmallChange"

    On Error GoTo EH

    ScrollXSmallChange = m_cScroll.SmallChange(efsHorizontal)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ScrollXSmallChange(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollXSmallChange"

    On Error GoTo EH

    m_cScroll.SmallChange(efsHorizontal) = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("ScrollXSmallChange")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ScrollXLargeChange() As Long
    Const PROC_NAME As String = "ScrollXLargeChange"

    On Error GoTo EH

    ScrollXLargeChange = m_cScroll.LargeChange(efsHorizontal)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ScrollXLargeChange(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollXLargeChange"

    On Error GoTo EH

    m_cScroll.LargeChange(efsHorizontal) = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("ScrollXLargeChange")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ShowControlPoints() As Boolean
    Const PROC_NAME As String = "ShowControlPoints"

    On Error GoTo EH
    
    ShowControlPoints = m_uVP.bShowControlPoints
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ShowControlPoints(ByVal newVal As Boolean)
    Const PROC_NAME As String = "ShowControlPoints"

    On Error GoTo EH
        
    m_uVP.bShowControlPoints = newVal
    tmrCPs.Enabled = newVal Or ShowActiveControlPointIndicator
    RePaint
    
    PropertyChanged ("ShowControlPoints")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ShowActiveControlPointIndicator() As Boolean

    Const PROC_NAME As String = "ShowActiveControlPointIndicator"

    On Error GoTo EH
    
    ShowActiveControlPointIndicator = m_uVP.bShowActiveControlPointIndicator

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ShowActiveControlPointIndicator(ByVal newVal As Boolean)
    
    Const PROC_NAME As String = "ShowActiveControlPointIndicator"

    On Error GoTo EH

    m_uVP.bShowActiveControlPointIndicator = newVal
    tmrCPs.Enabled = newVal Or ShowControlPoints
    PropertyChanged ("ShowActiveControlPointIndicator")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ShowControlPointSelector() As Boolean

    Const PROC_NAME As String = "ShowControlPointSelector"

    On Error GoTo EH
    
    ShowControlPointSelector = m_uVP.bShowControlPointSelector

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ShowControlPointSelector(ByVal newVal As Boolean)
    
    Const PROC_NAME As String = "ShowControlPointSelector"

    On Error GoTo EH

    m_uVP.bShowControlPointSelector = newVal
    PropertyChanged ("ShowControlPointSelector")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ActiveControlPointId() As Long
    Const PROC_NAME As String = "ActiveControlPointId"
    
    On Error GoTo EH

    ActiveControlPointId = m_uVP.lActiveCPId

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ActiveControlPointId(ByVal newVal As Long)
    Const PROC_NAME As String = "ActiveControlPointId"

    On Error GoTo EH

    m_uVP.lActiveCPId = newVal
    PropertyChanged ("ActiveControlPointId")
    RePaint
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Sub LoadMap(ByVal sFile As String)
    Const PROC_NAME As String = "LoadMap"

    On Error GoTo EH

    UnloadMap
    Set m_Map = bennulib.CreateMapFromFile(sFile)
    If Not m_Map Is Nothing Then RaiseEvent MapBecameAvailable
    UserControl_Resize

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Public Sub SetMap(oMap As cMap)
    Const PROC_NAME As String = "SetMap"

    On Error GoTo EH

    UnloadMap
    Set m_Map = oMap
    If Not m_Map Is Nothing Then RaiseEvent MapBecameAvailable
    UserControl_Resize

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Public Function GetMap() As cMap
    Const PROC_NAME As String = "GetMap"

    On Error GoTo EH

    Set GetMap = m_Map

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Public Sub UnloadMap()
    Const PROC_NAME As String = "UnloadMap"

    On Error GoTo EH

    Set m_Map = Nothing
    UserControl.Cls

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Locks repainting so as sucessive calls to RePaint have no effect
Public Sub LockRePaint()
    Const PROC_NAME As String = "LockRePaint"

    On Error GoTo EH

    m_bRepaintLocked = True

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Unlocks repainting
Public Sub UnlockRePaint()
    Const PROC_NAME As String = "UnlockRePaint"

    On Error GoTo EH

    m_bRepaintLocked = False

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Public Sub RePaint()
    Const PROC_NAME As String = "RePaint"

    On Error GoTo EH
    
    If m_bRepaintLocked = False Then
        UserControl_Paint
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'===============================================================================

' Retrieves the width of the viewer without considering the borders (or the
' scroll bar)
Private Property Get ViewerWidth()
    Dim uR As RECT
    Const PROC_NAME As String = "ViewerWidth"

    On Error GoTo EH

    GetClientRect UserControl.hWnd, uR
    ViewerWidth = uR.Right - uR.Left

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the height of the viewer without considering the borders
' (or the scroll bar)
Private Property Get ViewerHeight()
    Dim uR As RECT
    Const PROC_NAME As String = "ViewerHeight"

    On Error GoTo EH

    GetClientRect UserControl.hWnd, uR
    ViewerHeight = uR.Bottom - uR.Top

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Retrieves the position of the vertical scroll
Private Property Get ScrollYGlobal() As Long
    Const PROC_NAME As String = "ScrollYGlobal"

    On Error GoTo EH

    ScrollYGlobal = m_lScrollYGlobal

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Sets the position of the vertical scroll
Private Property Let ScrollYGlobal(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollYGlobal"

    On Error GoTo EH

    If newVal > m_cScroll.Max(efsVertical) Then
        m_lScrollYGlobal = m_cScroll.Max(efsVertical)
    ElseIf newVal < 0 Then
        m_lScrollYGlobal = 0
    Else
        m_lScrollYGlobal = newVal
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Retrieve the position of the vertical scroll
Private Property Get ScrollXGlobal() As Long
    Const PROC_NAME As String = "ScrollXGlobal"

    On Error GoTo EH

    ScrollXGlobal = m_lScrollXGlobal

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Sets the position of the vertical scroll
Private Property Let ScrollXGlobal(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollXGlobal"

    On Error GoTo EH

    If newVal > m_cScroll.Max(efsHorizontal) Then
        m_lScrollXGlobal = m_cScroll.Max(efsHorizontal)
    ElseIf newVal < 0 Then
        m_lScrollXGlobal = 0
    Else
        m_lScrollXGlobal = newVal
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Set the range for the scrollbars according to the current
' configuration
Private Sub UpdateScrollBarsRange()
    Dim a As Long
    Dim lH As Long, lW As Long
    
    Const PROC_NAME As String = "UpdateScrollBarsRange"

    On Error GoTo EH
    
    ' It is necessary to lock repaint because setting m_cScroll.Max(efsVertical)
    ' will raise the m_cScroll_Change event, which will call the RePaint function.
    ' That is not desirable as the horizontal scroll has not been updated yet
    ' and the GlobalXScroll may contain a value which is not admissible.
    LockRePaint
    
    If m_uVP.bStretch = True Then
        m_cScroll.Visible(efsHorizontal) = False
        m_cScroll.Visible(efsVertical) = False
    Else
        m_cScroll.Visible(efsHorizontal) = True
        m_cScroll.Visible(efsVertical) = True
        
        lW = m_Map.Width * m_uVP.lZoom / 100
        lH = m_Map.Height * m_uVP.lZoom / 100
        
        ' Vertical
        a = lH + m_uVP.lMarginTop + m_uVP.lMarginBottom - ViewerHeight
            '+ m_lScrollBarWidth
        m_cScroll.Max(efsVertical) = IIf(a < 0, 0, a)
        m_cScroll.LargeChange(efsVertical) = _
            m_uVP.lScrollYLargeChange * m_uVP.lZoom / 100
        m_cScroll.SmallChange(efsVertical) = _
            m_uVP.lScrollYSmallChange * m_uVP.lZoom / 100
        m_cScroll.Enabled(efsVertical) = IIf(a > 0, True, False)
        
        ' Horizontal
        a = lW + m_uVP.lMarginLeft + m_uVP.lMarginRight - ViewerWidth
            '+ m_lScrollBarWidth
        m_cScroll.Max(efsHorizontal) = IIf(a < 0, 0, a)
        m_cScroll.LargeChange(efsHorizontal) = _
            m_uVP.lScrollXLargeChange * m_uVP.lZoom / 100
        m_cScroll.SmallChange(efsHorizontal) = _
            m_uVP.lScrollXSmallChange * m_uVP.lZoom / 100
        m_cScroll.Enabled(efsHorizontal) = IIf(a > 0, True, False)
    End If

    UnlockRePaint
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Calculates several parameters related to the size and position
' of the drawing area.
Private Sub CalculateDrawingDimensions()

    Const PROC_NAME As String = "CalculateDrawingDimensions"

    On Error GoTo EH

    If Ambient.UserMode = False Then Exit Sub
    If m_Map Is Nothing Then Exit Sub
    
    With m_uDD
    .lEffViewerW = ViewerWidth - m_uVP.lMarginRight - m_uVP.lMarginLeft
    .lEffViewerH = ViewerHeight - m_uVP.lMarginTop - m_uVP.lMarginBottom
    
    If m_uVP.bStretch Then
        .lDrawingW = .lEffViewerW
        .lDrawingH = .lEffViewerH
        .lSrcWidth = m_Map.Width
        .lSrcHeight = m_Map.Height
        .lSrcX = 0
        .lSrcY = 0
        .lOffX = m_uVP.lMarginLeft
        .lOffY = m_uVP.lMarginTop
    Else
        .lMapZoomedW = (m_Map.Width * m_uVP.lZoom / 100)
        .lMapZoomedH = (m_Map.Height * m_uVP.lZoom / 100)
        If .lMapZoomedW > .lEffViewerW Then
            .lDrawingW = .lEffViewerW
'            .lSrcWidth = m_Map.Width - (.lMapZoomedW - .lEffViewerW) * 100 / _
'                m_uVP.lZoom
            .lSrcWidth = (.lEffViewerW * 100 / m_uVP.lZoom)
        Else
            .lSrcWidth = m_Map.Width
            .lDrawingW = .lMapZoomedW
        End If
        If .lMapZoomedH > .lEffViewerH Then
            .lDrawingH = .lEffViewerH
'            .lSrcHeight = m_Map.Height - (.lMapZoomedH - .lEffViewerH) * 100 / _
'                m_uVP.lZoom
            .lSrcHeight = (.lEffViewerH * 100 / m_uVP.lZoom)
        Else
            .lSrcHeight = m_Map.Height
            .lDrawingH = .lMapZoomedH
        End If
        ' Centered position
        .lOffX = (.lEffViewerW - .lDrawingW) / 2 + m_uVP.lMarginLeft
        .lOffY = (.lEffViewerH - .lDrawingH) / 2 + m_uVP.lMarginTop
        
        .lSrcX = ScrollXGlobal * 100 / m_uVP.lZoom
        .lSrcY = ScrollYGlobal * 100 / m_uVP.lZoom
    End If

    End With
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'Purpose: Gets the position of the mouse (in pixels)
Private Function GetMousePosition() As tMousePosition
    Dim dw As Double, dH As Double
    Dim uPoint As POINTAPI
        
    Const PROC_NAME As String = "GetMousePosition"

    On Error GoTo EH

    If Ambient.UserMode = False Then Exit Function
    
    ' Screen position
    GetCursorPos uPoint
    GetMousePosition.lScreenX = uPoint.X
    GetMousePosition.lScreenY = uPoint.Y
    
    ' Window position
    ScreenToClient UserControl.hWnd, uPoint
    GetMousePosition.lWindowX = uPoint.X
    GetMousePosition.lWindowY = uPoint.Y
    
    ' Graphic position (coordinates relative to the the MAP)
    If Not m_Map Is Nothing Then
        Debug.Assert m_uDD.lSrcWidth > 0
        Debug.Assert m_uDD.lSrcHeight > 0
        
        dw = m_uDD.lDrawingW / m_uDD.lSrcWidth
        dH = m_uDD.lDrawingH / m_uDD.lSrcHeight
        GetMousePosition.lGraphicX = Floor((uPoint.X - m_uDD.lOffX) / dw) + _
            m_uDD.lSrcX
        GetMousePosition.lGraphicY = Floor((uPoint.Y - m_uDD.lOffY) / dH) + _
            m_uDD.lSrcY
    End If
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

'===============================================================================

Private Sub m_cScroll_Change(eBar As EFSScrollBarConstants)
    Const PROC_NAME As String = "m_cScroll_Change"

    On Error GoTo EH

    If eBar = efsVertical Then
        ScrollYGlobal = m_cScroll.Value(eBar)
    Else
        ScrollXGlobal = m_cScroll.Value(eBar)
    End If
    CalculateDrawingDimensions
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub m_cScroll_MouseWheel(eBar As EFSScrollBarConstants, lAmount As Long, ByVal iShift As Integer)
    Const PROC_NAME As String = "m_cScroll_MouseWheel"
    
    On Error GoTo EH
    
    RaiseEvent MouseWheel(eBar, lAmount, iShift)
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub tmrCPs_Timer()
    Const PROC_NAME As String = "tmrCPs_Timer"

    On Error GoTo EH

    m_bInvertClr = Not m_bInvertClr
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Initialize()
    Const PROC_NAME As String = "UserControl_Initialize"

    On Error GoTo EH
    
    ' ScrollBar
    Set m_cScroll = New cScrollBars
    m_cScroll.Create UserControl.hWnd
    m_cScroll.Visible(efsVertical) = False
    m_cScroll.Visible(efsHorizontal) = False
    m_cScroll.Enabled(efsVertical) = False
    m_cScroll.Enabled(efsHorizontal) = False
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, X As _
    Single, Y As Single)
        
    Dim uMP As tMousePosition
        
    Const PROC_NAME As String = "UserControl_MouseDown"

    On Error GoTo EH
    
    uMP = GetMousePosition
    RaiseEvent MouseDown(Button, uMP.lWindowX, uMP.lWindowY, uMP.lGraphicX, _
        uMP.lGraphicY)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As _
        Single, Y As Single)
    
    Dim uMP As tMousePosition
        
    Const PROC_NAME As String = "UserControl_MouseMove"

    On Error GoTo EH
    
    uMP = GetMousePosition
    
    ' Detect MouseEnter and MouseLeave situations
    If (uMP.lWindowX < 0) Or (uMP.lWindowY < 0) _
            Or (uMP.lWindowX > ViewerWidth) _
            Or (uMP.lWindowY > ViewerHeight) Then ' Mouse leaves
        
        ReleaseCapture
        m_bMouseOver = False
    ElseIf GetCapture() <> UserControl.hWnd Then ' Mouse enters
        SetCapture UserControl.hWnd
        m_bMouseOver = True
    Else ' Mouse over
        ' ...
        ' Not used right now... for future uses
        ' ...
    End If
    
    RaiseEvent MouseMove(Button, uMP.lWindowX, uMP.lWindowY, uMP.lGraphicX, _
        uMP.lGraphicY)
        
    If m_uVP.bShowControlPoints Or m_uVP.bShowActiveControlPointIndicator Then
        RePaint
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, _
        X As Single, Y As Single)
        
    Dim uMP As tMousePosition
        
    Const PROC_NAME As String = "UserControl_MouseUp"

    On Error GoTo EH
    
    uMP = GetMousePosition
    RaiseEvent MouseUp(Button, uMP.lWindowX, uMP.lWindowY, uMP.lGraphicX, _
        uMP.lGraphicY)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Paint()
    ' NOTE: THE METHOD DRAWS ONLY THE PART OF THE PICTURE WHICH IS VISIBLE
    Dim bb As cGDICanvas, bg As cGDICanvas
    
    Dim lCpIds() As Long
    Dim lCpX As Long, lCpY As Long ' Position of CP indicator rectangle
    
    Dim dw As Double, dH As Double
    
    Dim iA As Integer, iB As Integer ' Used in border line drawing. TODO: Rename!
    Dim i As Integer
    
    Dim uMP As tMousePosition
    
    Const PROC_NAME As String = "UserControl_Paint"

    On Error GoTo EH
    
    If Ambient.UserMode = False Then Exit Sub
    If m_Map Is Nothing Then Exit Sub
    
    If ViewerHeight <= 0 Or ViewerWidth <= 0 Then Exit Sub
    
    ' Back buffer of the size of the viewer
    Set bb = New cGDICanvas
    bb.Create ViewerWidth, ViewerHeight
    
    ' Surface of the size of the portion of the MAP to be drawn
    Set bg = New cGDICanvas
    bg.Create m_uDD.lDrawingW, m_uDD.lDrawingH
    
    ' Note: we could avoid using two surfaces by drawing directly to the back
    ' buffer, but the way it's done would allow to clip drawing operations
    ' to the MAP portion surface
    
    ' Draw the portion of the MAP to the bg surface
    m_Map.Draw bg.hdc, 0, 0, m_uDD.lDrawingW, m_uDD.lDrawingH, m_uDD.lSrcX, _
        m_uDD.lSrcY, m_uDD.lSrcWidth, m_uDD.lSrcHeight, ShowTransparency
        
    ' Draw the MAP portion surface into the back buffer
    bb.Fill TranslateColor(UserControl.BackColor)
    bg.DrawToDC bb.hdc, m_uDD.lOffX, m_uDD.lOffY
    
    ' Draw the outline rectangle (in strech mode we don't want it)
    If Not m_uVP.bStretch Then
        If ScrollYGlobal = 0 Then
            bb.DrawLine m_uDD.lOffX, m_uDD.lOffY - 1, m_uDD.lOffX + _
                m_uDD.lDrawingW, m_uDD.lOffY - 1
            iA = 1
        End If
        If ScrollYGlobal = m_cScroll.Max(efsVertical) Then
            bb.DrawLine m_uDD.lOffX, m_uDD.lOffY + m_uDD.lDrawingH, m_uDD.lOffX + _
                m_uDD.lDrawingW, m_uDD.lOffY + m_uDD.lDrawingH
            iB = 1
        End If
        If ScrollXGlobal = 0 Then
            bb.DrawLine m_uDD.lOffX - 1, m_uDD.lOffY - iA, m_uDD.lOffX - 1, _
                m_uDD.lOffY + m_uDD.lDrawingH + iB
        End If
        If ScrollXGlobal = m_cScroll.Max(efsHorizontal) Then
            bb.DrawLine m_uDD.lOffX + m_uDD.lDrawingW, m_uDD.lOffY - iA, _
                m_uDD.lOffX + m_uDD.lDrawingW, m_uDD.lOffY + m_uDD.lDrawingH + iB
        End If
    End If
    
    ' Scale. Due to conversion rounding, these values may not be the same than
    ' m_vp.lZoom / 100
    dH = m_uDD.lDrawingH / m_uDD.lSrcHeight
    dw = m_uDD.lDrawingW / m_uDD.lSrcWidth
    
    ' Draw Control Point indicators
    lCpIds() = m_Map.GetControlPointsIds
    If m_uVP.bShowControlPoints Then
        ' 1 pixel dotted rectangle for all control points
        For i = 0 To m_Map.ControlPointsCount - 1
            m_Map.GetControlPoint lCpIds(i), lCpX, lCpY
            lCpX = m_uDD.lOffX + (lCpX - m_uDD.lSrcX) * dw
            lCpY = m_uDD.lOffY + (lCpY - m_uDD.lSrcY) * dH
            
            bb.DrawBorder lCpX, lCpY, lCpX + 1 * dw, lCpY + 1 * dH, _
                IIf(m_bInvertClr, vbBlack, vbWhite), PS_DOT, _
                RM_COPYPEN, IIf(m_bInvertClr, vbWhite, vbBlack), BM_OPAQUE
        Next
    ElseIf m_uVP.bShowActiveControlPointIndicator _
            And m_Map.ExistsControlPoint(m_uVP.lActiveCPId) Then
        ' Draw the rectangle only for the active CP
        m_Map.GetControlPoint m_uVP.lActiveCPId, lCpX, lCpY
        lCpX = m_uDD.lOffX + (lCpX - m_uDD.lSrcX) * dw
        lCpY = m_uDD.lOffY + (lCpY - m_uDD.lSrcY) * dH
        
        bb.DrawBorder lCpX, lCpY, lCpX + 1 * dw, lCpY + 1 * dH, _
            IIf(m_bInvertClr, vbBlack, vbWhite), PS_DOT, _
            RM_COPYPEN, IIf(m_bInvertClr, vbWhite, vbBlack), BM_OPAQUE
    End If
    
    ' Draw the indicator for the active control point
    If m_uVP.bShowActiveControlPointIndicator _
            And m_Map.ExistsControlPoint(m_uVP.lActiveCPId) Then
            
        m_Map.GetControlPoint m_uVP.lActiveCPId, lCpX, lCpY
        lCpX = m_uDD.lOffX + (lCpX - m_uDD.lSrcX) * dw
        lCpY = m_uDD.lOffY + (lCpY - m_uDD.lSrcY) * dH
        
        ' 3 'pixels' rectangle
        bb.DrawBorder lCpX - dw, lCpY - dH, lCpX + 2 * dw, lCpY + 2 * dH, _
            vbCyan
        ' Vertical lines
        bb.DrawLine lCpX + 0.5 * dw, 0, lCpX + 0.5 * dw, lCpY, vbCyan
        bb.DrawLine lCpX + 0.5 * dw, lCpY + dH, lCpX + 0.5 * dw, _
            ViewerHeight, vbCyan
        ' Horizontal Lines
        bb.DrawLine 0, lCpY + 0.5 * dH, lCpX, lCpY + 0.5 * dH, vbCyan
        bb.DrawLine lCpX + dw, lCpY + 0.5 * dH, ViewerWidth, lCpY + 0.5 * _
            dH, vbCyan
    End If
    
    ' Draw the indicator of the mouse
    If m_uVP.bShowControlPointSelector And m_bMouseOver Then
        uMP = GetMousePosition
                
        lCpX = uMP.lGraphicX
        lCpY = uMP.lGraphicY
        
        lCpX = m_uDD.lOffX + (lCpX - m_uDD.lSrcX) * dw
        lCpY = m_uDD.lOffY + (lCpY - m_uDD.lSrcY) * dH
        
        ' 3 'pixels' rectangle
        bb.DrawBorder lCpX - dw, lCpY - dH, lCpX + 2 * dw, lCpY + 2 * dH, , _
            , RM_INVERT
        ' 1 'pixel' rectangle
        bb.DrawBorder lCpX, lCpY, lCpX + dw, lCpY + dH, , , RM_INVERT
        ' Vertical lines
        bb.DrawLine lCpX + 0.5 * dw, 0, lCpX + 0.5 * dw, lCpY, , , RM_INVERT
        bb.DrawLine lCpX + 0.5 * dw, lCpY + dH, lCpX + 0.5 * dw, _
            ViewerHeight, , , RM_INVERT
        ' Horizontal Lines
        bb.DrawLine 0, lCpY + 0.5 * dH, lCpX, lCpY + 0.5 * dH, , , RM_INVERT
        bb.DrawLine lCpX + dw, lCpY + 0.5 * dH, ViewerWidth, lCpY + 0.5 * _
            dH, , , RM_INVERT
    End If

    ' Copy the backbuffer to the control
    bb.DrawToDC UserControl.hdc
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Resize()
    Dim uR As RECT
    
    Const PROC_NAME As String = "UserControl_Resize"

    On Error GoTo EH

    If Ambient.UserMode = False Then Exit Sub
    If Not MapAvailable Then Exit Sub

    ' Calculate the ScrollBar range
    UpdateScrollBarsRange
    CalculateDrawingDimensions ' Note, must be after UpdateScorllBarsRange
    
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Terminate()
    Const PROC_NAME As String = "UserControl_Terminate"

    On Error GoTo EH

    Set m_Map = Nothing

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_InitProperties()
    Const PROC_NAME As String = "UserControl_InitProperties"

    On Error GoTo EH

    BorderStyle = DEF_BORDERSTYLE
    m_uVP.lScrollYLargeChange = DEF_SCROLLY_LARGE_CHANGE
    m_uVP.lScrollYSmallChange = DEF_SCROLLY_SMALL_CHANGE
    m_uVP.lScrollXLargeChange = DEF_SCROLLX_LARGE_CHANGE
    m_uVP.lScrollXSmallChange = DEF_SCROLLX_SMALL_CHANGE
    
'    m_cScroll.LargeChange(efsVertical) = DEF_SCROLLY_LARGE_CHANGE
'    m_cScroll.SmallChange(efsVertical) = DEF_SCROLLY_SMALL_CHANGE
'    m_cScroll.LargeChange(efsHorizontal) = DEF_SCROLLX_LARGE_CHANGE
'    m_cScroll.SmallChange(efsHorizontal) = DEF_SCROLLX_SMALL_CHANGE
    
    With m_uVP
    .bStretch = DEF_STRETCH
    .lMarginBottom = DEF_MARGIN_BOTTOM
    .lMarginLeft = DEF_MARGIN_LEFT
    .lMarginRight = DEF_MARGIN_RIGHT
    .lMarginTop = DEF_MARGIN_TOP
    .bShowTransparency = DEF_SHOW_TRANSPARENCY
    .lZoom = DEF_ZOOM
    .bShowControlPoints = DEF_SHOWCPS
    .bShowActiveControlPointIndicator = DEF_SHOWACTIVECPINDICATOR
    .bShowControlPointSelector = DEF_SHOWCPSELECTOR
    .lActiveCPId = DEF_ACTIVECPID
    End With

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    Const PROC_NAME As String = "UserControl_ReadProperties"

    On Error GoTo EH

    With PropBag
    BorderStyle = .ReadProperty("BorderStyle", DEF_BORDERSTYLE)
    m_uVP.bStretch = .ReadProperty("Stretch", DEF_STRETCH)
    m_uVP.lMarginTop = .ReadProperty("MarginTop", DEF_MARGIN_TOP)
    m_uVP.lMarginBottom = .ReadProperty("MarginBottom", DEF_MARGIN_BOTTOM)
    m_uVP.lMarginLeft = .ReadProperty("MarginLeft", DEF_MARGIN_LEFT)
    m_uVP.lMarginRight = .ReadProperty("MarginRight", DEF_MARGIN_RIGHT)
    m_uVP.bShowTransparency = .ReadProperty("ShowTransparency", _
        DEF_SHOW_TRANSPARENCY)
    m_uVP.lZoom = .ReadProperty("Zoom", DEF_ZOOM)
    m_uVP.bShowControlPoints = .ReadProperty("ShowControlPoints", DEF_SHOWCPS)
    m_uVP.bShowActiveControlPointIndicator = .ReadProperty("ShowActiveControlPointIndicator", _
        DEF_SHOWACTIVECPINDICATOR)
    m_uVP.bShowControlPointSelector = .ReadProperty("ShowControlPointSelector", _
        DEF_SHOWCPSELECTOR)
    m_uVP.lActiveCPId = .ReadProperty("ActiveControlPointId", DEF_ACTIVECPID)
    ' Scroll
'    m_cScroll.SmallChange(efsVertical) = .ReadProperty("ScrollYSmallChange", _
'        DEF_SCROLLY_SMALL_CHANGE)
'    m_cScroll.LargeChange(efsVertical) = .ReadProperty("ScrollYLargeChange", _
'        DEF_SCROLLY_LARGE_CHANGE)
'    m_cScroll.SmallChange(efsHorizontal) = .ReadProperty("ScrollXSmallChange", _
'        DEF_SCROLLX_SMALL_CHANGE)
'    m_cScroll.LargeChange(efsHorizontal) = .ReadProperty("ScrollXLargeChange", _
'        DEF_SCROLLX_LARGE_CHANGE)
    m_uVP.lScrollYSmallChange = .ReadProperty("ScrollYSmallChange", _
        DEF_SCROLLY_SMALL_CHANGE)
    m_uVP.lScrollYLargeChange = .ReadProperty("ScrollYLargeChange", _
        DEF_SCROLLY_LARGE_CHANGE)
    m_uVP.lScrollXSmallChange = .ReadProperty("ScrollXSmallChange", _
        DEF_SCROLLX_SMALL_CHANGE)
    m_uVP.lScrollXLargeChange = .ReadProperty("ScrollXLargeChange", _
        DEF_SCROLLX_LARGE_CHANGE)
    End With

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Const PROC_NAME As String = "UserControl_WriteProperties"

    On Error GoTo EH

    With PropBag
    .WriteProperty "BorderStyle", m_eBorderStyle, DEF_BORDERSTYLE
    .WriteProperty "Stretch", m_uVP.bStretch, DEF_STRETCH
    .WriteProperty "MarginTop", m_uVP.lMarginTop, DEF_MARGIN_TOP
    .WriteProperty "MarginBottom", m_uVP.lMarginBottom, DEF_MARGIN_BOTTOM
    .WriteProperty "MarginLeft", m_uVP.lMarginLeft, DEF_MARGIN_LEFT
    .WriteProperty "MarginRight", m_uVP.lMarginRight, DEF_MARGIN_RIGHT
    .WriteProperty "ShowTransparency", m_uVP.bShowTransparency, _
        DEF_SHOW_TRANSPARENCY
    .WriteProperty "Zoom", m_uVP.lZoom, DEF_ZOOM
    .WriteProperty "ShowControlPoints", m_uVP.bShowControlPoints, _
        DEF_SHOWCPS
    .WriteProperty "ShowActiveControlPointIndicator", m_uVP.bShowActiveControlPointIndicator, _
        DEF_SHOWACTIVECPINDICATOR
    .WriteProperty "ShowControlPointSelector", m_uVP.bShowControlPointSelector, _
        DEF_SHOWCPSELECTOR
    .WriteProperty "ActiveControlPointId", m_uVP.lActiveCPId, DEF_ACTIVECPID
    ' Scroll
'    .WriteProperty "ScrollYSmallChange", m_cScroll.SmallChange(efsVertical), _
'        DEF_SCROLLY_SMALL_CHANGE
'    .WriteProperty "ScrollYLargeChange", m_cScroll.LargeChange(efsVertical), _
'        DEF_SCROLLY_LARGE_CHANGE
'    .WriteProperty "ScrollXSmallChange", m_cScroll.SmallChange(efsHorizontal), _
'        DEF_SCROLLX_SMALL_CHANGE
'    .WriteProperty "ScrollXLargeChange", m_cScroll.LargeChange(efsHorizontal), _
'        DEF_SCROLLX_LARGE_CHANGE
    .WriteProperty "ScrollYSmallChange", m_uVP.lScrollYSmallChange, _
        DEF_SCROLLY_SMALL_CHANGE
    .WriteProperty "ScrollYLargeChange", m_uVP.lScrollYLargeChange, _
        DEF_SCROLLY_LARGE_CHANGE
    .WriteProperty "ScrollXSmallChange", m_uVP.lScrollXSmallChange, _
        DEF_SCROLLX_SMALL_CHANGE
    .WriteProperty "ScrollXLargeChange", m_uVP.lScrollXLargeChange, _
        DEF_SCROLLX_LARGE_CHANGE
    End With

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
