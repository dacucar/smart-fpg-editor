VERSION 5.00
Begin VB.UserControl SmartFrame 
   BackStyle       =   0  'Transparent
   CanGetFocus     =   0   'False
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ScaleHeight     =   240
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   320
   Begin VB.Timer tmr 
      Interval        =   500
      Left            =   1680
      Top             =   2760
   End
   Begin VB.Shape shFore 
      BorderStyle     =   3  'Dot
      DrawMode        =   6  'Mask Pen Not
      Height          =   2055
      Left            =   0
      Top             =   0
      Width           =   1935
   End
   Begin VB.Shape shBg 
      DrawMode        =   1  'Blackness
      Height          =   1695
      Left            =   360
      Top             =   840
      Width           =   3015
   End
End
Attribute VB_Name = "SmartFrame"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const MODULE_NAME As String = "SmartBennuCtls.SmartFrame"

Private Sub tmr_Timer()
    Const PROC_NAME As String = "tmr_Timer"

    On Error GoTo EH

    shBg.DrawMode = IIf(shBg.DrawMode = 1, 16, 1)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Resize()
    Const PROC_NAME As String = "UserControl_Resize"

    On Error GoTo EH
    
    tmr.Enabled = Ambient.UserMode

    shBg.Move 0, 0, UserControl.ScaleWidth, UserControl.ScaleHeight
    shFore.Move 0, 0, UserControl.ScaleWidth, UserControl.ScaleHeight

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
