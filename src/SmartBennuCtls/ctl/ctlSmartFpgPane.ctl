VERSION 5.00
Begin VB.UserControl SmartFpgPane 
   Alignable       =   -1  'True
   BackColor       =   &H80000005&
   ClientHeight    =   1245
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2520
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   83
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   168
   ToolboxBitmap   =   "ctlSmartFpgPane.ctx":0000
End
Attribute VB_Name = "SmartFpgPane"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Bennu Controls.
'
' Smart Bennu Controls is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Bennu Controls is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Bennu Controls. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private Declare Function FlatSB_ShowScrollBar Lib "comctl32" (ByVal hWnd As Long, _
        ByVal code As Long, ByVal fShow As Boolean) As Boolean
        
'Keystatus (para Alt,Control y shift)
Private Const VK_CONTROL As Long = &H11
Private Const VK_MENU As Long = &H12
Private Const VK_SHIFT As Long = &H10
Private Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

'===============================================================================
' CONTROL OVERVIEW
'===============================================================================
' An FPG pane.
'===============================================================================

Public Enum E_BC_VerticalAlignConstants
    ebcAlignTop = 0
    ebcAlignMiddle
    ebcAlignBottom
End Enum

Public Enum E_BC_HorizontalAlignConstants
    ebcAlignLeft = 0
    ebcAlignCenter
    ebcAlignRight
End Enum

Public Enum E_BC_CaptionPositionConstants
    ebcTop = 0
    ebcBottom
End Enum

'===============================================================================

Public Event SelectionChanged(ByVal lSelectedIndex As Long)
Public Event GraphicDblClick(ByVal lMapIndex As Long)
Public Event MouseUp(iButton As Integer, X As Single, Y As Single)
Public Event MouseDown(iButton As Integer, X As Single, Y As Single)
Public Event MouseWheel(eScrollDirection As E_BC_ScrollDirection, lAmount As Long, _
    ByVal iShift As Integer)
Public Event FpgBecameAvailable()
' Drag&Dropp events
Public Event DDFilesDropped(sFiles() As String, _
        iButton As Integer, Shift As Integer, lX As Long, lY As Long, lIndex As Long)
Public Event DDDIBDropped(oPic As IPicture, _
        iButton As Integer, Shift As Integer, lX As Long, lY As Long, lIndex As Long)
Public Event DDCustomFormatDropped(avData() As Byte, _
        iButton As Integer, Shift As Integer, lX As Long, lY As Long, lIndex As Long)
Public Event OLESetData(data As DataObject, FormatID As Integer, ByVal Shift As Integer)
Public Event OLEStartDrag(data As DataObject, AllowedEffects As Long)
Public Event OLECompleteDrag(Effect As Long)
Public Event OLEGiveFeedback(Effect As Long, DefaultCursors As Boolean, ByVal Shift As Integer)
Public Event OLEDragOver(data As DataObject, Effect As Long, _
            Button As Integer, Shift As Integer, X As Single, Y As Single, _
            State As Integer)
        
'===============================================================================

Private Const DEF_SCROLLY_SMALL_CHANGE As Long = 4
Private Const DEF_SCROLLY_LARGE_CHANGE As Long = 90
Private Const DEF_STRETCHGRAPHICS As Boolean = False
Private Const DEF_CELL_WIDTH As Long = 62
Private Const DEF_CELL_HEIGHT As Long = 62
Private Const DEF_CELL_H_ALIGN As Long = 1
Private Const DEF_CELL_V_ALIGN As Long = 0
Private Const DEF_THUMB_WIDTH As Long = 48
Private Const DEF_THUMB_HEIGHT As Long = 48
Private Const DEF_THUMB_H_ALIGN As Long = 1
Private Const DEF_THUMB_V_ALIGN As Long = 1
Private Const DEF_NAME_HEIGHT As Long = 14
Private Const DEF_NAME_WIDTH  As Long = DEF_CELL_WIDTH
Private Const DEF_NAME_POSITION As Long = 1
Private Const DEF_NAME_OFFSETY As Long = -14
Private Const DEF_COL_SPACE As Long = 10
Private Const DEF_ROW_SPACE As Long = 10
Private Const DEF_MARGIN_LEFT As Long = 5
Private Const DEF_MARGIN_RIGHT As Long = 5
Private Const DEF_MARGIN_TOP As Long = 5
Private Const DEF_MARGIN_BOTTOM As Long = 5
Private Const DEF_BACKCOLOR As Long = &H80000005
Private Const DEF_FORECOLOR As Long = &HF0
Private Const DEF_GRIDCOLOR As Long = &HFF0000
Private Const DEF_HIGHLIGHT_FORECOLOR As Long = &HFF8080
Private Const DEF_HIGHLIGHT_BACKCOLOR As Long = &H8000000E
Private Const DEF_BORDERSTYLE As Long = 0
Private Const DEF_FPGNOTAVAILABLE_MSG As String = ""
Private Const DEF_SHOW_TRANSPARENCY As Boolean = False

Private Const STARTDRAG_AREA_SIZE As Long = 32

Private Const MODULE_NAME As String = "SmartBennuCtls.SmartFpgPane"

'===============================================================================

Private Declare Function GetSystemMetrics Lib "User32.dll" ( _
        ByVal nIndex As Long) As Long
Private Declare Function GetWindowRect Lib "User32.dll" (ByVal hWnd As Long, _
        lpRect As RECT) As Long
Private Declare Function GetClientRect Lib "User32.dll" ( _
     ByVal hWnd As Long, _
     lpRect As RECT) As Long
Private Declare Function SetRect Lib "User32.dll" (lpRect As RECT, _
        ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, _
        ByVal Y2 As Long) As Long
Private Declare Function PtInRect Lib "User32.dll" (lpRect As RECT, _
        ByVal X As Long, ByVal Y As Long) As Long
Private Declare Function InflateRect Lib "User32.dll" (lpRect As RECT, _
        ByVal X As Long, ByVal Y As Long) As Long
Private Declare Function DrawFocusRect Lib "user32" (ByVal hdc As Long, _
        lpRect As RECT) As Long
'Private Declare Function DeleteObject Lib "GDI32.dll" ( _
'        ByVal hObject As Long) As Long
'Private Declare Function CreateSolidBrush Lib "GDI32.dll" ( _
'        ByVal crColor As Long) As Long
'Private Declare Function Rectangle Lib "GDI32.dll" (ByVal hDC As Long, _
'        ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, _
'        ByVal Y2 As Long) As Long
'Private Declare Function FrameRect Lib "User32.dll" (ByVal hDC As Long, _
'        lpRect As RECT, ByVal hBrush As Long) As Long
'Private Declare Function FillRect Lib "User32.dll" (ByVal hDC As Long, _
'        lpRect As RECT, ByVal hBrush As Long) As Long
'Private Declare Function BitBlt Lib "GDI32.dll" (ByVal hDestDC As Long, _
'        ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, _
'        ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, _
'        ByVal ySrc As Long, ByVal dwRop As Long) As Long
Private Declare Function GetCursorPos Lib "User32.dll" ( _
        lpPoint As POINTAPI) As Long
Private Declare Function ScreenToClient Lib "User32.dll" ( _
        ByVal hWnd As Long, _
        lpPoint As POINTAPI) As Long

Private Type POINTAPI
    X As Long
    Y As Long
End Type

Private Const SM_CXVSCROLL As Long = 2

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'===============================================================================

Private Type tViewerParams
    CellWidth As Long
    CellHeight As Long
    CellVerticalAlign As Long
    CellHorizontalAlign As Long
    ThumbWidth As Long ' Thumb area width
    ThumbHeight As Long ' Thumb area height
    ThumbHorizontalAlign As E_BC_HorizontalAlignConstants
    ThumbVerticalAlign As E_BC_VerticalAlignConstants
    StretchGraphics As Boolean
    ColSpace As Long
    RowSpace As Long
    MarginLeft As Long
    MarginRight As Long
    MarginTop As Long
    MarginBottom As Long
    NameHeight As Long
    NameWidth As Long
    NamePosition As E_BC_CaptionPositionConstants
    NameOffsetY As Long
    FpgNotAvailableMsg As String
    GridColor As OLE_COLOR
    ShowTransparency As Boolean
End Type

Private WithEvents m_cScroll As cScrollBars
Attribute m_cScroll.VB_VarHelpID = -1

Private m_Fpg As cFpg

Private m_VP As tViewerParams
Private m_eBorderStyle As E_BC_BorderStyleConstants
Private m_HighlightForeColor As OLE_COLOR
Private m_HighlightBackColor As OLE_COLOR

' NOTE: Check whether a private property exist to access to this variables, in
' which case you should use the private property instead of the member variable

' Number of thumbs in a row
Private m_iThumbsPerRow As Integer
' Total number of rows that must be painted (including those partially hidden)
Private m_iVisibleRows As Integer
' First row to paint
Private m_iFirstVisibleRow As Integer
' Total number of rows if we wanted to paint all the thumbs
Private m_iTotalRowsNeeded As Integer

' Thoretical vertical displacement if all the maps were to be painted
Private m_lScrollYGlobal As Long
' "Hot" selection, that is, in a multiple selection it will be the element
' that has the focus inside the selection.
Private m_lSelectedIndex As Long

' Indexes of the thumbs that are selected.
' Key will be cStr(Index) and Value will be Index
' The presence in the collection of the element {Index, Cstr(Index)} means that
' the thumb whose index is Index, is selected
' An important property of the use of the collection is that it memmorizes the
' order in which elements were selected. This allows us to track back the "hot"
' selection when de-selecting elements.
Private m_Selected As Collection

' Real dimensions (Obtained with API calls)
Private m_lViewerWidth As Long
Private m_lViewerHeight As Long

' Controls whether calls to Repaint() method are ignored. Useful when several
' drawing operations are to be done to avoid flickering
Private m_bRepaintLocked As Boolean

' To determine if the properties of the control have been read (ReadProperties
' was released)
Private bPropertiesRead As Boolean

' Store the last coordinates of the mouse when the MouseEvent was raised. The
' information will be used to avoid too-sensitive drag start when moving mouse.
Private m_lLastMouseDownX As Long
Private m_lLastMouseDownY As Long

' When using Ctrl+Mouse to select, this variable set to true will prevent the
' MouseUp event to deselect the element if it is selected. This is needed when
' we select an unselected item with the Ctl key pressed
Private m_bPreventMouseUpDeselection

Private m_bDragCompleted As Boolean


'===============================================================================

' Retrieves the scale mode
Public Property Get ScaleMode() As Integer
    ScaleMode = UserControl.ScaleMode
End Property

' Retrieves the background color
Public Property Get BackColor() As OLE_COLOR
    Const PROC_NAME As String = "BackColor"

    On Error GoTo EH

    BackColor = UserControl.BackColor

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the background color
Public Property Let BackColor(newVal As OLE_COLOR)
    Const PROC_NAME As String = "BackColor"

    On Error GoTo EH

    UserControl.BackColor = IIf(newVal = -1, DEF_BACKCOLOR, newVal)
    PropertyChanged ("BackColor")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the foreground color
Public Property Get ForeColor() As OLE_COLOR
    Const PROC_NAME As String = "ForeColor"
    
    On Error GoTo EH
    
    ForeColor = UserControl.ForeColor
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the foreground color
Public Property Let ForeColor(newVal As OLE_COLOR)
    Const PROC_NAME As String = "ForeColor"

    On Error GoTo EH

    UserControl.ForeColor = IIf(newVal = -1, DEF_FORECOLOR, newVal)
    PropertyChanged ("ForeColor")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the grid color
Public Property Get GridColor() As OLE_COLOR
    Const PROC_NAME As String = "GridColor"
    
    On Error GoTo EH
    
    GridColor = m_VP.GridColor
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the grid color
Public Property Let GridColor(newVal As OLE_COLOR)
    Const PROC_NAME As String = "GridColor"

    On Error GoTo EH

    m_VP.GridColor = IIf(newVal = -1, DEF_GRIDCOLOR, newVal)
    PropertyChanged ("GridColor")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the message displayed when a FPG is not available
Public Property Get FpgNotAvailableMsg() As String
    Const PROC_NAME As String = "FpgNotAvailableMsg"
    
    On Error GoTo EH
    
    FpgNotAvailableMsg = m_VP.FpgNotAvailableMsg
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the message displayed when a FPG is not available
Public Property Let FpgNotAvailableMsg(ByVal newVal As String)
    Const PROC_NAME As String = "FpgNotAvailableMsg"

    On Error GoTo EH

    m_VP.FpgNotAvailableMsg = newVal
    PropertyChanged ("FpgNotAvailableMsg")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the font used to display text
Public Property Get Font() As StdFont
    Const PROC_NAME As String = "Font"

    On Error GoTo EH

    Set Font = UserControl.Font

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the font used to display text
Public Property Set Font(ByVal newVal As StdFont)
    Const PROC_NAME As String = "Font"

    On Error GoTo EH

    Set UserControl.Font = newVal
    PropertyChanged ("Font")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the border style of the pane
Public Property Get BorderStyle() As E_BC_BorderStyleConstants
    Const PROC_NAME As String = "BorderStyle"

    On Error GoTo EH

    BorderStyle = m_eBorderStyle

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the border style of the pane
Public Property Let BorderStyle(ByVal newVal As E_BC_BorderStyleConstants)
    Dim eBS As E_BorderStyleConstants
    
    Const PROC_NAME As String = "BorderStyle"

    On Error GoTo EH

    Select Case newVal
    Case ebcNone
        eBS = eNone
    Case ebcSunken
        eBS = eSunken
    Case ebcSunkenOuter
        eBS = eSunkenOuter
    Case Else
        Exit Property
    End Select
    
    m_eBorderStyle = newVal
    SetWindowBorder UserControl.hWnd, eBS

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the small increment of the scroll bar (in pixels)
Public Property Get ScrollYSmallChange() As Long
    Const PROC_NAME As String = "ScrollYSmallChange"

    On Error GoTo EH

    ScrollYSmallChange = m_cScroll.SmallChange(efsVertical)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the small increment of the scroll bar (in pixels)
Public Property Let ScrollYSmallChange(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollYSmallChange"

    On Error GoTo EH

    m_cScroll.SmallChange(efsVertical) = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("ScrollYSmallChange")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the large increment of the scroll bar (in pixels)
Public Property Get ScrollYLargeChange() As Long
    Const PROC_NAME As String = "ScrollYLargeChange"

    On Error GoTo EH

    ScrollYLargeChange = m_cScroll.LargeChange(efsVertical)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the large increment of the scroll bar (in pixels)
Public Property Let ScrollYLargeChange(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollYLargeChange"

    On Error GoTo EH

    m_cScroll.LargeChange(efsVertical) = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("ScrollYLargeChange")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the color of the text of a selected cell
Public Property Get HighlightForeColor() As OLE_COLOR
    Const PROC_NAME As String = "HighlightForeColor"

    On Error GoTo EH

    HighlightForeColor = m_HighlightForeColor

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the color of the text of a selected cell
Public Property Let HighlightForeColor(newVal As OLE_COLOR)
    Const PROC_NAME As String = "HighlightForeColor"

    On Error GoTo EH

    m_HighlightForeColor = IIf(newVal = -1, DEF_HIGHLIGHT_FORECOLOR, newVal)
    PropertyChanged ("HighlightForeColor")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the background color of a selected cell
Public Property Get HighlightBackColor() As OLE_COLOR
    Const PROC_NAME As String = "HighlightBackColor"

    On Error GoTo EH

    HighlightBackColor = m_HighlightBackColor

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the background color of a selected cell
Public Property Let HighlightBackColor(newVal As OLE_COLOR)
    Const PROC_NAME As String = "HighlightBackColor"

    On Error GoTo EH

    m_HighlightBackColor = IIf(newVal = -1, DEF_HIGHLIGHT_BACKCOLOR, newVal)
    PropertyChanged ("HighlightBackColor")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the margin (in pixels) from the left border of the control
Public Property Get MarginLeft() As Long
    Const PROC_NAME As String = "MarginLeft"

    On Error GoTo EH

    MarginLeft = m_VP.MarginLeft

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the margin (in pixels) from the left border of the control
Public Property Let MarginLeft(ByVal newVal As Long)
    Const PROC_NAME As String = "MarginLeft"

    On Error GoTo EH

    m_VP.MarginLeft = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("MarginLeft")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the margin (in pixels) from the top border of the control
Public Property Get MarginTop() As Long
    Const PROC_NAME As String = "MarginTop"

    On Error GoTo EH

    MarginTop = m_VP.MarginTop

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the margin (in pixels) from the top border of the control
Public Property Let MarginTop(ByVal newVal As Long)
    Const PROC_NAME As String = "MarginTop"

    On Error GoTo EH

    m_VP.MarginTop = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("MarginTop")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the margin (in pixels) from the bottom border of the control
Public Property Get MarginBottom() As Long
    Const PROC_NAME As String = "MarginBottom"

    On Error GoTo EH

    MarginBottom = m_VP.MarginBottom

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the margin (in pixels) from the bottom border of the control
Public Property Let MarginBottom(ByVal newVal As Long)
    Const PROC_NAME As String = "MarginBottom"

    On Error GoTo EH

    m_VP.MarginBottom = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("MarginBottom")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the margin (in pixels) from the right border of the control
Public Property Get MarginRight() As Long
    Const PROC_NAME As String = "MarginRight"

    On Error GoTo EH

    MarginRight = m_VP.MarginRight

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the margin (in pixels) from the right border of the control
Public Property Let MarginRight(ByVal newVal As Long)
    Const PROC_NAME As String = "MarginRight"

    On Error GoTo EH

    m_VP.MarginRight = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("MarginRight")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves whether the thumbs are stretched to fill the thumb area (true)
' or not (false)
Public Property Get StretchGraphics() As Boolean
    Const PROC_NAME As String = "StretchGraphics"

    On Error GoTo EH

    StretchGraphics = m_VP.StretchGraphics

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets whether the thumbs are stretched to fill the thumb area (true)
' or not (false)
Public Property Let StretchGraphics(ByVal newVal As Boolean)
    Const PROC_NAME As String = "StretchGraphics"

    On Error GoTo EH

    m_VP.StretchGraphics = newVal
    PropertyChanged ("StretchGraphics")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the width (in pixels) of a cell
Public Property Get CellWidth() As Long
    Const PROC_NAME As String = "CellWidth"

    On Error GoTo EH

    CellWidth = m_VP.CellWidth
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the width (in pixels) of a cell
Public Property Let CellWidth(ByVal newVal As Long)
    Const PROC_NAME As String = "CellWidth"

    On Error GoTo EH

    m_VP.CellWidth = IIf(newVal < 1, 1, newVal)
    PropertyChanged ("CellWidth")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the height (in pixels) of a cell
Public Property Get CellHeight() As Long
    Const PROC_NAME As String = "CellHeight"

    On Error GoTo EH

    CellHeight = m_VP.CellHeight

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the height (in pixels) of a cell
Public Property Let CellHeight(ByVal newVal As Long)
    Const PROC_NAME As String = "CellHeight"

    On Error GoTo EH

    m_VP.CellHeight = IIf(newVal < 1, 1, newVal)
    PropertyChanged ("CellHeight")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the horizontal alingment of the thumb area inside the cell area
Public Property Get CellHorizontalAlign() As E_BC_HorizontalAlignConstants
    Const PROC_NAME As String = "CellHorizontalAlign"

    On Error GoTo EH

    CellHorizontalAlign = m_VP.CellHorizontalAlign

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the horizontal alingment of the thumb area inside the cell area
Public Property Let CellHorizontalAlign( _
        ByVal newVal As E_BC_HorizontalAlignConstants)
    
    Const PROC_NAME As String = "CellHorizontalAlign"

    On Error GoTo EH

    Select Case newVal
    Case ebcAlignLeft To ebcAlignRight
        m_VP.CellHorizontalAlign = newVal
    Case Else
        Err.Raise 380 ' Invalid property value error
    End Select
    PropertyChanged ("CellHorizontalAlign")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the vertical alingment of the thumb area inside the cell area
Public Property Get CellVerticalAlign() As E_BC_VerticalAlignConstants
    Const PROC_NAME As String = "CellVerticalAlign"

    On Error GoTo EH

    CellVerticalAlign = m_VP.CellVerticalAlign

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the vertical alingment of the thumb area inside the cell area
Public Property Let CellVerticalAlign( _
        ByVal newVal As E_BC_VerticalAlignConstants)
    
    Const PROC_NAME As String = "CellVerticalAlign"

    On Error GoTo EH

    Select Case newVal
    Case ebcAlignTop To ebcAlignBottom
        m_VP.CellVerticalAlign = newVal
    Case Else
        Err.Raise 380 ' Invalid property value error
    End Select
    PropertyChanged ("CellVerticalAlign")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the thumb area width (in pixels)
Public Property Get ThumbWidth() As Long
    Const PROC_NAME As String = "ThumbWidth"

    On Error GoTo EH

    ThumbWidth = m_VP.ThumbWidth

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the thumb area width (in pixels)
Public Property Let ThumbWidth(ByVal newVal As Long)
    Const PROC_NAME As String = "ThumbWidth"

    On Error GoTo EH

    m_VP.ThumbWidth = IIf(newVal < 1, 1, newVal)
    PropertyChanged ("ThumbWidth")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the thumb area height (in pixels)
Public Property Get ThumbHeight() As Long
    Const PROC_NAME As String = "ThumbHeight"

    On Error GoTo EH

    ThumbHeight = m_VP.ThumbHeight
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the thumb area height (in pixels)
Public Property Let ThumbHeight(ByVal newVal As Long)
    Const PROC_NAME As String = "ThumbHeight"

    On Error GoTo EH

    m_VP.ThumbHeight = IIf(newVal < 1, 1, newVal)
    PropertyChanged ("ThumbHeight")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the horizontal alignment inside the thumb area. Note that if the
' StretchGraphics property is set to true, the ThumbHorizontalAlign will
' have no effect.
Public Property Get ThumbHorizontalAlign() As E_BC_HorizontalAlignConstants
    Const PROC_NAME As String = "ThumbHorizontalAlign"

    On Error GoTo EH

    ThumbHorizontalAlign = m_VP.ThumbHorizontalAlign

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the horizontal alignment inside the thumb area. Note that if the
' StretchGraphics property is set to true, the ThumbHorizontalAlign will
' have no effect.
Public Property Let ThumbHorizontalAlign( _
                            ByVal newVal As E_BC_HorizontalAlignConstants)
    Const PROC_NAME As String = "ThumbHorizontalAlign"

    On Error GoTo EH

    Select Case newVal
    Case ebcAlignLeft To ebcAlignRight
        m_VP.ThumbHorizontalAlign = newVal
    Case Else
        Err.Raise 380 ' Invalid property value error
    End Select
    PropertyChanged ("ThumbHorizontalAlign")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the vertical alignment inside the thumb area. Note that if the
' StretchGraphics property is set to true, the ThumbVerticalAlign will
' have no effect.
Public Property Get ThumbVerticalAlign() As E_BC_VerticalAlignConstants
    Const PROC_NAME As String = "ThumbVerticalAlign"

    On Error GoTo EH

    ThumbVerticalAlign = m_VP.ThumbVerticalAlign

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the vertical alignment inside the thumb area. Note that if the
' StretchGraphics property is set to true, the ThumbVerticalAlign will
' have no effect.
Public Property Let ThumbVerticalAlign( _
                            ByVal newVal As E_BC_VerticalAlignConstants)
    Const PROC_NAME As String = "ThumbVerticalAlign"

    On Error GoTo EH

    Select Case newVal
    Case ebcAlignTop To ebcAlignBottom
        m_VP.ThumbVerticalAlign = newVal
    Case Else
        Err.Raise 380 ' Invalid property value error
    End Select
    PropertyChanged ("ThumbVerticalAlign")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the horizontal space (in pixels) between two cells
Public Property Get ColSpace() As Long
    Const PROC_NAME As String = "ColSpace"

    On Error GoTo EH

    ColSpace = m_VP.ColSpace

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the horizontal space (in pixels) between two cells
Public Property Let ColSpace(ByVal newVal As Long)
    Const PROC_NAME As String = "ColSpace"

    On Error GoTo EH

    m_VP.ColSpace = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("ColSpace")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the vertical space (in pixels) between two cells
Public Property Get RowSpace() As Long
    Const PROC_NAME As String = "RowSpace"

    On Error GoTo EH

    RowSpace = m_VP.RowSpace

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the vertical space (in pixels) between two cells
Public Property Let RowSpace(ByVal newVal As Long)
    Const PROC_NAME As String = "RowSpace"

    On Error GoTo EH

    m_VP.RowSpace = IIf(newVal < 0, 0, newVal)
    PropertyChanged ("RowSpace")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the height of the area used to display the code and name of
' each graphic
Public Property Get NameHeight() As Long
    Const PROC_NAME As String = "NameHeight"

    On Error GoTo EH

    NameHeight = m_VP.NameHeight

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the height of the area used to display the code and name of
' each graphic
Public Property Let NameHeight(ByVal newVal As Long)
    Const PROC_NAME As String = "NameHeight"

    On Error GoTo EH

    m_VP.NameHeight = newVal
    PropertyChanged ("NameHeight")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the width of the area used to display the code and name of
' each graphic
Public Property Get NameWidth() As Long
    Const PROC_NAME As String = "NameWidth"

    On Error GoTo EH

    NameWidth = m_VP.NameWidth

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the width of the area used to display the code and name of
' each graphic
Public Property Let NameWidth(ByVal newVal As Long)
    Const PROC_NAME As String = "NameWidth"

    On Error GoTo EH

    m_VP.NameWidth = newVal
    PropertyChanged ("NameWidth")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the base position of the are used to display the code and name of
' each graphic
Public Property Get NamePosition() As E_BC_CaptionPositionConstants
    Const PROC_NAME As String = "NamePosition"

    On Error GoTo EH

    NamePosition = m_VP.NamePosition

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the base position of the are used to display the code and name of
' each graphic
Public Property Let NamePosition(ByVal newVal As E_BC_CaptionPositionConstants)
    Const PROC_NAME As String = "NamePosition"

    On Error GoTo EH

    m_VP.NamePosition = newVal
    PropertyChanged ("NamePosition")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the offset displacement (in pixels) from the base position of the
' area used to display the code and the name of each graphic
Public Property Get NameOffsetY() As Long
    Const PROC_NAME As String = "NameOffsetY"

    On Error GoTo EH

    NameOffsetY = m_VP.NameOffsetY

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the offset displacement (in pixels) from the base position of the
' area used to display the code and the name of each graphic
Public Property Let NameOffsetY(ByVal newVal As Long)
    Const PROC_NAME As String = "NameOffsetY"

    On Error GoTo EH

    m_VP.NameOffsetY = newVal
    PropertyChanged ("NameOffsetY")
    RePaint True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get ShowTransparency() As Boolean
    Const PROC_NAME As String = "ShowTransparency"

    On Error GoTo EH

    ShowTransparency = m_VP.ShowTransparency
    PropertyChanged ("ShowTransparency")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let ShowTransparency(ByVal newVal As Boolean)
    Const PROC_NAME As String = "ShowTransparency"

    On Error GoTo EH

    m_VP.ShowTransparency = newVal
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

'' Retrieves the optimum width of the control, that is, the width the control
'' should have in order not to have any extra space on the right border.
'' TODO: This property is not working yet.
'Public Property Get OptimumWidth() As Long
'    ' Calculate the optimum width
'    OptimumWidth = m_VP.MarginLeft _
'            + ThumbsPerRow * (m_VP.CellWidth + m_VP.ColSpace) _
'            - m_VP.ColSpace + m_VP.MarginRight
'End Property

' Retrieves the index of the selected cell. When there are several cells
' selected, this represents the main selected cell.
Public Property Get SelectedIndex() As Long
    Const PROC_NAME As String = "SelectedIndex"

    On Error GoTo EH

    SelectedIndex = m_lSelectedIndex

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the index of the selected cell. When there are several cells
' selected, this represents the main selected cell.
Public Property Let SelectedIndex(ByVal newVal As Long)
    Const PROC_NAME As String = "SelectedIndex"

    On Error GoTo EH

    If newVal < 0 Or newVal > m_Fpg.MapCount - 1 Then
        Err.Raise 380 ' Invalid property value error
    End If
    
    m_lSelectedIndex = newVal
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves whether a cell is selected
Public Property Get Selected(ByVal lIndex As Long) As Boolean
    Const PROC_NAME As String = "Selected"

    On Error GoTo EH

    If FpgAvailable = False Then Exit Property
    
    If lIndex < 0 Or lIndex > m_Fpg.MapCount - 1 Then
        Err.Raise 380 ' Invalid property value error
    End If
    
    Selected = ExistsValInCollection(lIndex, m_Selected)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets whether a cell is selected
Public Property Let Selected(ByVal lIndex As Long, ByVal newVal As Boolean)
    Dim bExists As Boolean
    
    Const PROC_NAME As String = "Selected"

    On Error GoTo EH

    If lIndex < 0 Or lIndex > m_Fpg.MapCount - 1 Then
        Err.Raise 380 ' Invalid property value error
    End If
    
    bExists = ExistsValInCollection(lIndex, m_Selected)
    
    If newVal = False And bExists = True Then
        m_Selected.Remove (CStr(lIndex))
    ElseIf newVal = True And bExists = False Then
        m_Selected.Add lIndex, CStr(lIndex)
    End If
    
    ' TODO: Currently this property does not raise the "SelectionChanged" event
    ' This is because the MouseUp and MouseDown events need to "deselect" several
    ' indexes avoiding multiple execution of this event
    ' Another approach must be used because the SelectionChanged event should
    ' obviously be called when the Selected property is changed manually
    
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the number of selected cells
Public Property Get SelectionCount() As Integer
    Const PROC_NAME As String = "SelectionCount"

    On Error GoTo EH

    SelectionCount = m_Selected.Count

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves an array containing the selected graphics codes
' @return an array containing the selected graphics codes
Public Function GetSelectedCodes() As Long()
    Dim i As Integer
    Dim alRes() As Long
    
    Const PROC_NAME As String = "GetSelectedCodes"

    On Error GoTo EH

    If m_Selected.Count = 0 Then Exit Function
    
    ReDim alRes(m_Selected.Count - 1) As Long
    For i = 1 To m_Selected.Count
        alRes(i - 1) = m_Fpg.Maps(m_Selected(i)).code
    Next
    
    GetSelectedCodes = alRes()

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Retrieves an array containing the selected indexes
' @return an array containing the selected graphics codes
Public Function GetSelectedIndexes() As Long()
    Dim i As Integer
    Dim alRes() As Long
    
    Const PROC_NAME As String = "GetSelectedIndexes"

    On Error GoTo EH

    If m_Selected.Count = 0 Then Exit Function
    
    ReDim alRes(m_Selected.Count - 1) As Long
    For i = 1 To m_Selected.Count
        alRes(i - 1) = m_Selected.Item(i)
    Next
    
    GetSelectedIndexes = alRes()

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Deselects all the selected cells
Public Sub DeselectAll()
    Dim lIndex As Variant
    
    Const PROC_NAME As String = "DeselectAll"

    On Error GoTo EH

    If m_Selected Is Nothing Then Exit Sub
    For Each lIndex In m_Selected
        m_Selected.Remove (CStr(lIndex))
    Next
    m_lSelectedIndex = -1
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' ==== FPG CREATION, LOADING, UNLOADING, INSERTION AND REMOVAL ====

' Retrieves whether the control has an FPG or not
Public Property Get FpgAvailable() As Boolean
    Const PROC_NAME As String = "FpgAvailable"

    On Error GoTo EH

    FpgAvailable = IIf(m_Fpg Is Nothing, False, True)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Loads an FPG from the specified file, optionaly using the specified decoder.
' To get an understanding of decoders check bennulib documentation.
' @param sFile the path to the file to be loaded
' @param oDecoder the decoder that will be used to read the file
Public Sub LoadFpg(ByVal sFile As String, Optional oDecoder As IFpgDecoder)
    Const PROC_NAME As String = "LoadFpg"

    On Error GoTo EH

    UnloadFpg
    Set m_Fpg = CreateFpgFromFile(sFile, oDecoder)
    If Not m_Fpg Is Nothing Then
        RaiseEvent FpgBecameAvailable
        m_cScroll.Visible(efsVertical) = True
    End If
    UserControl_Resize
        
    Exit Sub

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Creates a new FPG
'@param eDepth the depth of the FPG
'@param oPalette the palette of the FPG (required only for indexed FPGs)
Public Sub NewFpg(ByVal eDepth As E_BC_DepthModeConstants, _
        Optional oPalette As cPalette)
    
    Const PROC_NAME As String = "NewFpg"

    On Error GoTo EH

    UnloadFpg
    Set m_Fpg = CreateNewFpg(eDepth, oPalette)
    If Not m_Fpg Is Nothing Then RaiseEvent FpgBecameAvailable
    UpdateScrollBarState
    UserControl_Resize

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Saves an FPG to the specified file, optionally using the specified encoder.
' To get an understanding of encoders check bennulib documentation.
' @param sFile the path to the file to be saved
' @param oEncoder the encoder that will be used to write the file
Public Sub SaveFpg(ByVal sFile As String, Optional oEncoder As IFpgEncoder, _
        Optional oEncoderOptions As cOptionSet)
    
    Const PROC_NAME As String = "SaveFpg"

    On Error GoTo EH

    bennulib.SaveFpg m_Fpg, sFile, oEncoder, oEncoderOptions

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Retrieves the FPG object associated with the control
Public Function GetFpg() As cFpg
    Const PROC_NAME As String = "GetFpg"

    On Error GoTo EH

    Set GetFpg = m_Fpg

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Sets the FPG object associated with the control
Public Sub SetFpg(oFpg As cFpg)
    Const PROC_NAME As String = "SetFpg"

    On Error GoTo EH

    UnloadFpg
    Set m_Fpg = oFpg
    If Not m_Fpg Is Nothing Then RaiseEvent FpgBecameAvailable
    UpdateScrollBarState
    UserControl_Resize

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Unloads the FPG of the control
Public Sub UnloadFpg()
    Const PROC_NAME As String = "UnloadFpg"

    On Error GoTo EH

    LockRePaint
    Set m_Fpg = Nothing
    DeselectAll
    UpdateScrollBarState
    UnlockRePaint
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Adds a MAP to the fpg. This is a shortcut for GetFpg().
' @param oMap the map to be added
' @param lCode the code at which the MAP will be inserted
' @param bReplace whether an existing MAP with a code lCode should be replaced
Public Sub AddMap(oMap As cMap, ByVal lCode As Long, ByVal bReplace As Boolean)
    Const PROC_NAME As String = "AddMap"

    On Error GoTo EH

    If Not FpgAvailable Then Exit Sub
    
    m_Fpg.Add oMap, lCode, bReplace
    UserControl_Resize

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Removes a MAP from the fpg. This is a shortcut for GetFpg().RemoveByCode().
' @param lCode the code of the MAP that will be removed
Public Sub RemoveMapByCode(ByVal lCode As Long)
    Const PROC_NAME As String = "RemoveMapByCode"
    
    Dim lIndex As Long
    On Error GoTo EH

    If Not FpgAvailable Then Exit Sub
    
    lIndex = m_Fpg.IndexByCode(lCode)
    If Selected(lIndex) Then
        Selected(lIndex) = False
        If m_lSelectedIndex = lIndex Then
            ' TODO: This should be handled by the selected property, but changes me be required
            ' in the mouse-up event --> left for lateron
            If m_Selected.Count > 0 Then
                m_lSelectedIndex = m_Selected(m_Selected.Count)
            Else
                m_lSelectedIndex = -1
            End If
        End If
        RaiseEvent SelectionChanged(lIndex) ' TODO: This should be handled by selected property
    End If
    
    ' NOTE: The RemoveByCode of m_FpgClass should have an event, otherwise we do not know if
    ' we must update the selection
    m_Fpg.RemoveByCode lCode
    UserControl_Resize

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' ==== ==== ==== ====

' Locks repainting so as sucessive calls to RePaint have no effect
Public Sub LockRePaint()
    Const PROC_NAME As String = "LockRePaint"

    On Error GoTo EH

    m_bRepaintLocked = True

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Unlocks repainting
Public Sub UnlockRePaint()
    Const PROC_NAME As String = "UnlockRePaint"

    On Error GoTo EH

    m_bRepaintLocked = False

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Repaints the control
Public Sub RePaint(Optional ByVal bRecalculateRows As Boolean)
    Const PROC_NAME As String = "RePaint"

    On Error GoTo EH

    If m_bRepaintLocked = False Then
        If bRecalculateRows = True Then
            CalculateRows
            CalculateFirstVisibleRow
            UpdateScrollBarRange
        End If
        UserControl_Paint
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'===============================================================================

' Purpose: Retrieve the number of thumbs that fit in a row
Private Property Get ThumbsPerRow() As Integer
    Const PROC_NAME As String = "ThumbsPerRow"

    On Error GoTo EH

    ThumbsPerRow = IIf(FpgAvailable, m_iThumbsPerRow, 0)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Retrieve the number of rows that are visible, partially or completely
Private Property Get VisibleRows() As Integer
    Const PROC_NAME As String = "VisibleRows"

    On Error GoTo EH

    VisibleRows = IIf(FpgAvailable, m_iVisibleRows, 0)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Retrieve the position of the vertical scroll
Private Property Get ScrollYGlobal() As Long
    Const PROC_NAME As String = "ScrollYGlobal"

    On Error GoTo EH

    ScrollYGlobal = m_lScrollYGlobal

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Sets the position of the vertical scroll
Private Property Let ScrollYGlobal(ByVal newVal As Long)
    Const PROC_NAME As String = "ScrollYGlobal"

    On Error GoTo EH

    If newVal > m_cScroll.Max(efsVertical) Then
        m_lScrollYGlobal = m_cScroll.Max(efsVertical)
    ElseIf newVal < 0 Then
        m_lScrollYGlobal = 0
    Else
        m_lScrollYGlobal = newVal
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' NOTE: The next properties (FirstVisibleRow, TotalRowNeeded, ...) have also
' one related procedure which is used to update the underlaying member
' variable (m_iFirstVisibleRow, m_iTotalRowsNeeded, ...). This could have been
' done in the same Get Property method, but manual update when required was
' preferred because this properties are required by functions such as
' UserControl_Paint which are time-critical and so we want to minimize the
' calculations required. The programmer must ensure to call the "CalculateXXX"
' procedures when required.

' Purpose: Retrieve the row which will be the first to be painted. Note that
' m_iFirstVisibleRow is returned and the programmer is responsible to call
' CalculateFirstVisibleRow to update this variable when necessary
Private Property Get FirstVisibleRow() As Integer
    Const PROC_NAME As String = "FirstVisibleRow"

    On Error GoTo EH

    FirstVisibleRow = IIf(FpgAvailable, m_iFirstVisibleRow, 0)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Calculates the first row (0-based) which is visible or partially
' visible in the viewer
Private Sub CalculateFirstVisibleRow()
    Dim a As Long, b As Long
    
    ' Calculate the first visible row (according to the scroll offset)
    Const PROC_NAME As String = "CalculateFirstVisibleRow"

    On Error GoTo EH

    a = ScrollYGlobal - m_VP.MarginTop
    b = m_VP.CellHeight + m_VP.RowSpace
    m_iFirstVisibleRow = a \ b

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Retrieve the hypotetic number of rows that would be needed to show
' all the thumbs with the current width
Private Property Get TotalRowsNeeded() As Integer
    Const PROC_NAME As String = "TotalRowsNeeded"

    On Error GoTo EH

    TotalRowsNeeded = IIf(FpgAvailable, m_iTotalRowsNeeded, 0)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Calculates the number of rows that would be required to show all
' the thumbs with the current width. Since TotalRowsNeeded is related to
' the scroll bar range, the programmer must usually call UpdateScrollBarRange
' instead of this function, which will also call this function
Private Sub CalculateTotalRowsNeeded()
    Dim a As Long, b As Long
    
    Const PROC_NAME As String = "CalculateTotalRowsNeeded"

    On Error GoTo EH

    a = m_Fpg.MapCount
    b = ThumbsPerRow
    m_iTotalRowsNeeded = a \ b + IIf((a Mod b) = 0, 0, 1)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Creates or deletes de scroll bar
Private Sub UpdateScrollBarState()
    Const PROC_NAME As String = "UpdateScrollBarState"

    On Error GoTo EH
    
    If m_cScroll Is Nothing Then Exit Sub
    
    m_cScroll.Visible(efsHorizontal) = False
    
    If FpgAvailable Then
        m_cScroll.Visible(efsVertical) = True
        m_cScroll.Enabled(efsVertical) = True
    Else
        m_cScroll.Enabled(efsVertical) = False
        m_cScroll.Visible(efsVertical) = True
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Recalculates the range of the scroll bars
Private Sub UpdateScrollBarRange()
    Dim a As Long, b As Long
    
    Const PROC_NAME As String = "UpdateScrollBarRange"

    On Error GoTo EH

    If m_Fpg Is Nothing Then Exit Sub
    
    CalculateTotalRowsNeeded
    a = TotalRowsNeeded * (m_VP.CellHeight + m_VP.RowSpace) _
        - m_VP.RowSpace + m_VP.MarginTop + m_VP.MarginBottom - m_lViewerHeight
    m_cScroll.Max(efsVertical) = IIf(a < 0, 0, a)
    m_cScroll.Enabled(efsVertical) = IIf(a > 0, True, False)

    ScrollYGlobal = ScrollYGlobal
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Given X and Y (in pixels), calculates and return the index of the
' thumb which is in that position. If X and Y belong to a non-thumb area
' (i.e margins and space between rows or cells), the return value will be -1
Private Function GetIndexByCoord(X As Long, Y As Long) As Long
    ' Let us define:
    '   - OVERALL VIEWER AREA (OVA): Area of the necessary width
    '     and height required to show all the cells
    '   - RESTRICTED OVA: OVA without the left-top margin
    '   - CELL AREA: Area of a cell
    '   - EXTENDED CELL AREA: CELL AREA + col & row separation
    Dim x0 As Long, y0 As Long ' Ref system: OVA
    Dim X1 As Long, Y1 As Long ' Ref system: Restricted OVA
    Dim xMax As Long, yMax As Long ' Max coords possible in the OVA
    Dim X2 As Long, Y2 As Long ' Ref system: thumb
    Dim exCellW As Long, exCellH As Long ' Extended cell width and height
    Dim row As Long, col As Long
    Dim result As Long
    Dim r As RECT
    
    Const PROC_NAME As String = "GetIndexByCoord"

    On Error GoTo EH

    x0 = X: y0 = Y + ScrollYGlobal
    exCellW = m_VP.CellWidth + m_VP.ColSpace
    exCellH = m_VP.CellHeight + m_VP.RowSpace

    xMax = exCellW * ThumbsPerRow
    yMax = exCellH * TotalRowsNeeded
    X1 = x0 - m_VP.MarginLeft
    Y1 = y0 - m_VP.MarginTop
    SetRect r, 0, 0, xMax, yMax
    
    result = -1
    
    If PtInRect(r, X1, Y1) Then ' We are in the restricted OVA
        col = X1 \ exCellW
        row = Y1 \ exCellH
        X2 = X1 - col * exCellW
        Y2 = Y1 - row * exCellH
        SetRect r, 0, 0, m_VP.CellWidth, m_VP.CellHeight
        
        If PtInRect(r, X2, Y2) Then ' We are in the CELL AREA
            result = col + row * ThumbsPerRow
        End If
    End If
    
    If result > m_Fpg.MapCount - 1 Then result = -1
    
    GetIndexByCoord = result

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Check if a value exists in a collection
Private Function ExistsValInCollection(Value As Variant, c As Collection) As _
    Boolean
    
    Dim b As Boolean
    Dim v As Variant
    Dim i As Long
    
    Const PROC_NAME As String = "ExistsValInCollection"

    On Error GoTo EH

    b = False
    For i = 1 To c.Count
        v = c.Item(i)
        If v = Value Then
            b = True
            Exit For
        End If
    Next
    
    ExistsValInCollection = b

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Puropose: Calculates visible rows, completely visible rows, and the number
' of thumbs per row. This procedure should be called whenever a property that
' may affect to this value changes (p.g. CellHeight)
Private Sub CalculateRows()
    Dim completelyVisibleRows As Long
    Dim a As Long, b As Long ' Aux variables
    
    Const PROC_NAME As String = "CalculateRows"

    On Error GoTo EH
    
    ' Calculate number of visible cells per row
    m_iThumbsPerRow = (m_lViewerWidth - m_VP.MarginLeft _
                    - m_VP.MarginRight + m_VP.ColSpace) _
                    \ (m_VP.CellWidth + m_VP.ColSpace)
    
    ' Calculate the number of visible rows (including partially visible)
    a = (m_lViewerHeight - m_VP.MarginTop + m_VP.RowSpace)
    b = m_VP.CellHeight + m_VP.RowSpace
    m_iVisibleRows = a \ b + IIf((a Mod b) = 0, 0, 1)
    
    ' Calculate the number of completely visible rows
    completelyVisibleRows = a \ b
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Calculates the displacement in pixels of the position
' of the graphic relative to the thumb area, according to the current
' Horizontal and Vertical align modes
Private Sub CalculateGraphicOffset(lOffX As Long, lOffY As Long, _
            ByVal thumbW As Long, ByVal thumbH As Long)
    
    Const PROC_NAME As String = "CalculateGraphicOffset"

    On Error GoTo EH

    Select Case m_VP.ThumbHorizontalAlign
    Case ebcAlignCenter
        lOffX = (m_VP.ThumbWidth - thumbW) / 2
    Case ebcAlignRight
        lOffX = m_VP.ThumbWidth - thumbW
    Case Else
        lOffX = 0
    End Select
    
    Select Case m_VP.ThumbVerticalAlign
    Case ebcAlignMiddle
        lOffY = (m_VP.ThumbHeight - thumbH) / 2
    Case ebcAlignBottom
        lOffY = m_VP.ThumbHeight - thumbH
    Case Else
        lOffY = 0
    End Select

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Calculates the displacement in pixels of the position
' of the thumb area relative to the cell area, according to the current
' Horizontal and Vertical thumb align modes
Private Sub CalculateThumbOffset(lOffX As Long, lOffY As Long)
    Const PROC_NAME As String = "CalculateThumbOffset"

    On Error GoTo EH

    Select Case m_VP.CellHorizontalAlign
    Case ebcAlignCenter
        lOffX = (m_VP.CellWidth - m_VP.ThumbWidth) / 2
    Case ebcAlignRight
        lOffX = m_VP.CellWidth - m_VP.ThumbWidth
    Case Else
        lOffX = 0
    End Select
    
    Select Case m_VP.CellVerticalAlign
    Case ebcAlignMiddle
        lOffY = (m_VP.CellHeight - m_VP.ThumbHeight) / 2
    Case ebcAlignBottom
        lOffY = m_VP.CellHeight - m_VP.ThumbHeight
    Case Else
        lOffY = 0
    End Select

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Calculates the displacement in pixels of a rectangle area
' which is the area within the Description of the MAP will be printed
' The displacement is relative to the Cell
Private Sub CalculateNameOffset(lOffX As Long, lOffY As Long)
    ' For the moment, all the positions availables center the description
    ' with the cell
    Const PROC_NAME As String = "CalculateNameOffset"

    On Error GoTo EH

    lOffX = (m_VP.CellWidth - m_VP.NameWidth) / 2
    
    Select Case m_VP.NamePosition
    Case ebcTop
        lOffY = 0
    Case ebcBottom
        lOffY = m_VP.CellHeight
    End Select

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Calculates the width and height that the graphic to be
' painted must have according to the current StretchMode status.
' I.e.: If StretchMode = False, the Width and Height will be calculated
' so as the graphic keeps its proportions. If StretchMode = true, the
' returned Width and Height will be that of the thumb area
Private Sub CalculateGraphicDimensions(lWidth As Long, lHeight As Long, _
            lMapWidth As Long, lMapHeight As Long)
    
    Dim lGraphicW As Long
    Dim lGraphicH As Long
    Dim dWRatio As Double
    Dim dHRatio As Double
    
    ' Assume stretch mode is selected
    Const PROC_NAME As String = "CalculateGraphicDimensions"

    On Error GoTo EH

    lGraphicW = m_VP.ThumbWidth
    lGraphicH = m_VP.ThumbHeight
    
    If Not m_VP.StretchGraphics Then ' Stretch mode off
        dWRatio = lGraphicW / lMapWidth
        dHRatio = lGraphicH / lMapHeight
        If dHRatio > dWRatio Then
            lGraphicH = lMapHeight * dWRatio
        Else
            lGraphicW = lMapWidth * dHRatio
        End If
    End If
    
    ' Return values
    lWidth = lGraphicW
    lHeight = lGraphicH

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'===============================================================================

Private Sub m_cScroll_Change(eBar As EFSScrollBarConstants)
    Const PROC_NAME As String = "m_cScroll_Change"

    On Error GoTo EH

    ScrollYGlobal = m_cScroll.Value(eBar)
    CalculateFirstVisibleRow
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub m_cScroll_MouseWheel(eBar As EFSScrollBarConstants, lAmount As Long, _
        ByVal iShift As Integer)
        
    Const PROC_NAME As String = "m_cScroll_MouseWheel"
    
    On Error GoTo EH
    
    RaiseEvent MouseWheel(eBar, lAmount, iShift)
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'===============================================================================

Private Sub UserControl_DblClick()
    Dim lMapIndex As Long
    Dim uPoint As POINTAPI
    
    Const PROC_NAME As String = "UserControl_DblClick"

    On Error GoTo EH

    If Ambient.UserMode = False Then Exit Sub
    If FpgAvailable = False Then Exit Sub
    
    GetCursorPos uPoint
    ScreenToClient UserControl.hWnd, uPoint
    
    lMapIndex = GetIndexByCoord(uPoint.X, uPoint.Y)
    
    If lMapIndex > -1 Then
        LockRePaint
        DeselectAll
        Selected(lMapIndex) = True
        m_lSelectedIndex = lMapIndex
        UnlockRePaint
        RePaint
        
        RaiseEvent GraphicDblClick(lMapIndex)
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Initialize()
    Const PROC_NAME As String = "UserControl_Initialize"

    On Error GoTo EH

    Set m_Selected = New Collection
    m_lSelectedIndex = -1
    
    Set m_cScroll = New cScrollBars
    m_cScroll.Create UserControl.hWnd
    UpdateScrollBarState

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, _
        X As Single, Y As Single)
            
    Const PROC_NAME As String = "UserControl_MouseMove"
    
    Dim bWantsToDrag As Boolean
    
    bWantsToDrag = Abs(CLng(X) - m_lLastMouseDownX) > STARTDRAG_AREA_SIZE _
        Or Abs(CLng(Y) - m_lLastMouseDownY) > STARTDRAG_AREA_SIZE
    
    If Button = vbLeftButton And bWantsToDrag And Not m_bDragCompleted Then
        UserControl.OLEDrag
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
' The mouse up event must detect the following situations:
' - The user is using Ctrl key to de-select an already selected item
' - The user is in "single selection mode", which means that no Ctrl, Shift [or Alt] key is
'   pressed and all items except the one where we clicked must be deselected
'   The MouseDown event will not have performed this action because a drag operation
'   may occur.
    Const PROC_NAME As String = "UserControl_MouseUp"
    
    Dim lOldIndex As Long
    Dim lNewIndex As Long
    
    Dim bCtrl As Boolean
    Dim bShift As Boolean
    Dim bAlt As Boolean
    
    Dim bSelectionChanged As Boolean
    
    On Error GoTo EH
    
    If Ambient.UserMode = False Then Exit Sub
    
    If FpgAvailable Then
    
        lOldIndex = GetIndexByCoord(m_lLastMouseDownX, m_lLastMouseDownY)
        lNewIndex = GetIndexByCoord(CLng(X), CLng(Y))
        
        ' Only when the new index matches the index generated in last mouse down
        ' we will proceed
        If Not (lOldIndex < 0 Or lNewIndex < 0 Or lOldIndex <> lNewIndex) Then
        
            bCtrl = IIf((Shift And vbCtrlMask) = vbCtrlMask, True, False)
            bShift = IIf((Shift And vbShiftMask) = vbShiftMask, True, False)
            bAlt = IIf((Shift And vbAltMask) = vbAltMask, True, False)
            
            LockRePaint
            
            If Button = vbLeftButton And Not m_bPreventMouseUpDeselection Then
                If bCtrl Then
                    If Selected(lNewIndex) Then
                        Selected(lNewIndex) = False
                        If m_Selected.Count > 0 Then
                            m_lSelectedIndex = m_Selected(m_Selected.Count)
                        End If
                        bSelectionChanged = True
                    End If
                ElseIf Not bShift Then
                    DeselectAll
                    Selected(lNewIndex) = True
                    m_lSelectedIndex = lNewIndex
                    bSelectionChanged = True
                End If
            End If
            
            UnlockRePaint
            If bSelectionChanged Then
                RaiseEvent SelectionChanged(lNewIndex)
                RePaint
            End If
        
        End If
        
    End If ' Fpg Available
    
    RaiseEvent MouseUp(Button, X, Y)
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_OLECompleteDrag(Effect As Long)
    Const PROC_NAME As String = "UserControl_OLEDragDrop"
    
    On Error GoTo EH
    
    m_bDragCompleted = True
    
    ' Prevent selection change after a drag operation
    m_bPreventMouseUpDeselection = True
    
    RaiseEvent OLECompleteDrag(Effect)
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_OLEDragDrop(data As DataObject, Effect As Long, _
            Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Const PROC_NAME As String = "UserControl_OLEDragDrop"
    
    Dim sFiles() As String
    Dim vFile As Variant
    Dim l As Long
    Dim lIndex As Long
    Dim oPic As IPicture
    Dim avData() As Byte

    On Error GoTo EH
    
    If FpgAvailable Then
        lIndex = GetIndexByCoord(CLng(X), CLng(Y))
    Else
        lIndex = -1
    End If

    If data.GetFormat(vbCFFiles) Then ' User dropped files
        ReDim sFiles(data.Files.Count - 1) As String
        For Each vFile In data.Files
            sFiles(l) = CStr(vFile)
            l = l + 1
        Next
        RaiseEvent DDFilesDropped(sFiles, Button, Shift, CLng(X), CLng(Y), lIndex)
    ElseIf G_iCustomDDFormat <> 0 And data.GetFormat(G_iCustomDDFormat) Then
        avData = data.GetData(G_iCustomDDFormat)
        RaiseEvent DDCustomFormatDropped(avData, Button, Shift, CLng(X), CLng(Y), lIndex)
    ElseIf data.GetFormat(vbCFDIB) Then
        Set oPic = data.GetData(vbCFDIB)
        RaiseEvent DDDIBDropped(oPic, Button, Shift, CLng(X), CLng(Y), lIndex)
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_OLEDragOver(data As DataObject, Effect As Long, _
            Button As Integer, Shift As Integer, X As Single, Y As Single, _
            State As Integer)

    Const PROC_NAME As String = "UserControl_OLEDragOver"

    On Error GoTo EH

    ' TODO
    UserControl.SetFocus ' TODO: I DO NOT REMEMBER WHY THIS IS HERE! (but I guess it was necessary)

    RaiseEvent OLEDragOver(data, Effect, Button, Shift, X, Y, State)
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_OLEGiveFeedback(Effect As Long, DefaultCursors As Boolean)
    Const PROC_NAME As String = "UserControl_OLEGiveFeedback"
    
    Dim iShift As Integer
    
    On Error GoTo EH

    If GetKeyState(VK_SHIFT) And &H80000000 Then iShift = iShift Or 1
    If GetKeyState(VK_CONTROL) And &H80000000 Then iShift = iShift Or 2
    If GetKeyState(VK_MENU) And &H80000000 Then iShift = iShift Or 4

    RaiseEvent OLEGiveFeedback(Effect, DefaultCursors, iShift)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_OLESetData(data As DataObject, DataFormat As Integer)
    Const PROC_NAME As String = "UserControl_OLESetData"

    Dim iShift As Integer
    
    On Error GoTo EH

    If GetKeyState(VK_SHIFT) And &H80000000 Then iShift = iShift Or 1
    If GetKeyState(VK_CONTROL) And &H80000000 Then iShift = iShift Or 2
    If GetKeyState(VK_MENU) And &H80000000 Then iShift = iShift Or 4

    RaiseEvent OLESetData(data, DataFormat, iShift)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_OLEStartDrag(data As DataObject, AllowedEffects As Long)
    Const PROC_NAME As String = "UserControl_OLEStartDrag"

    On Error GoTo EH

    RaiseEvent OLEStartDrag(data, AllowedEffects)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Terminate()
    Const PROC_NAME As String = "UserControl_Terminate"

    On Error GoTo EH

    Set m_Selected = Nothing
    Set m_cScroll = Nothing
    UnloadFpg

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Resize()
    Dim r As RECT

    Const PROC_NAME As String = "UserControl_Resize"

    On Error GoTo EH

    If Ambient.UserMode = False Then Exit Sub
    
    ' Get the dimensions of the client area
    GetClientRect UserControl.hWnd, r
    m_lViewerWidth = r.Right - r.Left
    m_lViewerHeight = r.Bottom - r.Top
    
    If bPropertiesRead = False Then Exit Sub
    
    CalculateRows
    UpdateScrollBarRange
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Paint()
    Dim iX As Long, iY As Long ' Position of the Cell within the drawing area
    Dim lThOX As Long, lThOY As Long ' Pos. of the thumb area rel to the cell
    Dim lGphOX As Long, lGphOY As Long ' Pos. of the graphic rel. to the thumb area
    Dim lGraphicW As Long, lGraphicH As Long
    Dim lDescOX As Long, lDescOY As Long
    
    Dim scrollYLocal As Long ' Local scroll offset (ref to the usercontrol Rect)
    Dim firstMapIndex As Integer ' Index of the first map to be painted
    
    Dim col As Integer, row As Integer ' Cols and rows we have painted
    Dim i As Long, lItem As Long
    
    Dim bb As New cGDICanvas
    Dim selRect As cGDICanvas
    Dim f As New StdFont
    
    Dim sCaption As String
    
    Dim r As RECT
    
    Const PROC_NAME As String = "UserControl_Paint"

    On Error GoTo EH

    If Ambient.UserMode = False Then Exit Sub
    
    ' Create a GDICanvas to use as a backbuffer
    bb.Create m_lViewerWidth, m_lViewerHeight
    f.Name = UserControl.Font.Name
    f.Size = UserControl.Font.Size
    Set bb.Font = f
    bb.Fill (TranslateColor(UserControl.BackColor))
    
    ' Abort if FPG is not ready
    If FpgAvailable = False Then
        bb.CenterText m_VP.FpgNotAvailableMsg, 0, 0, m_lViewerWidth, m_lViewerHeight, _
            TranslateColor(UserControl.ForeColor)
    Else
        ' Calculate the local coordinates of the scroll
        scrollYLocal = ScrollYGlobal _
                - FirstVisibleRow * (m_VP.CellHeight + m_VP.RowSpace)
        
        ' Calculate position of the thumb area relative to the cell
        CalculateThumbOffset lThOX, lThOY
        
        ' Initialize cols and row counters and obtain the index
        ' of the first MAP to be painted
        col = 1: row = 1
        firstMapIndex = FirstVisibleRow * ThumbsPerRow
        
        ' Paint the MAPs into the back buffer
        iX = m_VP.MarginLeft
        iY = -scrollYLocal + m_VP.MarginTop
        For i = firstMapIndex To m_Fpg.MapCount - 1
            ' Calculate the dimensions of the graphic to be painted
            CalculateGraphicDimensions lGraphicW, lGraphicH, _
                    m_Fpg.Maps(i).Width, m_Fpg.Maps(i).Height
            
            ' If the Graphic is not to be stretched, calculate the position within
            ' the thumb area
            If Not m_VP.StretchGraphics Then
                CalculateGraphicOffset lGphOX, lGphOY, lGraphicW, lGraphicH
            End If
            
            ' TODO: Hacer el clipping a la regi�n de la CELDA
            ' Draw MAP streching to the calculated graphic dimensions
            m_Fpg.Maps(i).Draw bb.hdc, iX + lThOX + lGphOX, _
                        iY + lThOY + lGphOY, lGraphicW, lGraphicH, , , , , m_VP.ShowTransparency
            
            ' Draw MAP caption
            CalculateNameOffset lDescOX, lDescOY
            sCaption = m_Fpg.Maps(i).Description
            If Len(sCaption) > 0 Then sCaption = " - " & sCaption
            bb.CenterText m_Fpg.Maps(i).code & sCaption, _
                    iX + lDescOX, iY + lDescOY + m_VP.NameOffsetY, _
                    m_VP.NameWidth, m_VP.NameHeight, UserControl.ForeColor
        
            ' Draw the cell
            bb.DrawBorder iX, iY, iX + m_VP.CellWidth, iY + m_VP.CellHeight, _
                m_VP.GridColor, PS_DOT, RM_COPYPEN
                     
            ' Focus rect
            If i = m_lSelectedIndex Then
                SetRect r, iX + lThOX + lGphOX, iY + lThOY + lGphOY, _
                        iX + lThOX + lGphOX + lGraphicW, iY + lThOY + lGraphicH + lGphOY
                DrawFocusRect bb.hdc, r
            End If
            
            ' Advance one column and check if we need to go to the next row
            col = col + 1
            If col > ThumbsPerRow Then ' Go next row
                ' Advance one row. Stop if the new row is out of the visible rows
                row = row + 1
                If row > VisibleRows + 1 Then Exit For
                ' Set col to the first column
                col = 1
                ' Calculate the position of the first cell in the next row
                iY = iY + m_VP.RowSpace + m_VP.CellHeight
                iX = m_VP.MarginLeft
            Else
                iX = iX + m_VP.ColSpace + m_VP.CellWidth
            End If
        Next
        
        ' Create the selection rectangle if there are Cells selected
        If Not m_Selected.Count = 0 Then
            Set selRect = New cGDICanvas
            selRect.Create m_VP.CellWidth, m_VP.CellHeight
            selRect.Fill TranslateColor(HighlightForeColor)
        End If
        
        ' Draw the selection rectangle in all the cells which require it
        For i = 1 To m_Selected.Count
            lItem = m_Selected.Item(i)
            ' Only paint the rectangle if the thumb is in the visible area
            If lItem >= firstMapIndex And lItem < _
                        (firstMapIndex + (VisibleRows + 1) * ThumbsPerRow) Then
                row = lItem \ ThumbsPerRow
                col = lItem - row * ThumbsPerRow
                
                iX = m_VP.MarginLeft + (m_VP.ColSpace + m_VP.CellWidth) * col
                iY = -scrollYLocal + m_VP.MarginTop _
                    + (m_VP.RowSpace + m_VP.CellHeight) * (row - FirstVisibleRow)
                
                selRect.DrawToDC bb.hdc, iX, iY, 25
                bb.DrawBorder iX, iY, iX + m_VP.CellWidth, iY + m_VP.CellHeight, _
                    TranslateColor(m_VP.GridColor), PS_SOLID, RM_COPYPEN, , , 2
            End If
        Next
        Set selRect = Nothing
    End If
        
    ' Copy the contents of the backbuffer to the control
    bb.DrawToDC UserControl.hdc
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, _
            X As Single, Y As Single)
            
    Dim bCtrl As Boolean, bShift As Boolean, bAlt As Boolean
    Dim lIndex As Long
    Dim iStep As Integer
    Dim l As Long
    Dim lFirstSelected As Long
    Dim bSelectionChanged As Boolean
    
    Const PROC_NAME As String = "UserControl_MouseDown"

    On Error GoTo EH
    
    m_lLastMouseDownX = CLng(X)
    m_lLastMouseDownY = CLng(Y)
    m_bDragCompleted = False
    m_bPreventMouseUpDeselection = False
    
    If Ambient.UserMode = False Then Exit Sub
    
    If FpgAvailable = False Then Exit Sub
    
    lIndex = GetIndexByCoord(CInt(X), CInt(Y))
    
    bCtrl = IIf((Shift And vbCtrlMask) = vbCtrlMask, True, False)
    bShift = IIf((Shift And vbShiftMask) = vbShiftMask, True, False)
    bAlt = IIf((Shift And vbAltMask) = vbAltMask, True, False)
    
    LockRePaint
    
    If lIndex = -1 Then ' Coord is not related to any index
        If Not (bCtrl Or bShift) And m_Selected.Count > 0 Then
            DeselectAll
            bSelectionChanged = True
        End If
    Else
        If bCtrl Then ' Multiple-discreete selection
            If Not Selected(lIndex) Then
                Selected(lIndex) = True
                ' TODO: PREVENT MOUSE-UP TO DESELECT?
                m_bPreventMouseUpDeselection = True
                bSelectionChanged = True
                m_lSelectedIndex = lIndex
            End If
'            If lIndex <> m_lSelectedIndex Then
'                bSelectionChanged = True
'                m_lSelectedIndex = lIndex
'            End If
            
            '''''''' If there are still graphics selected, we set the "hot" selection to the
            '''''' last index of the selection collection.
            'If m_Selected.Count > 0 Then m_lSelectedIndex = m_Selected(m_Selected.Count)
        ElseIf bShift Then ' Multiple-continous selection
            If m_lSelectedIndex = -1 Then ' The same than single selection
                Selected(lIndex) = True
                m_lSelectedIndex = lIndex
                bSelectionChanged = True
            Else
                If Not (lFirstSelected = m_Selected(1) And lIndex = m_lSelectedIndex) Then
                    lFirstSelected = m_Selected(1)
                    DeselectAll
                    iStep = IIf((lFirstSelected - lIndex) < 0, 1, -1)
                    For l = lFirstSelected To lIndex Step iStep
                        Selected(l) = True
                    Next
                    m_lSelectedIndex = lIndex
                    bSelectionChanged = True
                End If
            End If
        Else ' Single selection
'            If Selected(lIndex) Then
'                m_bPreventMouseUpDeselection = True
'            End If
            If m_Selected.Count > 0 Or m_lSelectedIndex <> lIndex Then
                If Not Selected(lIndex) Then
                    DeselectAll
                    Selected(lIndex) = True
                    m_lSelectedIndex = lIndex
                    bSelectionChanged = True
                Else
                    'm_bPreventMouseUpDeselection = True
                End If
            End If
        End If
    End If
    
    UnlockRePaint
    If bSelectionChanged Then
        RaiseEvent SelectionChanged(m_lSelectedIndex)
        RePaint
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_InitProperties()
    ' *** Default Configuration Values ***
    Const PROC_NAME As String = "UserControl_InitProperties"

    On Error GoTo EH

    m_HighlightForeColor = DEF_HIGHLIGHT_FORECOLOR
    m_HighlightBackColor = DEF_HIGHLIGHT_BACKCOLOR
    m_cScroll.LargeChange(efsVertical) = DEF_SCROLLY_LARGE_CHANGE
    m_cScroll.SmallChange(efsVertical) = DEF_SCROLLY_SMALL_CHANGE
    
    ' Default Viewer Params
    With m_VP
    .StretchGraphics = DEF_STRETCHGRAPHICS
    .CellWidth = DEF_CELL_HEIGHT
    .CellHeight = DEF_CELL_WIDTH
    .ThumbHeight = DEF_THUMB_HEIGHT
    .ThumbWidth = DEF_THUMB_WIDTH
    .ThumbHorizontalAlign = DEF_THUMB_H_ALIGN
    .ThumbVerticalAlign = DEF_THUMB_V_ALIGN
    .NameHeight = DEF_NAME_HEIGHT
    .NameWidth = DEF_NAME_WIDTH
    .NamePosition = DEF_NAME_POSITION
    .NameOffsetY = DEF_NAME_OFFSETY
    .ColSpace = DEF_COL_SPACE
    .RowSpace = DEF_ROW_SPACE
    .MarginLeft = DEF_MARGIN_LEFT
    .MarginRight = DEF_MARGIN_RIGHT
    .MarginTop = DEF_MARGIN_TOP
    .MarginBottom = DEF_MARGIN_BOTTOM
    .FpgNotAvailableMsg = DEF_FPGNOTAVAILABLE_MSG
    .GridColor = DEF_GRIDCOLOR
    .ShowTransparency = DEF_SHOW_TRANSPARENCY
    End With

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    ' Appearance
    Const PROC_NAME As String = "UserControl_ReadProperties"

    On Error GoTo EH

    UserControl.BackColor = PropBag.ReadProperty("BackColor", DEF_BACKCOLOR)
    UserControl.ForeColor = PropBag.ReadProperty("ForeColor", DEF_FORECOLOR)
    Set UserControl.Font = PropBag.ReadProperty("Font", Ambient.Font)
    m_VP.GridColor = PropBag.ReadProperty("GridColor", DEF_GRIDCOLOR)
    m_HighlightForeColor = PropBag.ReadProperty("HighlightForeColor", _
        DEF_HIGHLIGHT_FORECOLOR)
    m_HighlightBackColor = PropBag.ReadProperty("HighlightBackColor", _
        DEF_HIGHLIGHT_BACKCOLOR)
    BorderStyle = PropBag.ReadProperty("BorderStyle", DEF_BORDERSTYLE)
    ' Scroll
    m_cScroll.SmallChange(efsVertical) = _
        PropBag.ReadProperty("ScrollYSmallChange", DEF_SCROLLY_SMALL_CHANGE)
    m_cScroll.LargeChange(efsVertical) = _
        PropBag.ReadProperty("ScrollYLargeChange", DEF_SCROLLY_LARGE_CHANGE)
    ' ViewerParams
    m_VP.MarginLeft = PropBag.ReadProperty("MarginLeft", DEF_MARGIN_LEFT)
    m_VP.MarginTop = PropBag.ReadProperty("MarginTop", DEF_MARGIN_TOP)
    m_VP.MarginRight = PropBag.ReadProperty("MarginRight", DEF_MARGIN_RIGHT)
    m_VP.MarginBottom = PropBag.ReadProperty("MarginBottom", DEF_MARGIN_BOTTOM)
    m_VP.ColSpace = PropBag.ReadProperty("ColSpace", DEF_COL_SPACE)
    m_VP.RowSpace = PropBag.ReadProperty("RowSpace", DEF_ROW_SPACE)
    m_VP.CellWidth = PropBag.ReadProperty("CellWidth", DEF_CELL_WIDTH)
    m_VP.CellHeight = PropBag.ReadProperty("CellHeight", DEF_CELL_HEIGHT)
    m_VP.CellHorizontalAlign = PropBag.ReadProperty("CellHoriontalAlign", _
        DEF_CELL_H_ALIGN)
    m_VP.CellVerticalAlign = PropBag.ReadProperty("CellVerticalAlign", _
        DEF_CELL_V_ALIGN)
    m_VP.ThumbWidth = PropBag.ReadProperty("ThumbWidth", DEF_THUMB_WIDTH)
    m_VP.ThumbHeight = PropBag.ReadProperty("ThumbHeight", DEF_THUMB_HEIGHT)
    m_VP.ThumbHorizontalAlign = PropBag.ReadProperty("ThumbHorizontalAlign", _
        DEF_THUMB_H_ALIGN)
    m_VP.ThumbVerticalAlign = PropBag.ReadProperty("ThumbVerticalAlign", _
        DEF_THUMB_V_ALIGN)
    m_VP.StretchGraphics = PropBag.ReadProperty("StretchGraphics", _
        DEF_STRETCHGRAPHICS)
    
    m_VP.NameHeight = PropBag.ReadProperty("NameHeight", DEF_NAME_HEIGHT)
    m_VP.NameWidth = PropBag.ReadProperty("NameWidth", DEF_NAME_WIDTH)
    m_VP.NamePosition = PropBag.ReadProperty("NamePosition", DEF_NAME_POSITION)
    m_VP.NameOffsetY = PropBag.ReadProperty("NameOffsetY", DEF_NAME_OFFSETY)
    
    m_VP.ShowTransparency = PropBag.ReadProperty("ShowTransparency", DEF_SHOW_TRANSPARENCY)
    
    m_VP.FpgNotAvailableMsg = PropBag.ReadProperty("FpgNotAvailableMsg", DEF_FPGNOTAVAILABLE_MSG)

    bPropertiesRead = True
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    ' Appearance
    Const PROC_NAME As String = "UserControl_WriteProperties"

    On Error GoTo EH

    PropBag.WriteProperty "BackColor", UserControl.BackColor, DEF_BACKCOLOR
    PropBag.WriteProperty "ForeColor", UserControl.ForeColor, DEF_FORECOLOR
    PropBag.WriteProperty "Font", UserControl.Font, Ambient.Font
    PropBag.WriteProperty "GridColor", m_VP.GridColor, DEF_GRIDCOLOR
    PropBag.WriteProperty "HighlightForeColor", m_HighlightForeColor, _
        DEF_HIGHLIGHT_FORECOLOR
    PropBag.WriteProperty "HighlightBackColor", m_HighlightBackColor, _
        DEF_HIGHLIGHT_BACKCOLOR
    PropBag.WriteProperty "BorderStyle", m_eBorderStyle, DEF_BORDERSTYLE
    ' Scroll
    PropBag.WriteProperty "ScrollYSmallChange", _
        m_cScroll.SmallChange(efsVertical), DEF_SCROLLY_SMALL_CHANGE
    PropBag.WriteProperty "ScrollYLargeChange", _
        m_cScroll.LargeChange(efsVertical), DEF_SCROLLY_LARGE_CHANGE
    ' ViewerParams
    PropBag.WriteProperty "MarginLeft", m_VP.MarginLeft, DEF_MARGIN_LEFT
    PropBag.WriteProperty "MarginTop", m_VP.MarginTop, DEF_MARGIN_TOP
    PropBag.WriteProperty "MarginRight", m_VP.MarginRight, DEF_MARGIN_RIGHT
    PropBag.WriteProperty "MarginBottom", m_VP.MarginBottom, DEF_MARGIN_BOTTOM
    PropBag.WriteProperty "ColSpace", m_VP.ColSpace, DEF_COL_SPACE
    PropBag.WriteProperty "RowSpace", m_VP.RowSpace, DEF_ROW_SPACE
    PropBag.WriteProperty "CellWidth", m_VP.CellWidth, DEF_CELL_WIDTH
    PropBag.WriteProperty "CellHeight", m_VP.CellHeight, DEF_CELL_HEIGHT
    PropBag.WriteProperty "CellHorizontalAlign", m_VP.CellHorizontalAlign, _
        DEF_CELL_H_ALIGN
    PropBag.WriteProperty "CellVerticalAlign", m_VP.CellVerticalAlign, _
        DEF_CELL_V_ALIGN
    PropBag.WriteProperty "ThumbWidth", m_VP.ThumbWidth, DEF_THUMB_WIDTH
    PropBag.WriteProperty "ThumbHeight", m_VP.ThumbHeight, DEF_THUMB_HEIGHT
    PropBag.WriteProperty "ThumbHorizontalAlign", m_VP.ThumbHorizontalAlign, _
        DEF_THUMB_H_ALIGN
    PropBag.WriteProperty "ThumbVerticalAlign", m_VP.ThumbVerticalAlign, _
        DEF_THUMB_V_ALIGN
    PropBag.WriteProperty "StretchGraphics", m_VP.StretchGraphics, _
        DEF_STRETCHGRAPHICS
    PropBag.WriteProperty "NameHeight", m_VP.NameHeight, DEF_NAME_HEIGHT
    PropBag.WriteProperty "NameWidth", m_VP.NameWidth, DEF_NAME_WIDTH
    PropBag.WriteProperty "NamePosition", m_VP.NamePosition, DEF_NAME_POSITION
    PropBag.WriteProperty "NameOffsetY", m_VP.NameOffsetY, DEF_NAME_OFFSETY
    PropBag.WriteProperty "FpgNotAvailableMsg", m_VP.FpgNotAvailableMsg, DEF_FPGNOTAVAILABLE_MSG

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
