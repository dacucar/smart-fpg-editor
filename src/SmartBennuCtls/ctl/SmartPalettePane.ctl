VERSION 5.00
Begin VB.UserControl SmartPalettePane 
   ClientHeight    =   3660
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3930
   ScaleHeight     =   244
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   262
End
Attribute VB_Name = "SmartPalettePane"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of Smart Bennu Controls.
'
' Smart Bennu Controls is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Smart Bennu Controls is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Smart Bennu Controls. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' A PALETTE pane.
'===============================================================================

' Control default configuration
Private Const DEF_CELL_WIDTH As Long = 12
Private Const DEF_CELL_HEIGHT As Long = 12
Private Const DEF_CELLS_PER_ROW As Long = 16
Private Const DEF_COL_SPACE As Long = 1
Private Const DEF_ROW_SPACE As Long = 1
Private Const DEF_BORDERSTYLE As Long = 0
Private Const DEF_CANEDIT As Boolean = False

'===============================================================================

Private Declare Function GetWindowRect Lib "User32.dll" (ByVal hWnd As Long, _
        lpRect As RECT) As Long
        
Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Declare Function GetCursorPos Lib "User32.dll" ( _
        lpPoint As POINTAPI) As Long
Private Declare Function ScreenToClient Lib "User32.dll" ( _
        ByVal hWnd As Long, _
        lpPoint As POINTAPI) As Long

Private Type POINTAPI
    X As Long
    Y As Long
End Type

Public Event ColorDblClick(ByVal lColorIndex As Long)
Public Event MouseMove(ByVal lColorIndex As Long)
Public Event SelectionChanged()

Private Const MODULE_NAME As String = "SmartBennuCtls.SmartPalettePane"

Private Const PALETTE_COLORS As Integer = 256

Private Type tViewerParams
    lCellWidth As Long
    lCellHeight As Long
    lCellsPerRow As Long
    lColSpace As Long
    lRowSpace As Long
End Type

Private m_bCanEdit As Boolean ' Determines if the control behaves as an editing control
Private m_lSelStart As Long
Private m_lSelEnd As Long

Private m_oPalette As cPalette
Private m_uVP As tViewerParams
Private m_lViewerHeight As Long
Private m_lViewerWidth As Long
Private m_eBorderStyle As E_BC_BorderStyleConstants

'===============================================================================

' Retrieves the border style of the pane
Public Property Get BorderStyle() As E_BC_BorderStyleConstants
    Const PROC_NAME As String = "BorderStyle"

    On Error GoTo EH

    BorderStyle = m_eBorderStyle

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the border style of the pane
Public Property Let BorderStyle(ByVal newVal As E_BC_BorderStyleConstants)
    Dim eBS As E_BorderStyleConstants
    
    Const PROC_NAME As String = "BorderStyle"

    On Error GoTo EH

    Select Case newVal
    Case ebcNone
        eBS = eNone
    Case ebcSunken
        eBS = eSunken
    Case ebcSunkenOuter
        eBS = eSunkenOuter
    Case Else
        Exit Property
    End Select
    
    m_eBorderStyle = newVal
    SetWindowBorder UserControl.hWnd, eBS
    PropertyChanged ("BorderStyle")

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Get CanEdit() As Boolean
' Returns if the control behaves as an editor (true) or a viewer (false)

    Const PROC_NAME As String = "CanEdit"
    On Error GoTo EH

    CanEdit = m_bCanEdit

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Public Property Let CanEdit(ByVal newVal As Boolean)
' Sets if the control behaves as an editor (true) or a viewer (false)

    Const PROC_NAME As String = "CanEdit"
    On Error GoTo EH

    m_bCanEdit = newVal

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves whether the control is enabled
Public Property Get Enabled() As Boolean
    Const PROC_NAME As String = "Enabled"

    On Error GoTo EH

    Enabled = UserControl.Enabled

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets whether the control is enabled
Public Property Let Enabled(ByVal newVal As Boolean)
    Const PROC_NAME As String = "Enabled"

    On Error GoTo EH

    UserControl.Enabled = newVal
    PropertyChanged ("Enabled")
    RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the width of each color cell
Public Property Get CellWidth() As Long
    Const PROC_NAME As String = "CellWidth"

    On Error GoTo EH

    CellWidth = m_uVP.lCellWidth

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the width of each color cell
Public Property Let CellWidth(ByVal newVal As Long)
    Const PROC_NAME As String = "CellWidth"

    On Error GoTo EH

    m_uVP.lCellWidth = newVal
    PropertyChanged ("CellWidth")
    If Ambient.UserMode = False Then RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the height of each color cell
Public Property Get CellHeight() As Long
    Const PROC_NAME As String = "CellHeight"

    On Error GoTo EH

    CellHeight = m_uVP.lCellHeight

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the height of each color cell
Public Property Let CellHeight(ByVal newVal As Long)
    Const PROC_NAME As String = "CellHeight"

    On Error GoTo EH

    m_uVP.lCellHeight = newVal
    PropertyChanged ("CellHeight")
    If Ambient.UserMode = False Then RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the number of color cells per row
Public Property Get CellsPerRow() As Long
    Const PROC_NAME As String = "CellsPerRow"

    On Error GoTo EH

    CellsPerRow = m_uVP.lCellsPerRow

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the number of color cells per row
Public Property Let CellsPerRow(ByVal newVal As Long)
    Const PROC_NAME As String = "CellsPerRow"

    On Error GoTo EH

    m_uVP.lCellsPerRow = newVal
    PropertyChanged ("CellsPerRow")
    If Ambient.UserMode = False Then RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the horizontal space between two cells
Public Property Get ColSpace() As Long
    Const PROC_NAME As String = "ColSpace"

    On Error GoTo EH

    ColSpace = m_uVP.lColSpace

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the horizontal space between two cells
Public Property Let ColSpace(ByVal newVal As Long)
    Const PROC_NAME As String = "ColSpace"

    On Error GoTo EH

    m_uVP.lColSpace = newVal
    PropertyChanged ("ColSpace")
    If Ambient.UserMode = False Then RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the vertical space between two cells
Public Property Get RowSpace() As Long
    Const PROC_NAME As String = "RowSpace"

    On Error GoTo EH

    RowSpace = m_uVP.lRowSpace

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the vertical space between two cells
Public Property Let RowSpace(ByVal newVal As Long)
    Const PROC_NAME As String = "RowSpace"

    On Error GoTo EH

    m_uVP.lRowSpace = newVal
    PropertyChanged ("RowSpace")
    If Ambient.UserMode = False Then RePaint

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves whether a palette is setted in the pane
Public Property Get PaletteAvailable() As Boolean
    Const PROC_NAME As String = "PaletteAvailable"

    On Error GoTo EH

    If Not m_oPalette Is Nothing Then PaletteAvailable = True

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

'
'Public Property Get

Public Property Get SelectionStart() As Long
    Const PROC_NAME As String = "SelectionStart"

    On Error GoTo EH
    
    SelectionStart = m_lSelStart

    Exit Property
EH:
    Err.Raise MODULE_NAME, PROC_NAME
End Property

Public Property Get SelectionEnd() As Long
    Const PROC_NAME As String = "SelectionEnd"

    On Error GoTo EH
    
    SelectionEnd = m_lSelEnd

    Exit Property
EH:
    Err.Raise MODULE_NAME, PROC_NAME
End Property

' Sets the palette of the pane
' @param oPalette the palette of the pane
Public Sub SetPalette(oPalette As cPalette)
    Const PROC_NAME As String = "SetPalette"

    On Error GoTo EH

    Set m_oPalette = Nothing
    Set m_oPalette = oPalette
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Retrieves the palette of the pane
' @return the palette
Public Function GetPalette() As cPalette
    Const PROC_NAME As String = "GetPalette"

    On Error GoTo EH

    Set GetPalette = m_oPalette

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Loads a palette from the specified file, optionally using the specified
' decoder
' @param the path to the file containing the palette
' @oDecoder the decoder which will be used to read the file
Public Sub LoadPalette(ByVal sFile As String, _
        Optional oDecoder As IPaletteDecoder)
    
    Const PROC_NAME As String = "LoadPalette"

    On Error GoTo EH

    Set m_oPalette = bennulib.CreatePaletteFromFile(sFile, oDecoder)
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Unloads the palette from the pane
Public Sub UnloadPalette()
    Const PROC_NAME As String = "UnloadPalette"

    On Error GoTo EH

    Set m_oPalette = Nothing
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Repaints the palette pane
Public Sub RePaint()
    Const PROC_NAME As String = "RePaint"

    On Error GoTo EH

    UserControl_Paint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_DblClick()
    Dim uPoint As POINTAPI
    Dim lIndex As Long
    
    Const PROC_NAME As String = "UserControl_DblClick"
    On Error GoTo EH

    If Ambient.UserMode = False Then Exit Sub
    If PaletteAvailable = False Then Exit Sub
    
    GetCursorPos uPoint
    ScreenToClient UserControl.hWnd, uPoint
    
    lIndex = GetIndexByCoord(uPoint.X, uPoint.Y)
    
    If lIndex >= 0 And lIndex <= 255 Then
        RaiseEvent ColorDblClick(lIndex)
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'===============================================================================

Private Sub UserControl_InitProperties()
    Const PROC_NAME As String = "UserControl_InitProperties"

    On Error GoTo EH

    With m_uVP
    .lCellHeight = DEF_CELL_HEIGHT
    .lCellWidth = DEF_CELL_WIDTH
    .lCellsPerRow = DEF_CELLS_PER_ROW
    .lColSpace = DEF_COL_SPACE
    .lRowSpace = DEF_ROW_SPACE
    End With
    UserControl.Enabled = True

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lOldSelStart As Long
    
    Const PROC_NAME As String = "UserControl_MouseDown"
    On Error GoTo EH
    
    If Button = vbLeftButton Then
        lOldSelStart = m_lSelStart
        m_lSelStart = GetIndexByCoord(CLng(X), CLng(Y))
        m_lSelEnd = GetIndexByCoord(CLng(X), CLng(Y))
        RePaint
        
        If lOldSelStart <> m_lSelStart Then
            RaiseEvent SelectionChanged
        End If
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Dim lIndex As Long
    Dim lOldSelEnd As Long
    
    Const PROC_NAME As String = "UserControl_MouseMove"
    On Error GoTo EH

    lIndex = GetIndexByCoord(CLng(X), CLng(Y))
    If Button = vbLeftButton Then
        lOldSelEnd = m_lSelEnd
        m_lSelEnd = lIndex
        
        If lOldSelEnd <> m_lSelEnd Then
            RePaint
            RaiseEvent SelectionChanged
        End If
    Else
        RaiseEvent MouseMove(lIndex)
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    Dim lOldSelEnd As Long
    
    Const PROC_NAME As String = "UserControl_MouseUp"
    On Error GoTo EH
    
    If Button = vbLeftButton Then
        lOldSelEnd = m_lSelEnd
        m_lSelEnd = GetIndexByCoord(CLng(X), CLng(Y))
        If lOldSelEnd <> m_lSelEnd Then
            RePaint
            RaiseEvent SelectionChanged
        End If
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_OLEDragOver(data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
    Debug.Print "drag"
End Sub

Private Sub UserControl_Paint()
    Dim i As Integer
    Dim iRow As Integer, iCol As Integer
    Dim lX As Long, lY As Long
    Dim selInnerColor As Long
    Dim lMinIndex As Long, lMaxIndex As Long
    
    Dim bb As cGDICanvas
    Dim clr As T_BL_RGBColor
    
    Const PROC_NAME As String = "UserControl_Paint"

    On Error GoTo EH

    Set bb = New cGDICanvas
    If Not bb.Create(m_lViewerWidth, m_lViewerHeight) Then Exit Sub
    
    bb.Fill RGB(255, 255, 255)
    Set bb.Font = UserControl.Font
    
    ' In desing mode, draw a sample palette
    If Ambient.UserMode = False Then
        For i = 0 To PALETTE_COLORS - 1
            iRow = i \ IIf(m_uVP.lCellsPerRow > 0, m_uVP.lCellsPerRow, DEF_CELLS_PER_ROW)
            iCol = i - iRow * m_uVP.lCellsPerRow
            lX = iCol * (m_uVP.lCellWidth + m_uVP.lColSpace)
            lY = iRow * (m_uVP.lCellHeight + m_uVP.lRowSpace)
            bb.FillRectangle lX, lY, m_uVP.lCellWidth, m_uVP.lCellHeight, _
                    RGB(iRow * 10, iCol * 10, iCol + iRow)
        Next
        bb.CenterText "Palette Sample", 0, 0, m_lViewerWidth, m_lViewerHeight
    Else ' Draw the real palette
        If Not PaletteAvailable Or Not Enabled Then
            bb.CenterText "No palette", 0, 0, m_lViewerWidth, m_lViewerHeight
        Else
            For i = 0 To PALETTE_COLORS - 1
                GetCellCoords i, lX, lY
                clr = m_oPalette.Color(i)
                bb.FillRectangle lX, lY, m_uVP.lCellWidth, m_uVP.lCellHeight, _
                        RGB(clr.r, clr.g, clr.b)
            Next
           
           ' Draw selection
            If m_bCanEdit Then
                If m_lSelStart > m_lSelEnd Then
                    lMinIndex = m_lSelEnd
                    lMaxIndex = m_lSelStart
                Else
                    lMinIndex = m_lSelStart
                    lMaxIndex = m_lSelEnd
                End If
                
                For i = lMinIndex To lMaxIndex
                    GetCellCoords i, lX, lY
                    bb.DrawBorder lX + 1, lY + 1, lX + m_uVP.lCellWidth, lY + m_uVP.lCellHeight, _
                        QBColor(1), , , , , 2
                Next
                
                ' Selection start
                GetCellCoords m_lSelStart, lX, lY
                bb.FillRectangle lX + m_uVP.lCellWidth / 4, lY + m_uVP.lCellHeight / 4, _
                     m_uVP.lCellWidth / 2 - 1 + 1, m_uVP.lCellHeight / 2 + 1, _
                    RGB(255, 255, 255)
                bb.DrawBorder lX + m_uVP.lCellWidth / 4 - 1, lY + m_uVP.lCellHeight / 4 - 1, _
                     lX + m_uVP.lCellWidth / 4 + m_uVP.lCellWidth / 2 + 1, _
                     lY + m_uVP.lCellHeight / 4 + m_uVP.lCellHeight / 2 + 1, _
                    QBColor(1)
                ' Selection end
                GetCellCoords m_lSelEnd, lX, lY
                bb.FillRectangle lX + m_uVP.lCellWidth / 4, lY + m_uVP.lCellHeight / 4, _
                     m_uVP.lCellWidth / 2 - 1 + 1, m_uVP.lCellHeight / 2 + 1, _
                    QBColor(1)
                bb.DrawBorder lX + m_uVP.lCellWidth / 4 - 1, lY + m_uVP.lCellHeight / 4 - 1, _
                     lX + m_uVP.lCellWidth / 4 + m_uVP.lCellWidth / 2 + 1, _
                     lY + m_uVP.lCellHeight / 4 + m_uVP.lCellHeight / 2 + 1, _
                    RGB(255, 255, 255)
            End If
        End If
    End If
    
    bb.DrawToDC UserControl.hdc
    Set bb = Nothing

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub GetCellCoords(ByVal lIndex As Long, ByRef lX As Long, ByRef lY As Long)
' Gets the the cell coords of a given index
    Dim iRow As Long
    Dim iCol As Long
    
    Const PROC_NAME As String = "GetCellCoords"
    On Error GoTo EH

    iRow = lIndex \ m_uVP.lCellsPerRow
    iCol = lIndex - iRow * m_uVP.lCellsPerRow
    lX = iCol * (m_uVP.lCellWidth + m_uVP.lColSpace)
    lY = iRow * (m_uVP.lCellHeight + m_uVP.lRowSpace)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    Const PROC_NAME As String = "UserControl_ReadProperties"

    On Error GoTo EH

    With PropBag
    BorderStyle = .ReadProperty("BorderStyle", DEF_BORDERSTYLE)
    m_uVP.lCellWidth = .ReadProperty("CellWidth", DEF_CELL_WIDTH)
    m_uVP.lCellHeight = .ReadProperty("CellHeight", DEF_CELL_HEIGHT)
    m_uVP.lCellsPerRow = .ReadProperty("CellsPerRow", DEF_CELLS_PER_ROW)
    m_uVP.lColSpace = .ReadProperty("ColSpace", DEF_COL_SPACE)
    m_uVP.lRowSpace = .ReadProperty("RowSpace", DEF_ROW_SPACE)
    m_bCanEdit = .ReadProperty("CanEdit", DEF_CANEDIT)
    UserControl.Enabled = .ReadProperty("Enabled", True)
    End With

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_Resize()
    Dim r As RECT
    
    Const PROC_NAME As String = "UserControl_Resize"

    On Error GoTo EH
    
    ' Get the dimensions of the window
    GetWindowRect UserControl.hWnd, r
    m_lViewerWidth = r.Right - r.Left
    m_lViewerHeight = r.Bottom - r.Top
    
    RePaint

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Const PROC_NAME As String = "UserControl_WriteProperties"

    On Error GoTo EH

    With PropBag
    .WriteProperty "BorderStyle", m_eBorderStyle, DEF_BORDERSTYLE
    .WriteProperty "CellWidth", m_uVP.lCellWidth, DEF_CELL_WIDTH
    .WriteProperty "CellHeight", m_uVP.lCellHeight, DEF_CELL_HEIGHT
    .WriteProperty "CellsPerRow", m_uVP.lCellsPerRow, DEF_CELLS_PER_ROW
    .WriteProperty "ColSpace", m_uVP.lColSpace, DEF_COL_SPACE
    .WriteProperty "RowSpace", m_uVP.lRowSpace, DEF_ROW_SPACE
    .WriteProperty "CanEdit", m_bCanEdit, DEF_CANEDIT
    .WriteProperty "Enabled", UserControl.Enabled, True
    End With

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Function GetIndexByCoord(ByVal lX As Long, lY As Long) As Long
    Dim lCol As Long, lRow As Long
    Dim lCellY As Long
    
    Const PROC_NAME As String = "GetIndexByCoord"
    On Error GoTo EH

    lCol = lX \ (m_uVP.lCellWidth + m_uVP.lColSpace)
    lRow = lY \ (m_uVP.lCellHeight + m_uVP.lRowSpace)
    
    GetIndexByCoord = lRow * CellsPerRow + lCol
    
    ' TODO: May result in a higher value than 255
    ' if the size of the viewer is larger than the
    ' size_of_a_cell * Rows (or cols)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function
