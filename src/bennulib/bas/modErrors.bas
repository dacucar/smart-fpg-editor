Attribute VB_Name = "modErrors"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' MODULE OVERVIEW
'===============================================================================
' bennulib error constants and utility functions.
'===============================================================================

' Error messages are defined in the ./res/bennulib.res resource file and are
' related to these error constants.
Public Const ERR_CANNOTOPENFILE As Long = 101
Public Const ERR_NOTANFPGFILE As Long = 102
Public Const ERR_UNSUPPORTEDVERSION As Long = 103
Public Const ERR_PALETTETRUNCATED As Long = 104
Public Const ERR_GAMMATRUNCATED As Long = 105
Public Const ERR_CPINFOTRUNCATED As Long = 106
Public Const ERR_ANIMATIONFLAGACTIVE As Long = 107
Public Const ERR_BITMAPDATATRUNCATED As Long = 108
' Public Const ERR_OBJECTUNAVAILABLE As Long = 109
Public Const ERR_NOTAMAPFILE As Long = 110
Public Const ERR_NOTAPALFILE As Long = 111
Public Const ERR_MAPINDEXOUTOFRANGE As Long = 112
Public Const ERR_DIFFERENTDEPTHS As Long = 113
Public Const ERR_INVALIDCODE As Long = 114
Public Const ERR_PALETTEREQUIRED As Long = 115
Public Const ERR_UNSUPPORTEDDEPTH As Long = 116
Public Const ERR_SETPALETTEINVALIDDEPTH As Long = 117
Public Const ERR_INVALIDCPID As Long = 118
Public Const ERR_INVALIDMAPWIDTH As Long = 119
Public Const ERR_INVALIDMAPHEIGHT As Long = 120
Public Const ERR_INVALIDCOLORINDEX As Long = 121
Public Const ERR_INSUFFICIENTARRAYELEMENTS As Long = 122
Public Const ERR_FILEDOESNOTCONTAINPALETTE As Long = 123
Public Const ERR_CANNOTCONVERTMAPINFPG As Long = 124
Public Const ERR_INVALIDDEPTH As Long = 125
Public Const ERR_NOTANFNTFILE As Long = 126
Public Const ERR_SERIALIZEDMAPINVALID As Long = 127
Public Const ERR_INVALIDCOMPRESSIONLEVEL As Long = 128
Public Const ERR_ZLIB As Long = 129
Public Const ERR_FILEWRITE As Long = 130

' FreeImage error constants
Public Const ERR_FREEIMAGE As Long = 2000
Public Const ERR_FREEIMAGE_NOTKNOWNFILE As Long = 2001

' As opposed to errors, warnings do not represent a bennulib failure and don't
' need to be handled
Public Const WARNING_DUPLICATEDCODE As Long = 1001
Public Const WARNING_UNSUPPORTEDOPERATION As Long = 1002
Public Const WARNING_CANNOTCREATEOBJECT As Long = 1003
Public Const WARNING_NOTSTANDARDDECODER As Long = 1004

'===============================================================================

' Reads string resource and replace given macros with given values
' Example, given a resource number of, say, 14:
'    "Could not read '|1' in drive |2"
' The call
'    ResolveResString(14, "|1", "TXTFILE.TXT", "|2", "A:")
' would return the string
'    "Could not read 'TXTFILE.TXT' in drive A:"
Public Function ResolveResString(ByVal nResID As Integer, _
    ParamArray vReplacements() As Variant) As String
    
    Dim nMacro     As Integer
    Dim sResString As String
     
    sResString = LoadResString(nResID)
     
    ' For each macro/value pair passed in ...
    For nMacro = LBound(vReplacements) To UBound(vReplacements) Step 2
         
        Dim sMacro As String
        Dim sValue As String
         
        sMacro = CStr(vReplacements(nMacro))
        sValue = vbNullString
         
        If nMacro < UBound(vReplacements) Then
            sValue = vReplacements(nMacro + 1)
        End If
         
        ' Replace all occurrences of sMacro with sValue.
        Dim nPos As Integer
        
        Do
            nPos = InStr(sResString, sMacro)
            
            If 0 <> nPos Then
                sResString = Left$(sResString, nPos - 1) & _
                             sValue & _
                             Mid$(sResString, nPos + Len(sMacro))
            End If
            
        Loop Until nPos = 0
    
    Next nMacro
     
    ResolveResString = sResString
End Function

