Attribute VB_Name = "modGlobals"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' MODULE OVERVIEW
'===============================================================================
' Public constants and utility functions used by the library. The module also
' contains the initialization function of the library (see Main()).
'===============================================================================

' Minimum & Maximum codes of a MAP in an FPG
Public Const MIN_CODE As Long = 1
Public Const MAX_CODE As Long = 999

' Bennu filetypes magic identifier string
Public Const M01_MAGIC As String = "m01"
Public Const MAP_MAGIC As String = "map"
Public Const M16_MAGIC As String = "m16"
Public Const M32_MAGIC As String = "m32"
Public Const PAL_MAGIC As String = "pal"
Public Const FNT_MAGIC As String = "fnt"
Public Const FPG_MAGIC As String = "fpg"
Public Const F16_MAGIC As String = "f16"
Public Const F32_MAGIC As String = "f32"

Public Const PALETTE_NCOLORS = 256 ' Number of colors of a palette
Public Const PALETTE_SIZE = 768 ' Size in bytes of a palette
Public Const PALETTE_UNUSEDBYTES = 576 ' Size of the gamma section
Public Const F_NCPOINTS = &HFFF ' Bit mask for the number of control points of
                                ' the flag field of Bennu file formats
Public Const F_ANIMATION = &H1000 ' Bit mask for enabled/disabled animation of
                                  ' the flag field of Bennu file formats

' List stores all the registered decoders
Public m_auDecoders() As T_BL_Decoder
Public m_lDecoders As Long ' Number of registered decoders

' Error handling routine
Public Err As New cError

'===============================================================================

Private Const MODULE_NAME As String = "bennulib.globals"

' Used as conversion table from and to Extended ASCII 850 coding (MS-DOS Spain
' and Europe charset. Used by Div, Fenix & Bennu).
' TODO: Maybe incompatible in locales such as Japan, etc.
Private Const EXTENDED_ASCII_850_CHARSET As String = _
    "������������������������������׃�����Ѫ��������������������" _
    & "++��++--+-+��++--�-+������i���++�_�̯�����յ������ݯ���=��" _
    & "���������� "

'===============================================================================

' Converts an unsigned interger (32 bits) to a signed integer (16 bits). This is
' to overcome VB6 limitation of not having unsigned interger types.
Public Function UInt32ToSInt16(ByVal UInt32 As Long) As Integer
    Const PROC_NAME As String = "UInt32ToSInt16"
    Dim x%

    On Error GoTo EH

    If UInt32 > 65535 Then
       MsgBox "You passed a value larger than 65535"
       Exit Function
    End If
    
    x% = UInt32 And &H7FFF
    UInt32ToSInt16 = x% Or -(UInt32 And &H8000)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Converts a signed interger (16 bits) to an unsigned integer (32 bits). This is
' to overcome VB6 limitation of not having unsigned interger types.
Public Function SInt16ToUint32(ByVal SInt16 As Integer) As Long
    Const PROC_NAME As String = "SInt16ToUint32"

    On Error GoTo EH

    SInt16ToUint32 = SInt16 And &HFFFF&

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Sifts to the right a certain number of bits. This is to overcome VB6
' limitation of not having unsigned interger types.
Public Function ShiftRightL(ByVal lValue As Long, ByVal n As Long) As Long
    Const PROC_NAME As String = "ShiftRightL"

    On Error GoTo EH

    If lValue < 0 Then
        lValue = ShiftRightL(((lValue And &HFFFFFFFE) \ 2) And &H7FFFFFFF, _
            n - 1)
    Else
        lValue = lValue \ 2 ^ n
    End If
    ShiftRightL = lValue

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Converts a string with the Extended Ascii 850 charset, of fixed length with a
' null termination string to a Unicode (VB6) string.
Public Function AsciiZToString(ByVal s As String, _
        Optional ByVal bFromASCII850 = True) As String
        
    Const PROC_NAME As String = "AsciiZToString"
    
    Dim p As Long
    Dim res$
    Dim i As Integer
    
    On Error GoTo EH

    p = InStr(1, s, Chr(0), vbBinaryCompare)
    If p > 0 Then
        res$ = Left(s, p - 1)
    Else
        res$ = s
    End If

    If bFromASCII850 Then
        ' Change from ASCII_850 encoding to Unicode (VB strings)
        For i = 1 To Len(res$)
            p = Asc(Mid(res$, i, 1))
            If p >= 128 Then
                Mid(res$, i, 1) = Mid(EXTENDED_ASCII_850_CHARSET, p - 127, 1)
            End If
        Next
    End If
    
    AsciiZToString = res$

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Converts a Unicode (VB6) string to  an string with theExtended Ascii 850
' charset, of fixed length with a null termination char.
Public Function StringToAsciiZ(ByVal s As String, ByVal lMaxLen As Long, _
        Optional ByVal bToASCII850 As Boolean = True) As String
        
    Const PROC_NAME As String = "StringToAsciiZ"
    Dim i As Integer
    Dim p As Integer
    Dim res$
    
    On Error GoTo EH

    If Len(s) >= lMaxLen Then
        res$ = Left(s, lMaxLen)
    Else
        res$ = s & String(lMaxLen - Len(s), Chr(0))
    End If
    
    ' Change from Unicode to ASCII_850 encoding
    If bToASCII850 Then
        For i = 1 To Len(res$)
            If Asc(Mid(res$, i, 1)) >= 128 Then
                p = InStr(1, EXTENDED_ASCII_850_CHARSET, Mid(res$, i, 1), _
                    vbBinaryCompare)
                If p > 0 Then Mid(res$, i, 1) = Chr(p + 127)
            End If
        Next
    End If
    
    StringToAsciiZ = res$

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Retrieves an RGBQUAD array with the colors of a the specified palette object.
Public Function GetRGBQuadPaletteFromPalette(oPalette As cPalette) As RGBQUAD()
    Const PROC_NAME As String = "GetRGBQuadPaletteFromPalette"
    Dim i As Integer
    Dim auColors(255) As RGBQUAD

    On Error GoTo EH

    For i = 0 To 255
        auColors(i).rgbRed = oPalette.Color(i).r
        auColors(i).rgbGreen = oPalette.Color(i).g
        auColors(i).rgbBlue = oPalette.Color(i).b
    Next
    
    GetRGBQuadPaletteFromPalette = auColors

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Retrieves an RGBQUAD array with the colors of a the specified palette object.
Public Function GetPaletteFromRGBQuadPalette( _
        auColorsQuad() As RGBQUAD) As cPalette
    
    Const PROC_NAME As String = "GetPaletteFromRGBQuadPalette"
    Dim i As Integer
    Dim auColors(255) As T_BL_RGBColor
    Dim iMaxClrindex As Integer
    Dim blg As New BennulibGlobals

    On Error GoTo EH

    iMaxClrindex = IIf(UBound(auColorsQuad) < 255, UBound(auColorsQuad), 255)
    
    For i = 0 To iMaxClrindex
        auColors(i).r = auColorsQuad(i).rgbRed
        auColors(i).g = auColorsQuad(i).rgbGreen
        auColors(i).b = auColorsQuad(i).rgbBlue
    Next
    
    Set GetPaletteFromRGBQuadPalette = blg.CreatePaletteFromColorArray(auColors)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Creates a Map based on a bitmap object
Public Function CreateMapFromBitmap(bmp As cBitmap) As cMap
    Const PROC_NAME As String = "CreateMapFromBitmap"
    
    Dim oMap As cMap
    Dim iDepth As Integer
    Dim oPal As cPalette
    
    On Error GoTo EH

    ' TODO: Review and test deepely this routine
    If bmp.BPP < 8 Then
        bmp.ConvertTo8Bits
        iDepth = 8
    ElseIf bmp.BPP = 8 Then
        iDepth = 8
    ElseIf bmp.BPP = 16 Then
        bmp.ConvertTo16Bits565
        iDepth = 16
    ElseIf bmp.BPP = 24 Then
        bmp.ConvertTo32Bits
        iDepth = 32
    ElseIf bmp.BPP = 32 Then
        iDepth = 32
    Else
        bmp.ConvertTo32Bits
        iDepth = 32
    End If
    
    If iDepth = 8 Then
        Set oPal = GetPaletteFromRGBQuadPalette(bmp.GetPalette())
    End If
    
    bmp.FlipVertical
    
    ' Create the Map
    Set oMap = New cMap
    oMap.CreateFromBits bmp.Width, bmp.Height, iDepth, bmp.GetBitsPtr, , _
        oPal, True

    ' Release the bmp
    Set bmp = Nothing
    
    Set CreateMapFromBitmap = oMap

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' bennulib library entry point.
Public Sub Main()
    Const PROC_NAME As String = "Main"
    Dim oBLD As New BennulibDecoders
    
    On Error GoTo EH
    
    ' Register embebed decoders
    oBLD.RegisterDecoder "bennulib.cDivFontFpgDecoder", eblDecoderTypeFpg
    oBLD.RegisterDecoder "bennulib.cDefaultMapDecoder", eblDecoderTypeMap
    oBLD.RegisterDecoder "bennulib.cDefaultPaletteDecoder", _
        eblDecoderTypePalette
    oBLD.RegisterDecoder "bennulib.cDefaultFpgDecoder", eblDecoderTypeFpg
    oBLD.RegisterDecoder "bennulib.cFreeImagePaletteDecoder", _
        eblDecoderTypePalette
    oBLD.RegisterDecoder "bennulib.cFreeImageMapDecoder", _
        eblDecoderTypeMap

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub



