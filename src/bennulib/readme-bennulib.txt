--------------------------------------------------------------------------------
                               BENNULIB README
--------------------------------------------------------------------------------
Folloring document applies to bennulib version 1.0.0.16
Author: Dar�o Cutillas Carrillo
Date of creation: 2009.06.04
Date of revision: 2011.05.07
--------------------------------------------------------------------------------

CONTENTS
  I.            Introduction
  II.           Authors
  III.          Copyright Credits & Aplicable Licenses
  IV.           Requeriments
  V.            Getting the bennulib.dll
  VI.           Installing and working with bennulib.dll
  VII.          Getting the sources and compiling

I.      INTRODUCTION
bennulib is an Active X library (dll) that provides a comfortable way to support
Div/Fenix/Bennu graphic, graphic collections, palettes and (not yet ready)
fonts in your programs. bennulib exposes a certain number of classes to create 
and manipulate MAP objects (bennulib graphics), FPG objects (bennulib graphic 
collections) and PALETTE objects(bennulib palettes). In the future, also FONT 
objects (bennulib fonts) will be supported.

bennulib provides also a way to read and write Div/Fenix/Bennu file formats 
(fpg, map, pal, ...) and create bennulib objects from them. Other standard
image and palette formats are supported by the library. bennulib formats support
is extensible, meaning that the programmer can read its own file format reading
and writing routines and create bennulib objects from them with minimal effort.

bennulib is written entirely in Visual Basic 6, but since it is a COM component,
it can be used from any language that supports this technology. This makes it
suitable for using it in VB6, .NET languages, Delphi, C++ among others.

bennulib was intially born under the name of "flamefilelib" and it was intended
to use with Flamebird MX (http://fbtwo.sf.net), to separate the Map and Fpg
treatment classes in a different module that could be reused in other projects.
Because the library was totally independet from Flamebird MX, the name was 
changed to bennulib and, although it finally was not included in Flamebird MX
(may be it will one day), another project was born that makes extensive use of
the library, Smart Fpg Editor (http://smartfpgeditor.googlecode.com).

bennulib is suitable for everyone who wishes to manipulate FPG, MAP, PALETTE and
(in the future) FONT objects, and give support to Div/Fenix/Bennu own file 
formats. This may be the case if you are writting an Fpg or Map editor, or an
entire Fenix/Bennu IDE.

II.     AUTHORS
- Dar�o Cutillas Carrillo <Danko> (lord_danko AT gmail DOT com)

If you wish to colaborate with the project, have any comment, suggestion or 
doubt feel free to contact me.

III.    COPYRIGHT, CREDITS & APLICABLE LICENSES
- Copyright (C) 2009 - 2011 Dar�o Cutillas Carrillo

- All bennulib code, unless otherwise noted, is licensed under GNU General 
  Public License, Version 3. You should have received a copy of the license 
  along with this software (see license-gpl3). You can also find this license at 
  http://www.opensource.org/licenses/gpl-3.0.html.
  
- This software uses the FreeImage open source image library. 
  See http://freeimage.sourceforge.net for details. FreeImage is licensed under
  GNU General Public License, Version 2. You should have received a copy of the
  license along with this software (see license-gpl2). You can also find this
  license at http://www.opensource.org/licenses/gpl-2.0.php.
  
  You have the right to access to the FreeImage source code of the binary 
  version that comes along with this software. You can do that by accessing to
  http://www.esnips.com/doc/c8a22c88-21a1-4d21-9338-f2f2089c1cec/FreeImage3150
  
- This software uses Zlib 1.2.3.
  Copyright (C) 1995-2005 Jean-loup Gailly and Mark Adler.  
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.
  More information can be found at http://www.zlib.net/

IV.     REQUERIMENTS
bennulib depends on two other extern libraries to execute:
- ZLib 1.2.3, compiled with the ZLIB_WINAPI macro defined.
- FreeImage 3.15.0

Both libraries can be obtained at their respective websites, but can also be got
from the Smart Fpg Editor project page (http://smartfpgeditor.googlecode.com), 
from the Smart Fpg Editor - Required Third Party Libraries package. 
You can download this package on the following link:
http://smartfpgeditor.googlecode.com/files/spfge-third-party-lib-2.zip

This package contains the above commented two libraries. The zlib.dll has been
renamed to zlibvb.dll to avoid conflics with any existing zlib dll on your
system. These libraries must be copied in the same folder where bennulib.dll
is placed, or to the system folder.

V.      GETTING BENNULIB
Latest version of bennulib, in binary format (bennulib.dll) can be found at the
Smart Fpg Editor project page (http://smartfpgeditor.googlecode.com), in the
download section. The library is also included with any installer package of
Smart Fpg Editor.

VI.     INSTALLING AND WORKING WITH BENNULIB.DLL
After bennulib.dll from the distribution package, you can copy bennulib.dll to 
whichever folder you prefer in your coputer.

Because bennulib is a COM object, in order to use it, you must first register it
in the system. You can do that by using the "regsvr32.exe" program. This program
is included in most Microsoft Windows systems. If you do not have it you can get
a free copy from http://support.microsoft.com/kb/267279.
The process to register the library consists in running the following command:
        regsvr32.exe "ABSOLUTE_PATH\bennulib.dll"
where ABSOLUTE_PATH is the absolute path of the folder where the library is
located.
You can unregister the library at anytime by calling
        regsvr32.exe \u "ABSOLUTE_PATH\bennulib.dll"
In fact, it is encouraged that you unregister any bennulib.dll version before 
you register a new one.

If have installed Smart Fpg Editor from an installer package, the library may be
already installed and registered in your computer, so you don't need to do
anything to install it.

To work with bennulib.dll, you may refer to the documentation of the language
you are using to know how to work with ActiveX components. 
In Visual Basic 6, start a new project and go to Project->References to show the
references' list. There, locate the bennulib.dll.
I have not tried to use it in other languages but I want to know it if you
managed to do it.

Currently, there is no documentation available about the classes exposed by
bennulib. You may have to refer to bennulib source code to see its capabilities.
However, if you are planning to use bennulib in your project, I will be willing
to support you to do it. You can contact me at the email direction specified in
the 'AUTHORS' section.

VI.     GETTING AND COMPILING THE SOURCES
Sources of bennulib are freely available to download at the download section of
the Smart Fpg Editor project page (http://smartfpgeditor.googlecode.com).
Alternatively, an SVN repository is available where you can get the most up to
date sources of the library. To download sources from SVN use:

svn checkout http://smartfpgeditor.googlecode.com/svn/trunk/bennulib your-folder

If you want to learn more about svn go to http://subversion.tigris.org/.

To compile the sources you will need to have a working copy of Microsoft Visual
Basic 6 environment. Available binary version has been compiled with the
'professional edition' and with the VB6 Service Pack 6 package installed. Only
professional and enterprise editions of VB6 are capable of generating ActiveX
dll projects, so you must have one if this VB6 editions.

In order to compile bennulib, you need to copy the zlibvb.dll and freeimage.dll
(see 'IV. Requirements') into the system folder.


