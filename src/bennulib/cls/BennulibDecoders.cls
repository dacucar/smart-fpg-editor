VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BennulibDecoders"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' This class provides several global method and definitions to deal with
' decoders.
'
' A bennulib decoder is a class that knows how to read files and build a
' certain kind of bennulib entity, such as for example a bennulib MAP or PALETTE
' object. bennulib includes decoders for most common file types, including most
' Div/Fenix/Bennu formats and other standarized formats.
'
' Normally, you read these file formats by using the LoadXXXFromFile (here XXX
' refers to Fpg, Map, Palette, etc.) methods (see BennulibGlobals.cls), which
' accepts a XXXDecoder object to specify how the file must be read. All these
' XXXDecoder classes implement a IXXXDecoder interface. If for any reason you
' need to open an certain file type that is not supported by the embedded
' decoders, you can create your own decoder class by implementing the appropiate
' interface, reading the file and building the FPG, MAP, PALETTE. For example,
' you could created a decoder to build an FPG object from a Animated-Gif file,
' or you may have your own graphic format.
'
' Embedded decoders implement also another interface: the IStandardDecoder
' interface. This is not required in order to use the LoadXXXFromFile functions
' commented above, however if you implement this interface you can get benefit
' of the decoder-registering system. Registering a decoer means make bennulib
' know about a decoder. To register a decoder call the RegisterDecoder method.
' A class must implement the IStandardDecoder in order to be registered as a
' decoder. By doing so, you can retrieve a suitable decoder to read a certain
' file using the GetDecoder method. Refer to the description of that method
' and the IStandardDecoder interface to know more.
'===============================================================================

Public Enum E_BL_DecoderTypeConstants
    eblDecoderTypeFpg = 1
    eblDecoderTypeMap = 2
    eblDecoderTypePalette = 3
End Enum
#If False Then
    Const eblDecoderTypeFpg = 1
    Const eblDecoderTypeMap = 2
    Const eblDecoderTypePalette = 3
#End If

Public Type T_BL_Decoder
    sClassName As String
    eType As E_BL_DecoderTypeConstants
End Type

'===============================================================================

Private Const MODULE_NAME As String = "bennulib.BennulibDecoders"

'===============================================================================

' Registers a IDecoder to be recognized by the library. Is not necessary to
' register decoders to be able to use them, but registering them allow
' using the GetDecoder function to discover the type of decoder suitable
' for an specified file.
' Note that this method does not verify if the class sClassName exists or
' it implements the IStandardDecoder interface.
' @param sClassName the name of a class that implements the IStandardDecoder
' interface
Public Sub RegisterDecoder(ByVal sClassName As String, _
        ByVal eType As E_BL_DecoderTypeConstants)
    
    Const PROC_NAME As String = "RegisterDecoder"
    On Error GoTo EH
    
    ReDim Preserve m_auDecoders(m_lDecoders) As T_BL_Decoder
    m_auDecoders(m_lDecoders).sClassName = sClassName
    m_auDecoders(m_lDecoders).eType = eType
    m_lDecoders = m_lDecoders + 1
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Retrieves an appropiate decoder of the specified type for the specified
' file. Decoders need to be previously registered via the RegisterDecoder
' method. If invalid class names or non IStandarDecoder decoders have been
' registered, this function will generate a warning (in form of a MessageBox).
' If several decoders of one type can read two files, those registered
' in last place will have preference over those registered in first place.
' @param sFileName the path and name of the file
' @param eType the type of decoder
' @return an object of the first decoder class than can read the specified file
Public Function GetDecoder(ByVal sFileName As String, _
        ByVal eType As E_BL_DecoderTypeConstants) As Object
        
    Const PROC_NAME As String = "GetDecoder"
    Dim o As Object
    Dim oDecoder As IStandardDecoder
    Dim l As Long
    
    For l = m_lDecoders - 1 To 0 Step -1
        If m_auDecoders(l).eType = eType Then
        
            Set o = CreateObject(m_auDecoders(l).sClassName)
            
            If TypeOf o Is IStandardDecoder Then
                Set oDecoder = o
                If oDecoder.CanRead(sFileName) Then ' Decoder can read
                    Set GetDecoder = o
                    Exit For
                End If
            Else
                ' Show a warning if the decoder does not implements the
                ' IStandardDecoder interface
                Set o = Nothing
                MsgBox ResolveResString(WARNING_NOTSTANDARDDECODER, "|class", _
                    m_auDecoders(l).sClassName), vbExclamation Or vbOKOnly
            End If
        End If
NEXTCHECK:
    Next
    
    Exit Function
EH:
    If Err.Number = 429 Then ' The object cannot be created
        MsgBox ResolveResString(WARNING_CANNOTCREATEOBJECT, "|class", _
            m_auDecoders(l).sClassName), vbExclamation Or vbOKOnly
        Resume NEXTCHECK
    Else
        Err.Raise , MODULE_NAME, , , , PROC_NAME
    End If
End Function
