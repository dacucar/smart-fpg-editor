VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BennulibGlobals"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' bennulib exposes a certain number of classes to work with "bennulib entities".
' By bennulib entities it must be understood a MAP, an FPG or a PALETTE objects.
' It is important to hilight that this Bennu entities concept must not be
' mixed with Map, Fpg, Pal, et.c Div/Fenix/Bennu file formats. bennulib entities
' do not require the existence of a file: A MAP object must be understood as
' a bennulib graphic, an FPG object as a bennulib collection of graphics and
' a PALETTE object as a bennulib palette.
'
' Classes that encapsulate these bennulib objects are cMap, cFpg and cPalette.
' You can not directly create new instances of these classes via the VB6 "new"
' operator. Instead, you must use the global functions exposed by this class.
'
' You can create entities from scratch, create them from existing files or
' generating files from them. The name of the functions of this class are
' self-explanatory so refer to them and their description to know how to create
' bennulib entities in your program
'
' This class also defines the error constants that the library uses
'===============================================================================
                        
' bennulib RGB color
Public Type T_BL_RGBColor
    r As Byte
    g As Byte
    b As Byte
End Type

' bennulib Control Point
Public Type T_BL_ControlPoint
    Id As Long
    x As Long
    y As Long
End Type

Public Enum E_BL_ErrorConstants
    eblCannotOpenFile
    eblNotAnFpgFile
    eblUnsupportedVersion
    eblPaletteTruncated
    eblGammaTruncated
    eblCpInfoTruncated
    eblAnimationFlagActive
    eblBitmapDataTruncated
    ' eblOBJECTUNAVAILABLE
    eblNotAMapFile
    eblNotAPalFile
    eblMapIndexOutOfRange
    eblDifferentDepths
    eblInvalidCode
    eblPaletteRequired
    eblUnsupportedDepth
    eblSetPaletteInvalidDepth
    eblInvalidCp
    eblInvalidMapWidth
    eblInvalidMapHeight
    eblInvalidColorIndex
    eblInsufficientArrayElements
    eblFileDoesNotContainPalette
    eblCannotConvertMapInFpg
    eblInvalidDepth
    eblNotAnFntFile
    ' FreeImage error constants
    eblFreeImage
    eblFreeImage_NotKnownFile
End Enum

'===============================================================================

Private Const MODULE_NAME As String = "bennulib.BennulibGlobals"

'===============================================================================

' Retrieves an string containing the version number of the library.
Public Property Get Version() As String
    Const PROC_NAME As String = "Version"
    On Error GoTo EH
    
    Version = App.Major & "." & App.Minor & "." & App.Revision
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the maximum allowable code for a MAP inside an FPG
Public Property Get MIN_CODE() As Long
    Const PROC_NAME As String = "MIN_CODE"
    On Error GoTo EH
    
    MIN_CODE = modGlobals.MIN_CODE
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the minimum allowable code for a MAP inside an FPG
Public Property Get MAX_CODE() As Long
    Const PROC_NAME As String = "MAX_CODE"
    On Error GoTo EH
    
    MAX_CODE = modGlobals.MAX_CODE
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Creates a new MAP object. This function may be useful when creating your own
' MAP decoders. Extreme care must be taken with the ptrData parameter, which
' should point to the bitmap data, which is expected to be in the appropiate
' format. Unexpected errors and critical failure of your application may occur
' if you don't meet this requirement.
' Depending on the iDepth parameter, the bitmap data is expected to have the
' following format:
' - 8bpp: an array of bytes, each byte holds one pixel of the image. The byte
' does not store color information, but an index to a 256-color palette
' (provided by the oPalette parameter).
' - 16bpp: an array of 16 bits integer. Each pixel uses 2 bytes in 565 format,
' 5 bits for Red, 6 bits for Green and 5 bits for blue.
' - 32bpp: an array of 32 bits interger. Each pixel uses 4 bytes, ARGB format.
' Use the bRawData parameter to specify whether your data is padded or not to
' a 32 bits boundary.
' @param lWidth the width of the MAP to be created
' @param lHeight the height of the MAP to be created
' @param iDepth the depth of the MAP to be created (8, 16, 32)
' @param ptrData a pointer to the bitmap data. A copy of the data will be
' done so you don't need to keep your reserved bitmap data memory
' @param sDescription a description for the MAP
' @param oPalette a PALETTE for the MAP. This parameter is ignored if iDepth
' is different than 8 and required otherwise
' @param bRawData whether the bitmap data to which ptrData points to is padded
' to a 32bits boundary or not. True if not padded, false if it is padded.
' @return a new MAP object
Public Function CreateMapFromBits(ByVal lWidth As Long, _
            ByVal lHeight As Long, ByVal iDepth As Integer, _
            ByVal ptrData As Long, Optional ByVal sDescription As String = "", _
            Optional oPalette As cPalette, _
            Optional bRawData As Boolean = False) As cMap
            
    Const PROC_NAME As String = "CreateMapFromBits"
    Dim oMap As New cMap
    
    On Error GoTo EH
    
    oMap.CreateFromBits lWidth, lHeight, iDepth, ptrData, _
        sDescription, oPalette, Not bRawData
    
    Set CreateMapFromBits = oMap
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Creates a MAP object from a serialized map
' @param avSerializedMap an array of bytes containing the serialized MAP
' @returns a MAP build from the serialized map
Public Function CreateMapFromSerializedMap(avSerializedMap() As Byte)
    Const PROC_NAME As String = "CreateMapFromSerializedMap"
   
    Dim oMap As New cMap
   
    On Error GoTo EH

    oMap.DeSerialize avSerializedMap

    Set CreateMapFromSerializedMap = oMap

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Creates a MAP object from the specified file, using the specified decoder
' @param sFile the path to the file to be read
' @param oDecoder an IMapDecoder object to read the specified file
' @return a MAP object created from the specified file
Public Function CreateMapFromFile(ByVal sFile As String, _
            Optional oDecoder As IMapDecoder) As cMap
    
    Const PROC_NAME As String = "CreateMapFromFile"
    On Error GoTo EH
    
    If oDecoder Is Nothing Then
        Set oDecoder = New cDefaultMapDecoder ' Default decoder
    End If
    
    Set CreateMapFromFile = oDecoder.Decode(sFile)
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Creates a MAP from an IPicture object.
' @param oOlePicture the IPicture object
' @return a MAP object created from the specified IPicture
Public Function CreateMapFromOlePicture(oPic As IPicture) As cMap
    Const PROC_NAME As String = "CreateMapFromOlePicture"

    On Error GoTo EH
    
    Dim bmp As New cBitmap
    
    bmp.CreateFromOLEPicture oPic
    
    If bmp.Dib = 0 Then
        Debug.Assert False
        ' TODO: Raise error?
    End If

    ' NOTE: 32bpp DIBs do not use the transparency information and it is set to 0.
    ' Thus, we remove it so as the resulting map is not completely black.
    If bmp.IsTransparent = False And bmp.BPP = 32 Then
        bmp.RemoveAlphaChannel
    End If
    
    Set CreateMapFromOlePicture = CreateMapFromBitmap(bmp)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Public Function CreateOlePictureFromMap(oMap As cMap) As IPicture
    Const PROC_NAME As String = "CreateOlePictureFromMap"

    On Error GoTo EH
    
    Set CreateOlePictureFromMap = oMap.GetBitmapCopy.ToOlePicture
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Saves a MAP into a file using the specified encoder
' @param sFile the path to the file to be written
' @param oEncoder an IMapEncoder object to write the file
Public Sub SaveMap(oMap As cMap, ByVal sFile As String, _
            Optional oEncoder As IMapEncoder, _
            Optional oEncoderOptions As cOptionSet)
    
    Dim uKvp As Variant
    Dim oStdEncoder As IStandardEncoder
    
    Const PROC_NAME As String = "SaveMap"
    On Error GoTo EH
    
    If oEncoder Is Nothing Then
        Set oEncoder = New cDefaultMapEncoder
    End If
    
    If Not oEncoderOptions Is Nothing Then
        Set oStdEncoder = oEncoder
        For Each uKvp In oEncoderOptions
            oStdEncoder.EncoderOption(uKvp.sKey) = uKvp.sValue
        Next
    End If
    
    oEncoder.Encode oMap, sFile
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Creates a new FPG of the specified depth (8, 16, 32).
' @param iDepth the depth of the FPG to be created (8, 16, 32)
' @param oPalette the palette for the new FPG. This parameter is required
' if iDepth = 8 and it will be ignored in any other case
Public Function CreateNewFpg(ByVal iDepth As Integer, _
            Optional oPalette As cPalette) As cFpg
            
    Const PROC_NAME As String = "CreateNewFpg"
    On Error GoTo EH
    
    Dim oFpg As New cFpg
    oFpg.CreateNew iDepth, oPalette
    
    Set CreateNewFpg = oFpg
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Creates a FPG object from the specified file, using the specified decoder
' @param sFile the path to the file to be read
' @param oDecoder an IFpgDecoder object to read the specified file
' @return a FPG object created from the specified file
Public Function CreateFpgFromFile(ByVal sFile As String, _
            Optional oDecoder As IFpgDecoder) As cFpg
    
    Const PROC_NAME As String = "CreateFpgFromFile"
    On Error GoTo EH
    
    If oDecoder Is Nothing Then
        Set oDecoder = New cDefaultFpgDecoder ' Default decoder
    End If
    
    Set CreateFpgFromFile = oDecoder.Decode(sFile)
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Saves an FPG into a file using the specified encoder
' @param sFile the path to the file to be written
' @param oEncoder an IFpgEncoder object to write the file
Public Sub SaveFpg(oFpg As cFpg, ByVal sFile As String, _
            Optional oEncoder As IFpgEncoder, _
            Optional oEncoderOptions As cOptionSet)
    
    Dim uKvp As Variant
    Dim oStdEncoder As IStandardEncoder
    
    Const PROC_NAME As String = "SaveFpg"
    On Error GoTo EH
    
    If oEncoder Is Nothing Then
        Set oEncoder = New cDefaultFpgEncoder
    End If
    
    If Not oEncoderOptions Is Nothing Then
        Set oStdEncoder = oEncoder
        For Each uKvp In oEncoderOptions
            oStdEncoder.EncoderOption(uKvp.sKey) = uKvp.sValue
        Next
    End If
    
    oEncoder.Encode oFpg, sFile
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Create a PALETTE from an array of bytes. The array is expected to have 768
' elements and every color is represented by three consecutive bytes (RGB).
' @param auColors an array of 256 T_BL_RGBColor containing the RGB colors of the
' palette
' @param bVGAPal if true, each color component of the avColors array is supposed
' to go in the range 0-63.
' @return the resulting PALETTE object
Public Function CreatePaletteFromByteArray(avColors() As Byte, _
            Optional bVGAPal As Boolean) As cPalette
    
    Const PROC_NAME As String = "CreatePaletteFromByteArray"
    Dim oPal As New cPalette
    
    On Error GoTo EH
    
    oPal.CreateFromByteArray avColors, bVGAPal
    
    Set CreatePaletteFromByteArray = oPal
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Create a PALETTE from an array of RGB colors
' @param auColors an array of 256 T_BL_RGBColor containing the RGB colors of the
' palette
' @return the resulting PALETTE object
Public Function CreatePaletteFromColorArray( _
        auColors() As T_BL_RGBColor) As cPalette
            
    Const PROC_NAME As String = "CreatePaletteFromColorArray"
    Dim oPal As New cPalette
    Dim avColors(PALETTE_SIZE - 1) As Byte
    Dim i As Integer
    
    On Error GoTo EH
    
    If UBound(auColors) < PALETTE_NCOLORS - 1 Then
        Err.Raise ERR_INSUFFICIENTARRAYELEMENTS Or vbObjectError, MODULE_NAME, _
            ResolveResString(ERR_INSUFFICIENTARRAYELEMENTS)
    End If
    
    For i = 0 To PALETTE_NCOLORS - 1
        avColors(i * 3) = auColors(i).r
        avColors(i * 3 + 1) = auColors(i).g
        avColors(i * 3 + 2) = auColors(i).b
    Next
    
    oPal.CreateFromByteArray avColors, False
    Set CreatePaletteFromColorArray = oPal
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Creates a PALETTE object from the specified file, using the specified decoder
' @param sFile the path to the file to be read
' @param oDecoder an IPaletteDecoder object to read the specified file
' @return a PALETTE object created from the specified file
Public Function CreatePaletteFromFile(ByVal sFile As String, _
            Optional oDecoder As IPaletteDecoder) As cPalette
    
    Const PROC_NAME As String = "CreatePaletteFromFile"
    On Error GoTo EH
    
    If oDecoder Is Nothing Then
        Set oDecoder = New cDefaultPaletteDecoder  ' Default decoder
    End If
    
    Set CreatePaletteFromFile = oDecoder.Decode(sFile)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Saves a PALETTE into a file using the specified encoder
' @param sFile the path to the file to be written
' @param oEncoder an IPaletteEncoder object to write the file
Public Sub SavePalette(oPalette As cPalette, ByVal sFile As String, _
            Optional oEncoder As IPaletteEncoder, _
            Optional oEncoderOptions As cOptionSet)
    
    Dim uKvp As Variant
    Dim oStdEncoder As IStandardEncoder
    
    Const PROC_NAME As String = "SavePalette"
    On Error GoTo EH
    
    If oEncoder Is Nothing Then
        Set oEncoder = New cDefaultPaletteEncoder
    End If
    
    If Not oEncoderOptions Is Nothing Then
        Set oStdEncoder = oEncoder
        For Each uKvp In oEncoderOptions
            oStdEncoder.EncoderOption(uKvp.sKey) = uKvp.sValue
        Next
    End If
    
    oEncoder.Encode oPalette, sFile
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
