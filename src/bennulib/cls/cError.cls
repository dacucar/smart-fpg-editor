VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' VB6 Error class replacement
'===============================================================================

Public Property Get Description() As String
    Description = VBA.Err.Description
End Property

Public Property Let Description(ByVal newVal As String)
    VBA.Err.Description = newVal
End Property

Public Property Get HelpContext() As Long
    HelpContext = VBA.Err.HelpContext
End Property

Public Property Let HelpContext(ByVal newVal As Long)
    VBA.Err.HelpContext = newVal
End Property

Public Property Get Erl() As Long
    Erl = VBA.Erl
End Property

Public Property Get HelpFile() As String
    HelpFile = VBA.Err.HelpFile
End Property

Public Property Let HelpFile(ByVal newVal As String)
    VBA.Err.HelpFile = newVal
End Property

Public Property Get Source() As String
    Source = VBA.Err.Source
End Property

Public Property Let Source(ByVal newVal As String)
    VBA.Err.Source = newVal
End Property

Public Property Get Number() As Long
    Number = VBA.Err.Number
End Property

Public Property Let Number(ByVal newVal As Long)
    VBA.Err.Number = newVal
End Property

Public Sub Clear()
    VBA.Err.Clear
End Sub

Public Sub Raise(Optional ByVal Number As Variant, _
    Optional ByVal Source As String, _
    Optional ByVal Description As String, Optional ByVal HelpFile As String, _
    Optional ByVal HelpContext As Long, Optional ByVal ProcName As String)
    
    Dim lNumber As Long
    Dim sDescription As String
    Dim sSource As String
    Dim sProcName As String
    
    ' If values are not provided, use those stored in the VBA.Err
    If IsMissing(Number) Then lNumber = VBA.Err.Number Else lNumber = Number
    If Len(Description) = 0 Then sDescription = VBA.Err.Description Else _
        sDescription = Description
    If Len(Source) = 0 Then sSource = VBA.Err.Source Else sSource = Source
    If Len(ProcName) = 0 Then sProcName = sSource Else sProcName = ProcName
    
    ' Format the error description into something more readable
    sDescription = FormatDescription(lNumber, sDescription, sSource, sProcName)
    
    VBA.Err.Raise lNumber, sSource, sDescription, HelpFile, HelpContext
End Sub

Private Function FormatDescription(ByVal lErrNo As Long, _
        ByVal sOldDescription As String, _
        ByVal sSource As String, _
        ByVal sProcName As String) As String
    
    Dim sErl As String
    Dim sCallStackElement As String
    Dim sCallStack As String
    Dim sText As String
    Const STR_CALL_STACK As String = "Call Stack:" & vbCrLf

    ' Negative error numbers are considered components' errors and we must
    ' substract the vbObjectError constant to get an understandable value
    If lErrNo < 0 Then lErrNo = lErrNo - vbObjectError
    
    ' Grab the line number if available
    If Erl <> 0 Then sErl = ", line " & CStr(Erl)
    
    ' Format the new call stack element
    sCallStackElement = sSource & "@" & sProcName & sErl
    
    ' If the description is already formatted, get the callstack and add a
    ' new line
    If InStr(1, sOldDescription, "Error #" & CStr(lErrNo) & "#") > 0 Then
        Dim iPos As Integer
        iPos = InStr(1, sOldDescription, STR_CALL_STACK)
        
        Debug.Assert iPos > 0
        
        ' Get the call stack
        sCallStack = Right(sOldDescription, _
            Len(sOldDescription) - iPos + 1 - Len(STR_CALL_STACK))
        
        ' Add the new element
        sCallStack = sCallStackElement & vbCrLf & sCallStack
        
        ' Create the new message
        sText = Left(sOldDescription, iPos - 1)
        sText = sText & STR_CALL_STACK & sCallStack
    Else ' Different error (or not formatted with our sintax)
        sText = "Error #" & CStr(lErrNo) & "#:" & vbCrLf
        sText = sText & "'" & sOldDescription & "'" & vbCrLf & vbCrLf
        sText = sText & STR_CALL_STACK
        sText = sText & sCallStackElement
    End If

    FormatDescription = sText
End Function

