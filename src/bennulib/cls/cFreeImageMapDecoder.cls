VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cFreeImageMapDecoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' MAP decoder for FreeImage known file formats. You can refer to FreeImage
' documentation (http://freeimage.sourceforge.net) to know which formats are
' supported, but most common formats are included (png, gif, bmp, jpg, etc.).
'===============================================================================

Implements IStandardDecoder
Implements IMapDecoder

'===============================================================================

Private Const MODULE_NAME As String = "bennulib.cFreeImageMapDecoder"

'===============================================================================

Private Function IStandardDecoder_CanRead(ByVal sFileName As String) As Boolean
    Const PROC_NAME As String = "IStandardDecoder_CanRead"
    Dim bmp As cBitmap
    
    On Error GoTo EH
    
    Set bmp = New cBitmap
    IStandardDecoder_CanRead = bmp.CanRead(sFileName)
    
    Set bmp = Nothing
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IMapDecoder_Decode(ByVal sFileName As String) As cMap
    Const PROC_NAME As String = "IMapDecoder_Decode"
    
    Dim bmp As cBitmap
    
    On Error GoTo EH
    
    Set bmp = New cBitmap
    bmp.Load sFileName
    
    Set IMapDecoder_Decode = CreateMapFromBitmap(bmp)
    IMapDecoder_Decode.Description = GetFileTitle(sFileName)
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Given a path, returns the file title (without the extension)
Private Function GetFileTitle(ByVal sPath As String) As String
    Const PROC_NAME As String = "GetFileTitle"
    
    Dim p As Integer
    Dim s As String
    
    p = InStrRev(sPath, "\")
    If p > 0 And Len(sPath) > p Then
        s = Right(sPath, Len(sPath) - p)
    End If
    
    p = InStrRev(s, ".")
    If p > 0 And Len(s) > p Then
        s = Left(s, p - 1)
    End If
    
    GetFileTitle = s
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

