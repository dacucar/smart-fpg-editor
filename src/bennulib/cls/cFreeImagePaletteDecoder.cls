VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cFreeImagePaletteDecoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' PALETTE decoder for FreeImage palettized file formats. You can refer to
' FreeImage documentation (http://freeimage.sourceforge.net) to know which
' formats are supported, but most common formats are included such as 8bpp png,
' gif, bmp, etc.
'===============================================================================

Implements IStandardDecoder
Implements bennulib.IPaletteDecoder

'===============================================================================

Private Const MODULE_NAME As String = "bennulib.cFreeImagePaletteDecoder"

'===============================================================================

Private Function IStandardDecoder_CanRead(ByVal sFileName As String) As Boolean
    Const PROC_NAME As String = "IStandardDecoder_CanRead"
    Dim bmp As cBitmap
    
    On Error GoTo EH
    
    Set bmp = New cBitmap
    If bmp.CanRead(sFileName) Then
        bmp.Load sFileName
        
        If bmp.BPP <= 8 Then IStandardDecoder_CanRead = True
    End If
    
    Set bmp = Nothing
    Exit Function
EH:
    ' bmp.Load raises an error if the file is not a known type. If the
    ' error is not caused by it, raise the error
    If Err.Number <> (ERR_FREEIMAGE_NOTKNOWNFILE Or vbObjectError) Then
        Err.Raise , MODULE_NAME, , , , PROC_NAME
    Else
        Err.Clear
    End If
End Function

Private Function IPaletteDecoder_Decode(ByVal sFileName As String) As cPalette
    Const PROC_NAME As String = "IPaletteDecoder_Decode"
    Dim bmp As cBitmap
    Dim auPal() As RGBQUAD
    
    On Error GoTo EH
    
    Set bmp = New cBitmap
    bmp.Load sFileName
    
    If bmp.BPP <= 8 Then
        auPal = bmp.GetPalette()
        Set IPaletteDecoder_Decode = GetPaletteFromRGBQuadPalette(auPal)
    Else
        Err.Raise vbObjectError Or ERR_FILEDOESNOTCONTAINPALETTE, MODULE_NAME, _
            ResolveResString(ERR_FILEDOESNOTCONTAINPALETTE, "|file", sFileName)
    End If
    
    Set bmp = Nothing
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

