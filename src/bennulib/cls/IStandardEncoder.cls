VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IStandardEncoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' A common interface to encoders that can be registered in bennulib (see
' BennulibEncoders.cls).
' NOTE: Registering encoders is not yet implemented.
'
' This extra complexity will allow having encoders which use different
' extensions (for example, a hipotetical cJpegEncoder may need to output to
' extensions jpeg and jpg).
' The application may know which extensions are related to the encoder by
' using these functions
'===============================================================================

Public Property Get DefaultFileExtension() As String

End Property

Public Property Get KnownFileExtensions() As String()

End Property

Public Property Let EncoderOption(ByVal sKey As String, ByVal sValue As String)
    
End Property

Public Function KnowsFileExtension(ByVal sExt As String) As Boolean

End Function
