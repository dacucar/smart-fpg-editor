VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOptionSet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Option Base 0

'===============================================================================

Private Const MODULE_NAME As String = "bennulib.cDefaultPaletteEncoder"

'===============================================================================

Public Type T_BL_KeyValuePair
    sKey As String
    sValue As String
End Type

Private m_Options As Collection

Private Sub Class_Initialize()
    Set m_Options = New Collection
End Sub

Public Sub SetOption(ByVal sKey As String, ByVal sValue As String)
    Dim uKvp As T_BL_KeyValuePair
    uKvp.sKey = sKey
    uKvp.sValue = sValue
    m_Options.Add uKvp, sKey
End Sub

' Allows For...each...next iteration
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Const PROC_NAME As String = "NewEnum"
    On Error GoTo EH
    
    Set NewEnum = m_Options.[_NewEnum]
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Public Property Get OptionItem(ByVal sKey As String) As T_BL_KeyValuePair
Attribute OptionItem.VB_UserMemId = 0
    
    Const PROC_NAME As String = "OptionItem"
    On Error GoTo EH

    OptionItem = m_Options(sKey)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property
