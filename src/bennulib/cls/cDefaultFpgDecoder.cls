VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDefaultFpgDecoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' FPG decoder for DIV / Fenix/ Bennu Fpg file format. This is also the default
' FPG decoder.
'===============================================================================

Implements IStandardDecoder
Implements IFpgDecoder

'===============================================================================

Private Const Version As Byte = &H0 ' Current readable Fpg version
Private Const MODULE_NAME As String = "bennulib.cDefaultFpgDecoder"

'===============================================================================

' Purpose: Reads the MAGIC of the file and optionally retrieves the Depth and
' the version. Returns false if the file is not a valid FPG file.
' NOTE: Use carefully. lFile must be a valid file id.
Private Function ReadMagic(ByVal lFile As Long, Optional iDepth As Integer, _
        Optional vVersion As Byte) As Boolean
    
    Const PROC_NAME As String = "ReadMagic"
    Dim sMagicFileType As String * 3
    Dim avMagicDescriptor(4) As Byte
    
    On Error GoTo EH
    
    Debug.Assert (lFile <> 0)
    
    ReadMagic = True
    
    ' 3 first bytes describe the depth of the FPG
    gzread lFile, ByVal sMagicFileType, 3
    sMagicFileType = LCase(sMagicFileType)
    
    Select Case sMagicFileType
    Case LCase(FPG_MAGIC)
        iDepth = 8
    Case LCase(F16_MAGIC)
        iDepth = 16
    Case LCase(F32_MAGIC)
        iDepth = 32
    Case Else ' Not map file
        ReadMagic = False
    End Select
    
    If ReadMagic Then
        ' Next 4 bytes are MS-DOS termination, and last is the FPG version
        gzread lFile, avMagicDescriptor(0), 5
        
        If Not (avMagicDescriptor(0) = &H1A And _
                avMagicDescriptor(1) = &HD And _
                avMagicDescriptor(2) = &HA And _
                avMagicDescriptor(3) = &H0) Then
            ReadMagic = False
        End If
        
        If ReadMagic Then
            vVersion = avMagicDescriptor(4)
            If vVersion > Version Then ReadMagic = False ' Different version
        End If
    End If
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IStandardDecoder_CanRead(ByVal sFileName As String) As Boolean
    Const PROC_NAME As String = "IStandardDecoder_CanRead"
    Dim lFile As Long
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    IStandardDecoder_CanRead = ReadMagic(lFile)
    
    Call gzclose(lFile)
    
    Exit Function
EH:
    If lFile <> 0 Then gzclose (lFile)
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IFpgDecoder_Decode(ByVal sFileName As String) As cFpg
    Const PROC_NAME As String = "IFpgDecoder_Decode"
    Dim lFile As Long
    Dim vVersion As Byte
    Dim iDepth As Integer
    Dim avPalette() As Byte
    Dim avPaletteUnused() As Byte
    
    ' MAP
    Dim lMapLen As Long
    Dim lWidth As Long, lHeight As Long
    Dim lCode As Long
    Dim sName As String * 12
    Dim sDescription As String * 32
    Dim lMapDataLength As Long
    Dim lFlags As Long
    Dim iNumCP As Integer ' Number of control points
    Dim aiControlPoints() As Integer
    Dim avData8() As Byte
    Dim aiData16() As Integer
    Dim alData32() As Long
    Dim lDataPtr As Long
    Dim iCPX As Integer, iCPY As Integer
    
    Dim i As Integer
    Dim oMap As cMap
    Dim oPal As cPalette
    Dim oFpg As cFpg
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    vVersion = Version
    If Not ReadMagic(lFile, iDepth, vVersion) Then
        ' ReadMagic will return false in two cases: when the file is not a FPG
        ' file or when the version is greater than VERSION
        If vVersion > Version Then
            Err.Raise vbObjectError Or ERR_UNSUPPORTEDVERSION, MODULE_NAME, _
                ResolveResString(ERR_UNSUPPORTEDVERSION, "|version", vVersion)
        Else
            Err.Raise vbObjectError Or ERR_NOTANFPGFILE, MODULE_NAME, _
                ResolveResString(ERR_NOTANFPGFILE, "|file", sFileName)
        End If
    End If
    
    ' Palette if 8bpp
    If iDepth = 8 Then
        ReDim avPalette(PALETTE_SIZE - 1) As Byte
        ReDim avPaletteUnused(PALETTE_UNUSEDBYTES - 1) As Byte
        If gzread(lFile, avPalette(0), PALETTE_SIZE) < PALETTE_SIZE Then
            Err.Raise vbObjectError Or ERR_PALETTETRUNCATED, MODULE_NAME, _
                ResolveResString(ERR_PALETTETRUNCATED)
        End If
        If gzread(lFile, avPaletteUnused(0), PALETTE_UNUSEDBYTES) _
                < PALETTE_UNUSEDBYTES Then
            Err.Raise vbObjectError Or ERR_GAMMATRUNCATED, MODULE_NAME, _
                ResolveResString(ERR_GAMMATRUNCATED)
        End If
            
        ' Create the palette object
        Set oPal = New cPalette
        oPal.CreateFromByteArray avPalette, True
    End If
    
    ' Create the FPG object
    Set oFpg = New cFpg
    oFpg.CreateNew iDepth, oPal
    
    Dim readFailed As Boolean
    
    Do
readgraphic:
        If gzread(lFile, lCode, 4) <> 4 Then Exit Do
        
        If (gzeof(lFile) <> 0 Or gztell(lFile) = -1) Then Exit Do ' EOF
        
        If gzread(lFile, lMapLen, 4) <> 4 Then Exit Do
        
        If gzread(lFile, ByVal sDescription, 32) <> 32 Then Exit Do
        
        If gzread(lFile, ByVal sName, 12) <> 12 Then Exit Do
        
        If gzread(lFile, lWidth, 4) <> 4 Then Exit Do
        
        If gzread(lFile, lHeight, 4) <> 4 Then Exit Do
        
        lMapDataLength = lWidth * lHeight * (iDepth / 8)
        
        If lWidth <= 0 Or lHeight <= 0 Then Exit Do
        
        ' Control Points
        ' First 12 bits of the iFlags short integer tells the higher id for the
        ' defined controlpoint
        ' Bit 13 set to 1 means that the map has animation activated (this is
        ' not supported by Fenix or Bennu)
        If gzread(lFile, lFlags, 4) <> 4 Then Exit Do
        iNumCP = (lFlags And F_NCPOINTS)
        If iNumCP > 0 Then
            ReDim aiControlPoints(iNumCP * 2 - 1) As Integer
            If gzread(lFile, aiControlPoints(0), iNumCP * 4) < (iNumCP * 4) Then
                Err.Raise vbObjectError Or ERR_CPINFOTRUNCATED, MODULE_NAME, _
                    ResolveResString(ERR_CPINFOTRUNCATED)
            End If
        End If
        
        ' Animation (not supported in Bennu/Fenix/Div so consider an error)
        If (lFlags And F_ANIMATION) = 1 Then
            Err.Raise vbObjectError Or ERR_ANIMATIONFLAGACTIVE, MODULE_NAME, _
                ResolveResString(ERR_ANIMATIONFLAGACTIVE)
        End If
        
        ' Bitmap data
        Select Case iDepth
        Case 8
            ReDim avData8(lWidth * lHeight - 1) As Byte
            lDataPtr = VarPtr(avData8(0))
        Case 16
            ReDim aiData16(lWidth * lHeight - 1) As Integer
            lDataPtr = VarPtr(aiData16(0))
        Case 32
            ReDim alData32(lWidth * lHeight - 1) As Long
            lDataPtr = VarPtr(alData32(0))
        End Select
        
        If gzread(lFile, ByVal lDataPtr, lMapDataLength) < lMapDataLength Then
            Err.Raise vbObjectError Or ERR_BITMAPDATATRUNCATED, MODULE_NAME, _
                ResolveResString(ERR_BITMAPDATATRUNCATED)
        End If
        
        ' Ignore codes outside the Legitimate ones...
        ' This is motivated by the fact that FPG Edit uses a "phantom graphic"
        ' to store information.
        ' Versions up to 2016 use code 1001 while successive versions
        ' use -1
        ' This relaxed validation policy increases program compatibility...
        If (lCode < 1 Or lCode > 999) Then
            GoTo readgraphic
        End If
        
        ' Create the MAP object
        Set oMap = New cMap
        oMap.CreateFromBits lWidth, lHeight, iDepth, lDataPtr, _
                AsciiZToString(sDescription), oPal
        
        ' Control points
        For i = 0 To iNumCP - 1
            iCPX = aiControlPoints(i * 2)
            iCPY = aiControlPoints(i * 2 + 1)
            If Not (iCPX = -1 And iCPY = -1) Then
                oMap.SetControlPoint i, iCPX, iCPY
            End If
        Next
        
        ' Add the MAP to the FPG
        If oFpg.Add(oMap, lCode, False) = False Then
            Debug.Assert False
            MsgBox ResolveResString(WARNING_DUPLICATEDCODE), _
                vbExclamation Or vbOKOnly
        End If
        
        ' Clean up
        Erase avData8
        Erase aiData16
        Erase alData32
        Erase aiControlPoints
        Set oMap = Nothing
    Loop
    
    gzclose lFile

    Erase avPalette
    Erase avPaletteUnused
    
    Set IFpgDecoder_Decode = oFpg

    Exit Function
EH:
    Set oFpg = Nothing
    Set oMap = Nothing
    Set oPal = Nothing
    Erase avData8
    Erase aiData16
    Erase alData32
    Erase aiControlPoints
    Erase avPalette
    Erase avPaletteUnused
    If lFile <> 0 Then gzclose lFile
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function ReadOrFail()
    Const PROC_NAME As String = "ReadOrFail"
    
   On Error GoTo EH

    

   On Error GoTo 0
   Exit Function

EH:

    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function
