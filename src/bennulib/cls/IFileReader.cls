VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IFileReader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2013 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Public Function FileOpen(ByVal sPath As String) As Long

End Function

Public Sub Dispose()

End Sub

Public Function ReadBuffer(ByVal buflen as Long) As Long

End Function

Public Function ReadString() As String

End Function

Public Function ReadInterger() As Integer

End Function

Public Function ReadIntergers(ByVal number as Long) As Integer()

End Function

Public Function ReadLong() As Long

End Function

Public Function ReadLongs(ByVal number as Long) As Long()

End Function

Public Function ReadByte() As Byte

End Function

Public Function ReadBytes(ByVal number as Long) As Byte()

End Function

