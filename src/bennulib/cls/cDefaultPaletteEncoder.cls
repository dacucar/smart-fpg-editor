VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDefaultPaletteEncoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' PALETTE encoder for Fenix / Bennu Pal file format. This is also the default
' PALETTE encoder.
'===============================================================================

Implements IPaletteEncoder
Implements IStandardEncoder

'===============================================================================

Private Const Version As Byte = &H0
Private Const MODULE_NAME As String = "bennulib.cDefaultPaletteEncoder"

'===============================================================================

Private m_CompressionLevel As Integer

Private Sub Class_Initialize()
    m_CompressionLevel = 9
End Sub

Private Sub IPaletteEncoder_Encode(oPalette As cPalette, _
        ByVal sFileName As String)
    Const PROC_NAME As String = "IPaletteEncoder_Encode"
    Dim avPaletteUnused(PALETTE_UNUSEDBYTES - 1) As Byte
    Dim avPalette() As Byte
    Dim sMagicFileType As String * 3
    Dim avMagicDescriptor(4) As Byte
    Dim lFile As Long
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "wb" & CStr(m_CompressionLevel))
    If lFile = 0 Then
        Err.Raise vbObjectError & ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    ' Filetype
    sMagicFileType = PAL_MAGIC
    gzwrite lFile, ByVal sMagicFileType, 3
    avMagicDescriptor(0) = &H1A
    avMagicDescriptor(1) = &HD
    avMagicDescriptor(2) = &HA
    avMagicDescriptor(3) = &H0
    avMagicDescriptor(4) = Version
    gzwrite lFile, avMagicDescriptor(0), 5
    
    ' Palette data
    avPalette() = oPalette.GetColorsAsByteArray(True)
    gzwrite lFile, avPalette(0), PALETTE_SIZE
    
    ' Palette Gammas:
    ' NOTE: not DIV-compatible, to make it compatible refer to the DIV manual
    ' documentation
    gzwrite lFile, avPaletteUnused(0), PALETTE_UNUSEDBYTES
    
    gzclose lFile
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub SetCompressionLevelOption(ByVal sValue As String)
    Dim iCompressionLevel As Integer
    Dim bSuccess As Boolean
    
    bSuccess = False
    
    If IsNumeric(sValue) Then
        iCompressionLevel = CInt(sValue)
        
        If iCompressionLevel >= 0 And iCompressionLevel < 9 Then
            bSuccess = True
        End If
    End If
    
    If bSuccess Then
        m_CompressionLevel = iCompressionLevel
    Else
        ' TODO: Log invalid option / raise error
    End If
End Sub

Private Property Let IStandardEncoder_EncoderOption(ByVal sKey As String, ByVal sValue As String)
    Select Case sKey
    Case "compression-level"
        SetCompressionLevelOption (sValue)
    Case Else
        ' TODO: Log invalid option / raise error
    End Select
End Property

Private Property Get IStandardEncoder_DefaultFileExtension() As String
    IStandardEncoder_DefaultFileExtension = "pal"
End Property

Private Property Get IStandardEncoder_KnownFileExtensions() As String()
    Dim s(0) As String
    s(0) = "pal"
    IStandardEncoder_KnownFileExtensions = s
End Property

Private Function IStandardEncoder_KnowsFileExtension(ByVal sExt As String) As Boolean
    IStandardEncoder_KnowsFileExtension = CBool(LCase(sExt) = "pal")
End Function
