VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPalette"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' cPalette class encapsulates a PALETTE object. A PALETTE is a set of 256 colors
' to be used with 8bpp MAPs and FPGs. The first index of the PALETTE is always
' considered to be the "transparency entry", that is, pixels of a 8 bpp graphic
' which are set to this first component will be shown as transparent.
'===============================================================================

Private Declare Sub CopyMemoryL Lib "kernel32" Alias "RtlMoveMemory" ( _
                        ByVal pDst As Long, _
                        ByVal pSrc As Long, _
                        ByVal ByteLen As Long)
                        
'===============================================================================

Private m_auColors(PALETTE_NCOLORS - 1) As T_BL_RGBColor
Private Const MODULE_NAME As String = "bennulib.cPalette"

'===============================================================================

'Public Event ColorChanged(ByVal lIndex As Long, _
'                    uNewColor As T_BL_RGBColor, _
'                    uOldColor As T_BL_RGBColor)
                    
'===============================================================================

' Returns the color for the specified index in the PALETTE
' Note, modification of the fields of the returned T_BL_RgbColor
' will NOT result in modification of the colors in the palette.
' Therefore the following may not work as could be guessed at first:
'  oPal As New cPalette
'  uColor As T_BL_RGBColor
'  uColor = oPal.Color(44)
'  uColor.r = 33 ' THIS WON'T CHANGE THE COLOR IN THE PALETTE!
Public Property Get Color(ByVal lIndex As Long) As T_BL_RGBColor
    Const PROC_NAME As String = "Color"
    On Error GoTo EH
    
    If lIndex > PALETTE_NCOLORS - 1 Or lIndex < 0 Then
        Err.Raise vbObjectError Or ERR_INVALIDCOLORINDEX, MODULE_NAME, _
            ResolveResString(ERR_INVALIDCOLORINDEX, "|index", CStr(lIndex))
    Else
        Color = m_auColors(lIndex)
    End If
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the color for the specified index in the PALETTE
Public Property Let Color(ByVal lIndex As Long, newVal As T_BL_RGBColor)
    Const PROC_NAME As String = "Color"
    On Error GoTo EH

    Dim uOldColor As T_BL_RGBColor
    If lIndex > PALETTE_NCOLORS - 1 Or lIndex < 0 Then
        Err.Raise vbObjectError Or ERR_INVALIDCOLORINDEX, MODULE_NAME, _
            ResolveResString(ERR_INVALIDCOLORINDEX, "|index", CStr(lIndex))
    Else
        uOldColor = m_auColors(lIndex)
        m_auColors(lIndex) = newVal
        'RaiseEvent ColorChanged(lIndex, newVal, uOldColor)
    End If
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

'===============================================================================

' If bVGAPal is true the avPalette array contains colors in the range 0-63
Friend Sub CreateFromByteArray(avColors() As Byte, ByVal bVGAPal As Boolean)
    Const PROC_NAME As String = "CreateFromByteArray"
    Dim i As Integer
    
    On Error GoTo EH
    
    If UBound(avColors) < (PALETTE_NCOLORS * 3 - 1) Then
        Err.Raise vbObjectError Or ERR_INSUFFICIENTARRAYELEMENTS, MODULE_NAME, _
            ResolveResString(ERR_INSUFFICIENTARRAYELEMENTS, "|method", _
            "CreateFromByteArray")
    Else
        Erase m_auColors
        If Not bVGAPal Then
            CopyMemoryL VarPtr(m_auColors(0)), _
                        VarPtr(avColors(0)), _
                        PALETTE_NCOLORS * 3
        Else ' VGA Mode
            For i = 0 To PALETTE_NCOLORS - 1
                m_auColors(i).r = avColors(i * 3) * 4
                m_auColors(i).g = avColors(i * 3 + 1) * 4
                m_auColors(i).b = avColors(i * 3 + 2) * 4
            Next
        End If
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Returns an array of bytes containing the colors. Each color is represented
' by three bytes and each color component is within the range 0-255 unless
' the bVGAPal is true, in which case the returned array contains colors in the
' range 0-63
Friend Function GetColorsAsByteArray(Optional bVGAPal As Boolean) As Byte()
    Const PROC_NAME As String = "GetColorsAsByteArray"
    Dim i As Integer
    Dim avColors(PALETTE_NCOLORS * 3 - 1) As Byte

    On Error GoTo EH

    If bVGAPal = False Then
        CopyMemoryL VarPtr(avColors(0)), VarPtr(m_auColors(0)), _
            PALETTE_NCOLORS * 3
    Else
        For i = 0 To PALETTE_NCOLORS - 1
            avColors(i * 3) = m_auColors(i).r \ 4
            avColors(i * 3 + 1) = m_auColors(i).g \ 4
            avColors(i * 3 + 2) = m_auColors(i).b \ 4
        Next
    End If
    
    GetColorsAsByteArray = avColors

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Returns a PALETTE which is a copy of the current PALETTE
Public Function GetCopy() As cPalette
    Const PROC_NAME As String = "GetCopy"
    Dim oPal As cPalette
    
    On Error GoTo EH
    
    Set oPal = New cPalette
    oPal.CreateFromByteArray GetColorsAsByteArray(False), False
    
    Set GetCopy = oPal
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

'===============================================================================

Private Sub Class_Initialize()
    Const PROC_NAME As String = "Class_Initialize"
    
    Dim i As Integer
    
    On Error GoTo EH

    For i = 0 To PALETTE_NCOLORS - 1
        m_auColors(i).r = 0
        m_auColors(i).g = 0
        m_auColors(i).b = 0
    Next

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
