VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDefaultPaletteDecoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' PALETTE Decoder for DIV / Fenix/ Bennu Pal and 8bpp Map, Fpg and Fnt formats.
' This is also the default PALETTE decoder.
'===============================================================================

Implements IStandardDecoder
Implements IPaletteDecoder

'===============================================================================

Private Const Version As Byte = &H0
Private Const MODULE_NAME As String = "bennulib.cDefaultPaletteDecoder"

'===============================================================================

' Purpose: Reads the MAGIC of the file and optionally retrieves the version
' and the type of file ("map", "fpg", "pal", "fnt"). Returns false if the
' file is not a valid PAL or 8bpp MAP, FPG or FNT file.
' NOTE: Use carefully. lFile must be a valid file id.
Private Function ReadMagic(ByVal lFile As Long, Optional sType As String, _
    Optional vVersion As Byte) As Boolean
    
    Const PROC_NAME As String = "ReadMagic"
    Dim sMagicFileType As String * 3
    Dim avMagicDescriptor(4) As Byte
    
    On Error GoTo EH
    
    Debug.Assert (lFile <> 0)
    
    ReadMagic = True
    
    ' 3 first bytes describe the kind of file
    gzread lFile, ByVal sMagicFileType, 3
    sMagicFileType = LCase(sMagicFileType)
    
    Select Case sMagicFileType
    Case LCase(PAL_MAGIC)
        sType = "pal"
    Case LCase(FNT_MAGIC)
        sType = "fnt"
    Case LCase(FPG_MAGIC)
        sType = "fpg"
    Case LCase(MAP_MAGIC)
        sType = "map"
    Case Else ' Unsupported format
        ReadMagic = False
    End Select
    
    If ReadMagic Then
        ' Next 4 bytes are MS-DOS termination, and last is the version
        gzread lFile, avMagicDescriptor(0), 5
        
        If Not (avMagicDescriptor(0) = &H1A And _
                avMagicDescriptor(1) = &HD And _
                avMagicDescriptor(2) = &HA And _
                avMagicDescriptor(3) = &H0) Then
            ReadMagic = False
        End If
        
        If ReadMagic Then
            vVersion = avMagicDescriptor(4)
            If vVersion > Version Then ReadMagic = False ' Different version
        End If
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IStandardDecoder_CanRead(ByVal sFileName As String) As Boolean
    Const PROC_NAME As String = "IStandardDecoder_CanRead"
    Dim lFile As Long
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    IStandardDecoder_CanRead = ReadMagic(lFile)
    
    Call gzclose(lFile)
    
    Exit Function
EH:
    If lFile <> 0 Then gzclose (lFile)
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IPaletteDecoder_Decode(ByVal sFileName As String) As cPalette
    Const PROC_NAME As String = "IPaletteDecoder_Decode"
    Dim lFile As Long
    Dim vVersion As Byte
    Dim sType As String ' To distinguish between MAP, PAL, FNT and FPG files
    Dim avPalette(PALETTE_SIZE - 1) As Byte
    Dim oPal As cPalette
    Dim av() As Byte ' To skip bytes
        
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    If lFile = 0 Then
        Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    vVersion = Version
    If Not ReadMagic(lFile, sType, vVersion) Then
        ' ReadMagic will return false in two cases: when the file is not and
        ' understandable format or when or when the version is greater than
        ' VERSION
        If vVersion > Version Then
            Err.Raise vbObjectError Or ERR_UNSUPPORTEDVERSION, MODULE_NAME, _
                ResolveResString(ERR_UNSUPPORTEDVERSION, "|version", vVersion)
        Else
            Err.Raise vbObjectError Or ERR_NOTAPALFILE, MODULE_NAME, _
                ResolveResString(ERR_NOTAPALFILE, "|file", sFileName)
        End If
    End If
    
    ' For MAP files we need to skip some bytes before reaching the palette
    ' to skip this bytes we read into an unused buffer
    If sType = "map" Then
        ReDim av(40 - 1) As Byte
        gzread lFile, av(0), 40
        Erase av
    End If
    
    ' Read the palette data
    If gzread(lFile, avPalette(0), PALETTE_SIZE) < PALETTE_SIZE Then
        Err.Raise vbObjectError Or ERR_PALETTETRUNCATED, MODULE_NAME, _
            ResolveResString(ERR_PALETTETRUNCATED)
    End If
    
    ' Close the file. Note: it is not necessary to read gamma data
    Call gzclose(lFile)
    
    ' Create the PAL object
    Set oPal = New cPalette
    oPal.CreateFromByteArray avPalette, True
    
    Set IPaletteDecoder_Decode = oPal
    
    Exit Function
EH:
    If lFile <> 0 Then gzclose (lFile)
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function
