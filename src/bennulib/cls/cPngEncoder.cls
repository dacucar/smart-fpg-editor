VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPngEncoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' Png encoder. Allows exporting to PNG files.
'===============================================================================

Implements IMapEncoder
Implements IStandardEncoder

'===============================================================================

Private Const MODULE_NAME As String = "bennulib.cPngEncoder"

'===============================================================================

Private Sub IMapEncoder_Encode(oMap As cMap, ByVal sFileName As String)
    Const PROC_NAME As String = "IMapEncoder_Encode"

    On Error GoTo EH
      
    oMap.GetBitmapCopy.Save sFileName, FIF_PNG
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Property Get IStandardEncoder_DefaultFileExtension() As String
    IStandardEncoder_DefaultFileExtension = "png"
End Property

Private Property Let IStandardEncoder_EncoderOption(ByVal sKey As String, ByVal RHS As String)

End Property

Private Property Get IStandardEncoder_KnownFileExtensions() As String()
    Dim s(0) As String
    s(0) = "png"
    IStandardEncoder_KnownFileExtensions = s
End Property

Private Function IStandardEncoder_KnowsFileExtension(ByVal sExt As String) As Boolean
    IStandardEncoder_KnowsFileExtension = CBool(LCase(sExt) = "png")
End Function
