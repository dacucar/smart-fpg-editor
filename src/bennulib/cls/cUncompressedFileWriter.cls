VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cUncompressedFileWriter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Implements IFileWriter

Private Const MODULE_NAME As String = "bennulib.cUncompressedFileWriter"

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' Encapsulates common file write operations for uncompressed files.
' Note: Programmer is responsible for always calling FileOpen before using
' any other method as they will not check if the file is opened.
'===============================================================================

Private m_lFile As Long
Private m_iCompressionLevel As Integer

Private Type T_BL_ApiError
    sMessage As String
    lNumber As Long
End Type

' API Declaration
Const CREATE_ALWAYS As Long = 2
Const GENERIC_WRITE As Long = &H40000000
Const FILE_ATTRIBUTE_NORMAL As Long = &H80
Private Const INVALID_HANDLE_VALUE As Long = -1

Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, _
    ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, _
    ByVal lpSecurityAttributes As Long, ByVal dwCreationDisposition As Long, _
    ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long

Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
    
Private Declare Function WriteFile Lib "kernel32" (ByVal hFile As Long, _
    lpBuffer As Any, ByVal nNumberOfBytesToWrite As Long, lpNumberOfBytesWritten As Long, _
    ByVal lpOverlapped As Long) As Long

Private Declare Function GetLastError Lib "kernel32.dll" () As Long

Private Const FORMAT_MESSAGE_FROM_SYSTEM As Long = &H1000

Private Declare Function FormatMessage Lib "kernel32.dll" Alias "FormatMessageA" ( _
    ByVal dwFlags As Long, ByRef lpSource As Any, ByVal dwMessageId As Long, _
    ByVal dwLanguageId As Long, ByVal lpBuffer As String, ByVal nSize As Long, _
    ByRef Arguments As Long) As Long


Private Sub Class_Initialize()
    Const PROC_NAME As String = "Class_Initialize"
    On Error GoTo EH
    
    m_lFile = INVALID_HANDLE_VALUE

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub Class_Terminate()
    
    Const PROC_NAME As String = "Class_Terminate"
    On Error GoTo EH

    IFileWriter_Dispose

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_Dispose()
    Const PROC_NAME As String = "IFileWriter_Dispose"
    On Error GoTo EH

    If m_lFile <> INVALID_HANDLE_VALUE Then
        CloseHandle (m_lFile)
        m_lFile = INVALID_HANDLE_VALUE
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Function IFileWriter_FileOpen(ByVal sPath As String) As Long
    Const PROC_NAME As String = "IFileWriter_FileOpen"
    On Error GoTo EH

    m_lFile = CreateFile(sPath, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, _
        FILE_ATTRIBUTE_NORMAL, 0)

    If m_lFile = INVALID_HANDLE_VALUE Then
        Err.Raise vbObjectError & ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sPath)
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Sub IFileWriter_WriteBuffer(ByVal lBufferPtr As Long, ByVal lBuffLen As Long)
    Dim lResult As Long
    Dim uError As T_BL_ApiError
    Dim lBytesWritten As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteBuffer"
    On Error GoTo EH

    lResult = WriteFile(m_lFile, ByVal lBufferPtr, lBuffLen, lBytesWritten, 0&)
    
    If lResult = 0 Then
        uError = GetApiError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If

    Exit Sub
EH:
    IFileWriter_Dispose
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteByte(ByVal vValue As Byte)
    Dim lResult As Long
    Dim uError As T_BL_ApiError
    Dim lBytesWritten As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteByte"
    On Error GoTo EH

    lResult = WriteFile(m_lFile, vValue, 1, lBytesWritten, 0&)
    
    If lResult = 0 Then
        uError = GetApiError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    ElseIf lBytesWritten <> 1 Then
        Err.Raise vbObjectError & ERR_FILEWRITE, MODULE_NAME, _
        ResolveResString(ERR_FILEWRITE, "|actual", CStr(lBytesWritten), _
        "|expected", CStr(1))
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteBytes(avValues() As Byte)
    Dim lResult As Long
    Dim uError As T_BL_ApiError
    Dim lBuffLen As Long
    Dim lBytesWritten As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteBytes"
    On Error GoTo EH
    
    lBuffLen = UBound(avValues) - LBound(avValues) + 1
    lResult = WriteFile(m_lFile, avValues(0), lBuffLen, lBytesWritten, 0&)

    If lResult = 0 Then
        uError = GetApiError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    ElseIf lBytesWritten <> lBuffLen Then
        Err.Raise vbObjectError & ERR_FILEWRITE, MODULE_NAME, _
        ResolveResString(ERR_FILEWRITE, "|actual", CStr(lBytesWritten), _
        "|expected", CStr(lBuffLen))
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteInterger(ByVal iValue As Integer)
    Dim lResult As Long
    Dim uError As T_BL_ApiError
    Dim lBytesWritten As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteInterger"
    On Error GoTo EH

    lResult = WriteFile(m_lFile, iValue, 2, lBytesWritten, 0&)
    
    If lResult = 0 Then
        uError = GetApiError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    ElseIf lBytesWritten <> 2 Then
        Err.Raise vbObjectError & ERR_FILEWRITE, MODULE_NAME, _
        ResolveResString(ERR_FILEWRITE, "|actual", CStr(lBytesWritten), _
        "|expected", CStr(2))
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteIntergers(aiValues() As Integer)
    Dim lResult As Long
    Dim uError As T_BL_ApiError
    Dim lBuffLen As Long
    Dim lBytesWritten As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteBytes"
    On Error GoTo EH
    
    lBuffLen = (UBound(aiValues) - LBound(aiValues) + 1) * 2
    lResult = WriteFile(m_lFile, aiValues(0), lBuffLen, lBytesWritten, 0&)

    If lResult = 0 Then
        uError = GetApiError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    ElseIf lBytesWritten <> lBuffLen Then
        Err.Raise vbObjectError & ERR_FILEWRITE, MODULE_NAME, _
        ResolveResString(ERR_FILEWRITE, "|actual", CStr(lBytesWritten), _
        "|expected", CStr(lBuffLen))
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteLong(ByVal lValue As Long)
    Dim lResult As Long
    Dim lBytesWritten As Long
    Dim uError As T_BL_ApiError
    
    Const PROC_NAME As String = "IFileWriter_WriteLong"
    On Error GoTo EH

    lResult = WriteFile(m_lFile, lValue, 4, lBytesWritten, 0&)
    
    If lResult = 0 Then
        uError = GetApiError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    ElseIf lBytesWritten <> 4 Then
        Err.Raise vbObjectError & ERR_FILEWRITE, MODULE_NAME, _
        ResolveResString(ERR_FILEWRITE, "|actual", CStr(lBytesWritten), _
        "|expected", CStr(4))
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteLongs(alValues() As Long)
    Dim lResult As Long
    Dim uError As T_BL_ApiError
    Dim lBuffLen As Long
    Dim lBytesWritten As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteBytes"
    On Error GoTo EH
    
    lBuffLen = (UBound(alValues) - LBound(alValues) + 1) * 4
    lResult = WriteFile(m_lFile, alValues(0), lBuffLen, lBytesWritten, 0&)

    If lResult = 0 Then
        uError = GetApiError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    ElseIf lBytesWritten <> lBuffLen Then
        Err.Raise vbObjectError & ERR_FILEWRITE, MODULE_NAME, _
        ResolveResString(ERR_FILEWRITE, "|actual", CStr(lBytesWritten), _
        "|expected", CStr(lBuffLen))
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteString(ByVal sText As String)
    Dim lResult As Long
    Dim lBytesWritten As Long
    Dim uError As T_BL_ApiError
    
    Const PROC_NAME As String = "IFileWriter_WriteString"
    On Error GoTo EH

    lResult = WriteFile(m_lFile, ByVal sText, Len(sText), lBytesWritten, 0&)
    
    If lResult = 0 Then
        uError = GetApiError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    ElseIf lBytesWritten <> Len(sText) Then
        Err.Raise vbObjectError & ERR_FILEWRITE, MODULE_NAME, _
        ResolveResString(ERR_FILEWRITE, "|actual", CStr(lBytesWritten), _
        "|expected", CStr(sText))
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Function GetApiError() As T_BL_ApiError
    Dim uError As T_BL_ApiError
    
    Const PROC_NAME As String = "GetApiError"
    On Error GoTo EH

    uError.lNumber = GetLastError()
    uError.sMessage = GetWin32ErrorDescription(uError.lNumber)

    GetApiError = uError

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Function GetWin32ErrorDescription(ByVal lErrorCode As Long) As String
    Dim lRet As Long
    Dim sAPIError As String
    
    ' Preallocate the buffer
    sAPIError = String$(2048, " ")
    
    ' Now get the formatted message
    lRet = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, _
        ByVal 0&, lErrorCode, 0, sAPIError, Len(sAPIError), 0)
    
    ' Shorten the string to the right length
    sAPIError = Left$(sAPIError, lRet)
    
    GetWin32ErrorDescription = sAPIError
End Function

