VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDefaultFpgEncoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' FPG encoder for DIV / Fenix/ Bennu Fpg file format. This is also the default
' FPG encoder.
'===============================================================================

Implements IFpgEncoder
Implements IStandardEncoder

'===============================================================================

Private Const Version As Byte = &H0 ' Current writeable FPG version
Private Const MODULE_NAME As String = "bennulib.cDefaultFpgEncoder"

'===============================================================================

Private m_CompressionLevel As Integer
Private m_bUseGZip As Boolean

Private Sub Class_Initialize()
    m_CompressionLevel = 9
    m_bUseGZip = True
End Sub

Public Property Let UseGzip(ByVal Value As Boolean)
    m_bUseGZip = Value
End Property

Public Property Get UseGzip() As Boolean
    UseGzip = m_bUseGZip
End Property

Private Sub IFpgEncoder_Encode(oFpg As cFpg, ByVal sFileName As String)
    Const PROC_NAME As String = "IFpgEncoder_Encode"
    Dim oMap As cMap
    Dim lAux As Long
    Dim iAux As Long
    Dim sMagicFileType As String * 3
    Dim avMagicDescriptor(4) As Byte
    Dim avPalette() As Byte
    Dim avPaletteUnused() As Byte
    Dim sName As String * 12
    Dim alCpIds() As Long
    Dim l As Long, k As Long, i As Long
    
    On Error GoTo EH
    
    Debug.Assert oFpg.Available
    
    ' Choose FileWriter strategy (Gzip / Uncompressed)
    Dim oFileWriter As IFileWriter
    If m_bUseGZip Then
        Dim oGzipFileWriter As New cGzipFileWriter
        oGzipFileWriter.CompressionLevel = m_CompressionLevel
        Set oFileWriter = oGzipFileWriter
    Else
        Set oFileWriter = New cUncompressedFileWriter
    End If
    
    oFileWriter.FileOpen (sFileName)
    
    'FileType
    Select Case oFpg.Depth
    Case 8
        sMagicFileType = FPG_MAGIC
    Case 16
        sMagicFileType = F16_MAGIC
    Case 32
        sMagicFileType = F32_MAGIC
    End Select
    oFileWriter.WriteString sMagicFileType
    
    avMagicDescriptor(0) = &H1A
    avMagicDescriptor(1) = &HD
    avMagicDescriptor(2) = &HA
    avMagicDescriptor(3) = &H0
    avMagicDescriptor(4) = Version
    oFileWriter.WriteBytes avMagicDescriptor
    
    ' Palette
    If oFpg.Depth = 8 Then
        ReDim avPalette(PALETTE_SIZE - 1) As Byte
        avPalette = oFpg.Palette.GetColorsAsByteArray(True)
        oFileWriter.WriteBytes avPalette
        ReDim avPaletteUnused(PALETTE_UNUSEDBYTES - 1) As Byte
        oFileWriter.WriteBytes avPaletteUnused
    End If
        
    For Each oMap In oFpg
        oFileWriter.WriteLong oMap.Code
        lAux = 64 + oMap.Width * oMap.Height * oMap.Depth / 8 + _
                oMap.ControlPointsCount * 4 ' TODO: Check that this is right...
                ' TODO: Consider when center = real center
        oFileWriter.WriteLong lAux
        oFileWriter.WriteString StringToAsciiZ(oMap.Description, 32)
        sName = StringToAsciiZ("Smart FPG :)", 12)
        oFileWriter.WriteString sName
        oFileWriter.WriteLong oMap.Width
        oFileWriter.WriteLong oMap.Height
        
        ' Control Points
        ' TODO: Current behaviour does not consider when the first controlpoint
        ' id is 0 and it is set to the real center of the map. In that case
        ' there is no need to write the control point.
        lAux = oMap.GetLastControlPointId + 1
        oFileWriter.WriteLong lAux
        alCpIds() = oMap.GetControlPointsIds
        l = 0
        For k = 0 To oMap.ControlPointsCount - 1
            ' Fill with (-1, -1) undefined intermediate CP
            For i = l To alCpIds(k) - 1
                iAux = -1
                oFileWriter.WriteInterger iAux
                oFileWriter.WriteInterger iAux
            Next
            iAux = oMap.GetControlPointX(alCpIds(k))
            oFileWriter.WriteInterger iAux
            iAux = oMap.GetControlPointY(alCpIds(k))
            oFileWriter.WriteInterger iAux
            l = alCpIds(k) + 1
        Next
        Erase alCpIds
        
        ' Bitmap data
        For i = 0 To oMap.Height - 1
            oFileWriter.WriteBuffer oMap.RealDataPtr + i * oMap.Stride, _
                oMap.Width * (oMap.Depth / 8)
        Next
    Next
    
    ' Close the file
    oFileWriter.Dispose
    
    Exit Sub
EH:
    Erase avPalette
    Erase avPaletteUnused
    Erase alCpIds
    If Not oFileWriter Is Nothing Then oFileWriter.Dispose 'gzclose lFile
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Property Get IStandardEncoder_DefaultFileExtension() As String
    IStandardEncoder_DefaultFileExtension = "fpg"
End Property

Private Sub SetCompressionLevelOption(ByVal sValue As String)
    Dim iCompressionLevel As Integer
    Dim bSuccess As Boolean
    
    bSuccess = False
    
    If IsNumeric(sValue) Then
        iCompressionLevel = CInt(sValue)
        
        If iCompressionLevel >= 0 And iCompressionLevel < 9 Then
            bSuccess = True
        End If
    End If
    
    If bSuccess Then
        m_CompressionLevel = iCompressionLevel
    Else
        ' TODO: Log invalid option / raise error / warning
    End If
End Sub

Private Sub SetUseGzipOption(ByVal sValue As String)
    Select Case LCase(sValue)
    Case "true"
        UseGzip = True
    Case "false"
        UseGzip = False
    Case Else
        ' TODO: Log invalid option / raise error / warning
    End Select
End Sub

Private Property Let IStandardEncoder_EncoderOption(ByVal sKey As String, ByVal sValue As String)
    Select Case sKey
    Case "compression-level"
        SetCompressionLevelOption (sValue)
    Case "use-gzip"
        SetUseGzipOption (sValue)
    Case Else
        ' TODO: Log invalid option / raise error / warning
    End Select
End Property

Private Property Get IStandardEncoder_KnownFileExtensions() As String()
    Dim s(0) As String
    s(0) = "fpg"
    IStandardEncoder_KnownFileExtensions = s
End Property

Private Function IStandardEncoder_KnowsFileExtension(ByVal sExt As String) As Boolean
    IStandardEncoder_KnowsFileExtension = CBool(LCase(sExt) = "fpg")
End Function

