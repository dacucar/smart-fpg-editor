VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IFileWriter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Public Function FileOpen(ByVal sPath As String) As Long

End Function

Public Sub Dispose()

End Sub

Public Sub WriteBuffer(ByVal lBufferPtr As Long, ByVal length As Long)

End Sub

Public Sub WriteString(ByVal sText As String)

End Sub

Public Sub WriteInterger(ByVal iValue As Integer)

End Sub

Public Sub WriteIntergers(aiValues() As Integer)

End Sub

Public Sub WriteLong(ByVal lValue As Long)

End Sub

Public Sub WriteLongs(alValues() As Long)

End Sub

Public Sub WriteByte(ByVal vValue As Byte)

End Sub

Public Sub WriteBytes(avValues() As Byte)

End Sub

