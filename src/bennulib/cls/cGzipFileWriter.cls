VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGzipFileWriter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit
Option Compare Binary

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' Encapsulates common file write operations for Gzip files.
' Note: Programmer is responsible for always calling FileOpen before using
' any other method as they will not check if the file is opened.
'===============================================================================

Implements IFileWriter

Private Const MODULE_NAME As String = "bennulib.cGzipFileWriter"

Private m_lFile As Long
Private m_iCompressionLevel As Integer

Private Type T_BL_ZlibError
    sMessage As String
    lNumber As Long
End Type

Public Property Get CompressionLevel() As Integer
    CompressionLevel = m_iCompressionLevel
End Property


' Allows defining the compression degree of the resulting Gzip file.
' The level is a number between 0 (no compression) and 9 (maximum
' compression). Note that even when the compression level is 0, the
' generated file will still be a GZip file (with header and footer).
'
' The compression level must be defined before the file is opened.
' Attempts to change the compression level once the file is opened
' will be omitted (and will have no effect).
'
' @param iLevel Compression Level. An interger from 0 to 9.
Public Property Let CompressionLevel(ByVal iLevel As Integer)
    
    Const PROC_NAME As String = "CompressionLevel"
    On Error GoTo EH

    If m_lFile <> 0 Then
        ' Do nothing, compression level cannot be changed after file
        ' is opened
    Else
        If iLevel < 0 Or iLevel > 9 Then
            Err.Raise vbObjectError & ERR_INVALIDCOMPRESSIONLEVEL, _
                MODULE_NAME, _
                ResolveResString(ERR_INVALIDCOMPRESSIONLEVEL, _
                    "|level", iLevel)
        End If
        
        m_iCompressionLevel = iLevel
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

Private Sub Class_Initialize()
    Const PROC_NAME As String = "Class_Initialize"
    On Error GoTo EH

    ' Default max compression level
    m_iCompressionLevel = 9

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub Class_Terminate()
    
    Const PROC_NAME As String = "Class_Terminate"
    On Error GoTo EH

    IFileWriter_Dispose

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_Dispose()
    Const PROC_NAME As String = "IFileWriter_Dispose"
    On Error GoTo EH

    If m_lFile <> 0 Then gzclose (m_lFile)
    
    m_lFile = 0

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Function IFileWriter_FileOpen(ByVal sPath As String) As Long
    Const PROC_NAME As String = "IFileWriter_FileOpen"
    On Error GoTo EH

    m_lFile = gzopen(sPath, "wb" & CStr(CompressionLevel))

    If m_lFile = 0 Then
        Err.Raise vbObjectError & ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sPath)
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Sub IFileWriter_WriteBuffer(ByVal lBufferPtr As Long, ByVal lBuffLen As Long)
    Dim lResult As Long
    Dim uError As T_BL_ZlibError
    
    Const PROC_NAME As String = "IFileWriter_WriteBuffer"
    On Error GoTo EH

    lResult = gzwrite(m_lFile, ByVal lBufferPtr, lBuffLen)
    
    If lResult <> lBuffLen Then
        uError = GetZLibError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If

    Exit Sub
EH:
    IFileWriter_Dispose
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteByte(ByVal vValue As Byte)
    Dim lResult As Long
    Dim uError As T_BL_ZlibError
    
    Const PROC_NAME As String = "IFileWriter_WriteByte"
    On Error GoTo EH

    lResult = gzwrite(m_lFile, vValue, 1)
    
    If lResult <> 1 Then
        uError = GetZLibError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteBytes(avValues() As Byte)
    Dim lResult As Long
    Dim uError As T_BL_ZlibError
    Dim lBufLen As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteBytes"
    On Error GoTo EH
    
    lBufLen = UBound(avValues) - LBound(avValues) + 1
    lResult = gzwrite(m_lFile, avValues(0), lBufLen)

    If lResult <> lBufLen Then
        uError = GetZLibError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteInterger(ByVal iValue As Integer)
    Dim lResult As Long
    Dim uError As T_BL_ZlibError
    
    Const PROC_NAME As String = "IFileWriter_WriteInterger"
    On Error GoTo EH

    lResult = gzwrite(m_lFile, iValue, 2)
    
    If lResult <> 2 Then
        uError = GetZLibError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteIntergers(aiValues() As Integer)
    Dim lResult As Long
    Dim uError As T_BL_ZlibError
    Dim lBufLen As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteBytes"
    On Error GoTo EH
    
    lBufLen = (UBound(aiValues) - LBound(aiValues) + 1) * 2
    lResult = gzwrite(m_lFile, aiValues(0), lBufLen)

    If lResult <> lBufLen Then
        uError = GetZLibError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteLong(ByVal lValue As Long)
    Dim lResult As Long
    Dim uError As T_BL_ZlibError
    
    Const PROC_NAME As String = "IFileWriter_WriteLong"
    On Error GoTo EH

    lResult = gzwrite(m_lFile, lValue, 4)
    
    If lResult <> 4 Then
        uError = GetZLibError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteLongs(alValues() As Long)
    Dim lResult As Long
    Dim uError As T_BL_ZlibError
    Dim lBufLen As Long
    
    Const PROC_NAME As String = "IFileWriter_WriteBytes"
    On Error GoTo EH
    
    lBufLen = (UBound(alValues) - LBound(alValues) + 1) * 4
    lResult = gzwrite(m_lFile, alValues(0), lBufLen)

    If lResult <> lBufLen Then
        uError = GetZLibError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub IFileWriter_WriteString(ByVal sText As String)
    Dim lResult As Long
    Dim uError As T_BL_ZlibError
    
    Const PROC_NAME As String = "IFileWriter_WriteString"
    On Error GoTo EH

    lResult = gzwrite(m_lFile, ByVal sText, Len(sText))
    
    If lResult <> Len(sText) Then
        uError = GetZLibError()
        Err.Raise vbObjectError & ERR_ZLIB, MODULE_NAME, _
            ResolveResString(ERR_ZLIB, "|num", uError.lNumber, _
            "|msg", uError.sMessage)
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Function GetZLibError() As T_BL_ZlibError
    Dim uError As T_BL_ZlibError
    
    Const PROC_NAME As String = "GetZLibError"
    On Error GoTo EH

    Dim msg As String
    Dim errnum As Long
    msg = gzerror(m_lFile, errnum)
    
    uError.lNumber = errnum
    uError.sMessage = msg

    GetZLibError = uError

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function
