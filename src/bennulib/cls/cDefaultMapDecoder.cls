VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDefaultMapDecoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' MAP Decoder for DIV / Fenix/ Bennu Map file format. This is also the default
' MAP decoder.
'===============================================================================

Implements IStandardDecoder
Implements IMapDecoder

'===============================================================================

Private Const Version As Byte = &H0 ' Current readable Map format version
Private Const MODULE_NAME As String = "bennulib.cDefaultMapDecoder"

'===============================================================================

' Purpose: Reads the MAGIC of the file and optionally retrieves the Depth and
' the version. Returns false if the file is not a valid MAP file.
' NOTE: Use carefully. lFile must be a valid file id.
Private Function ReadMagic(ByVal lFile As Long, Optional iDepth As Integer, _
        Optional vVersion As Byte) As Boolean
    
    Const PROC_NAME As String = "ReadMagic"
    Dim sMagicFileType As String * 3
    Dim avMagicDescriptor(4) As Byte
    
    On Error GoTo EH
    
    Debug.Assert (lFile <> 0)
    
    ReadMagic = True
    
    ' 3 first bytes describe the depth of the MAP
    gzread lFile, ByVal sMagicFileType, 3
    sMagicFileType = LCase(sMagicFileType)
    
    Select Case sMagicFileType
    Case LCase(MAP_MAGIC)
        iDepth = 8
    Case LCase(M16_MAGIC)
        iDepth = 16
    Case LCase(M32_MAGIC)
        iDepth = 32
    Case Else ' Not map file
        ReadMagic = False
    End Select
    
    If ReadMagic Then
        ' Next 4 bytes are MS-DOS termination, and last is the MAP version
        gzread lFile, avMagicDescriptor(0), 5
        
        If Not (avMagicDescriptor(0) = &H1A And _
                avMagicDescriptor(1) = &HD And _
                avMagicDescriptor(2) = &HA And _
                avMagicDescriptor(3) = &H0) Then
            ReadMagic = False
        End If
        
        If ReadMagic Then
            vVersion = avMagicDescriptor(4)
            If vVersion > Version Then ReadMagic = False ' Different version
        End If
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IStandardDecoder_CanRead(ByVal sFileName As String) As Boolean
    Const PROC_NAME As String = "IStandardDecoder_CanRead"
    Dim lFile As Long
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    IStandardDecoder_CanRead = ReadMagic(lFile)
    
    Call gzclose(lFile)
    
    Exit Function
EH:
    If lFile <> 0 Then gzclose (lFile)
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IMapDecoder_Decode(ByVal sFileName As String) As cMap
    Const PROC_NAME As String = "IMapDecoder_Decode"
    Dim lFile As Long
    Dim iAux As Integer ' To read with gzread
    Dim vVersion As Byte
    Dim iDepth As Integer
    Dim lWidth As Long, lHeight As Long
    Dim lCode As Long
    Dim sDescription As String * 32
    Dim avPalette() As Byte
    Dim avPaletteUnused() As Byte
    Dim lMapDataLength As Long
    Dim iFlags As Integer
    Dim iNumCP As Integer ' Number of control points
    Dim aiControlPoints() As Integer
    Dim avData8() As Byte
    Dim aiData16() As Integer
    Dim alData32() As Long
    Dim lDataPtr As Long
    Dim iCPX As Integer, iCPY As Integer
    Dim i As Integer
    
    Dim oMap As cMap
    Dim oPal As cPalette
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    vVersion = Version
    If Not ReadMagic(lFile, iDepth, vVersion) Then
        ' ReadMagic will return false in two cases: when the file is not a MAP
        ' file or when the version is greater than VERSION
        If vVersion > Version Then
            Err.Raise vbObjectError Or ERR_UNSUPPORTEDVERSION, MODULE_NAME, _
                ResolveResString(ERR_UNSUPPORTEDVERSION, "|version", vVersion)
        Else
            Err.Raise vbObjectError Or ERR_NOTAMAPFILE, MODULE_NAME, _
                ResolveResString(ERR_NOTAMAPFILE, "|file", sFileName)
        End If
    End If
    
    ' MAP properties
    gzread lFile, iAux, 2
    lWidth = SInt16ToUint32(iAux)
    gzread lFile, iAux, 2
    lHeight = SInt16ToUint32(iAux)
    gzread lFile, lCode, 4 ' The code is read but won't be used
    gzread lFile, ByVal sDescription, 32
    
    lMapDataLength = lWidth * lHeight * (iDepth / 8)
    
    ' Palette if 8bpp
    If iDepth = 8 Then
        ReDim avPalette(PALETTE_SIZE - 1) As Byte
        ReDim avPaletteUnused(PALETTE_UNUSEDBYTES - 1) As Byte
        If gzread(lFile, avPalette(0), PALETTE_SIZE) < PALETTE_SIZE Then
            Err.Raise vbObjectError Or ERR_PALETTETRUNCATED, MODULE_NAME, _
                ResolveResString(ERR_PALETTETRUNCATED)
        End If
        If gzread(lFile, avPaletteUnused(0), PALETTE_UNUSEDBYTES) _
                    < PALETTE_UNUSEDBYTES Then
            Err.Raise vbObjectError Or ERR_GAMMATRUNCATED, MODULE_NAME, _
                ResolveResString(ERR_GAMMATRUNCATED)
        End If
    End If
    
    ' Control Points
    ' First 12 bits of the NFlags short integer tells the higher id for the
    ' defined controlpoint.
    ' Bit 13 set to 1 means that the map has animation activated (this is
    ' actually not supported by Fenix or Bennu)
    gzread lFile, iFlags, 2
    iNumCP = (iFlags And F_NCPOINTS)
'    ReDim aiControlPoints(IIf(iNumCP > 0, iNumCP * 2 - 1, 1)) As Integer
'    aiControlPoints(0) = lWidth \ 2 ' Default Center X coord
'    aiControlPoints(1) = lHeight \ 2 ' Default Center Y coord
    If iNumCP > 0 Then
        ReDim aiControlPoints(iNumCP * 2 - 1) As Integer
        If gzread(lFile, aiControlPoints(0), iNumCP * 4) < (iNumCP * 4) Then
            Err.Raise vbObjectError Or ERR_CPINFOTRUNCATED, MODULE_NAME, _
                ResolveResString(ERR_CPINFOTRUNCATED)
        End If
'    Else
'        iNumCP = 1 ' At least we'll have the center
    End If
    
    ' Animation (not supported in Bennu/Fenix/Div so consider an error)
    If (iFlags And F_ANIMATION) = 1 Then
        Err.Raise vbObjectError Or ERR_ANIMATIONFLAGACTIVE, MODULE_NAME, _
            ResolveResString(ERR_ANIMATIONFLAGACTIVE)
    End If
    
    ' Map Data
    Select Case iDepth
    Case 8
        ReDim avData8(lWidth * lHeight - 1) As Byte
        lDataPtr = VarPtr(avData8(0))
    Case 16
        ReDim aiData16(lWidth * lHeight - 1) As Integer
        lDataPtr = VarPtr(aiData16(0))
    Case 32
        ReDim alData32(lWidth * lHeight - 1) As Long
        lDataPtr = VarPtr(alData32(0))
    End Select
    
    If gzread(lFile, ByVal lDataPtr, lMapDataLength) < lMapDataLength Then
        Err.Raise vbObjectError Or ERR_BITMAPDATATRUNCATED, MODULE_NAME, _
            ResolveResString(ERR_BITMAPDATATRUNCATED)
    End If
    
    ' Create the palette object
    If iDepth = 8 Then
        Set oPal = New cPalette
        oPal.CreateFromByteArray avPalette, True
    End If
    
    ' Create the MAP object
    Set oMap = New cMap
    oMap.CreateFromBits lWidth, lHeight, iDepth, _
            lDataPtr, AsciiZToString(sDescription), oPal
    
    ' Control points
    For i = 0 To iNumCP - 1
        iCPX = aiControlPoints(i * 2)
        iCPY = aiControlPoints(i * 2 + 1)
        If Not (iCPX = -1 And iCPY = -1) Then
            oMap.SetControlPoint i, iCPX, iCPY
        End If
    Next
    
    Call gzclose(lFile)
    
    Erase avData8
    Erase aiData16
    Erase alData32
    Erase aiControlPoints
    Erase avPalette
    Erase avPaletteUnused
    
    Set IMapDecoder_Decode = oMap
    
    Exit Function
EH:
    Erase avData8
    Erase aiData16
    Erase alData32
    Erase aiControlPoints
    Erase avPalette
    Erase avPaletteUnused
    Set oMap = Nothing
    Set oPal = Nothing
    If lFile <> 0 Then gzclose lFile
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function
