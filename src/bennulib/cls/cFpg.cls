VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cFpg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit
Option Compare Binary

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' cFpg encapsulates an FPG object. An FPG is a bennulib graphic collection, more
' specifically, a collection of MAP objects.
'
' This class provides methods to add MAPs, remove or access them, as well as a
' number of other useful methods.
'
' You can access to the MAPs objects via the "Maps" property. MAPs are accessed
' by its index in the collection, which must not be confused with the Code
' property of MAPs. Accessing by index allows iterating easily through all the
' MAPs. If you need to access to a MAP by its code, you can use the IndexByCode
' function to retrieve an appropiate index.
' The class implements the NewEnum function of the IUnknown interface, so you
' can also use For each ... Next block to iterate through the MAPs.
'
' This class has its Instancing attribute set to PublicNonCreatable, which means
' that you cannot created cFpg objects via the VB6 "New" operator outside
' bennulib scope. To create FPG objects you must, instead, use the global
' functions exposed by the BennulibGlobals.cls class. Refer to the source
' of that file to know more about how to create FPGs.
'===============================================================================

Private Const MODULE_NAME As String = "bennulib.cFpg"

' Stores all the MAPs of the FPG. Note that although VB6 collection are
' 1-based index, we will provide a 0-based index behaviour.
Private m_oMaps As Collection

' An array to keep track of which MAP codes are being used. When index I is set
' to 0, there is no MAP in the FPG with code I.
Private m_aiCodes(MIN_CODE To MAX_CODE) As Integer

Private m_oPalette As cPalette

Private m_iDepth As Integer

' Used internally to avoid errors. For example, since a cFpg object can be
' created the new operator inside the bennulib scope, one could call methods
' that require the FPG object to be intialized. If the FPG is not, uncontrolled
' errors could occur.
Private m_bAvailable As Boolean

'===============================================================================

' Retrieves the depth of the FPG in bits (8, 16 or 32), i.e. the number of bits
' used to store each pixel of the MAPs. All MAPs inside an FPG have the same
' depth, equal to that of the FPG.
Public Property Get Depth() As Integer
    Const PROC_NAME As String = "Depth"
    On Error GoTo EH
    
    Depth = m_iDepth

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves a copy of the FPG palette or Nothing if the FPG is not 8bpp.
'@return a copy of the FPG palette
Public Function GetPaletteCopy() As cPalette
    Const PROC_NAME As String = "GetPaletteCopy"
    On Error GoTo EH
    
    Set GetPaletteCopy = m_oPalette.GetCopy
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Public Sub SetPaletteColors(oPalette As cPalette)
    Dim oMap As cMap
    Dim oPaletteCopy As cPalette
    Dim avColors() As Byte
    
    Const PROC_NAME As String = "SetPaletteColors"
    On Error GoTo EH
    
    If m_iDepth <> 8 Then
        Err.Raise vbObjectError Or ERR_SETPALETTEINVALIDDEPTH, MODULE_NAME, _
            ResolveResString(ERR_SETPALETTEINVALIDDEPTH)
    Else
        If oPalette Is Nothing Then
            Err.Raise vbObjectError Or ERR_PALETTEREQUIRED, MODULE_NAME, _
                ResolveResString(ERR_PALETTEREQUIRED)
        End If
    End If

    Set oPaletteCopy = New cPalette
    oPaletteCopy.CreateFromByteArray oPalette.GetCopy.GetColorsAsByteArray(), False
    
    Set m_oPalette = oPaletteCopy
    
    Dim lCode As Long
    For Each oMap In m_oMaps
        ' Will forces oMap to rebuild its bitmap info with the new palette
        lCode = oMap.Code
        oMap.SetParentFpg Me, lCode
    Next
    
        
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Retrieves the number of MAPs inside the FPG
Public Property Get MapCount() As Long
    Const PROC_NAME As String = "MapCount"
    On Error GoTo EH
    
    MapCount = m_oMaps.Count
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the MAP at position lIndex in the collection
Public Property Get Maps(ByVal lIndex As Long) As cMap
    Const PROC_NAME As String = "Maps"
    On Error GoTo EH
    
    If Not (lIndex > m_oMaps.Count - 1) And lIndex >= 0 Then
        Set Maps = m_oMaps.Item(lIndex + 1)
    Else
        VBA.Err.Raise vbObjectError Or ERR_MAPINDEXOUTOFRANGE, MODULE_NAME, _
            ResolveResString(ERR_MAPINDEXOUTOFRANGE, "|index", CStr(lIndex))
    End If
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Allows For...each...next iteration
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Const PROC_NAME As String = "NewEnum"
    On Error GoTo EH
    
    Set NewEnum = m_oMaps.[_NewEnum]
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Gets the index of the map whose code is lCode. This function does not verify
' whether the specified code is valid.
' @param lCode the code of the MAP whose index want to retrieve
' @return the index of the map in the collection (base 0) or -1 if not found
Public Function IndexByCode(ByVal lCode As Long) As Long
    Const PROC_NAME As String = "IndexByCode"
    Dim l As Long
    Dim lIndex As Long
    
    On Error GoTo EH
    
    lIndex = -1
    'Look for the map in the collection
    For l = 1 To m_oMaps.Count
        If m_oMaps.Item(l).Code = lCode Then
            lIndex = l - 1
            Exit For
        End If
    Next
    
    IndexByCode = lIndex
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Adds a MAP to the FPG
' @param oMap the cMap object to be added to the FPG
' @param bReplace determines whether an existing MAP in the collection with the
' same code will be replaced with the new MAP (true), or the new MAP will be
' discarded (false)
' @return True if the Map was added to the FPG, False otherwise
Public Function Add(oMap As cMap, _
            ByVal lCode As Long, _
            Optional ByVal bReplace As Boolean) As Boolean
            
    Const PROC_NAME As String = "Add"
    Dim bAdded As Boolean
    Dim afterIndex As Long
    
    On Error GoTo EH
    
    Debug.Assert Not (oMap Is Nothing)
    Debug.Assert oMap.Available
    
    If oMap.Depth <> m_iDepth Then
        VBA.Err.Raise vbObjectError Or ERR_DIFFERENTDEPTHS, MODULE_NAME, _
            ResolveResString(ERR_DIFFERENTDEPTHS, "|depthMap", _
            CStr(oMap.Depth), "|depthFpg", CStr(Depth))
    End If
    
    bAdded = True
    If Not ExistsCode(lCode) Then
        oMap.SetParentFpg Me, lCode
        afterIndex = LookForAfterInsertionIndex(lCode)
        If afterIndex <> 0 Then
            m_oMaps.Add oMap, , , afterIndex
        Else
            If m_oMaps.Count >= 1 Then
                ' Add the map in the first position
                m_oMaps.Add oMap, , 1
            Else ' There were no maps
                m_oMaps.Add oMap
            End If
        End If
    Else ' The Map already exists
        If bReplace = True Then
            oMap.SetParentFpg Me, lCode
            ' Note that the funcion IndexByCode will search for the first map
            ' with the code given and so the new map must be added after the
            ' old one
            
            ' Add the new map after the old one
            m_oMaps.Add oMap, , , IndexByCode(lCode) + 1
            
            ' Remove the old map from the collection
            m_oMaps.Remove (IndexByCode(lCode) + 1)
        Else
            bAdded = False
        End If
    End If
    
    If bAdded Then
        m_aiCodes(lCode) = -1
    End If

    Add = bAdded
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Removes a MAP from the collection by its code. This method does nothing if
' there is no MAP with the specified code, but the code must be within the range
' of valid codes (see IsCodeValid function), otherwise an error will be raised.
' @param lCode the code of the MAP to be removed
Public Sub RemoveByCode(ByVal lCode As Long)
    Const PROC_NAME As String = "RemoveByCode"
    On Error GoTo EH
    
    Dim l As Long
    If ExistsCode(lCode) Then
        l = IndexByCode(lCode) + 1
        If l > 0 Then m_oMaps.Remove (l): m_aiCodes(lCode) = 0
    End If
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Checks if the FPG contains a MAP of a cerain code. The lCode argument must
' be within the range of valid codes (see IsCodeValid function). otherwise
' an error will be raised.
' @param lCode the code to be checked
' @return True if a MAP with the specified code is found, False otherwhise
Public Function ExistsCode(ByVal lCode As Long) As Boolean
    Const PROC_NAME As String = "ExistsCode"
    On Error GoTo EH

    If Not IsCodeValid(lCode) Then
        VBA.Err.Raise vbObjectError Or ERR_INVALIDCODE, MODULE_NAME, _
            ResolveResString(ERR_INVALIDCODE, "|code", CStr(lCode))
    End If
    
    ExistsCode = IIf(m_aiCodes(lCode) = 0, False, True)
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Checks whether a code is within the acceptable range of codes for the FPG.
' Valid codes go from (and including) 1 to (and including) 999.
' @param lCode the code to be checked
' @return True if lCode is a valid code or False otherwise.
Public Function IsCodeValid(ByVal lCode As Long) As Boolean
    Const PROC_NAME As String = "IsCodeValid"
    On Error GoTo EH
    
    IsCodeValid = Not (lCode > MAX_CODE Or lCode < MIN_CODE)
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Starting from the specified code, performs a search to retrieve the first code
' which has no MAP associated with.
' @param lStart code from which search will start
' @returns the next free code. If no free code is found, the return value will
' be MIN_CODE - 1
Public Function FreeCode(Optional ByVal lStart As Long = MIN_CODE) As Long
    Const PROC_NAME As String = "FreeCode"
    Dim i As Integer
    Dim lCode As Long
    
    On Error GoTo EH
    
    If Not IsCodeValid(lStart) Then
        VBA.Err.Raise vbObjectError Or ERR_INVALIDCODE, MODULE_NAME, _
            ResolveResString(ERR_INVALIDCODE, "|code", CStr(lStart))
    End If
    
    If m_oMaps.Count > 0 Then
        lCode = MIN_CODE - 1
        For i = lStart To MAX_CODE
            If Not ExistsCode(i) Then lCode = i: Exit For
        Next
    Else
        lCode = lStart
    End If
    
    FreeCode = lCode
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Starting from the specified code, performs a search to retrieve the first code
' which has no MAP associated with. The search is performed fromlStart to
' MIN_CODE.
' @param lStart Code from which search will start
' @returns the previous free code. If no free code is found, the return value
' will be MIN_CODE - 1
Public Function FreeCodeRev(Optional ByVal lStart As Long = MAX_CODE) As Long
    Const PROC_NAME As String = "FreeCodeRev"
    Dim i As Integer
    Dim lCode As Long
    
    On Error GoTo EH
    
    If Not IsCodeValid(lStart) Then
        VBA.Err.Raise vbObjectError Or ERR_INVALIDCODE, MODULE_NAME, _
            ResolveResString(ERR_INVALIDCODE, "|code", CStr(lStart))
    End If
    
    If m_oMaps.Count > 0 Then
        lCode = MIN_CODE - 1
        For i = lStart To MIN_CODE Step -1
            If Not ExistsCode(i) Then lCode = i: Exit For
        Next
    Else
        lCode = lStart
    End If
    
    FreeCodeRev = lCode
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Creates a copy of the FPG
' @return a new cFpg object which is a copy of the current FPG
Public Function GetCopy() As cFpg
    Const PROC_NAME As String = "GetCopy"
    Dim oFpg As cFpg
    Dim oMap As cMap
    Dim oPalCopy As cPalette
    
    On Error GoTo EH
    
    Debug.Assert m_bAvailable
    
    ' Get a copy of the Palette, if appliable
    If Not m_oPalette Is Nothing Then Set oPalCopy = m_oPalette.GetCopy
    
    ' Create the new FPG
    Set oFpg = New cFpg
    oFpg.CreateNew m_iDepth, oPalCopy
    ' Add the MAPs one by one
    For Each oMap In Me
        oFpg.Add oMap.GetCopy, oMap.Code, False
    Next
    
    Set GetCopy = oFpg
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

'===============================================================================

' Retrieves whether the FPG is available or not (see m_bAvailable comments in
' declaration section).
Friend Property Get Available() As Boolean
    Const PROC_NAME As String = "Available"
    
    On Error GoTo EH
    
    Available = m_bAvailable
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves the PALETTE of the FPG.
Friend Property Get Palette() As cPalette
    Const PROC_NAME As String = "Palette"
    
    On Error GoTo EH
    
    Set Palette = m_oPalette
    
    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Creates a new FPG of the specified depth and PALETTE. The oPalette parameter
' is required if iDepth = 8 and ignored in any other case.
Friend Sub CreateNew(ByVal iDepth As Integer, Optional oPalette As cPalette)
    Const PROC_NAME As String = "CreateNew"
    On Error GoTo EH
    
    Dispose
    
    If iDepth = 8 And oPalette Is Nothing Then
        VBA.Err.Raise vbObjectError Or ERR_PALETTEREQUIRED, MODULE_NAME, _
            ResolveResString(ERR_PALETTEREQUIRED)
    End If
    
    Select Case iDepth
    Case 8, 16, 32
        m_iDepth = iDepth
    Case Else
        VBA.Err.Raise vbObjectError Or ERR_UNSUPPORTEDDEPTH, MODULE_NAME, _
            ResolveResString(ERR_UNSUPPORTEDDEPTH, "|depth", CStr(iDepth))
    End Select
    
    If iDepth = 8 Then Set m_oPalette = oPalette
    
    Set m_oMaps = New Collection
    m_bAvailable = True
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'===============================================================================

' Purpose: Given a code, looks for the Index in the collection after which a map
' should be inserted to keep the collection sorted by MAP code.
' NOTE: The returned 1-based (just as the Collection Index property).
Private Function LookForAfterInsertionIndex(ByVal startCode As Long) As Long
    Const PROC_NAME As String = "LookForAfterInsertionIndex"
    Dim i As Long
    Dim lAfterIndex As Long
    
    On Error GoTo EH
    
    lAfterIndex = 0
    For i = startCode To MIN_CODE Step -1
        If m_aiCodes(i) = -1 Then
            lAfterIndex = IndexByCode(i) + 1
            Exit For
        End If
    Next
    
    LookForAfterInsertionIndex = lAfterIndex
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Given a code, looks for the Index in the collection before which a
' map should be inserted to keep the collection sorted by MAP code.
' NOTE: The returned 1-based (just as the Collection Index property).
Private Function LookForBeforeInsertionIndex(ByVal startCode As Long) As Long
    Const PROC_NAME As String = "LookForBeforeInsertionIndex"
    Dim i As Long
    Dim lBeforeIndex As Long
    
    On Error GoTo EH
    
    lBeforeIndex = 0
    For i = startCode To MAX_CODE
        If m_aiCodes(i) = -1 Then
            lBeforeIndex = IndexByCode(i) + 1
            Exit For
        End If
    Next
    
    LookForBeforeInsertionIndex = lBeforeIndex
    
    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Clean up of arrays, collections, etc.
Private Sub Dispose()
    Const PROC_NAME As String = "Dispose"
    On Error GoTo EH
    
    If Not m_oMaps Is Nothing Then
        'Remove all maps
        While (m_oMaps.Count > 0): m_oMaps.Remove (1): Wend
    End If
    
    Erase m_aiCodes
    Set m_oPalette = Nothing
    m_iDepth = 0
    m_bAvailable = False
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub Class_Initialize()
    Const PROC_NAME As String = "Class_Initialize"
    On Error GoTo EH
    
    Set m_oMaps = New Collection
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub Class_Terminate()
    Const PROC_NAME As String = "Class_Terminate"
    On Error GoTo EH
    
    Dispose
    Set m_oMaps = Nothing
    
    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
