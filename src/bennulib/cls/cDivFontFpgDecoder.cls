VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDivFontFpgDecoder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of bennulib.
'
' bennulib is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' bennulib is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with bennulib. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' FPG decoder for DIV / Fenix/ Bennu old FNT format.
'===============================================================================

Implements IStandardDecoder
Implements IFpgDecoder

'===============================================================================

Private Const Version As Byte = &H0 ' Current readable Fnt version
Private Const MODULE_NAME As String = "bennulib.cDivFontFpgDecoder"

Private Type tCharInfo
    lWidth As Long
    lHeight As Long
    lYOffset As Long    ' Vertical displacement
    lOffset As Long     ' Offset of the graphic in the file
End Type

'===============================================================================

' Purpose: Reads the MAGIC of the file and optionally retrieves the version
' NOTE: Use carefully. lFile must be a valid file id.
Private Function ReadMagic(ByVal lFile As Long, _
        Optional vVersion As Byte) As Boolean
    
    Const PROC_NAME As String = "ReadMagic"
    Dim sMagicFileType As String * 3
    Dim avMagicDescriptor(4) As Byte
    
    On Error GoTo EH
    
    Debug.Assert (lFile <> 0)
    
    ReadMagic = True
    
    gzread lFile, ByVal sMagicFileType, 3
    sMagicFileType = LCase(sMagicFileType)
    
    If sMagicFileType <> LCase(FNT_MAGIC) Then ReadMagic = False
    
    If ReadMagic Then
        ' Next 4 bytes are MS-DOS termination, and last is the FNT version
        gzread lFile, avMagicDescriptor(0), 5
        
        If Not (avMagicDescriptor(0) = &H1A And _
                avMagicDescriptor(1) = &HD And _
                avMagicDescriptor(2) = &HA And _
                avMagicDescriptor(3) = &H0) Then
            ReadMagic = False
        End If
        
        If ReadMagic Then
            vVersion = avMagicDescriptor(4)
            If vVersion > Version Then ReadMagic = False ' Different version
        End If
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IStandardDecoder_CanRead(ByVal sFileName As String) As Boolean
    Const PROC_NAME As String = "IStandardDecoder_CanRead"
    Dim lFile As Long
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    IStandardDecoder_CanRead = ReadMagic(lFile)
    
    Call gzclose(lFile)
    
    Exit Function
EH:
    If lFile <> 0 Then gzclose (lFile)
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function IFpgDecoder_Decode(ByVal sFileName As String) As cFpg
    Const PROC_NAME As String = "IFpgDecoder_Decode"
    Dim lFile As Long
    Dim vVersion As Byte
    Dim avPalette(PALETTE_SIZE - 1) As Byte
    Dim avPaletteUnused(PALETTE_UNUSEDBYTES - 1) As Byte
    Dim lFontInfo As Long
    Dim auCharsInfo(255) As tCharInfo
    Dim lDataLength As Long
    
    Dim avData8() As Byte
    
    Dim i As Integer
    Dim oMap As cMap
    Dim oPal As cPalette
    Dim oFpg As cFpg
    
    On Error GoTo EH
    
    lFile = gzopen(sFileName, "rb")
    
    If lFile = 0 Then
        Err.Raise vbObjectError Or ERR_CANNOTOPENFILE, MODULE_NAME, _
            ResolveResString(ERR_CANNOTOPENFILE, "|file", sFileName)
    End If
    
    vVersion = Version
    If Not ReadMagic(lFile, vVersion) Then
        ' ReadMagic will return false in two cases: when the file is not a FNT
        ' file or when the version is greater than VERSION
        If vVersion > Version Then
            Err.Raise vbObjectError Or ERR_UNSUPPORTEDVERSION, MODULE_NAME, _
                ResolveResString(ERR_UNSUPPORTEDVERSION, "|version", vVersion)
        Else
            Err.Raise vbObjectError Or ERR_NOTANFNTFILE, MODULE_NAME, _
                ResolveResString(ERR_NOTANFNTFILE, "|file", sFileName)
        End If
    End If
    
    ' Palette
    If gzread(lFile, avPalette(0), PALETTE_SIZE) < PALETTE_SIZE Then
        Err.Raise vbObjectError Or ERR_PALETTETRUNCATED, MODULE_NAME, _
            ResolveResString(ERR_PALETTETRUNCATED)
    End If
    If gzread(lFile, avPaletteUnused(0), PALETTE_UNUSEDBYTES) _
            < PALETTE_UNUSEDBYTES Then
        Err.Raise vbObjectError Or ERR_GAMMATRUNCATED, MODULE_NAME, _
            ResolveResString(ERR_GAMMATRUNCATED)
    End If
    
    ' Font info
    gzread lFile, lFontInfo, 4
    ' Font table
    If gzread(lFile, auCharsInfo(0), 4096) < 4096 Then
        Debug.Assert False
    End If
    
    Set oPal = New cPalette
    oPal.CreateFromByteArray avPalette, True
    
    ' Create the FPG object
    Set oFpg = New cFpg
    oFpg.CreateNew 8, oPal
    
    For i = 0 To 255
        lDataLength = auCharsInfo(i).lHeight * auCharsInfo(i).lWidth
        
        If Not (auCharsInfo(i).lOffset = 0 Or lDataLength = 0) Then
            ReDim avData8(lDataLength - 1) As Byte
            gzseek lFile, auCharsInfo(i).lOffset, 0
            If gzread(lFile, avData8(0), lDataLength) < lDataLength Then
                Err.Raise vbObjectError Or ERR_BITMAPDATATRUNCATED, MODULE_NAME, _
                    ResolveResString(ERR_BITMAPDATATRUNCATED)
            End If
            Set oMap = New cMap
            oMap.CreateFromBits auCharsInfo(i).lWidth, auCharsInfo(i).lHeight, _
                8, VarPtr(avData8(0)), "#" & CStr(i) & "#", oPal, False
            oFpg.Add oMap, oFpg.FreeCode()
        
            ' Clean up
            Erase avData8
            Set oMap = Nothing
        End If
    Next
    
    gzclose lFile

    Erase avPalette
    Erase avPaletteUnused
    
    Set IFpgDecoder_Decode = oFpg

    Exit Function
EH:
    Set oFpg = Nothing
    Set oMap = Nothing
    Set oPal = Nothing
    Erase avData8
    If lFile <> 0 Then gzclose lFile
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function
