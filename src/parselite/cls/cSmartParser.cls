VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSmartParser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 Dar�o Cutillas Carrillo
Option Explicit

' This class is a simple parser for evaluating expressions. It admits +, -, /
' and * binary operators, although it is easily extensible to other operatos
' by creating a new class that implements the IBinaryOperator interface
' and modifying the Initialize method of this class.
' Precedence in operators is as expected (*, /, +, -, from left to right).
' It also allows you to specify constants, which are pair of "NAMES-VALUES"
' The current limitations:
'   - Does not allow grouping with ( ) to modify the preference
'   - Does not accept operators or constants whose name contain spaces

Private m_Operators() As IBinaryOperator
Private m_Constants As Collection
Private m_CaseSensitive As Boolean

' Gets if the parser will be case sensitive
Public Property Get CaseSensitive() As Boolean
    CaseSensitive = m_CaseSensitive
End Property

' Sets if the parser will be case sensitive
Public Property Let CaseSensitive(ByVal newVal As Boolean)
    m_CaseSensitive = newVal
End Property

' Defines an integer constant
Public Function SetConstant(ByVal sKey As String, ByVal lValue As Long)
    Dim c As New Constant
    
    If IsAConstant(sKey) Then ' the constant already exists
        m_Constants(sKey) = CStr(lValue)
    Else
        c.Key = sKey
        c.Value = CStr(lValue)
        m_Constants.Add c, sKey
    End If
End Function

' Parses the string s and returns the computed result in lResult. If
' the syntax is not valid, the function returns false
Public Function Parse(ByVal s As String, Optional lResult As Long) As Boolean
    Dim iStart As Integer
    Dim sNextWord As String
    Dim t As String
    Dim a As String
    Dim c As Constant
    
    ' Remove blank spaces
    s = Replace(s, " ", "")
    
    ' Substitute constants for their values
    sNextWord = GetNextWord(s)
    iStart = 1
    While (Len(sNextWord) <> 0)
        If IsAConstant(sNextWord) Then
            Set c = m_Constants(sNextWord)
            s = Replace(s, sNextWord, c.Value, 1, 1, GetCompareMethod)
            iStart = Len(c.Value) + 2
            t = c.Value & " "
        ElseIf IsInteger(sNextWord) Then ' Skip
            iStart = Len(sNextWord) + iStart + 1
            t = sNextWord & " "
        Else ' Parsing error
            Parse = False
            Exit Function
        End If
        sNextWord = Right(GetNextWord(s, iStart), Len(s) - Len(a))
        a = t
        DoEvents
    Wend
    
    ' Parse
    Parse = InternalParse(s, lResult)
End Function

' Returns true if the argument can be interpreted as an integer number, that is
' it does not contain decimal part
Private Function IsInteger(ByVal s As String) As Boolean
    Dim i As Integer
    For i = 1 To Len(s)
        If Not (Mid(s, i, 1) Like "#") Then ' Character is not a digit
            Exit Function
        End If
    Next
    IsInteger = True
End Function

' Searches in the list of the constants to see if the string is contained
Private Function IsAConstant(ByVal s As String) As Boolean
    Dim c As Constant

    For Each c In m_Constants
        If s = c.Key Then
            IsAConstant = True
            Exit Function
        End If
    Next
End Function

' Returns the compare method
Private Function GetCompareMethod() As VbCompareMethod
    GetCompareMethod = IIf(m_CaseSensitive, vbBinaryCompare, vbTextCompare)
End Function

' A word is defined in the following situation:
' Any string starting by a char between two operators
' The same between the beginning and an operator
' The same between an operator and the end
Private Function GetNextWord(ByVal s As String, Optional iStart As Integer = 1) As String
    Dim i As Integer
    Dim op As IBinaryOperator
    Dim lMinPos As Long
    Dim lPos As Long
    
    If iStart > Len(s) Then Exit Function ' No next word
    
    lMinPos = 2000000
    ' Search all the operators and determine the first one
    For i = 0 To UBound(m_Operators)
        Set op = m_Operators(i)
        ' Search for the operator
        lPos = InStr(iStart, s, op.Key, GetCompareMethod)
        If lPos > 0 Then ' Operator found
            If lPos < Abs(lMinPos) Then lMinPos = lPos
        End If
    Next
    
    If lMinPos = 2000000 Then ' No operators, the word is the whole string, after iStart
        GetNextWord = Mid(s, iStart, Len(s) - iStart + 1)
    Else
        GetNextWord = Mid(s, iStart, lMinPos - iStart)
    End If
End Function

' Recursive function to parse the string, using the defined operators,
' to get numeric result (which is stored in lResult)
' Note, the boolean result could be change to something more descriptive, that
' would allow determining which kind of error has happened
Private Function InternalParse(ByVal s As String, lResult As Long) As Boolean
    Dim op As IBinaryOperator
    Dim i As Integer
    Dim sLeft As String, sRight As String
    Dim lPos As Long
    Dim lLeft As Long, lRight As Long
    
    If s = "" Then Exit Function ' Parse error
    
    ' If the string is an integer value (meaning, non-decimal), we are done
    If IsInteger(s) Then
        lResult = CLng(s)
        InternalParse = True
        Exit Function
    End If
    
    For i = 0 To UBound(m_Operators)
        Set op = m_Operators(i)
        ' Search the operator in the string
        lPos = InStr(1, s, op.Key, GetCompareMethod)
        If lPos > 0 Then
            ' Get the left side and parse it
            sLeft = Left(s, lPos - 1)
            If Not InternalParse(sLeft, lLeft) Then ' Parse failed
                Exit Function
            End If
            ' Get the right side and parse it
            sRight = Right(s, Len(s) - lPos)
            If Not InternalParse(sRight, lRight) Then ' Parse failed
                Exit Function
            End If
            ' Check if the operation can be performed
            If Not op.CanCompute(lLeft, lRight) Then
                Exit Function ' Parse failed
            End If
            lResult = op.Compute(lLeft, lRight)
            InternalParse = True
            Exit Function
        End If
    Next
End Function

Private Sub Class_Initialize()
    ' Create the Operators. The order defines the REVERSED order of preference
    ReDim m_Operators(3) As IBinaryOperator
    Set m_Operators(3) = New OperatorMult
    Set m_Operators(2) = New OperatorDiv
    Set m_Operators(1) = New OperatorSum
    Set m_Operators(0) = New OperatorSubtract
    
    Set m_Constants = New Collection
End Sub

Private Sub Class_Terminate()
    Erase m_Operators
    Set m_Constants = Nothing
End Sub
