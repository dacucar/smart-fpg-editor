VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Constant"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of parselite.
'
' parselite is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' parselite is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with parselite.  If not, see <http://www.gnu.org/licenses/>.

Option Explicit

Private m_sKey As String
Private m_sValue As String

Public Property Get Key() As String
    Key = m_sKey
End Property

Public Property Let Key(ByVal newVal As String)
    m_sKey = newVal
End Property

Public Property Get Value() As String
    Value = m_sValue
End Property

Public Property Let Value(ByVal newVal As String)
    m_sValue = newVal
End Property
