VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IBinaryOperator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of parselite.
'
' parselite is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' parselite is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with parselite.  If not, see <http://www.gnu.org/licenses/>.

Option Explicit

' Interface for a generic binary operator class (that works with numbers),

Public Property Get Key() As String

End Property

' Should return true if the Operator can perform the operatio for the
' left and right side values
Public Function CanCompute(ByVal lA As Long, ByVal lB As Long) As Boolean

End Function

' Returns the result of computing the left side (A) with the right side (B)
Public Function Compute(ByVal lA As Long, ByVal lB As Long) As Long

End Function
