VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSimpleParser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_Operators() As IBinaryOperator

' Returns true if the argument can be interpreted as an integer number, that is
' it does not contain decimal part
Private Function IsInteger(ByVal s As String) As Boolean
    Dim i As Integer
    For i = 1 To Len(s)
        If Not (Mid(s, i, 1) Like "#") Then ' Character is not a digit
            Exit Function
        End If
    Next
    IsInteger = True
End Function

' Recursive function to parse the string, using the defined operators,
' to get numeric result (which is stored in lResult)
' Note, the boolean result could be change to something more descriptive, that
' would allow determining which kind of error has happened
Private Function InternalParse(ByVal s As String, lResult As Long) As Boolean
    Dim op As IBinaryOperator
    Dim i As Integer
    Dim sLeft As String, sRight As String
    Dim lPos As Long
    Dim lLeft As Long, lRight As Long
    
    If s = "" Then Exit Function ' Parse error
    
    ' If the string is an integer value (meaning, non-decimal), we are done
    If IsInteger(s) Then
        lResult = CLng(s)
        InternalParse = True
        Exit Function
    End If
    
    For i = 0 To UBound(m_Operators)
        Set op = m_Operators(i)
        ' Search the operator in the string
        lPos = InStr(s, op.Key)
        If lPos > 0 Then
            ' Get the left side and parse it
            sLeft = Left(s, lPos - 1)
            If Not InternalParse(sLeft, lLeft) Then ' Parse failed
                Exit Function
            End If
            ' Get the right side and parse it
            sRight = Right(s, Len(s) - lPos)
            If Not InternalParse(sRight, lRight) Then ' Parse failed
                Exit Function
            End If
            ' Check if the operation can be performed
            If Not op.CanCompute(lLeft, lRight) Then
                Exit Function ' Parse failed
            End If
            lResult = op.Compute(lLeft, lRight)
            InternalParse = True
            Exit Function
        End If
    Next
End Function

' Returns false if parsing fails
Public Function Parse(ByVal s As String, lResult As Long) As Boolean
    Dim i As Integer
    
    ' Remove blank spaces
    s = Replace(s, " ", "")
    
    ' Parse
    Parse = InternalParse(s, lResult)
End Function

Private Sub Class_Initialize()
    ' Create the Operators. The order defines the REVERSED order of preference
    ReDim m_Operators(3) As IBinaryOperator
    Set m_Operators(3) = New OperatorMult
    Set m_Operators(2) = New OperatorDiv
    Set m_Operators(1) = New OperatorSum
    Set m_Operators(0) = New OperatorSubtract
End Sub

Private Sub Class_Terminate()
    Erase m_Operators
End Sub
