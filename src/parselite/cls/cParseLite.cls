VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cParseLite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of parselite.
'
' parselite is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' parselite is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with parselite.  If not, see <http://www.gnu.org/licenses/>.

Option Explicit

' This class is a simple parser and numeric expressions evaluator.
' It can interpret expressions such as "3+5-8" and calculate the appropiate
' value.
'
' It admits +, -, / and * operators, but it is easily extensible to other
' operators (there are some limitations, though). Refer to "extending operators"
' section to know more about this.
' Precedence in operators is as expected (*, /, +, -) and expressions are
' evaluated from left to right.
' Precedence cannot be modified by using "( )" (is not implemented)
'
' You can also define "constants" which the parser will recognize. Constants
' associate a name with a numeric value and are defined via the SetConstant
' method.
' The parser is case insensitive, although it would be possible to extend
' it.
'
' There are some limitations of this class that you should note:
'   - Keys (names) for constants cannot contain spaces
'
' Unexpected behaviout may occur if you do not attend to these indications
'
' EXTENDING OPERATORS
' If you need it, you can define new binary operators. To do so you should create
' a new class that implements the IBinaryOperator interface. Look at the several
' operator classes code to have an example (OperatorX.cls classes).
' and modifying the Initialize method of this class.
' You will also need to modify the Initialize method of this class, which create
' the new operators.
'
' Some limitations you should be aware of the current class implementation are:
'   - Keys (names) for constants and operators cannot contain spaces
'   - Operator's keys cannot be part of other operator keys. For instance, you
'     could not have an operator called "MULT" and another called "MYMULT"
'
' Again, unexpected behaviout may occur if you do not attend these indications


Private m_Operators() As IBinaryOperator
Private m_Constants As Collection
Private m_CaseSensitive As Boolean

' Defines numeric constant. The parser will authomatically replace
' constants by their value.
' @param sKey the name of the constant
' @param lValue the numeric value associated with this constant
Public Sub SetConstant(ByVal sKey As String, ByVal lValue As Long)
    Dim c As New Constant
    
    c.Key = sKey
    c.Value = CStr(lValue)
    If IsAConstant(sKey) Then ' the constant already exists
        m_Constants(sKey).Value = c.Value
    Else
        m_Constants.Add c, sKey
    End If
End Sub

' Checks if the String s has a valid Sintax.
' @param s the string to be parsed
' @return true if the sintax is valid, false otherwise
Public Function IsSintaxValid(ByVal s As String) As Boolean
    Dim lR As Long
    IsSintaxValid = Execute(s, lR)
End Function

' Parses the string s and returns the computed if the parser succeded and
' the computed result. The return value indicates whether s was sintactically
' correct (true if so). The result of evaluating the expression is stored in
' lResult. Do not trust on the value if the function returned true
' @param s the string to be evaluated
' @param [lResult] the variable where the result will be stored
' @return true if the parser succeds, false otherwise
Public Function Execute(ByVal s As String, lResult As Long) As Boolean
    Dim iStart As Integer
    Dim sNextWord As String
    Dim t As String
    Dim a As String
    Dim c As Constant
    
    ' Remove blank spaces
    s = Replace(s, " ", "")
    
    ' Substitute constants for their values
    sNextWord = GetNextWord(s)
    iStart = 1
    While (Len(sNextWord) <> 0)
        If IsAConstant(sNextWord) Then
            Set c = m_Constants(sNextWord)
            s = Replace(s, sNextWord, c.Value, 1, 1, GetCompareMethod)
            iStart = Len(c.Value) + 2
            t = c.Value & " "
        ElseIf IsInteger(sNextWord) Then ' Skip
            iStart = Len(sNextWord) + iStart + 1
            t = sNextWord & " "
        Else ' Parsing error
            Execute = False
            Exit Function
        End If
        sNextWord = Right(GetNextWord(s, iStart), Len(s) - Len(a))
        a = t
    Wend
    
    ' Parse
    Execute = InternalParse(s, lResult)
End Function

' NOTE: Case sensitive is not yet supported but the class is ready to easily
' extend it if necessary. The current limitation is imposed for the use
' of the constant's keys as the key in the collection that store the constants
' Gets if the parser will be case sensitive
Public Property Get CaseSensitive() As Boolean
    CaseSensitive = False 'm_CaseSensitive
End Property

' Sets if the parser will be case sensitive
Public Property Let CaseSensitive(ByVal newVal As Boolean)
    m_CaseSensitive = False 'newVal
End Property

' Returns the compare method
Private Function GetCompareMethod() As VbCompareMethod
    GetCompareMethod = IIf(m_CaseSensitive, vbBinaryCompare, vbTextCompare)
End Function

' Returns true if the argument can be interpreted as an integer number, that is
' it does not contain decimal part
Private Function IsInteger(ByVal s As String) As Boolean
    Dim i As Integer
    For i = 1 To Len(s)
        If Not (Mid(s, i, 1) Like "#") Then ' Character is not a digit
            Exit Function
        End If
    Next
    IsInteger = True
End Function

' Searches in the list of the constants to see if the string is contained
Private Function IsAConstant(ByVal s As String) As Boolean
    Dim c As Constant

    For Each c In m_Constants
        If s = c.Key Then
            IsAConstant = True
            Exit Function
        End If
    Next
End Function

' A word is defined in the following situation:
' Any string starting by a char between two operators
' The same between the beginning and an operator
' The same between an operator and the end
Private Function GetNextWord(ByVal s As String, Optional iStart As Integer = 1) As String
    Dim i As Integer
    Dim op As IBinaryOperator
    Dim lMinPos As Long
    Dim lPos As Long
    
    If iStart > Len(s) Then Exit Function ' No next word
    
    lMinPos = 2000000
    ' Search all the operators and determine the first one
    For i = 0 To UBound(m_Operators)
        Set op = m_Operators(i)
        ' Search for the operator
        lPos = InStr(iStart, s, op.Key, GetCompareMethod)
        If lPos > 0 Then ' Operator found
            If lPos < Abs(lMinPos) Then lMinPos = lPos
        End If
    Next
    
    If lMinPos = 2000000 Then ' No operators, the word is the whole string, after iStart
        GetNextWord = Mid(s, iStart, Len(s) - iStart + 1)
    Else
        GetNextWord = Mid(s, iStart, lMinPos - iStart)
    End If
End Function

' Recursive function to parse the string, using the defined operators,
' to get numeric result (which is stored in lResult)
' Note, the boolean result could be change to something more descriptive, that
' would allow determining which kind of error has happened
Private Function InternalParse(ByVal s As String, lResult As Long) As Boolean
    Dim op As IBinaryOperator
    Dim i As Integer
    Dim sLeft As String, sRight As String
    Dim lPos As Long
    Dim lLeft As Long, lRight As Long
    
    If s = "" Then Exit Function ' Parse error
    
    ' If the string is an integer value (meaning, non-decimal), we are done
    If IsInteger(s) Then
        lResult = CLng(s)
        InternalParse = True
        Exit Function
    End If
    
    For i = 0 To UBound(m_Operators)
        Set op = m_Operators(i)
        ' Search the operator in the string
        lPos = InStr(1, s, op.Key, GetCompareMethod)
        If lPos > 0 Then
            ' Get the left side and parse it
            sLeft = Left(s, lPos - 1)
            If Not InternalParse(sLeft, lLeft) Then ' Parse failed
                Exit Function
            End If
            ' Get the right side and parse it
            sRight = Right(s, Len(s) - lPos)
            If Not InternalParse(sRight, lRight) Then ' Parse failed
                Exit Function
            End If
            ' Check if the operation can be performed
            If Not op.CanCompute(lLeft, lRight) Then
                Exit Function ' Parse failed
            End If
            lResult = op.Compute(lLeft, lRight)
            InternalParse = True
            Exit Function
        End If
    Next
End Function

Private Sub Class_Initialize()
    ' Create the Operators. The order defines the REVERSED order of preference
    ReDim m_Operators(3) As IBinaryOperator
    Set m_Operators(3) = New OperatorMult
    Set m_Operators(2) = New OperatorDiv
    Set m_Operators(1) = New OperatorSum
    Set m_Operators(0) = New OperatorSubtract
    
    Set m_Constants = New Collection
End Sub

Private Sub Class_Terminate()
    Erase m_Operators
    Set m_Constants = Nothing
End Sub
