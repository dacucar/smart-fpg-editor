Attribute VB_Name = "modGlobals"
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of smartcpx.
'
' smartcpx is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' smartcpx is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with smartcpx. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' MODULE OVERVIEW
'===============================================================================
' Global variables/methods.
'===============================================================================

' VB error object replacement
Public Err As New cError
