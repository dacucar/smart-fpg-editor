VERSION 5.00
Begin VB.Form frmCpEditor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Control Point Editor"
   ClientHeight    =   6225
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7335
   Icon            =   "frmCpEditor.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   415
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   489
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkReplacementConfirmation 
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   5640
      Width           =   3495
   End
   Begin VB.CheckBox chkIncrement 
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   5400
      Width           =   3495
   End
   Begin VB.TextBox txtId 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   375
      Left            =   3360
      TabIndex        =   18
      Top             =   2160
      Width           =   615
   End
   Begin VB.CommandButton btUp 
      Height          =   375
      Left            =   4080
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmCpEditor.frx":038A
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   2160
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.CommandButton btDown 
      Height          =   375
      Left            =   2880
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmCpEditor.frx":0418
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   2160
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.PictureBox picMultipleEditing 
      Align           =   2  'Align Bottom
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   0
      ScaleHeight     =   19
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   489
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   5940
      Visible         =   0   'False
      Width           =   7335
      Begin VB.Image Image3 
         Height          =   240
         Left            =   6960
         Picture         =   "frmCpEditor.frx":04A7
         Top             =   15
         Width           =   240
      End
      Begin VB.Label lblMultiEditionOn 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   480
         TabIndex        =   31
         Top             =   45
         Width           =   6375
      End
      Begin VB.Image Image2 
         Height          =   240
         Left            =   120
         Picture         =   "frmCpEditor.frx":0895
         Top             =   15
         Width           =   240
      End
   End
   Begin VB.CommandButton btApply 
      Height          =   450
      Left            =   4920
      TabIndex        =   25
      Top             =   5415
      Width           =   1095
   End
   Begin VB.CommandButton btOk 
      Height          =   450
      Left            =   3720
      TabIndex        =   24
      Top             =   5415
      Width           =   1095
   End
   Begin VB.CommandButton btCancel 
      Cancel          =   -1  'True
      Height          =   450
      Left            =   6120
      TabIndex        =   26
      Top             =   5415
      Width           =   1095
   End
   Begin VB.Frame grbMain 
      Height          =   1215
      Left            =   120
      TabIndex        =   27
      Top             =   840
      Width           =   7095
      Begin VB.Frame grbAddMode 
         BorderStyle     =   0  'None
         Caption         =   "Frame3"
         Height          =   855
         Left            =   6120
         TabIndex        =   29
         Top             =   240
         Width           =   855
         Begin VB.OptionButton opAppend 
            Height          =   375
            Left            =   0
            MaskColor       =   &H000000FF&
            Picture         =   "frmCpEditor.frx":0C83
            Style           =   1  'Graphical
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Value           =   -1  'True
            Width           =   375
         End
         Begin VB.OptionButton opInsertBefore 
            Height          =   375
            Left            =   480
            MaskColor       =   &H0000FF00&
            Picture         =   "frmCpEditor.frx":0DE7
            Style           =   1  'Graphical
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   375
         End
         Begin VB.OptionButton opInsertAfter 
            Height          =   375
            Left            =   0
            MaskColor       =   &H0000FF00&
            Picture         =   "frmCpEditor.frx":0F6C
            Style           =   1  'Graphical
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   480
            UseMaskColor    =   -1  'True
            Width           =   375
         End
         Begin VB.OptionButton opManual 
            Height          =   375
            Left            =   480
            MaskColor       =   &H000000FF&
            Picture         =   "frmCpEditor.frx":11BE
            Style           =   1  'Graphical
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   480
            UseMaskColor    =   -1  'True
            Width           =   375
         End
      End
      Begin VB.TextBox txtY 
         Height          =   350
         Left            =   1980
         TabIndex        =   12
         Top             =   720
         Width           =   3255
      End
      Begin VB.TextBox txtX 
         Height          =   350
         Left            =   1980
         TabIndex        =   10
         Top             =   240
         Width           =   3255
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Predefined sets"
         Height          =   855
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   855
         Begin VB.CommandButton cmdTopLeft 
            Height          =   255
            Left            =   0
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":12B2
            Style           =   1  'Graphical
            TabIndex        =   0
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   255
         End
         Begin VB.CommandButton cmdTop 
            Height          =   255
            Left            =   300
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":15F6
            Style           =   1  'Graphical
            TabIndex        =   1
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   255
         End
         Begin VB.CommandButton cmdTopRight 
            Height          =   255
            Left            =   600
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":193A
            Style           =   1  'Graphical
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   255
         End
         Begin VB.CommandButton cmdLeft 
            Height          =   255
            Left            =   0
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":1C7E
            Style           =   1  'Graphical
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   300
            UseMaskColor    =   -1  'True
            Width           =   255
         End
         Begin VB.CommandButton cmdCenter 
            Height          =   255
            Left            =   300
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":1FC2
            Style           =   1  'Graphical
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   300
            UseMaskColor    =   -1  'True
            Width           =   255
         End
         Begin VB.CommandButton cmdRight 
            Height          =   255
            Left            =   600
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":2306
            Style           =   1  'Graphical
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   300
            UseMaskColor    =   -1  'True
            Width           =   255
         End
         Begin VB.CommandButton cmdBottomLeft 
            Height          =   255
            Left            =   0
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":264A
            Style           =   1  'Graphical
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   600
            UseMaskColor    =   -1  'True
            Width           =   255
         End
         Begin VB.CommandButton cmdBottom 
            Height          =   255
            Left            =   300
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":298E
            Style           =   1  'Graphical
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   600
            UseMaskColor    =   -1  'True
            Width           =   255
         End
         Begin VB.CommandButton cmdBottomRight 
            Height          =   255
            Left            =   600
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmCpEditor.frx":2CD2
            Style           =   1  'Graphical
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   600
            UseMaskColor    =   -1  'True
            Width           =   255
         End
      End
      Begin VB.Image imgX 
         Height          =   240
         Left            =   5351
         Picture         =   "frmCpEditor.frx":3016
         Top             =   300
         Width           =   240
      End
      Begin VB.Image imgY 
         Height          =   240
         Left            =   5351
         Picture         =   "frmCpEditor.frx":3274
         Top             =   795
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Y:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1560
         TabIndex        =   11
         Top             =   795
         Width           =   195
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "&X:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1560
         TabIndex        =   9
         Top             =   315
         Width           =   195
      End
   End
   Begin VB.CommandButton btRemove 
      Height          =   375
      Left            =   6240
      MaskColor       =   &H0000FF00&
      Picture         =   "frmCpEditor.frx":34D2
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   2160
      UseMaskColor    =   -1  'True
      Width           =   855
   End
   Begin VB.ListBox lst 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2700
      IntegralHeight  =   0   'False
      Left            =   120
      TabIndex        =   21
      Top             =   2640
      Width           =   7095
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   0
      TabIndex        =   35
      Top             =   0
      Width           =   7335
   End
   Begin VB.Image imgTitle 
      Height          =   600
      Left            =   0
      Picture         =   "frmCpEditor.frx":3636
      Stretch         =   -1  'True
      Top             =   0
      Width           =   7335
   End
   Begin VB.Label lblInsertionPoint 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   2055
      TabIndex        =   34
      Top             =   2250
      Width           =   645
   End
   Begin VB.Image imgUp 
      Height          =   240
      Left            =   1200
      Picture         =   "frmCpEditor.frx":3754
      Top             =   2160
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgDown 
      Height          =   240
      Left            =   840
      Picture         =   "frmCpEditor.frx":3A96
      Top             =   2160
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label lblConfirmation1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   165
      TabIndex        =   33
      Top             =   2250
      Visible         =   0   'False
      Width           =   2565
   End
   Begin VB.Label lblConfirmation2 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   4560
      TabIndex        =   32
      Top             =   2250
      Visible         =   0   'False
      Width           =   1605
   End
   Begin VB.Image imgWrong 
      Height          =   240
      Left            =   480
      Picture         =   "frmCpEditor.frx":3DD8
      Top             =   2160
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgGood 
      Height          =   240
      Left            =   120
      Picture         =   "frmCpEditor.frx":4036
      Top             =   2160
      Visible         =   0   'False
      Width           =   240
   End
End
Attribute VB_Name = "frmCpEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of smartcpx.
'
' smartcpx is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' smartcpx is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with smartcpx. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

' Raised when the user clicks Apply or Accept. The coordinates are stored as
' strings, which may not be just interger values if the user has defined
' a parser for the editor
' @param iCount the number of control points that were on the list
' @param asX an array containing the X coordinates of the defined control points
' @param asY an array containing the Y coordinates of the defined control points
' @param alIds an array containing the IDs of the defined control points
Public Event Action(ByVal iCount As Integer, _
            asX() As String, asY() As String, alIds() As Long)
            
Public Event Unload()

Private Type tControlPoint
    sX As String
    sY As String
    lId As Long
End Type

Private Const MODULE_NAME As String = "smartcpx.frmCpEditor"

' IMPORTANT: The array of CPs is always kept sorted by the control point ID!!
Private m_auControlPoints() As tControlPoint
Private m_iCPCount As Integer

Private m_bMultipleEditingMode As Boolean
Private m_bInConfirmationMode As Boolean

Private m_Parser As cParseLite

Private Const RID_OK As Integer = 101
Private Const RID_APPLY As Integer = 102
Private Const RID_CANCEL As Integer = 103
Private Const RID_APPENDMODE As Integer = 104
Private Const RID_INSERTMODE As Integer = 105
Private Const RID_WISEMODE As Integer = 106
Private Const RID_MANUALMODE As Integer = 107
Private Const RID_INSERTAT As Integer = 108
Private Const RID_CONFIRMATION1 As Integer = 109
Private Const RID_CONFIRMATION2 As Integer = 110
Private Const RID_AUTOINCREMENT As Integer = 111
Private Const RID_ASKCONFIRMATION As Integer = 112
Private Const RID_MULTIPLEEDITMODE As Integer = 113
Private Const RID_TITLE As Integer = 114

' Control point ID limit
Private Const MAX_CP_ID As Integer = 9999
Private Const MIN_CP_ID As Integer = 0
            
'===================================================================================
' FRIEND PROPERTIES AND METHODS
'===================================================================================
            
' Sets whether the Multiple Graph Editing mode caution message is visible or not
Friend Property Let MultipleEditingMode(ByVal newVal As Boolean)
    Const PROC_NAME As String = "MultipleEditingMode"

    On Error GoTo EH

    m_bMultipleEditingMode = newVal
    picMultipleEditing.Visible = newVal
    Me.Height = IIf(newVal, 6600, 6300)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves whether the Multiple Graph Editing mode caution message is visible or not
Friend Property Get MultipleEditingMode() As Boolean
    Const PROC_NAME As String = "MultipleEditingMode"

    On Error GoTo EH

    MultipleEditingMode = m_bMultipleEditingMode

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property
            
' Sets the list of control points
Friend Sub SetControlPoints(ByVal iCount As Integer, alX() As Long, _
            alY() As Long, alIds() As Long)
            
    Dim i As Integer
    
    Const PROC_NAME As String = "SetControlPoints"

    On Error GoTo EH

    Erase m_auControlPoints
    m_iCPCount = 0
    
    If iCount > 0 Then
        ReDim m_auControlPoints(iCount - 1) As tControlPoint
    End If
    
    For i = 0 To iCount - 1
        SetControlPoint CStr(alX(i)), CStr(alY(i)), CInt(alIds(i))
    Next
    
    RefreshList
    If lst.ListCount > 0 Then lst.ListIndex = 0

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'===================================================================================
' PRIVATE PROPERTIES AND METHODS
'===================================================================================

' Switchs the confirmation for replacement mode
Private Property Let InConfirmationMode(ByVal newVal As Boolean)
    Const PROC_NAME As String = "InConfirmationMode"

    On Error GoTo EH

    If newVal = False Then  ' Cancel
        btDown.Picture = imgDown.Picture
        btUp.Picture = imgUp.Picture
    Else
        btDown.Picture = imgWrong.Picture
        btUp.Picture = imgGood.Picture
    End If
    grbMain.Enabled = Not newVal
    txtX.Enabled = Not newVal
    txtY.Enabled = Not newVal
    chkReplacementConfirmation.Enabled = IIf(newVal, 0, 1)
    btOk.Enabled = Not newVal
    btApply.Enabled = Not newVal
    btRemove.Visible = Not newVal
    txtId.Enabled = Not newVal
    lblConfirmation1.Visible = newVal
    lblConfirmation2.Visible = newVal
    lblInsertionPoint.Visible = Not newVal
    
    m_bInConfirmationMode = newVal

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Returns true if we are waiting for confirmation of replacement
Private Property Get InConfirmationMode() As Boolean
    Const PROC_NAME As String = "InConfirmationMode"

    On Error GoTo EH

    InConfirmationMode = m_bInConfirmationMode

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Purpose: Searches for the CP whose ID is lID and returns its index. If the CP
' is not found, the return value will be -1
Private Function GetIndexForId(ByVal lId As Long) As Long
    Dim lIndex As Long
    Dim i As Integer
    
    Const PROC_NAME As String = "GetIndexForId"

    On Error GoTo EH

    lIndex = -1
    
    For i = 0 To m_iCPCount - 1
        If m_auControlPoints(i).lId = lId Then
            lIndex = i
            Exit For
        End If
    Next
    
    GetIndexForId = lIndex

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Removes the control point whose index is lIndex
Private Sub RemoveControlPoint(ByVal lIndex As Long)
    Dim i As Integer
    
    ' Move the upper CPs one index back in the array
    Const PROC_NAME As String = "RemoveControlPoint"

    On Error GoTo EH

    For i = lIndex To m_iCPCount - 2
        m_auControlPoints(i) = m_auControlPoints(i + 1)
    Next
    
    m_iCPCount = m_iCPCount - 1
    If m_iCPCount = 0 Then
        Erase m_auControlPoints
    Else
        ReDim Preserve m_auControlPoints(m_iCPCount - 1) As tControlPoint
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Adds a new control point whose ID will be one more than that of the
' last control point in the array
' Returns the index of the inserted control point
Private Function AppendControlPoint(ByVal sX As String, ByVal sY As String) As Long
    Const PROC_NAME As String = "AppendControlPoint"

    On Error GoTo EH

    ReDim Preserve m_auControlPoints(m_iCPCount) As tControlPoint
    
    m_auControlPoints(m_iCPCount).sX = sX
    m_auControlPoints(m_iCPCount).sY = sY
    
    If m_iCPCount > 0 Then
        m_auControlPoints(m_iCPCount).lId = _
                m_auControlPoints(m_iCPCount - 1).lId + 1
    Else
        m_auControlPoints(m_iCPCount).lId = 0
    End If
    
    m_iCPCount = m_iCPCount + 1
    AppendControlPoint = m_iCPCount - 1

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Inserts a control point before or after the control point whose index
' is lIndex. The Behaviour to determine the ID of the inserted CP is different
' depending if the insertion is done before or after (bAfterIndex paramter).
'   - Insertion BEFORE lIndex: The new control point will have the same ID than
'     that of the CP that currently occupies the lIndex position. All the CP
'     whose index is greater or equal to lIndex will increase there ID by one.
'   - Insertion AFTER lIndex: The new CP will have the same ID than that of the
'     CP that currently occupies the lIndex position plus one. If the CP whose
'     index is (lIndex + 1) have been defined, the CPs with an index > lIndex
'     will increase their ID in 1. If that is not the case, no changes will
'     be done to those CPs.
' Returns the index of the inserted control point
Private Function InsertControlPoint(ByVal sX As String, ByVal sY As String, _
            ByVal lIndex As Long, Optional ByVal bAfterIndex As Boolean) As Long
    
    Dim i As Integer
    
    Const PROC_NAME As String = "InsertControlPoint"

    On Error GoTo EH
    
    ReDim Preserve m_auControlPoints(m_iCPCount) As tControlPoint
    
    If bAfterIndex = False Then ' Insert Before Mode
        If m_iCPCount = 0 Then
            InsertControlPoint = AppendControlPoint(sX, sY)
        Else
            ' Move CPs whose index >= lIndex one position forward in the
            ' array and increse their ID by one
            For i = m_iCPCount To (lIndex + 1) Step -1
                m_auControlPoints(i).sX = m_auControlPoints(i - 1).sX
                m_auControlPoints(i).sY = m_auControlPoints(i - 1).sY
                m_auControlPoints(i).lId = m_auControlPoints(i - 1).lId + 1
            Next
            
            m_auControlPoints(lIndex).sX = sX
            m_auControlPoints(lIndex).sY = sY
            m_auControlPoints(lIndex).lId = m_auControlPoints(lIndex + 1).lId - 1
            
            m_iCPCount = m_iCPCount + 1
            
            InsertControlPoint = lIndex
        End If
    Else ' Insert After Mode
        If lIndex + 1 < m_iCPCount Then ' Not the last control point
            '  Is there free space between iIndex and the next ControlPoint?
            If m_auControlPoints(lIndex).lId _
                    <> m_auControlPoints(lIndex + 1).lId - 1 Then
                SetControlPoint sX, sY, m_auControlPoints(lIndex).lId + 1
            Else
                ' Inserting after lIndex is the same as inserting before lIndex + 1
                InsertControlPoint sX, sY, lIndex + 1, False
            End If
            InsertControlPoint = lIndex + 1
        Else ' Last control point
            InsertControlPoint = AppendControlPoint(sX, sY)
        End If
    End If

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Sets the control point whose ID is lID.
' Returns the index of the inserted control point
Private Function SetControlPoint(ByVal sX As String, ByVal sY As String, _
            ByVal lId As Long) As Long
    
    Dim i As Integer
    Dim lIndex As Long
    Dim bExists As Boolean
    
    Const PROC_NAME As String = "SetControlPoint"

    On Error GoTo EH

    lIndex = m_iCPCount
    ' Look for the index of the first control point whose id is >= lId
    For i = 0 To m_iCPCount - 1
        If m_auControlPoints(i).lId >= lId Then
            lIndex = i
            bExists = (m_auControlPoints(i).lId = lId)
            Exit For
        End If
    Next
    
    If bExists Then ' The ControlPoint exists already, just update it
        m_auControlPoints(lIndex).sX = sX
        m_auControlPoints(lIndex).sY = sY
    Else
        ReDim Preserve m_auControlPoints(m_iCPCount) As tControlPoint
        ' Move the upper control points forward in the array
        ' (without changing the ID)
        For i = m_iCPCount To (lIndex + 1) Step -1
            m_auControlPoints(i).sX = m_auControlPoints(i - 1).sX
            m_auControlPoints(i).sY = m_auControlPoints(i - 1).sY
            m_auControlPoints(i).lId = m_auControlPoints(i - 1).lId
        Next
        m_auControlPoints(lIndex).sX = sX
        m_auControlPoints(lIndex).sY = sY
        m_auControlPoints(lIndex).lId = lId
        
        m_iCPCount = m_iCPCount + 1
    End If
    
    SetControlPoint = lIndex

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

' Purpose: Selects the text of a text box, and set the focus to it
Private Sub SelectTextBox(oTB As TextBox)
    Const PROC_NAME As String = "SelectTextBox"

    On Error GoTo EH

    If oTB Is Nothing Then Exit Sub
    
    oTB.SetFocus
    oTB.SelStart = 0
    oTB.SelLength = Len(oTB.Text)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Purpose: Updates the txtID depending on the Mode selected
Private Sub UpdateInsertionID()
    Dim lId As Long
    
    Const PROC_NAME As String = "UpdateInsertionID"

    On Error GoTo EH

    lId = IIf(m_iCPCount = 0, 0, CLng(txtId.Text))

    If opAppend.Value = True Then
        If m_iCPCount > 0 Then lId = m_auControlPoints(m_iCPCount - 1).lId + 1
    ElseIf opInsertBefore.Value = True Then
        If lst.ListIndex >= 0 Then lId = m_auControlPoints(lst.ListIndex).lId
    ElseIf opInsertAfter.Value = True Then
        If lst.ListIndex >= 0 Then lId = m_auControlPoints(lst.ListIndex).lId + 1
    ElseIf opManual.Value = True Then
        If lst.ListIndex >= 0 Then lId = m_auControlPoints(lst.ListIndex).lId
    End If
    
    txtId.Text = CStr(lId)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

'===================================================================================
' OBJECT'S EVENTS
'===================================================================================

Private Sub btApply_Click()
    Dim asX() As String
    Dim asY() As String
    Dim alIds() As Long
    Dim i As Integer
    
    Const PROC_NAME As String = "btApply_Click"

    On Error GoTo EH

    If m_iCPCount > 0 Then
        ReDim asX(m_iCPCount - 1) As String
        ReDim asY(m_iCPCount - 1) As String
        ReDim alIds(m_iCPCount - 1) As Long
        
        For i = 0 To m_iCPCount - 1
            asX(i) = m_auControlPoints(i).sX
            asY(i) = m_auControlPoints(i).sY
            alIds(i) = m_auControlPoints(i).lId
        Next
    End If
    
    RaiseEvent Action(m_iCPCount, asX, asY, alIds)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub btCancel_Click()
    Const PROC_NAME As String = "btCancel_Click"

    On Error GoTo EH

    Unload Me

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub btDown_Click()
    Dim lIndex As Long
    Dim bExists As Boolean
    
    ' Prevent insertion if sintax is invalid
    Const PROC_NAME As String = "btDown_Click"

    On Error GoTo EH

    If Not (m_Parser.IsSintaxValid(txtX) And m_Parser.IsSintaxValid(txtY)) Then
        Exit Sub
        Beep
    End If
    
    If opAppend.Value Then
        lIndex = AppendControlPoint(txtX.Text, txtY.Text)
    ElseIf opInsertBefore.Value Then
        lIndex = InsertControlPoint(txtX.Text, txtY.Text, lst.ListIndex)
    ElseIf opInsertAfter.Value Then
        lIndex = InsertControlPoint(txtX.Text, txtY.Text, lst.ListIndex, True)
    ElseIf opManual.Value Then
        lIndex = GetIndexForId(CLng(txtId.Text))
        bExists = ((lIndex) > -1)
        ' Confirmation required?
        If chkReplacementConfirmation.Value = 1 And bExists Then
            InConfirmationMode = Not InConfirmationMode
        Else
            lIndex = SetControlPoint(txtX.Text, txtY.Text, txtId.Text)
        End If
    End If
    
    RefreshList
    lst.ListIndex = lIndex
    UpdateInsertionID
    
    ' Extra behaviour when Manual Mode is set
    If opManual.Value And Not InConfirmationMode Then
        ' Autoincrement
        If chkIncrement.Value = 1 Then txtId.Text = CStr(CInt(txtId.Text) + 1)
    End If
    
    If Not InConfirmationMode Then SelectTextBox txtX

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub btOk_Click()
    Const PROC_NAME As String = "btOk_Click"

    On Error GoTo EH

    btApply_Click
    Unload Me

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub btRemove_Click()
    Dim lIndex As Long
    
    Const PROC_NAME As String = "btRemove_Click"

    On Error GoTo EH

    lIndex = lst.ListIndex
    If lIndex >= 0 Then
        RemoveControlPoint lIndex
        RefreshList
        ' Select another row
        If lst.ListCount > 0 Then
            Select Case lIndex
            Case 0: lst.ListIndex = 0
            Case lst.ListCount: lst.ListIndex = lIndex - 1
            Case Else: lst.ListIndex = lIndex
            End Select
        End If
        
        UpdateInsertionID
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub btUp_Click()
    Dim lIndex As Long
    
    Const PROC_NAME As String = "btUp_Click"

    On Error GoTo EH

    If InConfirmationMode Then ' Button confirms replacement of CP
        lIndex = SetControlPoint(txtX.Text, txtY.Text, CLng(txtId.Text))
        InConfirmationMode = False
        RefreshList
        lst.ListIndex = lIndex
        If chkIncrement.Value = 1 Then txtId.Text = CStr(lIndex + 1)
        SelectTextBox txtX
    Else
        If lst.ListIndex >= 0 Then
            txtX.Text = m_auControlPoints(lst.ListIndex).sX
            txtY.Text = m_auControlPoints(lst.ListIndex).sY
            txtId.Text = m_auControlPoints(lst.ListIndex).lId
            opManual.Value = True
            SelectTextBox txtX
        End If
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdBottom_Click()
    Const PROC_NAME As String = "cmdBottom_Click"

    On Error GoTo EH

    txtX = "WIDTH / 2"
    txtY = "HEIGHT - 1"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdBottomLeft_Click()
    Const PROC_NAME As String = "cmdBottomLeft_Click"

    On Error GoTo EH

    txtX = "0"
    txtY = "HEIGHT - 1"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdBottomRight_Click()
    Const PROC_NAME As String = "cmdBottomRight_Click"

    On Error GoTo EH

    txtX = "WIDTH - 1"
    txtY = "HEIGHT - 1"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdCenter_Click()
    Const PROC_NAME As String = "cmdCenter_Click"

    On Error GoTo EH

    txtX = "WIDTH / 2"
    txtY = "HEIGHT / 2"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdLeft_Click()
    Const PROC_NAME As String = "cmdLeft_Click"

    On Error GoTo EH

    txtX = "0"
    txtY = "HEIGHT / 2"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdRight_Click()
    Const PROC_NAME As String = "cmdRight_Click"

    On Error GoTo EH

    txtX = "WIDTH - 1"
    txtY = "HEIGHT / 2"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdTop_Click()
    Const PROC_NAME As String = "cmdTop_Click"

    On Error GoTo EH

    txtX = "WIDTH / 2"
    txtY = "0"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdTopLeft_Click()
    Const PROC_NAME As String = "cmdTopLeft_Click"

    On Error GoTo EH

    txtX = "0"
    txtY = "0"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub cmdTopRight_Click()
    Const PROC_NAME As String = "cmdTopRight_Click"

    On Error GoTo EH

    txtX = "WIDTH - 1"
    txtY = "0"

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub RefreshList()
    Dim i As Long
    Dim iSel As Integer
    
    Const PROC_NAME As String = "RefreshList"

    On Error GoTo EH

    iSel = lst.ListIndex
    
    lst.Clear
    
    If Not m_iCPCount > 0 Then Exit Sub
    
    For i = 0 To m_iCPCount - 1
        lst.AddItem Format(m_auControlPoints(i).lId, "0000") & ":" _
                & String(2, " ") _
                & Format(m_auControlPoints(i).sX, "!@@@@@@@@@@@@@@@@@@@@@@@@") _
                & String(3, " ") _
                & Format(m_auControlPoints(i).sY)
    Next
    
'    lst.ListIndex = iSel

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub Form_Load()
    Const PROC_NAME As String = "Form_Load"

    On Error GoTo EH

    txtId.Text = "0"
    RefreshList
    
    ' Internationalization
    Me.Caption = ResolveResString(RID_TITLE)
    lblTitle.Caption = ResolveResString(RID_TITLE)
    opAppend.ToolTipText = ResolveResString(RID_APPENDMODE)
    opInsertBefore.ToolTipText = ResolveResString(RID_INSERTMODE)
    opInsertAfter.ToolTipText = ResolveResString(RID_WISEMODE)
    opManual.ToolTipText = ResolveResString(RID_MANUALMODE)
    lblInsertionPoint.Caption = ResolveResString(RID_INSERTAT)
    lblConfirmation1.Caption = ResolveResString(RID_CONFIRMATION1)
    lblConfirmation2.Caption = ResolveResString(RID_CONFIRMATION2)
    chkIncrement.Caption = ResolveResString(RID_AUTOINCREMENT)
    chkReplacementConfirmation.Caption = ResolveResString(RID_ASKCONFIRMATION)
    lblMultiEditionOn.Caption = ResolveResString(RID_MULTIPLEEDITMODE)
    btOk.Caption = ResolveResString(RID_OK)
    btApply.Caption = ResolveResString(RID_APPLY)
    btCancel.Caption = ResolveResString(RID_CANCEL)
    
    Set m_Parser = New cParseLite
    ' Default values for constants, so as we know if the sintax is valid
    ' we do not use 0 because division by 0 could occurr, which is consider an
    ' error in sintax.
    m_Parser.SetConstant "WIDTH", 1
    m_Parser.SetConstant "HEIGHT", 1
'    m_Parser.SetConstant "CENTER_X", 1
'    m_Parser.SetConstant "CENTER_Y", 1

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Const PROC_NAME As String = "Form_Unload"

    On Error GoTo EH

    RaiseEvent Unload

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub lst_Click()
    Const PROC_NAME As String = "lst_Click"

    On Error GoTo EH

    UpdateInsertionID

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub opAppend_Click()
    Const PROC_NAME As String = "opAppend_Click"

    On Error GoTo EH

    txtId.Enabled = False
    UpdateInsertionID

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub opInsertAfter_Click()
    Const PROC_NAME As String = "opInsertAfter_Click"

    On Error GoTo EH

    txtId.Enabled = False
    UpdateInsertionID

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub opInsertBefore_Click()
    Const PROC_NAME As String = "opInsertBefore_Click"

    On Error GoTo EH

    txtId.Enabled = False
    UpdateInsertionID

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub opManual_Click()
    Const PROC_NAME As String = "opManual_Click"

    On Error GoTo EH

    txtId.Enabled = True
    
    SelectTextBox txtId

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub txtId_KeyPress(KeyAscii As Integer)
    Const PROC_NAME As String = "txtId_KeyPress"

    On Error GoTo EH

    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    ElseIf KeyAscii <> 8 Then
        If Not IsNumeric(Chr(KeyAscii)) Then
            Beep
            KeyAscii = 0
        End If
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub txtId_Validate(Cancel As Boolean)
    Dim l As Long
    
    Const PROC_NAME As String = "txtId_Validate"

    On Error GoTo EH

    If Not IsNumeric(txtId) Then
        Cancel = True
    Else
        l = CLng(txtId.Text)
        If l < MIN_CP_ID Or l > MAX_CP_ID Then
            Cancel = True
        End If
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub txtX_Change()
    Const PROC_NAME As String = "txtX_Change"

    On Error GoTo EH

    Set imgX.Picture = IIf(m_Parser.IsSintaxValid(txtX), _
            imgGood.Picture, imgWrong.Picture)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub txtX_GotFocus()
    Const PROC_NAME As String = "txtX_GotFocus"

    On Error GoTo EH

    SelectTextBox txtX

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub txtY_Change()
    Const PROC_NAME As String = "txtY_Change"

    On Error GoTo EH

    Set imgY.Picture = IIf(m_Parser.IsSintaxValid(txtY), _
            imgGood.Picture, imgWrong.Picture)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub txtY_GotFocus()
    Const PROC_NAME As String = "txtY_GotFocus"

    On Error GoTo EH

    SelectTextBox txtY

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Reads string resource and replace given macros with given values
' Example, given a resource number of, say, 14:
'    "Could not read '|1' in drive |2"
' The call
'    ResolveResString(14, "|1", "TXTFILE.TXT", "|2", "A:")
' would return the string
'    "Could not read 'TXTFILE.TXT' in drive A:"
Private Function ResolveResString(ByVal iResID As Integer, _
        ParamArray wReplacements() As Variant) As String
    
    Dim iMacro As Integer
    Dim sResString As String
     
    Const PROC_NAME As String = "ResolveResString"

    On Error Resume Next
    sResString = LoadResString(iResID)
    On Error GoTo 0
    
    On Error GoTo EH
     
    ' For each macro/value pair passed in ...
    For iMacro = LBound(wReplacements) To UBound(wReplacements) Step 2
         
        Dim sMacro As String
        Dim sValue As String
         
        sMacro = CStr(wReplacements(iMacro))
        sValue = vbNullString
         
        If iMacro < UBound(wReplacements) Then
            sValue = wReplacements(iMacro + 1)
        End If
         
        ' Replace all occurrences of sMacro with sValue.
        Dim nPos As Integer
        
        Do
            nPos = InStr(sResString, sMacro)
            
            If 0 <> nPos Then
                sResString = Left$(sResString, nPos - 1) & _
                             sValue & _
                             Mid$(sResString, nPos + Len(sMacro))
            End If
            
        Loop Until nPos = 0
    
    Next iMacro
     
    ResolveResString = sResString

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

