VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCpEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Copyright (C) 2009 - 2017 Dar�o Cutillas Carrillo
'
' This file is part of smartcpx.
'
' smartcpx is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' smartcpx is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with smartcpx. If not, see <http://www.gnu.org/licenses/>.

Option Explicit

'===============================================================================
' CLASS OVERVIEW
'===============================================================================
' smartcpx is an ActiveX library which provides a Control Point editor window.
' It is intentionally designed for the Smart Fpg Editor application, but has
' been built separately to allow being reused in other applications.
'
' Smart Fpg Editor can be found at http://code.google.com/p/smartfpgeditor/
'
' The class requires of another ActiveX library, parselite, which can be also
' found in the same website.
'
' This class exposes an object that the programmer can use to communicate with
' the Control Point Editor window. The editor window cannot be accesed directly
' so this class acts as a link between it and your application.
'
' To create an editor window, create an object of this class and call the Show
' method:
'       ...
'       Set myEditor = New cCpEditor
'       myEditor.Show
'       ...
'
' The user can specify several parameters, among them, the control point list
' that will be shown in the editor window. These parameters can be accessed
' prior or after calling the Show method. Refer to the documentation of each
' public method to know which parameters can be set.
'
' When in the editor window the changes are to be applied, the event Action will
' be raised. You can use this event to retrieve the new control points list.
' Pressing cancel button does not generate any event.
'===============================================================================

' Raised when the user clicks Apply or Accept. The coordinates are stored as
' strings, which may not be just interger values if the user has defined
' a parser for the editor
' @param iCount the number of control points that were on the list
' @param asX an array containing the X coordinates of the defined control points
' @param asY an array containing the Y coordinates of the defined control points
' @param alIds an array containing the IDs of the defined control points
Public Event Action(ByVal iCount As Integer, asX() As String, asY() As String, _
            alIds() As Long)
            
'===============================================================================

Private Const MODULE_NAME As String = "smartcpx.cCpEditor"

Private Declare Function SetFocus Lib "user32.dll" ( _
     ByVal hwnd As Long) As Long
Private Declare Function SetWindowLong& Lib "user32" Alias "SetWindowLongA" ( _
     ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long)
Private Declare Function GetWindowLong Lib "user32.dll" Alias "GetWindowLongA" ( _
     ByVal hwnd As Long, _
     ByVal nIndex As Long) As Long

Const GWL_HWNDPARENT = (-8)
            
' The editor form
Private WithEvents m_fEditor As frmCpEditor
Attribute m_fEditor.VB_VarHelpID = -1

' Allow establishing the control point list before or while showing the editor
Private m_bMultipleEditingMode As Boolean
Private m_alX() As Long
Private m_alY() As Long
Private m_alIds() As Long
Private m_iCount As Integer
Private m_bAutoIncrement As Boolean
Private m_bReplacementConfirmation As Boolean
Private m_hWndOldOwner

'===============================================================================

' Retrieves whether the editor is currently being shown
Public Property Get Visible()
    Const PROC_NAME As String = "Visible"

    On Error GoTo EH

    Visible = Not (m_fEditor Is Nothing)

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets if the editor form should show the "Multiple Graph Editing caution"
' message
Public Property Let MultipleEditingMode(ByVal newVal As Boolean)
    Const PROC_NAME As String = "MultipleEditingMode"

    On Error GoTo EH

    m_bMultipleEditingMode = newVal
    
    ' Update the editor
    If Not m_fEditor Is Nothing Then
        m_fEditor.MultipleEditingMode = m_bMultipleEditingMode
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves if the editor should show the "Multiple Graph Editing caution"
' message
Public Property Get MultipleEditingMode() As Boolean
    Const PROC_NAME As String = "MultipleEditingMode"

    On Error GoTo EH

    MultipleEditingMode = m_bMultipleEditingMode

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets whether the CP ID should increase in manual mode
Public Property Let AutoIncrement(ByVal newVal As Boolean)
    Const PROC_NAME As String = "AutoIncrement"

    On Error GoTo EH

    m_bAutoIncrement = True
    
    If Not m_fEditor Is Nothing Then
        m_fEditor.chkIncrement.Value = IIf(m_bAutoIncrement, 1, 0)
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves whether the CP ID should increse in manual mode
Public Property Get AutoIncrement() As Boolean
    Const PROC_NAME As String = "AutoIncrement"

    On Error GoTo EH

    If Not m_fEditor Is Nothing Then
        m_bAutoIncrement = CBool(m_fEditor.chkIncrement.Value = 1)
    End If
    
    AutoIncrement = m_bAutoIncrement

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets whether the user must confirm before replacing an existing point
Public Property Let ReplacementConfirmation(ByVal newVal As Boolean)
    Const PROC_NAME As String = "ReplacementConfirmation"

    On Error GoTo EH

    m_bReplacementConfirmation = True
    
    If Not m_fEditor Is Nothing Then
        m_fEditor.chkIncrement.Value = IIf(m_bReplacementConfirmation, 1, 0)
    End If

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Retrieves whether the user must confirm before replacing an existing point
Public Property Get ReplacementConfirmation() As Boolean
    Const PROC_NAME As String = "ReplacementConfirmation"

    On Error GoTo EH

    If Not m_fEditor Is Nothing Then
        m_bReplacementConfirmation = _
                CBool(m_fEditor.chkReplacementConfirmation.Value = 1)
    End If
    
    AutoIncrement = m_bReplacementConfirmation

    Exit Property
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Property

' Sets the control point list of the editor
' @param iCount the number of control points
' @param alX an array containing the X coordinates of the defined control
' points
' @param alY an array containing the Y coordinates of the defined control
' points
' @param alIds an array containing the IDs of the defined control points
Public Sub SetControlPoints(ByVal iCount As Integer, alX() As Long, alY() As _
    Long, alIds() As Long)
            
    Const PROC_NAME As String = "SetControlPoints"

    On Error GoTo EH

    m_alX() = alX()
    m_alY() = alY()
    m_alIds() = alIds()
    m_iCount = iCount
      
    ' TODO Error if not enough elemnts on the arrays
    If iCount > 0 Then
        Debug.Assert (UBound(alX()) = iCount - 1)
        Debug.Assert (UBound(alY()) = iCount - 1)
        Debug.Assert (UBound(alIds()) = iCount - 1)
    End If
    
    ' Update the editor
    If Not m_fEditor Is Nothing Then
        m_fEditor.SetControlPoints iCount, alX, alY, alIds
    End If

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

' Shows the editor window
' @param bModal if true, the editor will be a modal form
Public Sub Show(Optional bModal As Boolean, Optional ByVal hWndOwner As Long)
    Dim bRestorePosition As Boolean
    Dim lLeft As Long, lTop As Long
    
    Const PROC_NAME As String = "Show"

    On Error GoTo EH

    If Not m_fEditor Is Nothing Then
        lLeft = m_fEditor.Left
        lTop = m_fEditor.Top
        Unload m_fEditor
        bRestorePosition = True
    End If
    
    Set m_fEditor = New frmCpEditor
    Load m_fEditor
    If bRestorePosition = True Then m_fEditor.Move lLeft, lTop
    
    m_fEditor.MultipleEditingMode = m_bMultipleEditingMode
    If m_iCount > 0 Then
        m_fEditor.SetControlPoints m_iCount, m_alX, m_alY, m_alIds
    End If
    
    If hWndOwner <> 0 Then
        m_hWndOldOwner = SetOwner(m_fEditor.hwnd, hWndOwner)
    Else
        m_hWndOldOwner = GetOwner(m_fEditor.hwnd)
    End If
    
    m_fEditor.Show IIf(bModal, vbModal, vbModeless)

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub ResetStatus()
    Const PROC_NAME As String = "ResetStatus"

    On Error GoTo EH

    Erase m_alX()
    Erase m_alY()
    Erase m_alIds()
    m_iCount = 0

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Function SetOwner(ByVal hwnd, ByVal hWndOwner) As Long
    Const PROC_NAME As String = "SetOwner"

    On Error GoTo EH

    SetOwner = SetWindowLong(hwnd, GWL_HWNDPARENT, hWndOwner)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

Private Function GetOwner(ByVal hwnd) As Long
    Const PROC_NAME As String = "GetOwner"

    On Error GoTo EH

    GetOwner = GetWindowLong(hwnd, GWL_HWNDPARENT)

    Exit Function
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Function

'===============================================================================

Private Sub m_fEditor_Action(ByVal iCount As Integer, asX() As String, _
            asY() As String, alIds() As Long)
            
    Const PROC_NAME As String = "m_fEditor_Action"

    On Error GoTo EH

    RaiseEvent Action(iCount, asX, asY, alIds)
    ResetStatus

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub m_fEditor_Unload()
    ' Restore the parent window
    Const PROC_NAME As String = "m_fEditor_Unload"

    On Error GoTo EH

    SetOwner m_fEditor.hwnd, m_hWndOldOwner

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub

Private Sub Class_Terminate()
    Const PROC_NAME As String = "Class_Terminate"

    On Error GoTo EH

    Unload m_fEditor

    Exit Sub
EH:
    Err.Raise , MODULE_NAME, , , , PROC_NAME
End Sub
